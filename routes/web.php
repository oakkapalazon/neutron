<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\AuthController@getLogin' );
Route::get('login', 'Auth\AuthController@getLogin' );
Route::get('logout', 'Auth\AuthController@logout' );

Route::group([ 'middleware' => [ 'auth' ] ], function ()
{
  // Ajax Route
  Route::get('/get-part-type-lists', ['as' => 'get-part-type-lists', 'uses' => 'AjaxController@getPartTypeLists']);
  Route::get('/get-parameter-lists', ['as' => 'get-parameter-lists', 'uses' => 'AjaxController@getParameterLists']);
  Route::get('/get-customer-lists', ['as' => 'get-customer-lists', 'uses' => 'AjaxController@getCustomerLists']);
  Route::get('/get-good-part-details', ['as' => 'get-good-part-details', 'uses' => 'AjaxController@getGoodPartDetails']);
  Route::get('/get-good-part-row', ['as' => 'get-good-part-row', 'uses' => 'AjaxController@getGoodPartRow']);

  Route::post('/post-customer-purchase-order', ['as' => 'post-customer-purchase-order', 'uses' => 'AjaxController@postCustomerPurchaseOrder']);
  Route::post('/post-part-master-serial-no-transfer', ['as' => 'post-part-master-serial-no-transfer', 'uses' => 'AjaxController@postPartMasterSerialNoTransfer']);
  Route::post('/post-header-customers', ['as' => 'post-header-customers', 'uses' => 'CustomerController@postHeaderCustomers']);

  Route::get('/get-setid', ['as' => 'get-setid', 'uses' => 'AjaxController@getSetId']);
  Route::get('/get-serialno', ['as' => 'get-serialno', 'uses' => 'AjaxController@getSerialNo']);
  Route::get('/get-max-rf-hrs', ['as' => 'get-max-rf-hrs', 'uses' => 'AjaxController@getMaxRfHrs']);
  Route::get('/get-arf-of-serialno', ['as' => 'get-arf-of-serialno', 'uses' => 'AjaxController@getArfOfSerialNo']);

  Route::get('/get-serialno-for-kitheader', ['as' => 'get-serialno-for-kitheader', 'uses' => 'AjaxController@getSerialNoForKitHeader']);
  Route::get('/get-part-master-serial-no-exist', ['as' => 'get-part-master-serial-no-exist', 'uses' => 'AjaxController@getPartMasterSerialNoExist']);

  Route::get('/get-delete-good-kit-detail', ['as' => 'get-delete-good-kit-detail', 'uses' => 'AjaxController@getDeleteGoodKitDetailById']);
  Route::get('/get-delete-good-part-detail', ['as' => 'get-delete-good-part-detail', 'uses' => 'AjaxController@getDeleteGoodPartDetailById']);
  Route::get('/get-delete-purchase-order', ['as' => 'get-delete-purchase-order', 'uses' => 'AjaxController@getDeletePurchaseOrder']);

  // Kit master module
  Route::get('/kit-master/get-part-parameter-by-id', ['as' => 'get-part-parameter-by-id', 'uses' => 'AjaxController@getPartParameterById']);
  Route::get('/kit-master/get-delete-part-type', ['as' => 'get-delete-part-type', 'uses' => 'AjaxController@getDeletePartType']);
  Route::get('/kit-master/get-delete-part-parameter', ['as' => 'get-delete-part-parameter', 'uses' => 'AjaxController@getDeletePartParameter']);
  Route::get('/kit-master/get-kit-master-data', ['as' => 'get-kit-master-data', 'uses' => 'AjaxController@getKitMasterData']);

  Route::post('/kit-master/post-part-type-master', ['as' => 'post-part-type-master', 'uses' => 'KitMasterController@postPartTypeMaster']);
  Route::post('/kit-master/post-part-type-master-multiple', ['as' => 'post-part-type-master-multiple', 'uses' => 'KitMasterController@postPartTypeMasterMultiple']);
  Route::post('/kit-master/post-part-parameter-master', ['as' => 'post-part-parameter-master', 'uses' => 'KitMasterController@postPartParameter']);
  Route::post('/kit-master/post-header-kit-master', ['as' => 'post-header-kit-master', 'uses' => 'KitMasterController@postHeaderKitMaster']);

  // Post cleaning module
  Route::get('/post-cleaning/get-grn-detail-by-part-type', ['as' => 'get-grn-detail-by-part-type', 'uses' => 'AjaxController@getGRNDetailByPartType']);

  Route::get('/post-cleaning-report/get-grn-details', ['as' => 'get-grn-details', 'uses' => 'AjaxController@getGRNDetail']);
  Route::get('/post-cleaning-report/get-kit-details', ['as' => 'get-kit-details', 'uses' => 'AjaxController@getKitDetail']);
  Route::get('/post-cleaning-report/get-kit-type-list', ['as' => 'get-kit-type-list', 'uses' => 'AjaxController@getKitTypeList']);
  Route::get('/post-cleaning-report/get-set-id-list', ['as' => 'get-set-id-list', 'uses' => 'AjaxController@getSetIDList']);

  //Pre cleaning module
  Route::get('/pre-cleaning/get-part-master-all-serial-no', ['as' => 'get-part-master-all-serial-no', 'uses' => 'AjaxController@getPartMasterAllSerialNo']);
  Route::get('/pre-cleaning/get-part-master-all-set-id', ['as' => 'get-part-master-all-set-id', 'uses' => 'AjaxController@getPartMasterAllSetId']);

  Route::get('/pre-cleaning/get-part-master-set-id', ['as' => 'get-part-master-set-id', 'uses' => 'AjaxController@getPartMasterSetId']);
  Route::get('/pre-cleaning/get-part-master-serial-no', ['as' => 'get-part-master-serial-no', 'uses' => 'AjaxController@getPartMasterSerialNo']);

  Route::get('/pre-cleaning/get-part-master-kit-identifier', ['as' => 'get-part-master-kit-identifier', 'uses' => 'AjaxController@getPartMasterKitIdentifier']);
  Route::get('/pre-cleaning/get-kit-master-remarks', ['as' => 'get-kit-master-remarks', 'uses' => 'AjaxController@getKitMasterRemarks']);

  Route::post('/pre-cleaning/post-action-part-master', ['as' => 'post-action-part-master', 'uses' => 'AjaxController@postActionPartMaster']);
  Route::post('/pre-cleaning/post-header-good-receipt-header', ['as' => 'post-header-good-receipt-header', 'uses' => 'PreCleaningController@postHeaderGoodReceiptHeader']);
  Route::post('/pre-cleaning/post-good-kit-detail-multiple', ['as' => 'post-good-kit-detail-multiple', 'uses' => 'PreCleaningController@postGoodKitDetailMultiple']);

  // Delivery module
  Route::any('/delivery_order/reopen-do', ['as' => 'reopen-do', 'uses' => 'DeliveryOrderController@reopenDo']);

  Route::get('/delivery-order/get-good-kit-detail', ['as' => 'get-good-kit-detail', 'uses' => 'AjaxController@getGoodKitDetail']);
  Route::get('/delivery-order/get-customer-info', ['as' => 'get-customer-info', 'uses' => 'AjaxController@getCustomerInfo']);
  Route::get('/delivery-order/get-purchase-order', ['as' => 'get-purchase-order', 'uses' => 'AjaxController@getPurchaseOrder']);
  Route::get('/delivery-order/get-grn-details-without-closed', ['as' => 'get-grn-details-without-closed', 'uses' => 'AjaxController@getGRNDetailsWithoutClosed']);
  Route::post('/delivery-order/post-delivery-order-child', ['as' => 'post-delivery-order-child', 'uses' => 'AjaxController@postDeliveryOrderChild']);

  Route::get('/delivery-order-report/get-do-details', ['as' => 'get-do-details', 'uses' => 'AjaxController@getDoDetails']);

  // Process
  Route::group(['prefix' => 'process'], function()
  {
    Route::post('/pre-cleaning/post-kit-type-precleaning', ['as' => 'post-kit-type-precleaning', 'uses' => 'PreCleaningController@postKitTypePreCleaning']);
    Route::post('/pre-cleaning/post-part-type-precleaning', ['as' => 'post-part-type-precleaning', 'uses' => 'PreCleaningController@postPartTypePreCleaning']);

    Route::post('/post-cleaning/post-grn-detail', ['as' => 'post-grn-detail', 'uses' => 'PostCleaningController@postGRNDetail']);

    Route::resource( 'pre_cleaning', 'PreCleaningController' );
    Route::resource( 'post_cleaning', 'PostCleaningController' );
  });

  // Reports
  Route::group(['prefix' => 'report'], function()
  {
    Route::get('/grn_query/get-grn-query-detail', ['as' => 'get-grn-query-detail', 'uses' => 'AjaxController@getGRNQueryDetail']);
    Route::get('/grn_query/get-good-part-detail-part-type', ['as' => 'get-good-part-detail-part-type', 'uses' => 'AjaxController@getGoodPartDetailPartType']);
    Route::get('/grn_query/get-good-part-detail-serial', ['as' => 'get-good-part-detail-serial', 'uses' => 'AjaxController@getGoodPartDetailSerial']);
    Route::get('/tracking_query/get-tracking-query-detail', ['as' => 'get-tracking-query-detail', 'uses' => 'AjaxController@getTrackingQueryDetail']);
    Route::get('/part_master_report/get-part-master-detail', ['as' => 'get-part-master-detail', 'uses' => 'AjaxController@getPartMasterDetail']);

    Route::get('/grn_query/export-grn-query-detail', ['as' => 'export-grn-query-detail', 'uses' => 'GrnReportController@exportGRNQueryDetail']);
    Route::get('/tracking_query/export-tracking-query-detail', ['as' => 'export-tracking-query-detail', 'uses' => 'TrackingReportController@exportTrackingQueryDetail']);
    Route::get('/part_master_report/export-past-master-detail', ['as' => 'export-past-master-detail', 'uses' => 'PartMasterReportController@exportPartMasterDetail']);

    Route::post('/post_cleaning_report/print_post_cleaning_report', ['as' => 'print_post_cleaning_report', 'uses' => 'PostCleaningReportController@printPostCleaningReport']);
      Route::post('/post_cleaning_report/export_post_cleaning_report', ['as' => 'export_post_cleaning_report', 'uses' => 'PostCleaningReportController@exportPostCleaningReport']);

    Route::any('/delivery_order_report/print_delivery_order_report', ['as' => 'print_delivery_order_report', 'uses' => 'DeliveryOrderReportController@printDeliveryOrderReport']);

    Route::resource( 'grn_query', 'GrnReportController' );
    Route::resource( 'tracking_query', 'TrackingReportController' );
    Route::resource( 'post_cleaning_report', 'PostCleaningReportController' );
    Route::resource( 'part_master_report', 'PartMasterReportController' );
    Route::resource( 'delivery_order_report', 'DeliveryOrderReportController' );
  });

  // Database Settings
  Route::resource( 'users', 'UserController' );
  Route::resource( 'companies', 'CompanyController' );
  Route::resource( 'customers', 'CustomerController' );
  Route::resource( 'kit_master', 'KitMasterController' );
  Route::resource( 'parameter_master', 'ParameterMasterController' );
  Route::resource( 'param_datapoint_master', 'ParamDatapointMasterController' );
  Route::resource( 'reason_master', 'ReasonMasterController' );
  Route::resource( 'uom_master', 'UomMasterController' );
  Route::resource( 'delivery_order', 'DeliveryOrderController' );

  // Database Settings > Parts
  Route::group(['prefix' => 'parts'], function()
  {
    Route::resource( 'part_master', 'PartMasterController' );
    Route::resource( 'part_type_master', 'PartTypeMasterController' );
    Route::resource( 'part_param_master', 'PartParamMasterController' );
    Route::resource( 'part_status_master', 'PartStatusMasterController' );
    Route::resource( 'visual_condition_master', 'VisualConditionMasterController' );
  });
});

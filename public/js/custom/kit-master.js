
$(function() {

  var kit_row = '';

  $('form#part-parameter-form').change(function() {

      $( this ).attr( 'formmodified', 1 );
  });

  $('form#form_header').change(function() {

      $( '.btn-cancel' ).attr( 'formheadermodified', 1 );
  });

  // append row in kit table
  $( '.add-btn' ).click(function() {

    kit_row = $( '#part-type-table tbody tr' ).length;

    var chk_length = $( "#part-type-table" ).find( "[name='kit_identifier']:checked" ).length;

    $( '#append-row > .sr_no' ).text( kit_row + 1 );

    $( '#append-row' ).clone().appendTo( '#part-type-table' );
    $( '#part-type-table tr:last' ).removeAttr( 'id' );
    $( '#part-type-table tr:last td:nth-child(2)' ).removeClass( 'sr_no' );

    if(chk_length == 1)
    {
      $( '#part-type-table tr:last' ).find( ':checkbox[name^=kit_identifier]' ).attr( "disabled" , true );
    }

    triCheckBox();
  });

  // remove row in kit table
  $( '#part-type-table' ).on('click', '.remove-row', function() {

    if (!confirm( 'Do you confirm you want to delete this record?' ))
    {
      return false;
    }

    else
    {
      if($(this).attr('data-id') != '')
      {
        var formData = {
          _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
          id: $( this ).attr('data-id')
        };

        $.ajax({
          type: 'GET',
          url: '/kit-master/get-delete-part-type',
          data: formData,
          dataType: 'json',
          success: function(response)
          {
            alert('Part type deleted successfully!');
          },

          error: function (response)
          {
            console.log(response);
          }
        });
      }

      $( this ).parent().parent().remove();
    }
  });

  // remove row in modal dialog box part parameter table
  $( '#part-parameter-table' ).on('click', '.remove-row', function() {

    if (!confirm( 'Do you confirm you want to delete this record?' ))
    {
      return false;
    }

    else
    {
      if($(this).attr('data-id') != '')
      {
        var formData = {
          _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
          id: $( this ).attr('data-id')
        };

        $.ajax({
          type: 'GET',
          url: '/kit-master/get-delete-part-parameter',
          data: formData,
          dataType: 'json',
          success: function(response)
          {
            // success alert
            if(response.data == 'success')
            {
              alert('Selected Part Type is successfully deleted.');
            }
          },

          error: function (response)
          {
            console.log(response);
          }
        });
      }

      $( this ).parent().parent().remove();
    }
  });

  $( '#part-type-table' ).on('click', '.kit-identifier', function(e) {

    if($(this).is( ':checked' ))
    {
      $( '#part-type-table' ).find( ':checkbox[name^=kit_identifier]' ).not(this).attr('disabled', true);
      $( '#part-type-table' ).find( '.hidden_kit_identifier' ).not(this).val(0);
    }

    else
    {
      $( "#part-type-table" ).find( ':checkbox[name^=kit_identifier]' ).removeAttr( 'disabled' );
    }
  });

  // populate dialog box belong with kit type
  $( '#part-type-table' ).on('click', '.dialog-arrow', function(e) {

    e.preventDefault();

    $( '.right-arrow' ).find( 'a' ).removeClass( 'active' );
    $( this ).parent().addClass( 'active' );

    var part_type = $( this ).closest( 'tr' ).find( '.part-type' ).val();
    var kit_master_id = $( '#kit-master-id' ).val();
    var max_rf_hrs = $( this ).closest( 'tr' ).find( '.max-rf-hrs' ).val();
    var max_rf_print = $( this ).closest( 'tr' ).find( '.max-rf-print' ).val();
    var part_sequence = $( this ).closest( 'tr' ).find( '.part-sequence' ).val();
    var kit_identifier = $( this ).closest( 'tr' ).find( '.kit-identifier' ).is( ':checked' );
    var grade_required = $( this ).closest( 'tr' ).find( '.grade-required' ).is( ':checked' );
    var initial_required = $( this ).closest( 'tr' ).find( '.initial-required' ).is( ':checked' );

    if( part_type == '' )
    {
      $( this ).closest( 'tr' ).find( '.part-type' ).parent().addClass( 'has-error' );
    }

    else
    {
      $( this ).closest( 'tr' ).find( '.part-type' ).parent().removeClass( 'has-error' );
    }

    // if( max_rf_hrs == '' )
    // {
    //   $( this ).closest( 'tr' ).find( '.max-rf-hrs' ).parent().addClass( 'has-error' );
    // }
    //
    // else
    // {
    //   $( this ).closest( 'tr' ).find( '.max-rf-hrs' ).parent().removeClass( 'has-error' );
    // }

    // if( max_rf_print == '' )
    // {
    //   $( this ).closest( 'tr' ).find( '.max-rf-print' ).parent().addClass( 'has-error' );
    // }
    //
    // else
    // {
    //   $( this ).closest( 'tr' ).find( '.max-rf-print' ).parent().removeClass( 'has-error' );
    // }

    if( part_sequence == '' )
    {
      $( this ).closest( 'tr' ).find( '.part-sequence' ).parent().addClass( 'has-error' );
    }

    else
    {
      $( this ).closest( 'tr' ).find( '.part-sequence' ).parent().removeClass( 'has-error' );
    }

    var kit_identifier_val = ( kit_identifier ) ? 1 : 0;
    var grade_required_val = ( grade_required ) ? 1 : 0;
    var initial_required_val = ( initial_required ) ? 1 : 0;

    // check selected row has error or not
    if( $( this ).closest( 'tr' ).find( '.has-error' ).length == 0 )
    {
      var part_type_id = $( this ).closest( 'td' ).find( '.active' ).attr( 'data-part-type-id' );

      $( '.modal-part-type-id' ).val( part_type_id );

      // check selected row is already exist in table or not
      var dataForm = {
        _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
        part_type: part_type,
        part_type_id: part_type_id,
        kit_master_id: kit_master_id,
        max_rf_hrs: max_rf_hrs,
        max_rf_print: max_rf_print,
        part_sequence: part_sequence,
        kit_identifier: kit_identifier_val,
        grade_required: grade_required_val,
        initial_required: initial_required_val
      };

      $.ajax({
        type: 'POST',
        url: '/kit-master/post-part-type-master',
        data: dataForm,
        dataType: 'json',
        context: this,
        success: function( response )
        {
          $( '.modal-part-type-id' ).val( response['part_type_id'] );
          $( this ).closest( 'td' ).find( '.active' ).attr( 'data-part-type-id', response['part_type_id'] );
          $( this ).closest( 'tr' ).find( '.remove-row' ).attr( 'data-id', response['part_type_id'] );
          $( this ).closest( 'tr' ).find( '.hidden_dialog-arrow' ).val( response['part_type_id'] );

          // if( response.no_of_goodpartdetail > 0 )
          // {
          //   $( ".modal-btn-save" ).attr("disabled", "disabled");
          // }
          //
          // else
          // {
          //   $( ".modal-btn-save" ).removeAttr("disabled");
          // }
        },

        error: function ( response )
        {
          console.log( response );
        }
      });

      var dataForm = {
        _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
        part_type_id: $( '.modal-part-type-id' ).val(),
      };

      $.ajax({
        type: 'GET',
        url: '/kit-master/get-part-parameter-by-id',
        data: dataForm,
        dataType: 'json',
        context: this,
        success: function( response )
        {
          if( response.data.length > 0 )
          {
            $.each(response.data, function( index, data ) {

              $( '#modal-append-row' ).clone().appendTo( '#part-parameter-table' );

              $( '#modal-append-row' ).find( '.remove-row' ).attr( 'data-id', data.id );
              $( '#modal-append-row' ).find( '.parameter-id' ).val( data.parameter_master_id );
              $( '#modal-append-row' ).find( '.point-msr' ).val( data.point_msr );
              $( '#modal-append-row' ).find( '.min-limit' ).val( data.min_limit );
              $( '#modal-append-row' ).find( '.max-limit' ).val( data.max_limit );
              $( '#modal-append-row' ).find( '.specs' ).val( data.specs );

              if( data.exclude_post_cleaning == 1 )
              {
                $( '#modal-append-row' ).find( '.exclude-post-cleaning' ).attr("checked", "checked");
                $( '#modal-append-row' ).find( '.point-pom-value' ).attr("disabled", "disabled");
                $( '#modal-append-row' ).find( '.print-average-value' ).attr("disabled", "disabled");
                $( '#modal-append-row' ).find( '.point-msr' ).attr("disabled", "disabled");
              }

              if( data.print_pom_value == 1 )
              {
                $( '#modal-append-row' ).find( '.point-pom-value' ).attr("checked", "checked");
              }

              if( data.print_average_value == 1 )
              {
                $( '#modal-append-row' ).find( '.print-average-value' ).attr("checked", "checked");
              }

              // remove id from table row
              $( '#part-parameter-table tr:last' ).removeAttr( 'id' );
            });

            if( response.no_of_goodpartdetail > 0 )
            {
              $( '#part-parameter-table' ).find( '.remove-row' ).hide();
            }
          }

          else
          {
            $( '#modal-append-row' ).clone().appendTo( '#part-parameter-table' );
          }
        },

        error: function ( response )
        {
          console.log( response );
        }
      });

      $( '.modal-part-type' ).val( part_type );
      $( '.modal-max-rf-hrs' ).val( max_rf_hrs );
      $( '.modal-max-rf-print' ).val( max_rf_print );
      $( '.modal-part-sequence' ).val( part_sequence );

      if( $( this ).closest( 'tr' ).find( '.kit-identifier' ).is(":checked") == true )
      {
        $( '.modal-kit-identifier' ).attr( 'checked', true );
      }

      else
      {
        $( '.modal-kit-identifier' ).attr( 'checked', false );
      }

      if( $( this ).closest( 'tr' ).find( '.grade-required' ).is(":checked")  == true )
      {
        $( '.modal-grade-required' ).attr( 'checked', true );
      }

      else
      {
        $( '.modal-grade-required' ).attr( 'checked', false );
      }

      $( '#modal-dialog-part-parameter' ).modal();
    }
  });

  $( '.modal-btn-save' ).click(function( e ) {

    e.preventDefault();
    post_part_param_master();

  });

  function post_part_param_master() {

    var count = 0;
    var errors = new Array();
    var validationFailed = false;

    var parameter_id = [];
    var point_msr = [];
    var min_limit = [];
    var max_limit = [];
    var specs = [];
    var exclude_post_cleaning = [];
    var print_pom_value = [];
    var print_average_value = [];
    var part_param_master_id = [];

    $( '#part-parameter-table .parameter-id' ).each(function() {

      var $optText = $(this).find( 'option:selected' );

      if ( $optText.val() == '' )
      {
        validationFailed = true;
        errors[count++] = "Part Parameters are empty.";
        return false;
      }

      else
      {
        parameter_id.push( $optText.val() );
      }
    });

    $( "#part-parameter-table .point-msr" ).each(function() {

      if (!$.trim($(this).val()).length)
      {
        validationFailed = true;
        errors[count++] = "Pint Msr are empty.";
        return false;
      }

      else
      {
        point_msr.push( $(this).val() );
      }
    });

    $( "#part-parameter-table .min-limit" ).each(function() {

      // if ( !$.trim($(this).val()).length )
      // {
      //   validationFailed = true;
      //   errors[count++] = "Min limit are empty.";
      //   return false;
      // }
      //
      // else
      // {
        min_limit.push( $(this).val() );
      // }
    });

    $( "#part-parameter-table .max-limit" ).each(function() {

      // if ( !$.trim($(this).val()).length )
      // {
      //   validationFailed = true;
      //   errors[count++] = "Max limit are empty.";
      //   return false;
      // }
      //
      // else
      // {
        max_limit.push( $(this).val() );
      // }
    });

    $( "#part-parameter-table .specs" ).each(function() {

      // if ( !$.trim($(this).val()).length )
      // {
      //   validationFailed = true;
      //   errors[count++] = "Specs are empty.";
      //   return false;
      // }
      //
      // else
      // {
        specs.push( $(this).val() );
      // }
    });

    $( "#part-parameter-table .exclude-post-cleaning" ).each(function() {

      if ( !$.trim($(this).val()).length )
      {
        validationFailed = true;
        errors[count++] = "Exclude of Post Cleaning are empty.";
        return false;
      }

      else
      {
        var value = ( $( this ).is( ':checked' ) ) ? 1 : 0;
        exclude_post_cleaning.push( value );
      }
    });

    $( "#part-parameter-table .point-pom-value" ).each(function() {

      if ( !$.trim($(this).val()).length )
      {
        validationFailed = true;
        errors[count++] = "Print POM Value are empty.";
        return false;
      }

      else
      {
        var value = ( $( this ).is( ':checked' ) ) ? 1 : 0;
        print_pom_value.push( value );
      }
    });

    $( "#part-parameter-table .print-average-value" ).each(function() {

      if ( !$.trim($(this).val()).length )
      {
        validationFailed = true;
        errors[count++] = "Print Average Value are empty.";
        return false;
      }

      else
      {
        var value = ( $( this ).is( ':checked' ) ) ? 1 : 0;
        print_average_value.push( value );
      }
    });

    $( '#part-parameter-table .remove-row' ).each(function() {
        part_param_master_id.push( $(this).attr( 'data-id' ) );
    });

    if ( validationFailed )
    {
      var errorMsgs = '';

      for(var i = 0; i < count; i++)
      {
        errorMsgs = errorMsgs + errors[i] + "<br/>";
      }

      $('html,body').animate({ scrollTop: 0 }, 'slow');

      $( '.check-part-parameter-fields' ).show();
      $( '.check-part-parameter-fields' ).html( errorMsgs );

      return false;
    }

    else
    {
      var formData = {
        _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
        kit_master_id: $( '#kit-master-id' ).val(),
        part_type_id:	$( '.modal-part-type-id' ).val(),
        parameter_id: parameter_id,
        point_msr: point_msr,
        min_limit: min_limit,
        max_limit: max_limit,
        specs: specs,
        exclude_post_cleaning: exclude_post_cleaning,
        print_pom_value: print_pom_value,
        print_average_value: print_average_value,
        part_param_master_id: part_param_master_id
      };

      $.ajax({
        type: 'POST',
        url: '/kit-master/post-part-parameter-master',
        data: formData,
        dataType: 'json',
        success: function( response )
        {
          alert( 'Part Parameters are successfully created.' );
          $( 'form#part-parameter-form' ).removeAttr( 'formmodified' );
          $('#modal-dialog-part-parameter').modal('hide');
        },

        error: function ( response ) {
          console.log( response );
        }
      });

      $( '.check-part-parameter-fields' ).hide();
      $( '.check-part-parameter-fields' ).empty();
    }
  };

  // append row in part type table
  $( '.add-part-parameter' ).click(function() {
    $( '#modal-append-row' ).clone().appendTo( '#part-parameter-table' );
    $( '#part-parameter-table tr:last' ).removeAttr( 'id' );
    $( '#part-parameter-table tr:last' ).find( '.point-msr' ).val( '0' ).removeAttr( 'disabled' );
    $( '#part-parameter-table tr:last' ).find( '.min-limit' ).val( '' );
    $( '#part-parameter-table tr:last' ).find( '.max-limit' ).val( '' );
    $( '#part-parameter-table tr:last' ).find( '.specs' ).val( '' );
    $( '#part-parameter-table tr:last' ).find( '.exclude-post-cleaning' ).prop('checked', false);
    $( '#part-parameter-table tr:last' ).find( '.point-pom-value' ).prop('checked', false).removeAttr( 'disabled' );
    $( '#part-parameter-table tr:last' ).find( '.print-average-value' ).prop('checked', false).removeAttr( 'disabled' );
  });

  // clear rows when part parameter modal dialog is closed
  $( '.modal-btn-close' ).click(function() {

    var formmodified = $( this ).closest( 'div' ).parent().find( 'form' ).attr( 'formmodified' );

    if(formmodified == 1)
    {
      if( confirm("Data will not be saved, do you still want to proceed?\nClick OK to proceed!") )
      {
        // post_part_param_master();
        $( this ).closest( 'div' ).parent().find( 'form' ).removeAttr( 'formmodified' );
      }

      else
      {
        return false;
      }
    }

    $( '#part-parameter-table' ).find( 'tr:gt(0)' ).remove();
    $( '.check-part-parameter-fields' ).hide();

  });

  // part parameter modal dialog close event
  $( '#modal-dialog-part-parameter' ).on('hide.bs.modal', function () {
    $( '#part-parameter-table' ).find( 'tr:gt(0)' ).remove();
  });

  // disable checkbox print pom and avg value when tick exclude post cleaning checkbox
  $( "body" ).delegate( ".exclude-post-cleaning", "change", function() {

    if( $(this).is(':checked') )
    {
      $( this ).closest( 'td' ).parent().find( '.point-pom-value' ).attr("disabled", "disabled").prop('checked', false);
      $( this ).closest( 'td' ).parent().find( '.print-average-value' ).attr("disabled", "disabled").prop('checked', false);
      $( this ).closest( 'td' ).parent().find( '.point-msr' ).val('0').attr("disabled", "disabled");
    }

    else
    {
      $( this ).closest( 'td' ).parent().find( '.point-pom-value' ).removeAttr("disabled");
      $( this ).closest( 'td' ).parent().find( '.print-average-value' ).removeAttr("disabled");
      $( this ).closest( 'td' ).parent().find( '.point-msr' ).removeAttr("disabled");
    }
  });

  // hide part type information when there is no header
  if( $( '#kit_master_id' ).val() != '' )
  {
    var kit_master_id_onload;

    var dataForms = {
      _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
      kit_master_id: $( '#kit-master-id' ).val()
    };

    $.ajax({
      type: 'GET',
      url: '/kit-master/get-kit-master-data',
      data: dataForms,
      dataType: 'json',
      context: this,
      success: function( response )
      {
        kit_master_id_onload = response.data;

        if( ( kit_master_id_onload !== null ) )
        {
          $( ".subSection" ).show();
        }

        else
        {
          $( ".subSection" ).hide();
        }
      },

      error: function ( response )
      {
        console.log( response );
      }
    });
  }

  else
  {
    $( ".subSection" ).hide();
  }

  // trigger it when add new row of part type
  function triCheckBox()
  {
    $( '#part-type-table' ).on('change', '.kit-identifier', function() {

      if( $( this ).is(':checked') )
      {
        $( this ).closest( 'td' ).parent().find( '.hidden_kit_identifier' ).val(1);
      }

      else
      {
        $( this ).closest( 'td' ).parent().find( '.hidden_kit_identifier' ).val(0);
      }
    });

    $( '#part-type-table' ).on('change', '.grade-required', function() {

      if( $( this ).is(':checked') )
      {
        $( this ).closest( 'td' ).parent().find( '.hidden_grade_required' ).val(1);
      }

      else
      {
        $( this ).closest( 'td' ).parent().find( '.hidden_grade_required' ).val(0);
      }
    });
  }

  $( '#part-type-table' ).on('change', '.kit-identifier', function() {

    if( $( this ).is(':checked') )
    {
      $( this ).closest( 'td' ).parent().find( '.hidden_kit_identifier' ).val(1);
    }

    else
    {
      $( this ).closest( 'td' ).parent().find( '.hidden_kit_identifier' ).val(0);
    }
  });

  $( '#part-type-table' ).on('change', '.initial-required', function() {

    if( $( this ).is(':checked') )
    {
      $( this ).closest( 'td' ).parent().find( '.hidden_initial_required' ).val(1);
    }

    else
    {
      $( this ).closest( 'td' ).parent().find( '.hidden_initial_required' ).val(0);
    }
  });

  $( '#part-type-table' ).on('change', '.grade-required', function() {

    if( $( this ).is(':checked') )
    {
      $( this ).closest( 'td' ).parent().find( '.hidden_grade_required' ).val(1);
    }

    else
    {
      $( this ).closest( 'td' ).parent().find( '.hidden_grade_required' ).val(0);
    }
  });

  $( '.btn-save' ).click( function(e) {

    e.preventDefault();

    // validate header
    if( $( '#customer_id' ).val() == '' )
    {
      alert( "Customer's field is required" );
      return false;
    }

    if( $( '#descn' ).val() == '' )
    {
      alert( "Description's field is required" );
      return false;
    }

    if( $( '#uom_id' ).val() == '' )
    {
      alert( "UOM Code's field is required" );
      return false;
    }

    if( $( '#uom_id' ).val() == '' )
    {
      alert( "UOM Code's field is required" );
      return false;
    }

    if( $( '#kit_type' ).val() == '' )
    {
      alert( "Kit Type's field is required" );
      return false;
    }

    if( $( '#folder_name' ).val() == '' )
    {
      alert( "Folder Name's field is required" );
      return false;
    }

    //validate footer
    var validationFailed = false;

    var part_type = [];
    var max_rf_hrs = [];
    var max_rf_print = [];
    var part_sequence = [];
    var hidden_kit_identifier = [];
    var hidden_grade_required = [];
    var hidden_initial_required = [];
    var hidden_dialog_arrow = [];

    $( "#part-type-table .part-type" ).each(function() {

      if( !$.trim($(this).val()).length )
      {
        validationFailed = true;
        $( this ).closest( 'tr' ).find( '.part-type' ).parent().addClass( 'has-error' );
      }

      else
      {
        $( this ).closest( 'tr' ).find( '.part-type' ).parent().removeClass( 'has-error' );
        part_type.push( $(this).val() );
      }

    });

    $( "#part-type-table .max-rf-hrs" ).each(function() {

      // if( !$.trim($(this).val()).length )
      // {
      //   validationFailed = true;
      //   $( this ).closest( 'tr' ).find( '.max-rf-hrs' ).parent().addClass( 'has-error' );
      // }
      //
      // else
      // {
      //   $( this ).closest( 'tr' ).find( '.max-rf-hrs' ).parent().removeClass( 'has-error' );
        max_rf_hrs.push( $(this).val() );
      // }

    });

    $( "#part-type-table .max-rf-print" ).each(function() {

      // if( !$.trim($(this).val()).length )
      // {
      //   validationFailed = true;
      //   $( this ).closest( 'tr' ).find( '.max-rf-print' ).parent().addClass( 'has-error' );
      // }
      //
      // else
      // {
      //   $( this ).closest( 'tr' ).find( '.max-rf-print' ).parent().removeClass( 'has-error' );
        max_rf_print.push( $(this).val() );
      // }

    });

    $( "#part-type-table .part-sequence" ).each(function() {

      if( !$.trim($(this).val()).length )
      {
        validationFailed = true;
        $( this ).closest( 'tr' ).find( '.part-sequence' ).parent().addClass( 'has-error' );
      }

      else
      {
        $( this ).closest( 'tr' ).find( '.part-sequence' ).parent().removeClass( 'has-error' );
        part_sequence.push( $(this).val() );
      }

    });

    $( "#part-type-table .hidden_kit_identifier" ).each(function() {
        hidden_kit_identifier.push( $(this).val() );
    });

    $( "#part-type-table .hidden_grade_required" ).each(function() {
        hidden_grade_required.push( $(this).val() );
    });

    $( "#part-type-table .hidden_initial_required" ).each(function() {
      hidden_initial_required.push( $(this).val() );
    });

    $( "#part-type-table .hidden_dialog-arrow" ).each(function() {
        hidden_dialog_arrow.push( $(this).val() );
    });

    if ( validationFailed == false )
    {
      // save header
      var dataForm = {
        _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
        kit_master_id: $( '#kit-master-id' ).val(),
        customer_id: $( '#customer_id' ).val(),
        descn: $( '#descn' ).val(),
        uom_id: $( '#uom_id' ).val(),
        kit_type: $( '#kit_type' ).val(),
        process: $( '#process' ).val(),
        remarks: $( '#remarks' ).val(),
      };

      console.log(dataForm);
      $.ajax({
        type: 'POST',
        url: '/kit-master/post-header-kit-master',
        data: dataForm,
        dataType: 'json',
        context: this,
        success: function( response )
        {
          // if( response.data == 'success' && hidden_dialog_arrow != '' )
          if( response.data == 'success' )
          {
            var kit_master_id = $( '#kit-master-id' ).val();
            var data_id = [];

            // save footer
            var dataForm = {
              _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
              part_type_id: hidden_dialog_arrow,
              kit_master_id: kit_master_id,
              part_type: part_type,
              max_rf_hrs: max_rf_hrs,
              max_rf_print: max_rf_print,
              part_sequence: part_sequence,
              kit_identifier: hidden_kit_identifier,
              grade_required: hidden_grade_required,
              initial_required: hidden_initial_required
            };

            $.ajax({
              type: 'POST',
              url: '/kit-master/post-part-type-master-multiple',
              data: dataForm,
              dataType: 'json',
              context: this,
              success: function( response )
              {
                console.log(response.part_type_id);
                if( response.part_type_id != '' )
                {
                  $.each(response.part_type_id, function (index, data) {

                    $('#part-type-table').find('.remove-row:eq(' + index + ')').attr('data-id', data);
                    $('#part-type-table').find('.right-arrow:eq(' + index + ')').children("a").attr('data-part-type-id', data);
                    $('#part-type-table').find('.hidden_dialog-arrow:eq(' + index + ')').val(data);

                  });

                  $('.btn-cancel').removeAttr('formheadermodified');

                  alert('Save information successfully.');
                }

                else
                {
                  $( '.btn-cancel' ).removeAttr( 'formheadermodified' );

                  alert('Save Kit Type successfully.');
                }
              },

              error: function ( response )
              {
                console.log( response );
              }
            });
          }
        },

        error: function ( response )
        {
          console.log( response );
        }
      });
    }

  });

  $( '.btn-cancel' ).click( function(e) {

    if( $( this ).attr( 'formheadermodified' ) == 1 )
    {
      if( confirm("Data will not be saved, do you still want to proceed?\nClick OK to proceed!") )
      {

      }

      else
      {
        return false;
      }
    }
  });

});

$(function() {

    $( ".export-btn" ).hide();
    $(".filter-part-master-box").on('click', '.search-btn', function(e) {
  
      event.preventDefault();
  
      var kit_type = $("#kit_type").val();
      var part_type = $("#part_type").val();
      var serial_no = $("#serial_no").val();
      var set_id = $("#set_id").val();
      var created_at = $("#created_at").val();
      var row = '';
      var no = '1';
  
      var dataForm = {
        _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
        kit_type: kit_type,
        part_type: part_type,
        serial_no: serial_no,
        set_id: set_id,
        created_at: created_at,
      };
  
      $.ajax({
        type: 'GET',
        url: '/report/part_master_report/get-part-master-detail',
        data: dataForm,
        dataType: 'json',
        context: this,
        success: function( response )
        {console.log(response);
  
          $( "#grn-report-table tbody" ).empty();
  
          if(response.data.length > 0)
          {
            $("input[type=hidden][name=hidden_kit_type]").val( kit_type );
            $("input[type=hidden][name=hidden_part_type]").val( part_type );
            $("input[type=hidden][name=hidden_serial_no]").val( serial_no );
            $("input[type=hidden][name=hidden_set_id]").val( set_id );
            $("input[type=hidden][name=hidden_created_at]").val( created_at );
  
            $.each(response.data, function( index, data ) {
  
              row += '<tr>';
              row += '<td>' + no + '</td>';
              row += '<td>' + data.kit_type + '</td>';
              row += '<td>' + data.part_type + '</td>';
              row += '<td>' + data.serial_no + '</td>';
              row += '<td>' + data.set_id + '</td>';
              row += '<td>' + data.specs + '</td>';
              row += '<td>' + data.status + '</td>';
              row += '<td>' + data.additional_info + '</td>';
              row += '<td>' + data.grade + '</td>';
              row += '<td>' + data.remarks + '</td>';
              row += '<td>' + data.created_at + '</td>';
              row += "</tr>";
              no++;
            });
  
            $( ".export-btn" ).show();
          }
          else
          {
            $( ".export-btn" ).hide();
  
            row += '<tr>';
            row += '<td>No data found</td>';
            row += "</tr>";
          }
  
          $( '#grn-report-table tbody' ).append( row );
        },
  
        error: function ( response )
        {
          console.log( response );
  
          $( "#grn-report-table tbody" ).empty();
          $( ".export-btn" ).hide();
  
          row += '<tr>';
          row += '<td>No data found</td>';
          row += "</tr>";
  
          $( '#grn-report-table tbody' ).append( row );
        }
      });
  
    });

  // get part type Lists
  $( '#kit_type' ).change(function() {

    var kit_master_id = $( '#kit_type' ).val();

    if(kit_master_id != '')
    {
      var formData = {
        _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
        kit_master_id: kit_master_id
      };

      $.ajax({
        type: 'GET',
        url: '/get-part-type-lists',
        data: formData,
        dataType: 'json',
        success: function(response)
        {
          $( '#part_type' ).find( 'option' ).not( ':first' ).remove();

          $.each(response.data, function(index, data) {
            $( '#part_type' ).append( '<option value="' + data.id + '">' + data.part_type + '</option>' );
          });
        },

        error: function (response) {
          console.log(response);
        }
      });
    }

    else
    {
      $( '#part_type_id' ).find( 'option' ).not( ':first' ).remove();
      // $('#part_type_id').trigger('change');
    }
  });

});
  
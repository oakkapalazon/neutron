$(function() {

  $( ".export-btn" ).hide();
  $( ".divHide" ).hide();

  $(".filter-tracking-query-box").on('click', '.search-btn', function(e) {

    e.preventDefault();

    // var grn_no = $("#grn_no").val();
    var set_id = $("#set_id").val();
    var customer_code = $("#customer_code").val();
    var job_id = $("#job_id").val();
    var date_from = $("#date_from").val();
    var date_to = $("#date_to").val();
    var kit_master_id = $("#kit_type").val();
    var part_type = $("#part_type").val();
    var serial_no = $("#serial_no").val();
    var row = '';
    var no = '1';

    var dataForm = {
      _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
      // grn_no: grn_no,
      set_id: set_id,
      job_id: job_id,
      customer_code: customer_code,
      date_from: date_from,
      date_to: date_to,
      kit_master_id: kit_master_id,
      part_type: part_type,
      serial_no: serial_no
    };

    $.ajax({
      type: 'GET',
      url: '/report/tracking_query/get-tracking-query-detail',
      data: dataForm,
      dataType: 'json',
      context: this,
      success: function( response )
      {console.log(response);

        $( "#tracking-report-table tbody" ).empty();

        if(response.data.length > 0)
        {
          // $("input[type=hidden][name=hidden_grn_no]").val( grn_no );
          $("input[type=hidden][name=hidden_set_id]").val( set_id );
          $("input[type=hidden][name=hidden_customer_code]").val( customer_code );
          $("input[type=hidden][name=hidden_job_id]").val( job_id );
          $("input[type=hidden][name=hidden_date_from]").val( date_from );
          $("input[type=hidden][name=hidden_date_to]").val( date_to );
          $("input[type=hidden][name=hidden_kit_type]").val( kit_master_id );
          $("input[type=hidden][name=hidden_part_type]").val( part_type );
          $("input[type=hidden][name=hidden_serial_no]").val( serial_no );

          $.each(response.data, function( index, data ) {

            row += '<tr>';
            row += '<td>' + no + '</td>';
            row += '<td>' + data.grn_no + '</td>';
            row += '<td>' + data.collection_date + '</td>';
            row += '<td>' + data.customer_name + '</td>';
            row += '<td>' + data.job_id + '</td>';
            row += '<td>' + data.kit_type + '</td>';
            row += '<td>' + data.set_id + '</td>';
            row += '<td>' + data.part_type_name + '</td>';
            row += '<td>' + data.serial_no + '</td>';
            row += '<td>' + data.rf + '</td>';
            row += '<td>' + data.arf + '</td>';
            row += '<td>' + data.param_datapoint_value + '</td>';
            row += '<td>' + data.datapoint + '</td>';
            row += '<td>' + data.avg + '</td>';
            row += "</tr>";
            no++;
          });

          $( ".export-btn" ).show();
        }
        else
        {
          $( ".export-btn" ).hide();

          row += '<tr>';
          row += '<td>No data found</td>';
          row += "</tr>";
        }

        $( '#tracking-report-table tbody' ).append( row );
      },

      error: function ( response )
      {
        console.log( response );

        $( "#tracking-report-table tbody" ).empty();
        $( ".export-btn" ).hide();

        row += '<tr>';
        row += '<td>No data found</td>';
        row += "</tr>";

        $( '#tracking-report-table tbody' ).append( row );
      }
    });

  });

  $( '.filter-tracking-query-box' ).on('change', '#customer_code', function () {

    // $( '.customKitBox' ).hide();
    $( ".divHide" ).hide();

    var select_job = [];
    var select_grn = [];
    var location = $( this );

    if( $( this ).val() != '' )
    {
      var formData = {
        _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
        cust_code:	$( this ).val()
      }

      $.ajax({
        type: 'GET',
        url: '/post-cleaning-report/get-grn-details',
        data: formData,
        dataType: 'json',
        success: function( response )
        {
          $( '#job_id' ).remove();
          $( '.job_id' ).append( '<select class="form-control" id="job_id" name="job_id"><option value="">Please select</option></select>' );

          // $( '#grn_no' ).remove();
          // $( '.grn_no' ).append( '<select class="form-control" id="grn_no" name="grn_no"><option value="">Please select</option></select>' );

          if( response.data.length != 0 )
          {
            $.each(response.data, function( index, data ) {
              select_job.push( data.job_id );
              // select_grn.push( data.grn_no );
            });

            $.each(select_job, function( index, data ) {
              $( '#job_id' ).append( '<option value="' + data + '">' + data + '</option>' );
              $( '#grn_no' ).append( '<option value="' + select_grn[index] + '">' + select_grn[index] + '</option>' );
            });
          }

          // else
          // {
          //   $( '#job_id' ).append( '<option value="">Please select</option>' );
          //   $( '#grn_no' ).append( '<option value="">Please select</option>' );
          // }

          var formData = {
            _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
            cust_code:	location.val()
          }

          $.ajax({
            type: 'GET',
            url: '/post-cleaning-report/get-kit-type-list',
            data: formData,
            dataType: 'json',
            success: function( response )
            {
              $( '#kit_type' ).remove();
              $( '.kit_type' ).append( '<select class="form-control" id="kit_type" name="kit_type"><option value="">Please select</option></select>' );

              if( response.data.length != 0 )
              {
                $.each(response.data, function( index, data ) {
                  $( '#kit_type' ).append( '<option value="' + data.id + '">' + data.kit_type + '</option>' );
                });
              }

              $.ajax({
                type: 'GET',
                url: '/post-cleaning-report/get-set-id-list',
                data: formData,
                dataType: 'json',
                success: function( response )
                {
                  $( '#set_id' ).remove();
                  $( '.set_id' ).append( '<select class="form-control" id="set_id" name="set_id"><option value="">Please select</option></select>' );

                  if( response.data.length != 0 )
                  {
                    $.each(response.data, function( index, data ) {
                      $( '#set_id' ).append( '<option value="' + data.set_id + '">' + data.set_id + '</option>' );
                    });
                  }
                },

                error: function ( response )
                {
                  console.log( response );
                }
              });
            },

            error: function ( response )
            {
              console.log( response );
            }
          });

        },

        error: function ( response )
        {
          console.log( response );
        }
      });
    }
  });

  $( '.filter-tracking-query-box' ).on('change', '#job_id', function () {

    var select_job = [];
    // var select_grn = [];
    var select_id = [];
    var c = [];
    var location = $( this );

    if( $( this ).val() != '' )
    {
      $( ".divHide" ).show();

      var formData = {
        _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
        job_id:	$( this ).val()
      }

      $.ajax({
        type: 'GET',
        url: '/report/grn_query/get-good-part-detail-part-type',
        data: formData,
        dataType: 'json',
        success: function( response )
        {
          $( '#part_type' ).remove();
          $( '.part_type' ).append( '<select class="form-control" id="part_type" name="part_type"><option value="">Please select</option></select>' );

          if( response.data.length != 0 )
          {
            $.each(response.data, function( index, data ) {
              $( '#part_type' ).append( '<option value="' + data.part_type_name + '">' + data.part_type_name + '</option>' );
            });
          }

          $.ajax({
            type: 'GET',
            url: '/report/grn_query/get-good-part-detail-serial',
            data: formData,
            dataType: 'json',
            success: function( response )
            {
              $( '#serial_no' ).remove();
              $( '.serial_no' ).append( '<select class="form-control" id="serial_no" name="serial_no"><option value="">Please select</option></select>' );

              if( response.data.length != 0 )
              {
                $.each(response.data, function( index, data ) {
                  $( '#serial_no' ).append( '<option value="' + data.serial_no + '">' + data.serial_no + '</option>' );
                });
              }
            },

            error: function ( response )
            {
              console.log( response );
            }
          });
        },

        error: function ( response )
        {
          console.log( response );
        }
      });
    }
  });

});

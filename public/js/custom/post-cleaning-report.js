$(function() {

  $( '.filter-grn-query-box' ).on('change', '#customer_code', function () {

    $( '.customKitBox' ).hide();

    var select_job = [];
    var select_grn = [];

    if( $( this ).val() != '' )
    {
      var formData = {
        _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
        cust_code:	$( this ).val()
      }

      $.ajax({
        type: 'GET',
        url: '/post-cleaning-report/get-grn-details',
        data: formData,
        dataType: 'json',
        success: function( response )
        {
          $( '#job_id' ).remove();
          $( '.job_id' ).append( '<select class="form-control" id="job_id" name="job_id"><option value="">Please select</option></select>' );

          // $( '#grn_no' ).remove();
          // $( '.grn_no' ).append( '<select class="form-control" id="grn_no" name="grn_no">' );

          if( response.data.length != 0 )
          {
            $.each(response.data, function( index, data ) {
              select_job.push( data.job_id );
              select_grn.push( data.grn_no );
            });

            $.each(select_job, function( index, data ) {
              $( '#job_id' ).append( '<option value="' + data + '">' + data + '</option>' );
              // $( '#grn_no' ).append( '<option value="' + select_grn[index] + '">' + select_grn[index] + '</option>' );
            });
          }

          // else
          // {
          //   $( '#job_id' ).append( '<option value="">Please select</option>' );
          //   $( '#grn_no' ).append( '<option value="">Please select</option>' );
          // }
        },

        error: function ( response )
        {
          console.log( response );
        }
      });
    }
  });

  $( '.customKitBox' ).hide();

  $( '.filter-grn-query-box' ).on('change', '#job_id', function () {

    var grn_no = '';
    var newRowContent = '';
    var job_id = $( '#job_id' ).val();
    var customer_code = $( '#customer_code' ).val();

    if( job_id != '' )
    {
      var formData = {
        _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
        // grn_no:	grn_no,
        job_id: job_id,
        customer_code: customer_code
      }

      $.ajax({
        type: 'GET',
        url: '/post-cleaning-report/get-kit-details',
        data: formData,
        dataType: 'json',
        success: function( response )
        {
          // $( '#kit_type' ).remove();
          // $( '.kit_type' ).append( '<select multiple="multiple" class="form-control" id="kit_type" name="kit_type[]"></select>' );

          if( response.data.length != 0 )
          {
            $( '#kit-info-table tbody' ).find( 'tr' ).remove();
            $( '.customKitBox' ).show();

            $.each(response.data, function( index, data ) {

              grn_no = data.grn_no;
              newRowContent = '<tr><input type="hidden" name="good_kit_detail_id[]" value="' + data.good_kit_detail_id + '"><input type="hidden" name="kit_master_id[]" value="' + data.id + '"><input type="hidden" name="tool_kit[]" value="' + data.tool_kit + '"><input type="hidden" name="set_id[]" value="' + data.set_id + '"><td>' +
                              data.kit_type + '</td><td>' + data.tool_kit + '</td><td>' + data.set_id +
                               '</td><td><input type="checkbox" id="select_kit" name="select_kit[]"><input type="hidden" id="hidden_select_kit" name="hidden_select_kit[]" value="0"></td><tr>';

              $( '#kit-info-table tbody' ).append( newRowContent );
            });

            $( '#grn_no' ).val( grn_no );
          }

          else
          {
            $( '.customKitBox' ).hide();
          }
        },

        error: function ( response )
        {
          console.log( response );
        }
      });
    }
  });

  $( '#kit-info-table' ).on('change', '#select_kit', function () {

    if( $(this).is( ':checked' ) )
    {
      $(this).closest('td').find('#hidden_select_kit').val( 1 );
    }

    else
    {
      $(this).closest('td').find('#hidden_select_kit').val( 0 );
    }
  });

});


$(function() {

  var kit_row = '';

  $('form#part-type-form').change(function() {

      $( this ).attr( 'formmodified', 1 );
  });

  // Switching form autocomplete attribute to off
  $("form").attr('autocomplete', 'off');

  // append row in kit table
  $( '.add-kit' ).click(function() {

    kit_row = $( '#kit-table tbody tr' ).length;

    $( '#append-row > .sr_no' ).text( kit_row + 1 );

    $( '#append-row' ).clone().appendTo( '#kit-table' );
    $( '#kit-table tr:last' ).removeAttr( 'id' );
    $( '#kit-table tr:last td:nth-child(2)' ).removeClass( 'sr_no' );
  });

  // remove row in kit table
  $( '#kit-table' ).on('click', '.remove-row', function() {

    if (!confirm( 'Do you confirm you want to delete this record?' ))
    {
      return false;
    }

    else
    {
      if( $( this ).attr( 'data-id' ) != '' )
      {
        var formData = {
          _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
          id: $( this ).attr( 'data-id' )
        };

        $.ajax({
          type: 'GET',
          url: '/get-delete-good-kit-detail',
          data: formData,
          dataType: 'json',
          success: function( response )
          {
              if(response.data)
              {
                  alert('Selected Kit Type is successfully deleted.');
              }
          },

          error: function ( response )
          {
            console.log( response );
          }
        });
      }

      $( this ).parent().parent().remove();
    }
  });

  // autocomplete for set id
  $(document).on('keydown.autocomplete', '.set-id', function() {

    var kit_master_id = $( this ).closest( 'tr' ).find( '.kit-master-id' ).val();

    if(kit_master_id != '')
    {
      $( this ).autocomplete({
        source: '/get-setid?kit_master_id=' + kit_master_id,
        minLength: 1,
        // change: function (event, ui) {
        //   if (ui.item === null)
        //   {
        //     $( this ).val( '' );
        //     // $( this ).closest( 'tr' ).find( '.serial-no' ).val( '' );
        //   }
        //
        //   else
        //   {
        //     var serial_no = [];
        //     var set_id = $( this ).val();
        //     var location = $( this ).closest( 'td' ).parent().find( '.serial-no' );
        //     var kit_master_id = $( this ).closest( 'tr' ).find( '.kit-master-id' ).val();
        //
        //     var formData = {
        //       _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
        //       set_id:	set_id,
        //       kit_master_id:	kit_master_id
        //     }
        //
        //     $.ajax({
        //       type: 'GET',
        //       url: '/pre-cleaning/get-part-master-serial-no',
        //       data: formData,
        //       dataType: 'json',
        //       success: function( response )
        //       {
        //         location.val( response.data );
        //       },
        //
        //       error: function ( response )
        //       {
        //         console.log( response );
        //       }
        //     });
        //   }
        // }
        select: function(event, ui) {
      		$( this ).val( ui.item.value );
      	}
      });
    }
  });

  // autocomplete for serial no
  $('#kit-table').on('keydown.autocomplete', 'input:text[id^="serial-no"]', function() {
  // $(document).on('keydown.autocomplete', "input:text[id^='serial-no']", function() {

    var kit_master_id = $( this ).closest( 'tr' ).find( '.kit-master-id' ).val();

    $( this ).autocomplete({
      source: '/get-serialno?kit_master_id=' + kit_master_id,
      minLength: 1,
      // change: function (event, ui) {
      //   if (ui.item === null)
      //   {
      //     $( this ).val( '' );
      //     // $( this ).closest( 'tr' ).find( '.set-id' ).val( '' );
      //   }
      //
      //   else
      //   {
      //     var set_id = [];
      //     var serial_no = $( this ).val();
      //     var location = $( this ).closest( 'td' ).parent().find( '.set-id' );
      //     var kit_master_id = $( this ).closest( 'tr' ).find( '.kit-master-id' ).val();
      //
      //     var formData = {
      //       _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
      //       serial_no:	serial_no,
      //       kit_master_id:	kit_master_id
      //     }
      //
      //     $.ajax({
      //       type: 'GET',
      //       url: '/pre-cleaning/get-part-master-set-id',
      //       data: formData,
      //       dataType: 'json',
      //       success: function( response )
      //       {
      //         location.val( response.data );
      //       },
      //
      //       error: function ( response )
      //       {
      //         console.log( response );
      //       }
      //     });
      //   }
      // }
      select: function(event, ui) {
    		$( this ).val( ui.item.value );
    	}
    });
  });

  // fill in set id when there is serial no
  $( '#kit-table' ).on('focusout', '.serial-no', function () {

    if( $( this ).val() != '' )
    {
      var set_id = [];
      var serial_no = $( this ).val();
      var location = $( this ).closest( 'td' ).parent().find( '.set-id' );
      var kit_master_id = $( this ).closest( 'tr' ).find( '.kit-master-id' ).val();

      var formData = {
        _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
        serial_no:	serial_no,
        kit_master_id:	kit_master_id
      }

      $.ajax({
        type: 'GET',
        url: '/pre-cleaning/get-part-master-set-id',
        data: formData,
        dataType: 'json',
        success: function( response )
        {
          console.log(response);
          location.val( response.data );
        },

        error: function ( response )
        {
          console.log( response );
        }
      });
    }

  });

  // fill in serial no when there is set id
  $( '#kit-table' ).on('focusout', '.set-id', function () {

    if( $( this ).val() != '' )
    {
      var serial_no = [];
      var set_id = $( this ).val();
      var location = $( this ).closest( 'td' ).parent().find( '.serial-no' );
      var kit_master_id = $( this ).closest( 'tr' ).find( '.kit-master-id' ).val();

      var formData = {
        _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
        set_id:	set_id,
        kit_master_id:	kit_master_id
      }

      $.ajax({
        type: 'GET',
        url: '/pre-cleaning/get-part-master-serial-no',
        data: formData,
        dataType: 'json',
        success: function( response )
        {
          location.val( response.data );
        },

        error: function ( response )
        {
          console.log( response );
        }
      });
    }

  });

  // autocomplete for serial no in kit header
  $( '#part-type-table' ).on('keydown.autocomplete', 'input:text[id^="serial-no"]', function() {

    var part_type_id = $( this ).closest( 'tr' ).find( '.modal-part-type-id' ).val();
    var kit_master_id = $( this ).closest( '.modal-body' ).find( '.modal-kit-master-id' ).val();

    $( this ).autocomplete({
      source: '/get-serialno-for-kitheader?kit_master_id=' + kit_master_id + '&part_type_id=' + part_type_id,
      minLength: 0,
      select: function(event, ui) {
    		$( this ).val( ui.item.value );

    	}
    });
  });

  $( '#part-type-table' ).on('focusin', '#serial-no', function () {

    if( $( this ).is(":focus") )
    {
      $( '.modal-btn-save' ).attr( 'disabled', 'disabled' );
    }

  });

  $( '#part-type-table' ).on('focusout', '#serial-no', function (event) {

    $( '.modal-btn-save' ).removeAttr( 'disabled' );

    var location = $( this );
    var rf = $( this ).closest( 'tr' ).find( '.part-type-rf' ).val();
    var arf = $( this ).closest( 'tr' ).find( '.arf' ).val();
    var set_id = $( this ).closest( '.modal-content' ).find( '.modal-set-id' ).val();

    if( $( this ).val() != '' )
    {
      var formData = {
        _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
        serial_no:	$( this ).val(),
        part_type_id:	$( this ).closest( 'tr' ).find( '.modal-part-type-id' ).val(),
        kit_master_id:	$( '.modal-kit-master-id' ).val(),
        set_id: set_id
      }

      $.ajax({
        type: 'GET',
        url: '/get-part-master-serial-no-exist',
        data: formData,
        dataType: 'json',
        success: function( response )
        {
          // check that particular kit and part type with kit identifier has not been closed
          if( response.kit_identifier_notclosing )
          {
              alert( 'You are not allowed to have ' + location.val() + ' in this part type as it has not been closed or not belongs to current set id!' );
              location.val( '' );
              return false;
          }

          // check kit, part type id and serial no. exist in table or not to get arf value
          if( !response.arf )
          {
            location.closest( 'tr' ).find( '.arf' ).val( rf );
          }

          else
          {
            location.closest( 'tr' ).find( '.arf' ).val( parseFloat(response.arf) + parseFloat(rf) )
          }

          // check serial no. is not new
          if( !response.data )
          {
            if( confirm('New serial no. ' + location.val() + '! Do you want to proceed?\nClick OK to proceed!' ) )
            {
              return false;
            }

            else
            {
               location.val( '' );
               return false;
            }
          }

          // check serial no. exists in other kit type with similar part type and serial is not NA
          if( response.no_part_master == 1 && !response.kit_part_serial_exist && ( location.val() != 'NA' ) )
          {
            var kit_type_name = response.kit_type;

            if( confirm( location.val() + ' already exists in ' + response.kit_type + '. Do you want to transfer to ' + response.existing_kit_master + '?\nClick OK to proceed!') )
            {
              // update kit_master_id based on serial-no and part_type_id
              var formData = {
                _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
                trans_kit_master_id:	response.trans_kit_master_id,
                kit_master_id:	$( '.modal-kit-master-id' ).val(),
                part_type_id:	location.closest( 'tr' ).find( '.modal-part-type-id' ).val(),
                serial_no: location.val()
              }

              $.ajax({
                type: 'POST',
                url: '/post-part-master-serial-no-transfer',
                data: formData,
                dataType: 'json',
                success: function( response )
                {
                  console.log(response);
                  // if the logic goes here, means got bug, kit and part type ids are not updated in arf table
                  if( !response.data && !response.update_arf )
                  {
                    location.val( '' );
                    alert( 'Unable to transfer, please update ' + location.val() + ' for ' + kit_type_name + ' in part master db setting.' );
                  }

                  else if( response.data && response.update_arf )
                  {
                    alert( 'Part Master has been updated.' );
                    location.trigger( 'focusout' );
                  }

                  else
                  {
                    location.val( '' );
                    alert( 'Current part type is not exist in ' + kit_type_name );
                  }
                },

                error: function ( response )
                {
                  console.log( response );
                }
              });

              return false;
            }

            else
            {
              location.val( '' );
              return false;
            }
          }

          // else if( response.no_part_master > 1 && ( location.val() != 'NA' || location.val() != 'na' ) )
          else if( response.no_part_master > 1 && ( location.val().toUpperCase() != 'NA' ) )
          {
            alert( location.val().toUpperCase() + ' already exists in multiple kit type, please make adjustment in Part Master setting' );
            location.val( '' );
            return false;
          }

          // else if( location.val().toUpperCase() == 'NA' )
          // {
          //   location.closest( 'tr' ).find( '.part-type-rf' ).val( 0 );
          //   location.closest( 'tr' ).find( '.arf' ).val( parseFloat( arf ) - parseFloat( rf ) );
          // }
        },

        error: function ( response )
        {
          console.log( response );
        }
      });
    }
  });

  // get max rf hrs when part type id is changed
  $('#part-type-table').on('change', '.modal-part-type-id', function() {

    var kit_master_id = $( this ).closest( 'form' ).parent().find( '.modal-kit-master-id' ).val();

    $( this ).closest( 'tr' ).find( '.modal-hidden-part-type-id' ).val( $( this ).val() );
    $( this ).closest( 'tr' ).find( '.serial-no' ).val('');
    $( this ).closest( 'tr' ).find( '.part-type-rf' ).val(0);
    $( this ).closest( 'tr' ).find( '.arf' ).val('');

    var formData = {
      _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
      part_type_id: $( this ).val(),
      kit_master_id: kit_master_id

    };

    $.ajax({
      type: 'GET',
      url: '/get-max-rf-hrs',
      data: formData,
      dataType: 'json',
      context: this,
      success: function( response )
      {
        if( response.data != null )
        {
          $( this ).closest( 'tr' ).find( '.max-rf-hrs' ).val( response.data['max_rf_hrs'] );
        }

        else
        {
          $( this ).closest( 'tr' ).find( '.max-rf-hrs' ).val( '' );
        }

        // if( response.part_master != null && response.part_master.length == 1 )
        // {
        //   $( this ).closest( 'tr' ).find( '.serial-no' ).remove();
        //   $( this ).closest( 'tr' ).find( 'td:eq(2)' ).append( '<input type="text" name="serial_no[]" id="serial-no" class="form-control serial-no" value="' + response.part_master[0]['serial_no'] + '" disabled />' );
        // }
        //
        // else if( response.part_master.length > 1 )
        // {
        //   $( this ).closest( 'tr' ).find( '.serial-no' ).remove();
        //   $( this ).closest( 'tr' ).find( 'td:eq(2)' ).append( '<select name="serial_no[]" id="serial-no" class="form-control serial-no"></select>' );
        //
        //   var location = $( this ).closest( 'tr' ).find( 'td:eq(2) select' );
        //   var serial = ['Please select'];
        //
        //   $.each(response.part_master, function( index, data ) {
        //     serial.push( data.serial_no );
        //   });
        //
        //   $.each(serial, function( index, data ) {
        //     location.append( '<option value="' + data + '">' + data + '</option>' );
        //   });
        // }
        //
        // else
        // {
        //   $( this ).closest( 'tr' ).find( '.serial-no' ).remove();
        //   $( this ).closest( 'tr' ).find( 'td:eq(2)' ).append( '<input type="text" name="serial_no[]" id="serial-no" class="form-control serial-no" />' );
        // }

        // if( response.arf != null )
        // {
        //   $( this ).closest( 'tr' ).find( '.arf' ).val( response.arf['arf'] );
        // }

        // else
        // {
        //   $( this ).closest( 'tr' ).find( '.arf' ).val( 0 );
        // }
      },

      error: function ( response )
      {
        console.log( response );
      }
    });
  });

  $('#part-type-table').on('change', 'select.serial-no', function() {

    var kit_master_id = $( this ).closest( 'form' ).parent().find( '.modal-kit-master-id' ).val();
    var part_type_id = $( this ).closest( 'tr' ).find( '.modal-part-type-id' ).val();
    var location = $( this ).closest( 'tr' ).find( '.arf' );

    var formData = {
      _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
      part_type_id: part_type_id,
      kit_master_id: kit_master_id,
      serial_no: $( this ).val()
    };

    $.ajax({
      type: 'GET',
      url: '/get-arf-of-serialno',
      data: formData,
      dataType: 'json',
      success: function( response )
      {
        if( !response.arf['arf'] )
        {
          location.val( 0 );
        }

        else
        {
          location.val( response.arf['arf'] );
        }
      },

      error: function ( response )
      {
        console.log( response );
      }
    });
  });

  // remove row in modal dialog box part type table
  $( '#part-type-table' ).on('click', '.remove-row', function() {

    var kit_master_id = $( this ).closest( 'form' ).parent().find( '.modal-kit-master-id' ).val();
    var rf = $( this ).closest( 'tr' ).find( '.part-type-rf' ).val();
    var serial_no = $( this ).closest( 'tr' ).find( '.serial-no' ).val();
    var temp_data_id = $( this ).attr( 'temp-data-id' );
    // var part_type_id = $( this ).closest( 'tr' ).find( '.modal-part-type-id' ).val();

    if (!confirm( 'Do you confirm you want to delete this record?' ))
    {
      return false;
    }

    else
    {
      if( $(this).attr( 'data-id' ) != '' )
      {
        var formData = {
          _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
          id: $( this ).attr( 'data-id' ),
          rf: rf,
          serial_no: serial_no,
          kit_master_id: kit_master_id
        };

        $.ajax({
          type: 'GET',
          url: '/get-delete-good-part-detail',
          data: formData,
          dataType: 'json',
          success: function( response )
          {
            // success alert
            if( response.data == 'success' )
            {
              // alert('Selected Part Type is successfully deleted.');
            }
          },

          error: function ( response )
          {
            console.log( response );
          }
        });
      }

      else
      {
        var location = $( '#part-type-table' ).find( "[data-id=" + temp_data_id + "]" );

        location.closest( 'tr' ).find( '.part-type-rf' ).removeAttr( 'disabled' );
        location.closest( 'tr' ).find( '.actions' ).removeAttr( 'disabled' ).val( 1 );
      }

      $( this ).parent().parent().remove();
    }
  });

  $( '#kit-table' ).on('change', '.kit-master-id', function () {

    var location = $( this );

    $( this ).closest( 'tr' ).find( '.kit-master-id' ).parent().removeClass( 'has-error' );
    $( this ).closest( 'tr' ).find( '.reason-id' ).parent().removeClass( 'has-error' );
    $( this ).closest( 'tr' ).find( '.set-id' ).parent().removeClass( 'has-error required-field' );
    $( this ).closest( 'tr' ).find( '.serial-no' ).parent().removeClass( 'has-error required-field' );
    $( this ).closest( 'tr' ).find( '.tool-kit' ).parent().removeClass( 'has-error' );
    $( this ).closest( 'tr' ).find( '.quantity' ).parent().removeClass( 'has-error required-field' );
    $( this ).closest( 'tr' ).find( '.rf' ).parent().removeClass( 'has-error required-field' );

    if( $( this ).val() != '' )
    {
      // get kit identifier to validate required fields
      var formData = {
        _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
        kit_master_id: $( this ).val()
      };

      $.ajax({
        type: 'GET',
        url: '/pre-cleaning/get-part-master-kit-identifier',
        data: formData,
        dataType: 'json',
        success: function( response )
        {
          if(response.data)
          {
            location.closest( 'tr' ).find( '.set-id' ).parent().addClass( 'required-field' );
            location.closest( 'tr' ).find( '.serial-no' ).parent().addClass( 'required-field' );
            location.closest( 'tr' ).find( '.quantity' ).parent().addClass( 'required-field' );
            location.closest( 'tr' ).find( '.rf' ).parent().addClass( 'required-field' );
          }
        },

        error: function ( response )
        {
          console.log( response );
        }
      });
    }
  });

  // populate dialog box belong with kit type
  $( '#kit-table' ).on('click', '.dialog-arrow', function(e) {

    e.preventDefault();

    $( '.check-part-type-fields' ).hide();
    $( '.check-part-type-fields' ).empty();

    $( '.right-arrow' ).find( 'a' ).removeClass( 'active' );
    $( this ).parent().addClass( 'active' );

    var good_receipt_header_id = $( '.grn-id' ).val();
    var kit_master = $( this ).closest( 'tr' ).find( '.kit-master-id option:selected' ).text();
    var kit_master_id = $( this ).closest( 'tr' ).find( '.kit-master-id' ).val();
    var reason = $( this ).closest( 'tr' ).find( '.reason-id option:selected' ).text();
    var reason_id = $( this ).closest( 'tr' ).find( '.reason-id' ).val();
    var set_id = $( this ).closest( 'tr' ).find( '.set-id' ).val();
    var serial_no = $( this ).closest( 'tr' ).find( '.serial-no' ).val();
    var tool_kit = $( this ).closest( 'tr' ).find( '.tool-kit' ).val();
    var quantity = $( this ).closest( 'tr' ).find( '.quantity' ).val();
    var rf = $( this ).closest( 'tr' ).find( '.rf' ).val();
    var job_status = $( "input[type=hidden][name=hidden_job_status]" ).val();
    var location = $( this );

    var set_id_class = $( this ).closest( 'tr' ).find( '.set-id' ).parent();
    var serial_no_class = $( this ).closest( 'tr' ).find( '.serial-no' ).parent();
    var quantity_class = $( this ).closest( 'tr' ).find( '.quantity' ).parent();
    var rf_class = $( this ).closest( 'tr' ).find( '.rf' ).parent();

    if( kit_master_id == '' )
    {
      $( this ).closest( 'tr' ).find( '.kit-master-id' ).parent().addClass( 'has-error' );
    }

    else
    {
      $( this ).closest( 'tr' ).find( '.kit-master-id' ).parent().removeClass( 'has-error' );
    }

    if( reason_id == '' )
    {
      $( this ).closest( 'tr' ).find( '.reason-id' ).parent().addClass( 'has-error' );
    }

    else
    {
      $( this ).closest( 'tr' ).find( '.reason-id' ).parent().removeClass( 'has-error' );
    }

    if( tool_kit == '' )
    {
      $( this ).closest( 'tr' ).find( '.tool-kit' ).parent().addClass( 'has-error' );
    }

    else
    {
      $( this ).closest( 'tr' ).find( '.tool-kit' ).parent().removeClass( 'has-error' );
    }

    if( set_id == '' && set_id_class.hasClass( 'required-field' ) )
    {
      $( this ).closest( 'tr' ).find( '.set-id' ).parent().addClass( 'has-error' );
    }

    else
    {
      $( this ).closest( 'tr' ).find( '.set-id' ).parent().removeClass( 'has-error' );
    }

    if( serial_no == '' && serial_no_class.hasClass( 'required-field' ) )
    {
      $( this ).closest( 'tr' ).find( '.serial-no' ).parent().addClass( 'has-error' );
    }

    else
    {
      $( this ).closest( 'tr' ).find( '.serial-no' ).parent().removeClass( 'has-error' );
    }

    if( quantity == '' && quantity_class.hasClass( 'required-field' ) )
    {
      $( this ).closest( 'tr' ).find( '.quantity' ).parent().addClass( 'has-error' );
    }

    else
    {
      $( this ).closest( 'tr' ).find( '.quantity' ).parent().removeClass( 'has-error' );
    }

    if( rf == '' && rf_class.hasClass( 'required-field' ) )
    {
      $( this ).closest( 'tr' ).find( '.rf' ).parent().addClass( 'has-error' );
    }

    else
    {
      $( this ).closest( 'tr' ).find( '.rf' ).parent().removeClass( 'has-error' );
    }

    // check selected row has error or not
    if( $( this ).closest( 'tr' ).find( '.has-error' ).length == 0 )
    {
      var kit_type_id = $( this ).closest( 'td' ).find( '.active' ).attr( 'data-kit-type-id' );
      var data_closed = $( this ).closest( 'td' ).find( '.hidden_dialog-arrow' ).attr( 'data-closed' );

      if(data_closed == 1)
      {
        $( '.modal-btn-save' ).hide();
        $( '.add-part-type' ).hide();
      }

      else
      {
        $( '.modal-btn-save' ).show();
        $( '.add-part-type' ).show();
      }

      $( '.modal-kit-type-id' ).val( kit_type_id );

      // check selected row is already exist in table or not
      var dataForm = {
        _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
        good_kit_detail_id: kit_type_id,
        good_receipt_header_id: good_receipt_header_id,
        kit_master_id: kit_master_id,
        reason_master_id: reason_id,
        tool_kit: tool_kit,
        set_id: set_id,
        serial_no: serial_no,
        quantity: quantity,
        rf: rf,
        job_status: job_status
      };

      $.ajax({
        type: 'POST',
        url: '/process/pre-cleaning/post-kit-type-precleaning',
        data: dataForm,
        dataType: 'json',
        context: this,
        success: function( response )
        {
          $( '.modal-kit-type-id' ).val( response['kit_type_id'] );
          $( this ).closest( 'td' ).find( '.active' ).attr( 'data-kit-type-id', response['kit_type_id'] );
          $( this ).closest( 'td' ).find( '.hidden_dialog-arrow' ).val( response['kit_type_id'] );
          $( this ).closest( 'tr' ).find( '.remove-row' ).attr( 'data-id', response['kit_type_id'] );
          $( this ).closest( 'tr' ).find( '.kit-master-id' ).attr( 'disabled', 'disabled' );
          $( this ).closest( 'tr' ).find( '.remove-row' ).remove();
        },

        error: function ( response )
        {
          console.log( response );
        }
      });

      var part_type_id_array = [];
      var arf_array = [];
      var arf_check = [];

      // get part type
      var formData = {
        _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
        kit_master_id: kit_master_id
      };

      $.ajax({
        type: 'GET',
        url: '/get-part-type-lists',
        data: formData,
        dataType: 'json',
        success: function( response )
        {
          $( '#modal-part-type-id' ).find( 'option' ).not( ':first' ).remove();

          $.each(response.data, function( index, data ) {
            $( '#modal-part-type-id' ).append( '<option value="' + data.id + '">' + data.part_type + '</option>' );
            part_type_id_array.push( data.id );
          });

          //load get-part-type-lists before get-good-part-details
          var dataForm = {
            _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
            good_kit_detail_id: $( '.modal-kit-type-id' ).val(),
            kit_master_id: kit_master_id,
            set_id: set_id,
            serial_no: serial_no,
            part_type_id_array: part_type_id_array,
            good_receipt_header_id: good_receipt_header_id
          };

          $.ajax({
            type: 'GET',
            url: '/get-good-part-details',
            data: dataForm,
            dataType: 'json',
            context: this,
            success: function( response )
            {
              if( response.data.length > 0 )
              {
                $.each(response.arf_array, function( index, arf ) {
                  arf_array.push( arf );
                });

                $.each(response.data, function( index, data ) {

                  $( '#modal-append-row' ).clone().appendTo( '#part-type-table' );

                  $( '#modal-append-row' ).find( '.remove-row' ).attr( 'data-id', data.id );
                  $( '#modal-append-row' ).find( '.modal-part-type-id' ).val( data.part_type_id ).attr( 'disabled', 'disabled' );
                  $( '#modal-append-row' ).find( '.modal-hidden-part-type-id' ).val( data.part_type_id );

                  if( data.rf != '' )
                  {
                    $( '#modal-append-row' ).find( '.serial-no' ).val( data.serial_no ).attr( 'disabled', 'disabled' );
                  }

                  else if( data.serial_no != '' )
                  {
                    $( '#modal-append-row' ).find( '.serial-no' ).val( data.serial_no );
                  }

                  else
                  {
                    $( '#modal-append-row' ).find( '.serial-no' ).val( '' );
                  }

                  $( '#modal-append-row' ).find( '.visual-condition' ).val( data.visual_condition.split(',') );
                  $( '#modal-append-row' ).find( '.max-rf-hrs' ).val( data.max_rf_hrs );

                  // for rf
                  if( data.rf != '' )
                  {
                    $( '#modal-append-row' ).find( '.part-type-rf' ).val( data.rf );
                  }

                  // else if( data.serial_no == 'NA' || data.serial_no == 'na' )
                  // {
                  //   $( '#modal-append-row' ).find( '.part-type-rf' ).val( 0 );
                  // }

                  else if( data.serial_no.toUpperCase() == 'NA' )
                  {
                    $( '#modal-append-row' ).find( '.part-type-rf' ).val( 0 );
                  }

                  else
                  {
                    $( '#modal-append-row' ).find( '.part-type-rf' ).val( rf );
                  }

                  arf_check.push( data.arf );

                  if( data.rf == '' && data.arf == '' )
                  {
                    var final_rf = $( '#modal-append-row' ).find( '.part-type-rf' ).val();

                    // serial no. is NA/na, so don't add in default rf
                    if( final_rf == 0 )
                    {
                      $( '#modal-append-row' ).find( '.arf' ).val( parseFloat( arf_array[index] ) + 0 );
                    }

                    else
                    {
                      $( '#modal-append-row' ).find( '.arf' ).val( parseFloat( arf_array[index] ) + parseFloat( rf ) );
                    }
                  }

                  else if( data.rf != '' && data.arf == '' )
                  {
                    $( '#modal-append-row' ).find( '.arf' ).val( parseFloat( arf_array[index] + parseFloat( data.rf ) ));
                    // $( '#modal-append-row' ).find( '.part-type-rf' ).attr( 'disabled', 'disabled' );
                  }

                  else
                  {
                    $( '#modal-append-row' ).find( '.arf' ).val( parseFloat( parseFloat( data.arf ) ));

                    if( data.rf != '' && data.arf != '' && response.latest_good_kit )
                    {
                      $( '#modal-append-row' ).find( '.part-type-rf' ).attr( 'disabled', 'disabled' );
                    }
                  }

                  // to highlight red if arf > max rf hours
                  if( data.max_rf_hrs )
                  {
                    var tot_arf = $( '#modal-append-row' ).find( '.arf' ).val();
                    var tot_max_rfh = data.max_rf_hrs;

                    if( parseFloat( tot_arf ) > parseFloat( tot_max_rfh ) )
                    {
                      $( '#modal-append-row' ).find( '.arf' ).css( 'background-color', 'red' );
                    }
                  }

                  // for actions
                  // if( data.actions == 2 )
                  // {
                  //   $( '#modal-append-row' ).find( '.actions' ).val( data.actions ).attr( 'disabled', 'disabled' );
                  // }
                  //
                  // else
                  // {
                    $( '#modal-append-row' ).find( '.actions' ).val( data.actions );
                  // }

                  // remove id from table row
                  $( '#part-type-table tr:last' ).removeAttr( 'id' );

                });

                // if( response.datapoint_exist == true || arf_check[0] != '' )
                // {
                //   $( '.modal-btn-save' ).attr( 'disabled', 'disabled' );
                //   $( '.add-part-type' ).attr( 'disabled', 'disabled' );
                // }
                //
                // else
                // {
                //   $( '.modal-btn-save' ).removeAttr('disabled');
                //   $( '.add-part-type' ).removeAttr( 'disabled' );
                // }
              }

              else
              {
                $( '#modal-append-row' ).clone().appendTo( '#part-type-table' );
              }
            },

            error: function ( response )
            {
              console.log( response );
            }
          });

        },

        error: function ( response )
        {
          console.log( response );
        }
      });

      // get remark from kit master
      var dataForm = {
        _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
        kit_master_id: kit_master_id
      };

      $.ajax({
        type: 'GET',
        url: '/pre-cleaning/get-kit-master-remarks',
        data: dataForm,
        dataType: 'json',
        context: this,
        success: function( response )
        {
          if(response.data != '')
          {
            $( '.modal-remarks' ).val( response.data );
          }
        },

        error: function ( response )
        {
          console.log( response );
        }
      });

      // clear first form input fields for part type table
      $( '#part-type-table' ).find( '.modal-part-type-id' ).val( '' );
      $( '#part-type-table' ).find( '.serial-no' ).val( '' );
      $( '#part-type-table' ).find( '.visual-condition' ).val( '' );
      $( '#part-type-table' ).find( '.arf' ).val( '' );

      $( '.modal-kit-no' ).val( kit_master );
      $( '.modal-kit-master-id' ).val( kit_master_id );
      $( '.modal-set-id' ).val( set_id );
      $( '.modal-tool-kit' ).val( tool_kit );


      // if rf is greater than or equal 100, assign all rf values in part type table
      if( rf > 100 )
      {
        $( '.part-type-rf' ).val( rf );
      }

      else
      {
        $( '.part-type-rf' ).val( '' );
      }

      $( '#modal-dialog-kit-type' ).modal();
    }
  });

  $( '.modal-btn-save' ).click(function( e ) {

    e.preventDefault();

    var count = 0;
    var errors = new Array();
    var validationFailed = false;
    var kit_master_id = $( this ).closest( '.modal-footer' ).parent().find( '.modal-kit-master-id' ).val();
    var set_id = $( this ).closest( '.modal-footer' ).parent().find( '.modal-set-id' ).val();

    var various_conditions = [];
    var part_type_id = [];
    var good_part_detail_ids = [];
    var serial_no = [];
    var part_type_rf = [];
    var arf = [];
    var actions = [];
    var unique_part_type_id = [];
    var unique_serial_no = [];
    var unique_actions = [];

    var str = "";

    $( '#part-type-table .modal-hidden-part-type-id' ).each(function() {

      if ( !$.trim($(this).val()).length )
      {
        validationFailed = true;
        errors[count++] = "Part Types are empty.";
        return false;
      }

      else
      {
        part_type_id.push( $(this).val() );
      }
    });

    $( "#part-type-table .remove-row" ).each(function() {

      good_part_detail_ids.push( $(this).attr( 'data-id' ) );
    });


    $( "#part-type-table .serial-no" ).each(function() {

      if ( !$.trim($(this).val()).length )
      {
        validationFailed = true;
        errors[count++] = "Serial No are empty.";
        return false;
      }

      else
      {
        serial_no.push( $(this).val() );
      }
    });

    $( '#part-type-table .visual-condition' ).each(function() {

      // if ( !$.trim($(this).val()).length )
      // {
      //   validationFailed = true;
      //   errors[count++] = "Visual Conditions are empty.";
      //   return false;
      // }
      //
      // else
      // {
        var vc = $( this ).val();
        vc = vc.toString();
        str = vc.replace(",", "/");

        various_conditions.push( str );
      // }
    });

    $( '#part-type-table .part-type-rf' ).each(function() {

      if ( !$.trim($(this).val()).length )
      {
        validationFailed = true;
        errors[count++] = "RF are empty.";
        return false;
      }

      else
      {
        part_type_rf.push( $(this).val() );
      }
    });

    $( '#part-type-table .arf' ).each(function() {

      if ( !$.trim($(this).val()).length )
      {
        validationFailed = true;
        errors[count++] = "ARF are empty.";
        return false;
      }

      else
      {
        arf.push( $(this).val() );
      }
    });

    $( '#part-type-table .actions' ).each(function() {

      var $optText = $(this).find( 'option:selected' );

      if ( $optText.val() == '' )
      {
        validationFailed = true;
        errors[count++] = "Actions are empty.";
        return false;
      }

      else
      {
        actions.push( $optText.val() );
      }
    });

    $.each(part_type_id, function (i, data) {
      if( $.inArray(data, unique_part_type_id) === -1 )
      {
        unique_part_type_id.push(data);
      }
    });

    $.each(serial_no, function (i, data) {
      if( $.inArray(data, unique_serial_no) === -1 )
      {
        unique_serial_no.push(data);
      }
    });

    $.each(actions, function (i, data) {
      if( data == 2 )
      {
        unique_actions.push(data);
      }
    });

    var result_cal = parseFloat(serial_no.length) - parseFloat(unique_serial_no.length);

    // check duplicate data based on part type and serial no.
    // if( (unique_part_type_id.length != part_type_id.length) && (unique_serial_no.length != serial_no.length)
    //     && (result_cal != unique_actions.length) )
    if( (unique_part_type_id.length != part_type_id.length) && (unique_serial_no.length != serial_no.length) )
    {
      validationFailed = true;
      errors[count++] = "Duplicate part type and serial no.";
    }

    if ( validationFailed )
    {
      var errorMsgs = '';

      for(var i = 0; i < count; i++)
      {
        errorMsgs = errorMsgs + errors[i] + "<br/>";
      }

      $( '#modalId' ).scrollTop( 0 );

      $( '.check-part-type-fields' ).show();
      $( '.check-part-type-fields' ).html( errorMsgs );

      return false;
    }

    else
    {
      var formData = {
        _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
        good_kit_detail_id:	$( '.modal-kit-type-id' ).val(),
        part_type_id: part_type_id,
        good_part_detail_ids: good_part_detail_ids,
        serial_no: serial_no,
        various_conditions: various_conditions,
        part_type_rf: part_type_rf,
        arf: arf,
        actions: actions,
        kit_master_id: kit_master_id,
        set_id: set_id
      };

      $.ajax({
        type: 'POST',
        url: '/process/pre-cleaning/post-part-type-precleaning',
        data: formData,
        dataType: 'json',
        success: function( response )
        {
          $( 'form#part-type-form' ).removeAttr( 'formmodified' );

          if(response.final)
          {
            alert( 'Good Part Details are successfully created.' );

            $('#modal-dialog-kit-type').modal('hide');
          }

          else
          {
            alert( 'Unable to save as there is outstanding transaction has not been saved in other GRN!' );
          }
        },

        error: function ( response )
        {
          console.log( response );
        }
      });

      $( '.check-part-type-fields' ).hide();
      $( '.check-part-type-fields' ).empty();
    }
  });

  // append row in part type table
  $( '.add-part-type' ).click(function() {
    $( '#modal-append-row' ).clone().appendTo( '#part-type-table' );
    $( "#part-type-form tr:last" ).find( '.part-type-rf' ).val( 0 );
    $( '#part-type-table tr:last' ).removeAttr( 'id' );
  });

  // clear rows when part type modal dialog is closed
  $( '.modal-btn-close' ).click(function() {

     // var dis_attr = $( this ).parent().find( '.modal-btn-save' ).attr( 'disabled' );

     if( $( 'form#part-type-form' ).attr( 'formmodified' ) == 1 )
     {
       if( confirm("Data will not be saved, do you still want to proceed?\nClick OK to proceed!") )
       {
         // post_part_param_master();
         $( 'form#part-type-form' ).removeAttr( 'formmodified' );
       }

       else
       {
         return false;
       }
     }

     // if( !dis_attr )
     // {
     //   alert('Please save before you close');
     //   return false;
     // }

     $( '#part-type-table' ).find( 'tr:gt(0)' ).remove();
  });

  // part type modal dialog close event
  $( '#modal-dialog-kit-type' ).on('hide.bs.modal', function () {
    $( '#part-type-table' ).find( 'tr:gt(0)' ).remove();
  });

  // $( '#kit-table' ).on('change', '.kit-master-id', function () {
  //
  //   if( $(this).val() != '' )
  //   {
  //     // autocomplete get all serial no
  //     var serial_no = [];
  //     var location = $( this ).closest( 'td' ).parent().find( '#serial-no' );
  //
  //     var formData = {
  //       _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
  //       // kit_master_id:	kit_master_id
  //     }
  //
  //     $.ajax({
  //       type: 'GET',
  //       url: '/pre-cleaning/get-part-master-all-serial-no',
  //       data: formData,
  //       dataType: 'json',
  //       success: function( response )
  //       {
  //         $.each(response.data, function( index, data ) {
  //           serial_no.push( data.serial_no );
  //         });
  //
  //         var options = {
  //           data: serial_no,
  //           list: {
  //               		match: {
  //               			enabled: true
  //               		}
  //               	}
  //         };
  //
  //         location.easyAutocomplete(options);
  //
  //       },
  //
  //       error: function ( response )
  //       {
  //         console.log( response );
  //       }
  //     });
  //   }
  //
  // });
  //
  // $( '#kit-table' ).on('change', '.kit-master-id', function () {
  //
  //   if( $(this).val() != '' )
  //   {
  //     // autocomplete get all set id
  //     var  set_id = [];
  //     var location = $( this ).closest( 'td' ).parent().find( '#set-id' );
  //
  //     var formData = {
  //       _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
  //       // kit_master_id:	kit_master_id
  //     }
  //
  //     $.ajax({
  //       type: 'GET',
  //       url: '/pre-cleaning/get-part-master-all-set-id',
  //       data: formData,
  //       dataType: 'json',
  //       success: function( response )
  //       {console.log(response);
  //         $.each(response.data, function( index, data ) {
  //           set_id.push( data.set_id );
  //         });
  //
  //         var options = {
  //           data: set_id
  //         };
  //
  //         location.easyAutocomplete(options);
  //       },
  //
  //       error: function ( response )
  //       {
  //         console.log( response );
  //       }
  //     });
  //   }
  // });

  // get previous values of rf
  $( '#part-type-table' ).on('focus', '.part-type-rf', function () {
    $( this ).attr( 'oldValue', $( this ).val() );
  });

  $( '#part-type-table' ).on('focusout', '.part-type-rf', function () {

    var oldValue = $( this ).attr( 'oldValue' );

    if($( this ).val() == '')
    {
      $( this ).val( oldValue );
    }

  });

  $( '#part-type-table' ).on('change', '.part-type-rf', function () {

    var oldValue = $( this ).attr( 'oldValue' );
    var before_arf = $( this ).closest( 'td' ).parent().find( '.arf' ).val();
    var after_arf = parseFloat(before_arf) - parseFloat(oldValue);
    var rf = $( this ).val();

    if($( this ).val() == '')
    {
      var rf = oldValue;
    }

    var max_rf_hrs = $.trim( $( this ).closest( 'tr' ).find( '.max-rf-hrs' ).val() );
    $( this ).closest( 'td' ).parent().find( '.arf' ).val( parseFloat(rf) + parseFloat(after_arf) );
    var arf = $.trim( $( this ).closest( 'tr' ).find( '.arf' ).val() );

    // to highlight red if arf > max rf hours
    if( max_rf_hrs != '' )
    {
      if( parseFloat( arf ) > parseFloat( max_rf_hrs ) )
      {
        $( this ).closest( 'tr' ).find( '.arf' ).css( 'background-color', 'red' );
      }

      else
      {
        $( this ).closest( 'tr' ).find( '.arf' ).css( 'background-color', '' );
      }
    }

  });

  $( '#part-type-table' ).on('change', '.actions', function () {

    var location = $( this );
    var kit_master_id = $( this ).closest( '.modal-body' ).find( '.modal-kit-master-id' ).val();
    var part_type_id = $( this ).closest( 'tr' ).find( '.modal-part-type-id' ).val();
    var serial_no = $( this ).closest( 'tr' ).find( '.serial-no' ).val();

    if( kit_master_id != '' && part_type_id != '' && serial_no != '' && location.val() != '' )
    {
      var formData = {
        _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
        kit_master_id:	kit_master_id,
        part_type_id:	part_type_id,
        serial_no: serial_no,
        action: location.val()
      }

      $.ajax({
        type: 'POST',
        url: '/pre-cleaning/post-action-part-master',
        data: formData,
        dataType: 'json',
        success: function( response )
        {
          if( response.data && location.val() == 2 )
          {
            location.closest( 'td' ).parent().find( '.serial-no' ).attr( 'disabled', 'disabled' );
            location.closest( 'td' ).parent().find( '.visual-condition' ).attr( 'disabled', 'disabled' );
            location.closest( 'td' ).parent().find( '.part-type-rf' ).attr( 'disabled', 'disabled' );
          }

          else
          {
            location.closest( 'td' ).parent().find( '.serial-no' ).removeAttr( 'disabled' );
            location.closest( 'td' ).parent().find( '.visual-condition' ).removeAttr( 'disabled' );
            location.closest( 'td' ).parent().find( '.part-type-rf' ).removeAttr( 'disabled' );
          }

          alert( 'Update status of part master successfully!');

        },

        error: function ( response )
        {
          console.log( response );
        }
      });
    }

    // var data_id = $( this ).closest( 'td' ).parent().find( '.remove-row' ).attr( 'data-id' );
    // var part_type_id = $( this ).closest( 'td' ).parent().find( '.modal-part-type-id' ).val();
    // var hidden_part_type_id = $( this ).closest( 'td' ).parent().find( '.modal-hidden-part-type-id' ).val();
    // var seria_no = $( this ).closest( 'td' ).parent().find( '.serial-no' ).val();
    // var visual_condition = $( this ).closest( 'td' ).parent().find( '.visual-condition' ).val();
    // var max_rf_hrs = $( this ).closest( 'td' ).parent().find( '.max-rf-hrs' ).val();
    // var rf = $( this ).closest( 'td' ).parent().find( '.part-type-rf' ).val();
    // var arf = $( this ).closest( 'td' ).parent().find( '.arf' ).val();
    // var max_rf_hrs = $( this ).closest( 'td' ).parent().find( '.max-rf-hrs' ).val();
    //
    // if( $( this ).val() == 2 && (parseFloat(arf) >= parseFloat(max_rf_hrs)) )
    // {
    //   $( this ).attr( 'disabled', 'disabled' );
    //   $( this ).closest( 'td' ).parent().find( '.part-type-rf' ).attr( 'disabled', 'disabled' );
    //   $( '#modal-append-row' ).clone().appendTo( '#part-type-table' );
    //
    //   $( '#modal-append-row' ).find( '.remove-row' ).attr( 'temp-data-id', data_id );
    //   $( '#modal-append-row' ).find( '.modal-part-type-id' ).val( part_type_id ).attr( 'disabled', 'disabled' );
    //   $( '#modal-append-row' ).find( '.modal-hidden-part-type-id' ).val( hidden_part_type_id );
    //   $( '#modal-append-row' ).find( '.serial-no' ).val( seria_no ).attr( 'disabled', 'disabled' );
    //   $( '#modal-append-row' ).find( '.visual-condition' ).val( visual_condition );
    //   $( '#modal-append-row' ).find( '.max-rf-hrs' ).val( max_rf_hrs );
    //   $( '#modal-append-row' ).find( '.part-type-rf' ).val( 0 );
    //   $( '#modal-append-row' ).find( '.arf' ).val( 0 );
    //   $( '#modal-append-row' ).find( '.actions' ).val( 1 );
    //
    //   $( '#part-type-table tr:last' ).removeAttr( 'id' );
    // }

  });

  // hide kit information when there is no header
  if( $( "#grn_id" ).val() != '' )
  {
    $( ".subSection" ).show();
  }

  else
  {
    $( ".subSection" ).hide();
  }

  $( '.btn-save' ).click( function(e) {

    e.preventDefault();

    // validate header
    if( $( '#grn_no' ).val() == '' )
    {
      alert( "GRN No's field is required" );
      return false;
    }

    if( $( '#customer_id' ).val() == '' )
    {
      alert( "Customer's field is required" );
      return false;
    }

    if( $( '#customer_name' ).val() == '' )
    {
      alert( "Customer Name's field is required" );
      return false;
    }

    if( $( '#job_id' ).val() == '' )
    {
      alert( "Job ID's field is required" );
      return false;
    }

    if( $( '#collection_date' ).val() == '' )
    {
      alert( "Collection Date's field is required" );
      return false;
    }

    //validate footer
    var validationFailed = false;

    var kit_master_id = [];
    var reason_id = [];
    var tool_kit = [];
    var set_id = [];
    var serial_no = [];
    var quantity = [];
    var rf = [];
    var hidden_dialog_arrow = [];
    var unique_tool_kit = [];
    var unique_kit_master_id = [];
    var unique_set_id = [];
    var unique_serial_no = [];

    $( "#kit-table .kit-master-id" ).each(function() {

      if( !$.trim($(this).val()).length )
      {
        validationFailed = true;
        $( this ).closest( 'tr' ).find( '.kit-master-id' ).parent().addClass( 'has-error' );
        return false;
      }

      else
      {
        $( this ).closest( 'tr' ).find( '.kit-master-id' ).parent().removeClass( 'has-error' );
        kit_master_id.push( $(this).val() );
      }

    });

    $( "#kit-table .reason-id" ).each(function() {

      if( !$.trim($(this).val()).length )
      {
        validationFailed = true;
        $( this ).closest( 'tr' ).find( '.reason-id' ).parent().addClass( 'has-error' );
        return false;
      }

      else
      {
        $( this ).closest( 'tr' ).find( '.reason-id' ).parent().removeClass( 'has-error' );
        reason_id.push( $(this).val() );
      }

    });

    $( "#kit-table .tool-kit" ).each(function() {

      if( !$.trim($(this).val()).length )
      {
        validationFailed = true;
        $( this ).closest( 'tr' ).find( '.tool-kit' ).parent().addClass( 'has-error' );
        return false;
      }

      else
      {
        $( this ).closest( 'tr' ).find( '.tool-kit' ).parent().removeClass( 'has-error' );
        tool_kit.push( $(this).val() );
      }

    });

    $( "#kit-table .set-id" ).each(function() {

      var set_id_class = $( this ).closest( 'tr' ).find( '.set-id' ).parent();

      if( !$.trim($(this).val()).length && set_id_class.hasClass( 'required-field' ) )
      {
        validationFailed = true;
        $( this ).closest( 'tr' ).find( '.set-id' ).parent().addClass( 'has-error' );
        return false;
      }

      else
      {
        $( this ).closest( 'tr' ).find( '.set-id' ).parent().removeClass( 'has-error' );
        set_id.push( $(this).val() );
      }

    });

    $( "#kit-table .serial-no" ).each(function() {

      var serial_no_class = $( this ).closest( 'tr' ).find( '.serial-no' ).parent();

      if( !$.trim($(this).val()).length && serial_no_class.hasClass( 'required-field' ) )
      {
        validationFailed = true;
        $( this ).closest( 'tr' ).find( '.serial-no' ).parent().addClass( 'has-error' );
        return false;
      }

      else
      {
        $( this ).closest( 'tr' ).find( '.serial-no' ).parent().removeClass( 'has-error' );
        serial_no.push( $(this).val() );
      }

    });

    $( "#kit-table .quantity" ).each(function() {

      var quantity_class = $( this ).closest( 'tr' ).find( '.quantity' ).parent();

      if( !$.trim($(this).val()).length && quantity_class.hasClass( 'required-field' ) )
      {
        validationFailed = true;
        $( this ).closest( 'tr' ).find( '.quantity' ).parent().addClass( 'has-error' );
        return false;
      }

      else
      {
        $( this ).closest( 'tr' ).find( '.quantity' ).parent().removeClass( 'has-error' );
        quantity.push( $(this).val() );
      }

    });

    $( "#kit-table .rf" ).each(function() {

      var rf_class = $( this ).closest( 'tr' ).find( '.rf' ).parent();

      if( !$.trim($(this).val()).length && rf_class.hasClass( 'required-field' ) )
      {
        validationFailed = true;
        $( this ).closest( 'tr' ).find( '.rf' ).parent().addClass( 'has-error' );
        return false;
      }

      else
      {
        $( this ).closest( 'tr' ).find( '.rf' ).parent().removeClass( 'has-error' );
        rf.push( $(this).val() );
      }

    });

    $( "#kit-table .hidden_dialog-arrow" ).each(function() {
        hidden_dialog_arrow.push( $(this).val() );
    });

    $.each(tool_kit, function (i, data) {
      if( $.inArray(data, unique_tool_kit) === -1 )
      {
        unique_tool_kit.push(data);
      }
    });

    $.each(kit_master_id, function (i, data) {
      if( $.inArray(data, unique_kit_master_id) === -1 )
      {
        unique_kit_master_id.push(data);
      }
    });

    $.each(set_id, function (i, data) {
      if( $.inArray(data, unique_set_id) === -1 )
      {
        unique_set_id.push(data);
      }
    });

    $.each(serial_no, function (i, data) {
      if( $.inArray(data, unique_serial_no) === -1 )
      {
        unique_serial_no.push(data);
      }
    });

    // check duplicate data based on part type and serial no.
    // if( (unique_part_type_id.length != part_type_id.length) && (unique_serial_no.length != serial_no.length)
    //     && (result_cal != unique_actions.length) )
    // if( (tool_kit.length != unique_tool_kit.length) )
    // {
    //   validationFailed = true;
    //   alert('Duplicate Tool ID found!');
    //   return false;
    // }

    // if( (kit_master_id.length != unique_kit_master_id.length) && (set_id.length != unique_set_id.length) &&
    // (serial_no.length != unique_serial_no.length) )
    // {
    //   validationFailed = true;
    //   alert('Duplicate Kit, Set ID and Serial No. found!');
    //   return false;
    // }

    if ( validationFailed == false )
    {
      // save header
      var dataForm = {
        _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
        grn_id: $( '#grn_id' ).val(),
        collection_date: $( '#collection_date' ).val(),
        job_id: $( '#job_id' ).val(),
        job_status: $("input[name='job_status']:checked").val(),
        narration: $( '#narration' ).val()
      };

      $.ajax({
        type: 'POST',
        url: '/pre-cleaning/post-header-good-receipt-header',
        data: dataForm,
        dataType: 'json',
        context: this,
        success: function( response )
        {
          if( response.data == 'success' )
          {
            if( response.status == 'closed' )
            {
              $( '.add-kit' ).hide();
              $( '.btn-save' ).hide();
            }

            if( hidden_dialog_arrow.length != 0 )
            {
              // save footer
              var dataForm = {
                _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
                good_kit_detail_id: hidden_dialog_arrow,
                grn_id: $( '#grn_id' ).val(),
                reason_id: reason_id,
                kit_master_id: kit_master_id,
                tool_kit: tool_kit,
                set_id: set_id,
                serial_no: serial_no,
                quantity: quantity,
                rf: rf
              };

              $.ajax({
                type: 'POST',
                url: '/pre-cleaning/post-good-kit-detail-multiple',
                data: dataForm,
                dataType: 'json',
                context: this,
                success: function( response )
                {
                  $.each(response.good_kit_detail_id, function( index, data ) {

                    $( '#kit-table' ).find( '.remove-row:eq(' + index + ')' ).attr( 'data-id', data);
                    $( '#kit-table' ).find( '.right-arrow:eq(' + index + ')' ).children( "a" ).attr( 'data-kit-type-id', data);
                    $( '#kit-table' ).find( '.hidden_dialog-arrow:eq(' + index + ')' ).val(data);

                  });

                  // $( '.btn-cancel' ).removeAttr( 'formheadermodified' );

                  alert('Save information successfully.');
                },

                error: function ( response )
                {
                  console.log( response );
                }
              });
            }

            else
            {
              // $( '.btn-cancel' ).removeAttr( 'formheadermodified' );

              alert('Save Pre Cleaning successfully.');
            }
          }
        },

        error: function ( response )
        {
          console.log( response );
        }
      });
    }

  });

  $( '.btn-cancel' ).click( function(e) {

    if( $( this ).attr( 'formheadermodified' ) == 1 )
    {
      if( confirm("Data will not be saved, do you still want to proceed?\nClick OK to proceed!") )
      {

      }

      else
      {
        return false;
      }
    }
  });

  // $( '#kit-table .remove-row' ).each(function() {
  //
  //   var location = $( this );
  //
  //   if( $( this ).attr( 'data-id' ) != '' )
  //   {
  //     var formData = {
  //       _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
  //       good_kit_detail_id: $( this ).attr( 'data-id' )
  //     };
  //
  //     $.ajax({
  //       type: 'GET',
  //       url: '/get-good-part-row',
  //       data: formData,
  //       dataType: 'json',
  //       success: function( response )
  //       {
  //         if( response.data > 0 )
  //         {
  //           location.remove();
  //         }
  //       },
  //
  //       error: function ( response )
  //       {
  //         console.log( response );
  //       }
  //     });
  //   }
  // });
});

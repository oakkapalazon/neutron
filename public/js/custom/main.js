$(function() {

  // get part type Lists
  $( '#kit_master_id' ).change(function() {

    var kit_master_id = $( '#kit_master_id' ).val();

    if(kit_master_id != '')
    {
      var formData = {
        _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
        kit_master_id: kit_master_id
      };

      $.ajax({
        type: 'GET',
        url: '/get-part-type-lists',
        data: formData,
        dataType: 'json',
        success: function(response)
        {
          $( '#part_type_id' ).find( 'option' ).not( ':first' ).remove();
          $( '#parameter_master_id' ).find( 'option' ).not( ':first' ).remove();

          $.each(response.data, function(index, data) {
            $( '#part_type_id' ).append( '<option value="' + data.id + '">' + data.part_type + '</option>' );
          });
        },

        error: function (response) {
          console.log(response);
        }
      });
    }

    else
    {
      $( '#part_type_id' ).find( 'option' ).not( ':first' ).remove();
      // $('#part_type_id').trigger('change');
      $( '#parameter_master_id' ).find( 'option' ).not( ':first' ).remove();
    }
  });

  $( '#part_type_id' ).change(function() {

    var part_type_id = $( '#part_type_id' ).val();

    if(part_type_id != '')
    {
      var formData = {
        _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
        kit_master_id: $( '#kit_master_id' ).val(),
        part_type_id: part_type_id
      };

      $.ajax({
        type: 'GET',
        url: '/get-parameter-lists',
        data: formData,
        dataType: 'json',
        success: function(response)
        {
          $( '#parameter_master_id' ).find( 'option' ).not( ':first' ).remove();

          $.each(response.data, function(index, data) {
            $( '#parameter_master_id' ).append( '<option value="' + data.id + '">' + data.param + '</option>' );
          });
        },

        error: function (response) {
          console.log(response);
        }
      });
    }

    else
    {
      $( '#parameter_master_id' ).find( 'option' ).not( ':first' ).remove();
    }
  });
});

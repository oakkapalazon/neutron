$(function() {

  // append row in customers table
  $( '.add-btn' ).click(function() {

    var cust_row = $( '#customers-table tbody tr' ).length;

    $( '#append-row > .sr_no' ).text( cust_row + 1 );
    $( '#append-row' ).clone().appendTo( '#customers-table' );
    $( '#customers-table tr:last' ).removeAttr( 'id' );
    $( '#customers-table tr:last td:nth-child(2)' ).removeClass( 'sr_no' );

    $( '#customers-table tr:last' ).find( ".effective" ).datepicker({
      format: 'dd/mm/yyyy',
    });
  });

  // remove row in kit table
  $( '#customers-table' ).on('click', '.remove-row', function() {

    if (!confirm( 'Do you confirm you want to delete this record?' ))
    {
      return false;
    }

    else
    {
      if( $(this).attr('data-id') != '' )
      {
        var formData = {
          _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
          id: $( this ).attr('data-id')
        };

        $.ajax({
          type: 'GET',
          url: '/get-delete-purchase-order',
          data: formData,
          dataType: 'json',
          success: function(response)
          {
            alert('Purchase order is deleted.');
          },

          error: function (response)
          {
            console.log(response);
          }
        });
      }

      $( this ).parent().parent().remove();
    }
  });

  $( '.btn-save' ).click( function(e) {

    e.preventDefault();

    // validate header
    if( $( '#company_id' ).val() == '' )
    {
      alert( "Company's field is required" );
      return false;
    }

    if( $( '#customer_code' ).val() == '' )
    {
      alert( "Customer's field is required" );
      return false;
    }

    if( $( '#folder_name' ).val() == '' )
    {
      alert( "Folder name's field is required" );
      return false;
    }

    if( $( '#customer_address' ).val() == '' )
    {
      alert( "Customer address's field is required" );
      return false;
    }

    if( $( '#customer_name' ).val() == '' )
    {
      alert( "Customer name's field is required" );
      return false;
    }

    // validate footer
    var validationFailed = false;

    var po_no = [];
    var effective = [];

    $( "#customers-table .po_no" ).each(function() {

      if( !$.trim($(this).val()).length )
      {
        validationFailed = true;
        $( this ).closest( 'tr' ).find( '.po_no' ).parent().addClass( 'has-error' );
      }

      else
      {
        $( this ).closest( 'tr' ).find( '.po_no' ).parent().removeClass( 'has-error' );
        po_no.push( $(this).val() );
      }

    });

    $( "#customers-table .effective" ).each(function() {

      if( !$.trim($(this).val()).length )
      {
        validationFailed = true;
        $( this ).closest( 'tr' ).find( '.effective' ).parent().addClass( 'has-error' );
      }

      else
      {
        $( this ).closest( 'tr' ).find( '.effective' ).parent().removeClass( 'has-error' );
        effective.push( $(this).val() );
      }

    });

    if ( validationFailed == false )
    {
      // save header
      var dataForm = {
        _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
        customer_id: $( '#customer_id' ).val(),
        company_id: $( '#company_id' ).val(),
        vendor_code: $( '#vendor_code' ).val(),
        purchaser: $( '#purchaser' ).val(),
        customer_address: $( '#customer_address' ).val(),
        delivery_contact: $( '#delivery_contact' ).val(),
        delivery_address: $( '#delivery_address' ).val()
      };

      $.ajax({
        type: 'POST',
        url: '/post-header-customers',
        data: dataForm,
        dataType: 'json',
        context: this,
        success: function( response )
        {
          if( response.data == 'success' )
          {
            //save footer
            var dataForm = {
              _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
              cust_id: $( ".cust_id" ).val(),
              po_no: po_no,
              effective: effective
            };

            $.ajax({
              type: 'POST',
              url: '/post-customer-purchase-order',
              data: dataForm,
              dataType: 'json',
              context: this,
              success: function( response )
              {
                if( response.data == 'success' )
                {
                  $.each(response.result, function( index, data ) {

                    $( '#customers-table' ).find( '.remove-row:eq(' + index + ')' ).attr( 'data-id', data);
                    $( '#customers-table' ).find( '.po_no' ).attr( 'disabled', 'disabled');
                    $( '#customers-table' ).find( '.effective' ).attr( 'disabled', 'disabled');

                  });

                  alert('Save information successfully.');
                }

                else
                {
                  console.log( response );
                }
              },

              error: function ( response )
              {
                console.log( response );
              }
            });
          }

          else
          {
            alert('Save customer info successfully.');
          }
        },

        error: function ( response )
        {
          console.log( response );
        }
      });
    }

  });
});

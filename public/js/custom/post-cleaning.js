
$(function() {

  $( '.list-group' ).on('click', '.part-type', function(e) {

    $( '#tree1' ).find( 'a' ).removeClass( 'active' );
    $( this ).addClass( "active" );

    $( '#tree1' ).find( 'li' ).removeClass( 'hover' );
    $( this ).closest( 'li' ).addClass( "hover" );

    e.preventDefault();

    var part_type_id = $( this ).attr( 'data-id' );
    var good_kit_detail_id = $( this ).attr( 'data-good-kit-detail-id' );
    var good_part_detail_id = $( this ).attr( 'data-good-part-detail-id' );
    var id_array = [];

    var dataForm = {
      _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
      good_kit_detail_id: good_kit_detail_id,
      part_type_id: part_type_id,
      good_part_detail_id: good_part_detail_id
    };

    $.ajax({
      type: 'GET',
      url: '/post-cleaning/get-grn-detail-by-part-type',
      data: dataForm,
      dataType: 'json',
      context: this,
      success: function( response )
      {
        console.log(response);
        $( "#grn-table tbody" ).empty();

        //make array value to string
        $.each(response.parameter_master_id_array, function( index, id ) {
          id_array.push( id.toString() );
        });

        $.each(response.group, function( index, group ) {

          //make value to string
          var parameter_master_id = group.parameter_master_id.toString();

          //check exclude post cleaning feature
          if( $.inArray( parameter_master_id, id_array) != '-1' )
          {
            var row = '';

            if( group.avg != null )
            {
              row += '<tr class="group"><td colspan="3">' + group.param + '</td><td>Avg :</td><td>' + group.avg.toFixed(3) + '</td></tr>';
            }
            // no avg value as it is boolean
            else
            {
              row += '<tr class="group"><td colspan="3">' + group.param + '</td><td></td><td></td></tr>';
            }


            $.each(response.data, function( index, data ) {

              if(group.param == data.param)
              {
                // var row = '';

                row += '<tr>';
                // row += '<td><input type="" class="good_param_datapoint_detail_id" value="' + data.good_param_datapoint_detail_id + '" name="good_param_datapoint_detail_id[]" />' + data.grn_no + '</td>';
                row += '<td><input type="hidden" class="good_param_datapoint_detail_id" value="' + data.good_param_datapoint_detail_id + '" name="good_param_datapoint_detail_id[]" />' + ( data.set_id != null ? data.set_id : '' ) + '</td>';
                row += '<td>' + data.serial_no + '</td>';
                row += '<td>' + data.datapoint + '</td>';
                // row += '<td>' + data.param + '</td>';
                row += '<td>' + data.arf + '</td>';

                if(data.is_boolean == 1)
                {
                  if(data.param_datapoint_value == 'Pass')
                  {
                    row += "<td><select class='form-control value' name='value[]'><option value='Pass' selected>Pass</option><option value='Fail'>Fail</option></select></td>";
                  }

                  else if(data.param_datapoint_value == 'Fail')
                  {
                    row += "<td><select class='form-control value' name='value[]'><option value='Pass'>Pass</option><option value='Fail' selected>Fail</option></select></td>";
                  }

                  else
                  {
                    row += "<td><select class='form-control value' name='value[]'><option value='Pass'>Pass</option><option value='Fail'>Fail</option></select></td>";
                  }
                }

                else
                {
                  if(data.param_datapoint_value != "")
                  {
                    row += "<td><input type='number' class='form-control value' min='0' name='value[]' value='" + data.param_datapoint_value + "'></td>";
                  }

                  else
                  {
                    row += "<td><input type='number' class='form-control value' min='0' name='value[]'></td>";
                  }
                }

                //get hidden input id
                // row += '<td style="display: none;"><input type="hidden" name="hidden_good_part_detail_id" value="' + data.good_part_detail_id + '"</td>';
                // row += '<td style="display: none;"><input type="hidden" name="hidden_kit_master_id" value="' + data.kit_master_id + '"</td>';
                // row += '<td style="display: none;"><input type="hidden" name="hidden_part_type_id" value="' + data.part_type_id + '"</td>';
                // row += '<td style="display: none;"><input type="hidden" name="hidden_parameter_master_id[]" class="hidden_parameter_master_id" value="' + data.parameter_master_id + '"</td>';

                row += "</tr>";
              }
            });

            row += "<tr style='display: none;'><td><input type='hidden' value=" + good_kit_detail_id +
            " name='good_kit_detail_id' /></td><td><input type='hidden' value=" + part_type_id +
            " name='part_type_id' /></td><td><input type='hidden' value=" + good_part_detail_id +
            " name='good_part_detail_id' /></td></tr>";

            $( '#grn-table tbody' ).append( row );
          }
        });
      },

      error: function ( response )
      {
        console.log( response );
      }
    });
  });

  $( '.btn-save' ).click(function(e) {

    e.preventDefault();

    var count = 0;
    var errors = new Array();
    var validationFailed = false;

    var good_param_datapoint_detail_id = [];
    var value = [];

    var good_kit_detail_id = $( 'input[type=hidden][name=good_kit_detail_id' ).val();
    var good_part_detail_id = $( 'input[type=hidden][name=good_part_detail_id' ).val();
    var part_type_id = $( 'input[type=hidden][name=part_type_id]' ).val();
    // var parameter_master_id = [];

    $( '#grn-table .good_param_datapoint_detail_id' ).each(function() {

      good_param_datapoint_detail_id.push( $(this).val() );

    });

    // $( '#grn-table .hidden_parameter_master_id' ).each(function() {
    //
    //   parameter_master_id.push( $(this).val() );
    //
    // });

    $( '#grn-table .value' ).each(function() {

      if( $( this ).attr( 'type' ) == 'number' )
      {
        if (!$.trim($(this).val()).length)
        {
          validationFailed = true;
          errors[count++] = "Values are empty.";
          return false;
        }

        else
        {
          value.push( $( this ).val() );
        }
      }

      else
      {
        var $optText = $( this ).find( 'option:selected' );

        if ( $optText.val() == '' )
        {
          validationFailed = true;
          errors[count++] = "Values are empty.";
          return false;
        }

        else
        {
          value.push( $optText.val() );
        }
      }
    });

    if ( validationFailed )
    {
      var errorMsgs = '';

      for(var i = 0; i < count; i++)
      {
        errorMsgs = errorMsgs + errors[i] + "<br/>";
      }

      $('html,body').animate({ scrollTop: 0 }, 'slow');

      $( '.check-grn-fields' ).show();
      $( '.check-grn-fields' ).html( errorMsgs );

      return false;
    }

    else
    {
      var formData = {
        _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
        good_param_datapoint_detail_id:	good_param_datapoint_detail_id,
        good_kit_detail_id: good_kit_detail_id,
        good_part_detail_id : good_part_detail_id,
        part_type_id : part_type_id,
        // parameter_master_id : parameter_master_id,
        value: value
      };

      $.ajax({
        type: 'POST',
        url: '/process/post-cleaning/post-grn-detail',
        data: formData,
        dataType: 'json',
        success: function( response )
        {
          $( '.active' ).closest( 'li' ).find( 'i' ).addClass( 'glyphicon glyphicon-ok' );
          alert( 'GRN Details are successfully created.' );
          // location.reload();

          var row = '';

          $.each(response.test, function( index, data ) {

            if( data || data === 0 )
            {
              $( '#grn-table tbody tr.group' ).eq( index ).children( 'td' ).eq( 1 ).remove();
              $( '#grn-table tbody tr.group' ).eq( index ).children( 'td' ).eq( 1 ).remove();

              row += '<td>Avg :</td><td>' + data.toFixed(3) + '</td>';
              $( '#grn-table tbody tr.group' ).eq( index ).append( row );
              row = '';
            }
          });
        },

        error: function ( response )
        {
          console.log( response );
        }
      });

      $( '.check-grn-fields' ).hide();
      $( '.check-grn-fields' ).empty();
    }
  });
});

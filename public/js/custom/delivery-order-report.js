$(function() {

  $( '.customKitBox' ).hide();

  $( '.filter-delivery-order-box' ).on('change', '#customer_code', function () {

    var select_job = [];
    var select_grn = [];

    if( $( this ).val() != '' )
    {
      var formData = {
        _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
        cust_code:	$( this ).val()
      }

      $.ajax({
        type: 'GET',
        url: '/post-cleaning-report/get-grn-details',
        data: formData,
        dataType: 'json',
        success: function( response )
        {
          $( '#job_id' ).remove();
          $( '.job_id' ).append( '<select class="form-control" id="job_id" name="job_id"><option value="">Please select</option></select>' );

          // $( '#grn_no' ).remove();
          // $( '.grn_no' ).append( '<select class="form-control" id="grn_no" name="grn_no">' );

          if( response.data.length != 0 )
          {
            $.each(response.data, function( index, data ) {
              select_job.push( data.job_id );
              select_grn.push( data.grn_no );
            });

            $.each(select_job, function( index, data ) {
              $( '#job_id' ).append( '<option value="' + data + '">' + data + '</option>' );
              // $( '#grn_no' ).append( '<option value="' + select_grn[index] + '">' + select_grn[index] + '</option>' );
            });
          }

          // else
          // {
          //   $( '#job_id' ).append( '<option value="">Please select</option>' );
          //   $( '#grn_no' ).append( '<option value="">Please select</option>' );
          // }
        },

        error: function ( response )
        {
          console.log( response );
        }
      });
    }
  });

  $( '.filter-delivery-order-box' ).on('change', '#job_id', function () {

    var job_id = $( '#job_id' ).val();
    var customer_code = $( '#customer_code' ).val();

    if( job_id != '' )
    {
      var formData = {
        _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
        job_id: job_id,
        customer_code: customer_code
      }

      $.ajax({
        type: 'GET',
        url: '/delivery-order-report/get-do-details',
        data: formData,
        dataType: 'json',
        success: function( response )
        {
          $( '#do_no' ).remove();
          $( '.do_no' ).append( '<select class="form-control" id="do_no" name="do_no"><option value="">Please select</option></select>' );

          if( response.data.length != 0 )
          {
            // $.each(response.data, function( index, data ) {
            //   select_job.push( data.job_id );
            //   select_grn.push( data.grn_no );
            // });

            $.each(response.data, function( index, data ) {

              $( '#do_no' ).append( '<option value="' + data.id + '">' + data.do_no + '</option>' );
            });
          }
        },

        error: function ( response )
        {
          console.log( response );
        }
      });
    }
  });

  $( '.filter-delivery-order-box' ).on('change', 'form', function () {

    var customer_code = $( '#customer_code' ).val();
    var job_id = $( '#job_id' ).val();
    var do_no = $( '#do_no' ).val();

    if( customer_code != '' && job_id != '' && do_no != '' )
    {
      $( '.customKitBox' ).show();
    }

    else
    {
      $( '.customKitBox' ).hide();
    }
  });

});

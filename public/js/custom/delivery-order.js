$(function() {

  //get kit information based on good receipt header number
  var showed_child = $( "input[type=hidden][name=showed_child]" ).val();
  var delivery_order_id = $( "input[type=hidden][name=delivery_order_id]" ).val();
  var cust_code = $( "#cust_code" ).val();
  var grn_no = $( "#grn_no" ).val();
  var job_id = $( "#job_id" ).val();
  var date = $( "#date" ).val();
  var po_no = $( "#po_no" ).val();
  var status = $( "#status" ).val();

  if( cust_code != '' && grn_no != '' && job_id != '' && date != '' && po_no != '' && status != ''  )
  {
    $( ".main-delivery-order-table" ).show();
  }

  else
  {
    $( ".main-delivery-order-table" ).hide();
  }

  if(showed_child == 'yes')
  {
    var good_receipt_header_id = $("#grn_no_hidden").val();
    var do_child_kit = new Array;
    var do_child_tool = new Array;
    var do_child_serial = new Array;
    var do_child_qty = new Array;

    var dataForm = {
      _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
      good_receipt_header_id: good_receipt_header_id,
      delivery_order_id: delivery_order_id
    };

    $.ajax({
      type: 'GET',
      url: '/delivery-order/get-good-kit-detail',
      data: dataForm,
      dataType: 'json',
      context: this,
      success: function( response )
      {
        $( "#delivery-order-table tbody" ).empty();
        $( ".main-delivery-order-table" ).css('display', '');

        $.each(response.result_child, function( index, data ) {

          do_child_kit.push(data.kit_master_id);
          do_child_tool.push(data.tool_id);
          do_child_serial.push(data.serial_no);
          // do_child_qty.push(data.qty);
        });

        $.each(response.data, function( index, data ) {

          var row = '';

          row += '<tr>';
          row += '<td>' + data.kit_type + '</td>';
          row += '<td>' + data.reason_desc + '</td>';
          row += '<td>' + data.tool_kit + '</td>';
          row += '<td>' + data.set_id + '</td>';
          // row += '<td>' + data.serial_no + '</td>';
          // row += '<td>' + data.rf + '</td>';
          row += '<td>' + data.quantity + '</td>';
          row += '<td>' + parseInt( data.quantity - response.do_sum_qty[index] ) + '</td>';

          //check whether data exist in array
          if( do_child_kit.includes(data.kit_master_id) && do_child_tool.includes(data.tool_kit) &&
              do_child_serial.includes(data.serial_no) )
          {
            row += '<td><input type="checkbox" value="' + data.kit_master_id + '" name="do_child_checked[]" checked id="do_child_checked">' +
            '<input type="hidden" name="selected_tool_id[]" value="' + data.tool_kit + '" id="selected_tool_id">' +
            '<input type="hidden" name="selected_serial_no[]" value="' + data.serial_no + '" id="selected_serial_no">' +
            '<input type="number" name="selected_qty[]" value="' + response.do_qty[index] + '" id="selected_qty"></td>';
          }
          else
          {
            // there is no balance quantity
            if( parseInt( data.quantity - response.do_sum_qty[index] ) === 0 )
            {
              // disable checkbox
              row += '<td><input disabled type="checkbox" value="' + data.kit_master_id + '" name="do_child_checked[]" id="do_child_checked">' +
              '<input type="hidden" name="selected_tool_id[]" value="' + data.tool_kit + '" id="selected_tool_id">' +
              '<input type="hidden" name="selected_serial_no[]" value="' + data.serial_no + '" id="selected_serial_no">' +
              '<input type="number" name="selected_qty[]" value="" id="selected_qty" style="display: none;"></td>';
            }

            else
            {
              row += '<td><input type="checkbox" value="' + data.kit_master_id + '" name="do_child_checked[]" id="do_child_checked">' +
              '<input type="hidden" name="selected_tool_id[]" value="' + data.tool_kit + '" id="selected_tool_id">' +
              '<input type="hidden" name="selected_serial_no[]" value="' + data.serial_no + '" id="selected_serial_no">' +
              '<input type="number" name="selected_qty[]" value="" id="selected_qty" style="display: none;"></td>';
            }
          }

          row += "</tr>"

          $( '#delivery-order-table tbody' ).append( row );

          if( response.status == 'C' )
          {
            $( "#delivery-order-table :input" ).prop( "disabled", true );
          }
        });
      },

      error: function ( response )
      {
        console.log( response );
      }
    });
  }

  else
  {
    $( ".main-delivery-order-table" ).css('display', 'none');
  }

  $( '.btn-reopen' ).click(function(e) {

    if(confirm("Are you sure want to reopen current delivery order no.?"))
    {
      // open do no.
    }

    else
    {
      e.preventDefault();
    }
  });

  //post kit information according to good receipt header
  $( ".main-delivery-order-table" ).on('click', '.btn-post', function(e) {

    event.preventDefault();

    var job_id = $( '#job_id' ).val();

    var kit_master_id = [];
    var tool_id = [];
    var serial_no = [];
    var qty = [];

    var validationFailed = false;
    // var delivery_order_id = $( "input[type=hidden][name=delivery_order_id]" ).val();

    // validate user input of qty
    $( '#delivery-order-table input:checkbox:checked' ).each(function() {

      if( !$.trim($(this).parent().find( '#selected_qty' ).val()).length )
      {
        validationFailed = true;
        alert( 'Selected qty is empty!' );
        return false;
      }
    });

    // validate user input of qty
    $( '#delivery-order-table input:checkbox:checked' ).each(function() {

      if( $(this).parent().find( '#selected_qty' ).val() == 0 )
      {
        validationFailed = true;
        alert( 'Selected qty cannot be 0!' );
        return false;
      }
    });

    if( !validationFailed )
    {
      if(confirm("Are you sure want to post selected kits?"))
      {
        $("#delivery-order-table input:checkbox:checked").map(function(){

            kit_master_id.push($(this).val());
        });

        $( '#delivery-order-table input:checkbox:checked' ).each(function() {

            tool_id.push( $(this).parent().find( '#selected_tool_id' ).val() );
        });

        $( '#delivery-order-table input:checkbox:checked' ).each(function() {

            if( $(this).parent().find( '#selected_serial_no' ).val() == 'null' )
            {
              serial_no.push( '' );
            }

            else
            {
              serial_no.push( $(this).parent().find( '#selected_serial_no' ).val() );
            }

        });

        $( '#delivery-order-table input:checkbox:checked' ).each(function() {

            qty.push( $(this).parent().find( '#selected_qty' ).val() );
        });

        var dataForm = {
          _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
          delivery_order_id: delivery_order_id,
          kit_master_id: kit_master_id,
          tool_id: tool_id,
          serial_no: serial_no,
          qty: qty,
          job_id: job_id
        };

        $.ajax({
          type: 'POST',
          url: '/delivery-order/post-delivery-order-child',
          data: dataForm,
          dataType: 'json',
          context: this,
          success: function( response )
          {
            alert( 'Post selected kit successfully!' );
            location.reload();

            // if( response.data )
            // {
            //   alert( 'Post selected kit successfully!' );
            // }
          },

          error: function ( response )
          {
            console.log( response );
          }
        });
      }
    }

  });

  $( '#delivery-order-table' ).on('change', '#do_child_checked', function() {

    // get balance quantity
    var bal_qty = parseInt( $( this ).closest( 'tr' ).find( 'td:eq(5)' ).text() );

    if( $( this ).is( ':checked' ) )
    {
      $( this ).parent().find( '#selected_qty' ).show().val( bal_qty );

      // var sel_qty = parseInt( $( this ).parent().find( '#selected_qty' ).val() );

      // $( this ).closest( 'tr' ).find( 'td:eq(5)' ).text( parseInt( bal_qty - sel_qty ) );
    }

    else
    {
      // var sel_qty = parseInt( $( this ).parent().find( '#selected_qty' ).val() );

      // $( this ).closest( 'tr' ).find( 'td:eq(5)' ).text( parseInt( bal_qty + sel_qty ) );
      $( this ).parent().find( '#selected_qty' ).val( '' ).hide();
    }
  });

  $( '#delivery-order-table' ).on('keyup', '#selected_qty', function(e) {

    var char = String.fromCharCode( e.keyCode );
    // get balance quantity
    var bal_qty = $( this ).closest( 'tr' ).find( 'td:eq(5)' ).text();

    if( $( this ).val().length == 1 )
    {
      if( char == 0 )
      {
        $( this ).val( '' );
        alert('Minumum quantity is "1"!');
      }
    }

    // cannot be more than balance quantity
    if( $( this ).val() > bal_qty )
    {
      $( this ).val( '' );
      alert('Quantity cannot be more than balance quantity!');
    }
  });

  // $( "#grn_no" ).change(function() {
  //
  //   if( $( this ).val() != '' )
  //   {
  //     var dataForm = {
  //       _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
  //       grn_no: $( this ).val()
  //     };
  //
  //     $.ajax({
  //       type: 'GET',
  //       url: '/delivery-order/get-customer-info',
  //       data: dataForm,
  //       dataType: 'json',
  //       context: this,
  //       success: function( response )
  //       {
  //         $( "#cust_code" ).val( response.data.customer_code );
  //         // $( "#cust_name" ).val( response.data.customer_name );
  //
  //         var dataForm = {
  //           _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
  //           cust_code: response.data.customer_code
  //         };
  //
  //         $.ajax({
  //           type: 'GET',
  //           url: '/delivery-order/get-purchase-order',
  //           data: dataForm,
  //           dataType: 'json',
  //           context: this,
  //           success: function( response )
  //           {
  //             $.each(response.data, function( index, data ) {
  //               $( "#po_no" ).append( '<option value="' + data.po_no + '">' + data.po_no + '</option>' );
  //             });
  //           },
  //
  //           error: function ( response )
  //           {
  //             console.log( response );
  //           }
  //         });
  //       },
  //
  //       error: function ( response )
  //       {
  //         console.log( response );
  //       }
  //     });
  //   }
  // });

  $( '#cust_code' ).change(function() {

    $( ".main-delivery-order-table" ).css('display', 'none');

    var select_job = [];
    var select_grn = [];
    var cust_code = $( this ).val();

    $( '#grn_no' ).val( '' );

    if( $( this ).val() != '' )
    {
      var formData = {
        _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
        cust_code:	cust_code
      }

      $.ajax({
        type: 'GET',
        url: '/delivery-order/get-grn-details-without-closed',
        data: formData,
        dataType: 'json',
        success: function( response )
        {
          $( '#job_id' ).remove();
          $( '.job_id' ).append( '<select class="form-control" id="job_id" name="job_id"><option value="">Please select</option></select>' );

          $( '#po_no' ).remove();
          $( '.po_no' ).append( '<select class="form-control" id="po_no" name="po_no"><option value="">Please select</option></select>' );

          // $( '#grn_no' ).remove();
          // $( '.grn_no' ).append( '<select class="form-control" id="grn_no" name="grn_no">' );

          if( response.data.length != 0 )
          {
            $.each(response.data, function( index, data ) {
              select_job.push( data.job_id );
              select_grn.push( data.grn_no );
            });

            $.each(select_job, function( index, data ) {
              $( '#job_id' ).append( '<option value="' + data + '">' + data + '</option>' );
              // $( '#grn_no' ).append( '<option value="' + select_grn[index] + '">' + select_grn[index] + '</option>' );
            });

          }

          // else
          // {
          //   $( '#job_id' ).append( '<option value="">Please select</option>' );
          //   $( '#grn_no' ).append( '<option value="">Please select</option>' );
          // }

          var dataForm = {
            _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
            cust_code: cust_code
          };

          $.ajax({
            type: 'GET',
            url: '/delivery-order/get-purchase-order',
            data: dataForm,
            dataType: 'json',
            context: this,
            success: function( response )
            {
              $.each(response.data, function( index, data ) {
                $( "#po_no" ).append( '<option value="' + data.po_no + '">' + data.po_no + '</option>' );
              });
            },

            error: function ( response )
            {
              console.log( response );
            }
          });
        },

        error: function ( response )
        {
          console.log( response );
        }
      });
    }
  });

  $( '.job_id' ).change(function() {

    var grn_no = '';
    var grn_id = '';
    var newRowContent = '';
    var job_id = $( '#job_id' ).val();
    var customer_code = $( '#cust_code' ).val();

    if( job_id != '' )
    {
      var formData = {
        _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
        // grn_no:	grn_no,
        job_id: job_id,
        customer_code: customer_code
      }

      $.ajax({
        type: 'GET',
        url: '/post-cleaning-report/get-kit-details',
        data: formData,
        dataType: 'json',
        success: function( response )
        {
          // $( '#kit_type' ).remove();
          // $( '.kit_type' ).append( '<select multiple="multiple" class="form-control" id="kit_type" name="kit_type[]"></select>' );

          $( '#grn_id' ).val( '' );

          if( response.data.length != 0 )
          {
            $.each(response.data, function( index, data ) {

              grn_no = data.grn_no;
              grn_id = data.grn_id;
            });

            $( '#grn_no' ).val( grn_no );
            $( '#grn_id' ).val( grn_id );
          }
        },

        error: function ( response )
        {
          console.log( response );
        }
      });
    }
  });

  if( $( "#cust_code" ).val() != '' )
  {
    // get po no. onload
    var dataForm = {
      _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
      cust_code: $( "#cust_code" ).val(),
      do_id: $( "#delivery_order_id" ).val()
    };

    $.ajax({
      type: 'GET',
      url: '/delivery-order/get-purchase-order',
      data: dataForm,
      dataType: 'json',
      context: this,
      success: function( response )
      {
        $.each(response.data, function( index, data ) {

          if( response.exist_po == data.po_no )
          {
            $( "#po_no" ).append( '<option selected value="' + data.po_no + '">' + data.po_no + '</option>' );
          }

          else
          {
            $( "#po_no" ).append( '<option value="' + data.po_no + '">' + data.po_no + '</option>' );
          }
        });
      },

      error: function ( response )
      {
        console.log( response );
      }
    });
  }

  if( $( "#cust_code" ).val() != '' )
  {
    var select_job = [];
    var select_grn = [];
    var select_grn_id = [];
    var grn_no = '';
    var grn_id = '';

    // get job id onload
    var formData = {
      _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
      cust_code:	$( "#cust_code" ).val()
    }

    $.ajax({
      type: 'GET',
      url: '/delivery-order/get-grn-details-without-closed',
      data: formData,
      dataType: 'json',
      success: function( response )
      {
        console.log(response);
        // $( '#job_id' ).remove();
        // $( '.job_id' ).append( '<select class="form-control" id="job_id" name="job_id"><option value="">Please select</option></select>' );

        // $( '#po_no' ).remove();
        // $( '.po_no' ).append( '<select class="form-control" id="po_no" name="po_no"><option value="">Please select</option></select>' );

        // $( '#grn_no' ).remove();
        // $( '.grn_no' ).append( '<select class="form-control" id="grn_no" name="grn_no">' );

        if( response.data.length != 0 )
        {
          $.each(response.data, function( index, data ) {
            select_job.push( data.job_id );
            select_grn.push( data.grn_no );
            select_grn_id.push( data.id );
          });

          $.each(select_job, function( index, data ) {

            if( $( '#hidden_job_id' ).val() == data )
            {
              $( '#job_id' ).append( '<option selected value="' + data + '">' + data + '</option>' );
            }

            else
            {
              $( '#job_id' ).append( '<option value="' + data + '">' + data + '</option>' );
            }
          });

          if( $( "#job_id" ).val() != '' )
          {
            // get grn no. onload
            var formData = {
              _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
              // grn_no:	grn_no,
              job_id: $( '#job_id' ).val(),
              customer_code: $( "#cust_code" ).val()
            }

            $.ajax({
              type: 'GET',
              url: '/post-cleaning-report/get-kit-details',
              data: formData,
              dataType: 'json',
              success: function( response )
              {
                // $( '#kit_type' ).remove();
                // $( '.kit_type' ).append( '<select multiple="multiple" class="form-control" id="kit_type" name="kit_type[]"></select>' );

                $( '#grn_id' ).val( '' );

                if( response.data.length != 0 )
                {
                  $.each(response.data, function( index, data ) {

                    grn_no = data.grn_no;
                    grn_id = data.grn_id;
                  });

                  $( '#grn_no' ).val( grn_no );
                  $( '#grn_id' ).val( grn_id );
                }
              },

              error: function ( response )
              {
                console.log( response );
              }
            });
          }
        }
      },

      error: function ( response )
      {
        console.log( response );
      }
    });
  }

  // if( $( "#job_id" ).val() != '' )
  // {
  //   var grn_no = '';
  //   var grn_id = '';

  //   // get grn no. onload
  //   var formData = {
  //     _token: $( 'meta[name="csrf-token"]' ).attr( 'content' ),
  //     // grn_no:	grn_no,
  //     job_id: $( '#job_id' ).val(),
  //     customer_code: $( "#cust_code" ).val()
  //   }

  //   $.ajax({
  //     type: 'GET',
  //     url: '/post-cleaning-report/get-kit-details',
  //     data: formData,
  //     dataType: 'json',
  //     success: function( response )
  //     {
  //       // $( '#kit_type' ).remove();
  //       // $( '.kit_type' ).append( '<select multiple="multiple" class="form-control" id="kit_type" name="kit_type[]"></select>' );

  //       $( '#grn_id' ).val( '' );

  //       if( response.data.length != 0 )
  //       {
  //         $.each(response.data, function( index, data ) {

  //           grn_no = data.grn_no;
  //           grn_id = data.grn_id;
  //         });

  //         $( '#grn_no' ).val( grn_no );
  //         $( '#grn_id' ).val( grn_id );
  //       }
  //     },

  //     error: function ( response )
  //     {
  //       console.log( response );
  //     }
  //   });
  // }

  // else
  // {
  //   $( '#grn_no' ).val( '' );
  //   $( '#grn_id' ).val( '' );
  // }

});

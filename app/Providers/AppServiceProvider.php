<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Bouncer;
use App\Models\User;

class AppServiceProvider extends ServiceProvider
{
  /**
   * Bootstrap any application services.
   *
   * @return void
   */
  public function boot()
  {
    // Bouncer::seeder( function ()
    // {
    //   Bouncer::allow( 'admin' )->to([
    //     'users.view',
    //     'companies.view',
    //   ]);
    //
    //   Bouncer::allow( 'standard' )->to([
    //     'process.view'
    //   ]);
    //
    //   Bouncer::assign( 'admin' )->to( User::first() );
    //   Bouncer::assign( 'standard' )->to( User::where( 'id', '!=', 1 )->first() );
    // });
  }

  /**
   * Register any application services.
   *
   * @return void
   */
  public function register()
  {
    //
  }
}

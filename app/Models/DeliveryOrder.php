<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryOrder extends Model
{
  protected $table = 'delivery_order';

  public $dates = [ 'created_at', 'updated_at' ];
  protected $guarded = [ ];
}

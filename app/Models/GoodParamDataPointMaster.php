<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodParamDataPointMaster extends Model
{
  protected $table = 'good_param_datapoint_detail';

  public $dates = [ 'created_at', 'updated_at' ];
  protected $guarded = [ ];
}

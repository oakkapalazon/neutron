<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UomMaster extends Model
{
  protected $table = 'uom_master';

  public $dates = [ 'created_at', 'updated_at' ];
  protected $guarded = [ ];

  public static function getUOMMasters()
  {
    $select = ['id', 'uom_code'];

    $result = \App\Models\UomMaster::select($select)->get();

    $uom[''] = 'Please select';

		if(count($result) > 0)
    {
      foreach ($result as $data)
  		{
        $uom[$data->id] = $data->uom_code;
      }
    }

    return $uom;
  }
}

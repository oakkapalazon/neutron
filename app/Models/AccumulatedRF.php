<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccumulatedRF extends Model
{
    protected $table = 'accumulated_rf';

    public $dates = [ 'created_at', 'updated_at' ];
    protected $guarded = [ ];
}

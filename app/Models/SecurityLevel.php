<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SecurityLevel extends Model
{
  protected $table = 'security_level';

  public $dates = [ 'created_at', 'updated_at' ];
  protected $guarded = [ ];

  public static function accessRight()
  {
    $select = ['id', 'access_level'];

    $result = \App\Models\SecurityLevel::select($select)->get();

    $access_right[''] = 'Please select'; 

		foreach ($result as $data)
		{
      $access_right[$data->id] = $data->access_level;
    }

    return $access_right;
  }
}

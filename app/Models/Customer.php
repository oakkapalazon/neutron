<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
  protected $table = 'customer';

  public $dates = [ 'created_at', 'updated_at' ];
  protected $guarded = [ ];

  public static function getCustomers()
  {
    $select = ['id', 'customer_name'];

    $result = \App\Models\Customer::select($select)->get();

    $customers[''] = 'Please select';

		if(count($result) > 0)
    {
      foreach ($result as $data)
  		{
        $customers[$data->id] = $data->customer_name;
      }
    }

    return $customers;
  }

  public static function getCustomerCodes()
  {
    $select = ['id', 'customer_code'];

    $result = \App\Models\Customer::select($select)->get();

    $customers[''] = 'Please select';

		if(count($result) > 0)
    {
      foreach ($result as $data)
  		{
        $customers[$data->id] = $data->customer_code;
      }
    }

    return $customers;
  }

  public static function getCustomerCodesOnly()
  {
    $select = ['customer_code'];

    $result = \App\Models\Customer::select($select)->get();

    $customers[''] = 'Please select';

		if(count($result) > 0)
    {
      foreach ($result as $data)
  		{
        $customers[$data->customer_code] = $data->customer_code;
      }
    }

    return $customers;
  }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParameterDatapointMaster extends Model
{
  protected $table = 'param_datapoint_master';

  public $dates = [ 'created_at', 'updated_at' ];
  protected $guarded = [ ];
}

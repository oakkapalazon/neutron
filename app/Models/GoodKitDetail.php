<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodKitDetail extends Model
{
  protected $table = 'good_kit_detail';

  public $dates = [ 'created_at', 'updated_at' ];
  protected $guarded = [ ];
}

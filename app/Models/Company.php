<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
  protected $table = 'company';

  public $dates = [ 'created_at', 'updated_at' ];
  protected $guarded = [ ];

  public static function getCompanyLists()
  {
    $select = ['id', 'company_name'];

    $result = \App\Models\Company::select($select)->whereNull('is_assign')->get();

    $compaines[''] = 'Please select';

		if(count($result) > 0)
    {
      foreach ($result as $data)
  		{
        $compaines[$data->id] = $data->company_name;
      }
    }

    return $compaines;
  }

  public function user()
  {
    return $this->hasOne( 'App\Models\User' );
  }
}

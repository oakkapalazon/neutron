<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartTypeMaster extends Model
{
  protected $table = 'part_type_master';

  public $dates = [ 'created_at', 'updated_at' ];
  protected $guarded = [ ];

  public static function getPartTypeMaster($id = null)
  {
    $select = ['id', 'part_type'];

    $result = \App\Models\PartTypeMaster::query();

    if($id != null)
    {
      $result = $result->where('kit_master_id', $id);
    }

    $result = $result->select($select)->get();

    $type[''] = 'Please select';

		if(count($result) > 0)
    {
      foreach ($result as $data)
  		{
        $type[$data->id] = $data->part_type;
      }
    }

    return $type;
  }

  public function kit_master()
  {
    return $this->belongsTo('App\Models\KitMaster', 'kit_type_id');
  }
}

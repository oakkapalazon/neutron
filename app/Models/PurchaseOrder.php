<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model
{
  protected $table = 'purchase_order';

  public $dates = [ 'created_at', 'updated_at' ];
  protected $guarded = [ ];
}

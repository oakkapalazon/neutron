<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VisualConditionMaster extends Model
{
  protected $table = 'visual_condition_master';

  public $dates = [ 'created_at', 'updated_at' ];
  protected $guarded = [ ];

  public static function getVisualCondition($id = null)
  {
    $select = ['id', 'code'];

    $result = \App\Models\VisualConditionMaster::query();

    $result = $result->select($select)->get();

		if(count($result) > 0)
    {
      foreach ($result as $data)
  		{
        $status[$data->id] = $data->code;
      }
    }

    return $status;
  }
}

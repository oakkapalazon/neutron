<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RunningNo extends Model
{
  protected $table = 'running_no';

  public $dates = [ 'created_at', 'updated_at' ];
  protected $guarded = [ ];
}

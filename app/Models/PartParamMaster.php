<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartParamMaster extends Model
{
  protected $table = 'part_param_master';

  public $dates = [ 'created_at', 'updated_at' ];
  protected $guarded = [ ];

  public static function getPartParamMaster($id = null)
  {
    $select = ['parameter_master.id', 'param'];

    $result = \App\Models\PartParamMaster::leftjoin('parameter_master', 'parameter_master.id', 'part_param_master.parameter_master_id');

    if($id != null)
    {
      $result = $result->where('part_type_id', $id);
    }

    $result = $result->select($select)->get();

    $type[''] = 'Please select';

		if(count($result) > 0)
    {
      foreach ($result as $data)
  		{
        $type[$data->id] = $data->param;
      }
    }

    return $type;
  }
}

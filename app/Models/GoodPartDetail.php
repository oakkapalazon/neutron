<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodPartDetail extends Model
{
  protected $table = 'good_part_detail';

  public $dates = [ 'created_at', 'updated_at' ];
  protected $guarded = [ ];
}

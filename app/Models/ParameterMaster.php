<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParameterMaster extends Model
{
  protected $table = 'parameter_master';

  public $dates = [ 'created_at', 'updated_at' ];
  protected $guarded = [ ];

  public static function getParameterMaster()
  {
    $select = ['id', 'param'];

    $result = \App\Models\ParameterMaster::where('is_assign', 0)->select($select)->get();

    $param[''] = 'Please select';

    if(count($result) > 0)
    {
      foreach ($result as $data)
  		{
        $param[$data->id] = $data->param;
      }
    }

    return $param;
  }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KitMaster extends Model
{
  protected $table = 'kit_master';

  public $dates = [ 'created_at', 'updated_at' ];
  protected $guarded = [ ];

  public static function getKitTypes()
  {
    $select = ['id', 'kit_type'];

    $result = \App\Models\KitMaster::select($select)->orderBy('kit_type', 'desc')->get();

    $type[''] = 'Please select';

		if(count($result) > 0)
    {
      foreach ($result as $data)
  		{
        $type[$data->id] = $data->kit_type;
      }
    }

    return $type;
  }

  public static function getKitTypesBasedCust($cust_id)
  {
    $select = ['id', 'kit_type'];

    $result = \App\Models\KitMaster::where('customer_id', $cust_id)
              ->orderBy('kit_type', 'desc')
              ->select($select)->get();

    $type[''] = 'Please select';

		if(count($result) > 0)
    {
      foreach ($result as $data)
  		{
        $type[$data->id] = $data->kit_type;
      }
    }

    return $type;
  }

  public function part_master()
  {
    return $this->hasMany('App\Models\PartMaster');
  }

  public function part_type_master()
  {
    return $this->hasMany('App\Models\PartTypeMaster');
  }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartStatusMaster extends Model
{
  protected $table = 'part_status_master';

  public $dates = [ 'created_at', 'updated_at' ];
  protected $guarded = [ ];

  public static function getPartStatus($id = null)
  {
    $select = ['id', 'description'];

    $result = \App\Models\PartStatusMaster::query();

    if($id != null)
    {
      $result = $result->where('kit_master_id', $id);
    }

    $result = $result->select($select)->get();

		if(count($result) > 0)
    {
      foreach ($result as $data)
  		{
        $status[$data->id] = $data->description;
      }
    }

    return $status;
  }

  public static function getStatusDesc()
  {
    $select = ['description'];

    $result = \App\Models\PartStatusMaster::select($select)->get();

    // $status[''] = 'Please select';

		if(count($result) > 0)
    {
      foreach ($result as $data)
  		{
        $status[$data->description] = $data->description;
      }
    }

    return $status;
  }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodReceiptHeader extends Model
{
  protected $table = 'good_receipt_header';

  public $dates = [ 'created_at', 'updated_at' ];
  protected $guarded = [ ];

  public static function getGoodReceiptHeader()
  {
    $select = ['id', 'grn_no'];

    $result = \App\Models\GoodReceiptHeader::select($select)->get();

    $grn[''] = 'Please select';

		if(count($result) > 0)
    {
      foreach ($result as $data)
  		{
        $grn[$data->id] = $data->grn_no;
      }
    }

    return $grn;
  }

  public static function getGoodReceiptHeaderOpenStatus()
  {
    $select = ['id', 'grn_no'];

    $result = \App\Models\GoodReceiptHeader::select($select)
              ->where('job_status', 'open')
              ->get();

    $grn[''] = 'Please select';

		if(count($result) > 0)
    {
      foreach ($result as $data)
  		{
        //check if there are parameter data points have not updated yet in every grn
        $check_datapoint_value = \App\Models\GoodReceiptHeader::join('good_kit_detail', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id')
                                 ->join('good_part_detail', 'good_kit_detail.id', '=', 'good_part_detail.good_kit_detail_id')
                                 ->join('good_param_datapoint_detail', 'good_part_detail.id', '=', 'good_param_datapoint_detail.good_part_detail_id')
                                 ->where('good_receipt_header.id', $data->id)
                                 ->whereNull('good_param_datapoint_detail.param_datapoint_value')
                                 ->select('good_param_datapoint_detail.param_datapoint_value')
                                 ->get();

        if(count($check_datapoint_value) > 0)
        {
          continue;
        }

        $check_kit_part_exist = \App\Models\GoodReceiptHeader::join('good_kit_detail', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id')
                                ->join('good_part_detail', 'good_kit_detail.id', '=', 'good_part_detail.good_kit_detail_id')
                                ->where('good_receipt_header.id', $data->id)
                                ->get();

        if(count($check_kit_part_exist) == 0)
        {
          continue;
        }

        $grn[$data->id] = $data->grn_no;
      }
    }

    return $grn;
  }
}

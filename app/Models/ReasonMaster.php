<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReasonMaster extends Model
{
  protected $table = 'reason_master';

  public $dates = [ 'created_at', 'updated_at' ];
  protected $guarded = [ ];

  public static function getReasonMasters()
  {
    $select = ['id', 'reason_desc'];

    $result = \App\Models\ReasonMaster::select($select)
              ->orderBy('reason_desc', 'asc')
              ->get();

    $reason[''] = 'Please select';

		if(count($result) > 0)
    {
      foreach ($result as $data)
  		{
        $reason[$data->id] = $data->reason_desc;
      }
    }

    return $reason;
  }
}

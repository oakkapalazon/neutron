<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryOrderChild extends Model
{
  protected $table = 'delivery_order_child';

  public $dates = [ 'created_at', 'updated_at' ];
  protected $guarded = [ ];
}

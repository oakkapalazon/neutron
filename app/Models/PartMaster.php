<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartMaster extends Model
{
  protected $table = 'part_master';

  public $dates = [ 'created_at', 'updated_at' ];
  protected $guarded = [ ];

  public function kit_master()
  {
    return $this->belongsTo('App\Models\KitMaster', 'kit_type_id');
  }
}

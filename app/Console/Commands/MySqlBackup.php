<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

class MySqlBackup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mysql:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run a MySql Backup';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $host = env('DB_HOST');
        $user = env('DB_USERNAME');
        $password = env('DB_PASSWORD');
        $database = env('DB_DATABASE','pts');
        $filename = "backup-" . Carbon::now()->format('Y-m-d') . ".sql";
        $command = "\"C:\\xampp\\mysql\\bin\\mysqldump.exe\" --user={$user} --password={$password} --host={$host} --flush-logs --single-transaction {$database} > " .storage_path(). "/backup/" .$filename;
        // $command = "\"C:\\xampp\\mysql\\bin\\mysqldump.exe\" --user=" . $user ." --password=" . $password . " --host=" . $host . " " . $database . "  | gzip > C:/xampp/mysql/" . $filename;

        exec($command);
        $this->info('Your backup is being saved to the directory');
    }
}

<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class PostCleanExport implements FromView
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function __construct($do_detail)
    {
        $this->do_detail = $do_detail;

        return $this;
    }

    public function view(): View
    {
        return view('post_cleaning_report.post-report-export', [
            'do_detail' => $this->do_detail
        ]);
    }
}

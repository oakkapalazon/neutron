<?php

namespace App\Exports;

use App\Factories\ModelFactory;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Session;

class PartMasterExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
      $select = [
        'kit_type',
        'part_type',
        'serial_no',
        'set_id',
        'specs',
        'status',
        'additional_info',
        'part_master.grade',
        'part_master.remarks',
        'part_master.created_at'
      ];
  
      $part_masters = ModelFactory::getInstance('PartMaster')
                      ->leftjoin('kit_master', 'kit_master.id', '=', 'part_master.kit_master_id')
                      ->leftjoin('part_type_master', 'part_type_master.id', '=', 'part_master.part_type_id')
                      ->select($select)
                      ->orderby('kit_type', 'asc')
                      ->orderby('part_type', 'asc')
                      ->orderby('serial_no', 'asc')
                      ->orderby('set_id', 'asc')
                      ->orderby('created_at', 'asc');

      if(!empty(session('kit_type')))
      {
        $part_masters = $part_masters->where('kit_master.id', session('kit_type'));
      }

      if(!empty(session('part_type')))
      {
        $part_masters = $part_masters->where('part_type_master.id', session('part_type'));
      }

      if(!empty(session('serial_no')))
      {
        $part_masters = $part_masters->where('serial_no', session('serial_no'));
      }

      if(!empty(session('set_id')))
      {
        $part_masters = $part_masters->where('set_id', session('set_id'));
      }

      if(!empty(session('created_at')))
      {
        // $date_from = Carbon::parse($request->date_from)->format("Y-m-d");
        // $date_to = Carbon::parse($request->date_to)->format("Y-m-d");
   
        $part_masters = $part_masters->whereDate('part_master.created_at', session('created_at'));
      }
    
      $part_masters = $part_masters->select($select)->get();

      return $part_masters;
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'KitType',
            'PartType',
            'PartSerialNo',
            'SetID',
            'Specs',
            'Status',
            'AdditionalInfo',
            'grade',
            'Remarks',
            'CreatedAt'
        ];
    }
}

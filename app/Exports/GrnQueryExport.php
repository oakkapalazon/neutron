<?php

namespace App\Exports;

use App\Factories\ModelFactory;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Session;

class GrnQueryExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
      $select = [
        'customer_code',
        'grn_no',
        'collection_date',
        'job_id',
        'kit_master.process',
        'kit_master.descn',
        'kit_type',
        'set_id',
        'good_kit_detail.tool_kit',
        'reason_desc',
        'part_type_name',
        'good_part_detail.serial_no',
        'good_part_detail.rf',
        'good_part_detail.arf',
        'good_part_detail.visual_condition_desc',
        'part_status_master.description',
        'part_type_master.part_sequence'
      ];

      $grn_report = ModelFactory::getInstance('GoodReceiptHeader')
                    ->join('customer', 'good_receipt_header.customer_id', '=', 'customer.id')
                    ->join('good_kit_detail', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id')
                    ->join('kit_master', 'good_kit_detail.kit_master_id', '=', 'kit_master.id')
                    ->leftjoin('good_part_detail', 'good_kit_detail.id', '=', 'good_part_detail.good_kit_detail_id')
                    ->join('part_type_master', 'good_part_detail.part_type_id', '=', 'part_type_master.id')
                    ->join('part_status_master', 'good_part_detail.actions', '=', 'part_status_master.id')
                    ->join('reason_master', 'reason_master.id', '=', 'good_kit_detail.reason_master_id')
                    // ->orderby('grn_no', 'asc')
                    // ->orderby('kit_type', 'asc')
//                    ->where('good_kit_detail.closed', '0')
//                      ->orderby('job_id', 'desc')
                      ->orderby('set_id', 'asc')
                      ->orderby('kit_type', 'asc')
                      ->orderby('part_type_master.part_sequence', 'asc');
//                    ->groupby('good_kit_detail.id')
//                    ->groupby('part_type_id');

//      if(!empty(session('grn_no')))
//      {
//        $grn_report = $grn_report->where('grn_no', session('grn_no'));
//      }

        if(!empty(session('set_id')))
        {
            $grn_report = $grn_report->where('set_id', session('set_id'));
        }

      if(!empty(session('customer_code')))
      {
        $grn_report = $grn_report->where('customer_code', session('customer_code'));
      }

      if(!empty(session('job_id')))
      {
        $grn_report = $grn_report->where('job_id', session('job_id'));
      }

      if(!empty(session('date_from')) && !empty(session('date_to')))
      {
        // $date_from = Carbon::parse($request->date_from)->format("Y-m-d");
        // $date_to = Carbon::parse($request->date_to)->format("Y-m-d");

        $grn_report = $grn_report->whereBetween('collection_date', [session('date_from'), session('date_to')]);
      }

      if(!empty(session('kit_master_id')))
      {
        $grn_report = $grn_report->where('kit_master.id', session('kit_master_id'));
      }

        if(!empty(session('part_type')))
        {
            $grn_report = $grn_report->where('good_part_detail.part_type_name', session('part_type'));
        }

        if(!empty(session('serial_no')))
        {
            $grn_report = $grn_report->where('good_part_detail.serial_no', session('serial_no'));
        }

      $grn_report = $grn_report->select($select)->get();

      foreach($grn_report as $row)
      {
        $visual_array[] = $row->visual_condition;
      }

      // foreach($visual_array as $row => $array)
      // {
      //   if( strpos($array, ',') !== false )
      //   {
      //     $explode_visual = explode(",", $array);
      //
      //     foreach($explode_visual as $array2)
      //     {
      //       $visual_table = \App\Models\VisualConditionMaster::find($array2);
      //
      //       $result_visual[] = $visual_table->description;
      //     }
      //
      //     $implode_visual = implode(",", $result_visual);
      //     $get_visual_desc[] = $implode_visual;
      //     $result_visual = array();
      //   }
      //   else
      //   {
      //     $visual_table = \App\Models\VisualConditionMaster::find($array);
      //     $get_visual_desc[] = $visual_table->description;
      //   }
      // }

      return $grn_report;
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'CustCode',
            'Grn_No',
            'Date',
            'JobID',
            'Process',
            'Descn',
            'KitType',
            'SetID',
            'ToolID',
            'Reason',
            'PartType',
            'PartSerialNo',
            'RF_Hrs',
            'ARF',
            'VisualCondition',
            'PartStatus',
            'PartSequence'
        ];
    }
}

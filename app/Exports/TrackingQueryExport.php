<?php

namespace App\Exports;

use App\Factories\ModelFactory;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Session;

class TrackingQueryExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
      $select = [
        'grn_no',
        'collection_date',
        'customer_code',
        'job_id',
        'kit_type',
        'set_id',
        'part_type_name',
        'good_part_detail.serial_no',
        'good_part_detail.rf',
        'good_part_detail.arf',
        'parameter_master.param',
        'param_datapoint_value',
        'datapoint',
        'avg',
        'reason_desc',
        'good_kit_detail.tool_kit',
        'kit_master.descn',
        'kit_master.process',
        'good_part_detail.visual_condition_desc',
          'part_type_master.part_sequence'
      ];

      $tracking_report = ModelFactory::getInstance('GoodReceiptHeader')
                        ->join('customer', 'good_receipt_header.customer_id', '=', 'customer.id')
                        ->join('good_kit_detail', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id')
                        ->join('kit_master', 'good_kit_detail.kit_master_id', '=', 'kit_master.id')
                        ->join('good_part_detail', 'good_kit_detail.id', '=', 'good_part_detail.good_kit_detail_id')
                        ->join('part_type_master', 'good_part_detail.part_type_id', '=', 'part_type_master.id')
                        ->join('good_param_datapoint_detail', 'good_part_detail.id', '=', 'good_param_datapoint_detail.good_part_detail_id')
                        ->join('param_datapoint_master', 'good_param_datapoint_detail.param_datapoint_master_id', '=', 'param_datapoint_master.id')
                        ->join('parameter_master', 'param_datapoint_master.parameter_master_id', '=', 'parameter_master.id')
                        ->join('reason_master', 'reason_master.id', '=', 'good_kit_detail.reason_master_id')
//                          ->orderby('job_id', 'desc')
//                          ->orderby('set_id', 'desc')
//                          ->orderby('kit_type', 'asc')
//                          ->orderby('part_type_name', 'asc')
                          ->orderby('set_id', 'asc')
                          ->orderby('kit_type', 'asc')
                          ->orderby('part_type_master.part_sequence', 'asc');
                        // ->whereNotNull('param_datapoint_value');
//                        ->groupby('good_kit_detail.id')
//                        ->groupby('good_part_detail.part_type_id')
//                        ->groupby('param_datapoint_master.id');

//      if(!empty(session('grn_no')))
//      {
//        $tracking_report = $tracking_report->where('grn_no', session('grn_no'));
//      }

        if(!empty(session('set_id')))
        {
            $tracking_report = $tracking_report->where('set_id', session('set_id'));
        }

      if(!empty(session('customer_code')))
      {
        $tracking_report = $tracking_report->where('customer_code', session('customer_code'));
      }

      if(!empty(session('job_id')))
      {
        $tracking_report = $tracking_report->where('job_id', session('job_id'));
      }

      if(!empty(session('date_from')) && !empty(session('date_to')))
      {
        // $date_from = Carbon::parse($request->date_from)->format("Y-m-d");
        // $date_to = Carbon::parse($request->date_to)->format("Y-m-d");

        $tracking_report = $tracking_report->whereBetween('collection_date', [session('date_from'), session('date_to')]);
      }

      if(!empty(session('kit_master_id')))
      {
        $tracking_report = $tracking_report->where('kit_master.id', session('kit_master_id'));
      }

        if(!empty(session('part_type')))
        {
            $tracking_report = $tracking_report->where('good_part_detail.part_type_name', session('part_type'));
        }

        if(!empty(session('serial_no')))
        {
            $tracking_report = $tracking_report->where('good_part_detail.serial_no', session('serial_no'));
        }

      $tracking_report = $tracking_report->select($select)->get();

      return $tracking_report;
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
          'Grn_No',
          'Collection Date',
          'CustCode',
          'JobID',
          'KitType',
          'SetID',
          'PartType',
          'PartSerialNo',
          'RF_Hrs',
          'ARF',
          'Parameter',
          'Value',
          'POM',
          'Avg Value',
          'Reason',
          'ToolID',
          'KitDescn',
          'Process',
          'Visual Condition',
            'PartSequence'
        ];
    }
}

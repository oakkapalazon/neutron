<?php

namespace App\Filters;
use Carbon\Carbon;

class SearchFilter
{
  public static function searchKeyword($model, $value = null, $module)
  {
    if($module == 'company')
    {
      return $model->whereNested(function($query) use ($value) {
  			$query->where('company_name','like', '%'.$value.'%');
  			$query->orWhere('company_code','like', '%'.$value.'%');
  		});
    }

    if($module == 'customer')
    {
      return $model->whereNested(function($query) use ($value) {
  			$query->where('customer_name','like', '%'.$value.'%');
  			$query->orWhere('customer_code','like', '%'.$value.'%');
  		});
    }

    if($module == 'kit_master')
    {
      return $model->whereNested(function($query) use ($value) {
  			$query->where('kit_type','like', '%'.$value.'%');
  			$query->orWhere('descn','like', '%'.$value.'%');
        $query->orWhere('customer.customer_name','like', '%'.$value.'%');
  		});
    }

    if($module == 'part_master')
    {
      return $model->whereNested(function($query) use ($value) {
  			$query->where('kit_master.kit_type','like', '%'.$value.'%');
  			$query->orWhere('part_type_master.part_type','like', '%'.$value.'%');
        $query->orWhere('serial_no','like', '%'.$value.'%');
  		});
    }

    if($module == 'part_type_master')
    {
      return $model->whereNested(function($query) use ($value) {
  			$query->where('part_type','like', '%'.$value.'%');
  			$query->orWhere('kit_master.kit_type','like', '%'.$value.'%');
  		});
    }

    if($module == 'part_param_master')
    {
      return $model->whereNested(function($query) use ($value) {
  			$query->where('kit_master.kit_type','like', '%'.$value.'%');
  			$query->orWhere('part_type_master.part_type','like', '%'.$value.'%');
        $query->orWhere('parameter_master.param','like', '%'.$value.'%');
  		});
    }

    if($module == 'part_status_master')
    {
      return $model->whereNested(function($query) use ($value) {
  			$query->where('status_code','like', '%'.$value.'%');
  			$query->orWhere('description','like', '%'.$value.'%');
  		});
    }

    if($module == 'parameter_master')
    {
      return $model->whereNested(function($query) use ($value) {
  			$query->where('param','like', '%'.$value.'%');
  			$query->orWhere('description','like', '%'.$value.'%');
  		});
    }

    if($module == 'param_datapoint_master')
    {
      return $model->whereNested(function($query) use ($value) {
  			$query->where('kit_master.kit_type','like', '%'.$value.'%');
  			$query->orWhere('part_type_master.part_type','like', '%'.$value.'%');
        $query->orWhere('parameter_master.param','like', '%'.$value.'%');
        $query->orWhere('datapoint','like', '%'.$value.'%');
  		});
    }

    if($module == 'reason_master')
    {
      return $model->whereNested(function($query) use ($value) {
  			$query->where('reason_code','like', '%'.$value.'%');
  			$query->orWhere('reason_desc','like', '%'.$value.'%');
  		});
    }

    if($module == 'uom_master')
    {
      return $model->whereNested(function($query) use ($value) {
  			$query->where('uom_code','like', '%'.$value.'%');
  			$query->orWhere('uom_desc','like', '%'.$value.'%');
  		});
    }

    if($module == 'pre_cleaning')
    {
      $collection_date = '';

      if(isset($value[0]['date']))
  		{
  			$collection_date = str_replace('/', '-', $value[0]['date']);
  			$collection_date = date('Y-m-d', strtotime($collection_date));
  		}

      return $model->whereNested(function($query) use ($value, $collection_date) {

        if(!empty($collection_date))
        {
          $query->whereDate('collection_date','=', $collection_date);
        }

        if(!empty($value[0]['q']))
        {
          $query->where('grn_no','like', '%'.$value[0]['q'].'%');
          $query->orwhere('customer_name','like', '%'.$value[0]['q'].'%');
        }
      });
    }

    if($module == 'delivery_order')
    {
      return $model->whereNested(function($query) use ($value) {
  			$query->where('do_no','like', '%'.$value.'%');
  			$query->orWhere('po_no','like', '%'.$value.'%');
  		});
    }

    if($module == 'grn_report')
    {
      return $model->whereNested(function($query) use ($value) {
  			$query->where('grn_no','like', '%'.$value.'%');
  			$query->orWhere('collection_date','like', '%'.$value.'%');
        $query->orWhere('part_type','like', '%'.$value.'%');
  		});
    }

    if($module == 'post_cleaning')
    {

    }
  }
}

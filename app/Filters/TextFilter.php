<?php

namespace App\Filters;

use App\Core\FilterCore;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class TextFilter extends FilterCore
{
	/**
	 * (non-PHPdoc)
	 * @see \App\Core\FilterCore::addFilter()
	 */
	public function addFilter($model, $name, $scope='')
	{
		$this->setName($name);
		$this->value = $this->get();

		if(!$this->request->has($name) && !$this->getValue())
		{
			return $model;
		}
		elseif($this->request->get($name))
		{
			$this->setValue($this->request->get($name));
			//$this->store();
		}

		if($model instanceof Model)
		{
			$name = $model->getTable().'.'.$name;
		}
		else
		{
			$name = $model->getModel()->getTable().'.'.$name;
		}

		return $scope ? $this->$scope($model) : $model->where($name,'=',$this->getValue());
	}

	/**
	 * Get the input
	 * @return \App\Core\unknown
	 */
	public function getInput()
	{
		return 'text';
	}

	public function searchCompany($model, $values='')
	{
		if(!$values)
		{
			$values = $this->getValue();
		}

		$table = ($model instanceof Model) ? $model->getTable() : $model->getModel()->getTable();

		return $model->whereNested(function($query) use ($values, $table) {
			$query->where($table.'.id','like','%'.$values.'%', 'or');
			$query->Orwhere($table.'.name','like','%'.$values.'%', 'or');
			$query->Orwhere($table.'.description','like','%'.$values.'%', 'or');
			$query->Orwhere($table.'.code','like','%'.$values.'%', 'or');
			$query->Orwhere($table.'.duration','like','%'.$values.'%', 'or');
			$query->Orwhere($table.'.minimum_attendee','like','%'.$values.'%', 'or');
			$query->Orwhere($table.'.maximum_attendee','like','%'.$values.'%', 'or');
		});
	}


}

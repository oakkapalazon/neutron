<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Factories\ModelFactory;
use App\Filters\SearchFilter;
use Flash;

class KitMasterController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $select = [
      'kit_master.id',
      'kit_type',
      'customer_name',
      'descn',
      'process',
      'remarks',
      'po_no',
      'uom_code'
    ];

    $kit_masters = ModelFactory::getInstance('KitMaster')
                    ->leftjoin('customer', 'customer.id', '=', 'kit_master.customer_id')
                    ->leftjoin('uom_master', 'uom_master.id', '=', 'kit_master.uom_id')
                    ->select($select);

    $searchFilter = SearchFilter::searchKeyword($kit_masters, $request->q, 'kit_master');
    $kit_masters = $searchFilter->paginate(50);

    // check part master and part type master are assigned in kit master table
    if(count($kit_masters) > 0)
    {
      foreach($kit_masters as $row)
      {
        $row->no_of_partmaster = \App\Models\PartMaster::where('kit_master_id', $row->id)->count();
        $row->no_of_parttypemaster = \App\Models\PartTypeMaster::where('kit_master_id', $row->id)->count();
      }
    }

    return view('kit_master.index')->with('kit_masters', $kit_masters);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $customers = \App\Models\Customer::getCustomers();
    $uom_masters = \App\Models\UomMaster::getUOMMasters();

    return view('kit_master.create', compact('uom_masters', 'customers'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $input = $request->except('_token');

    $validator = \Validator::make($request->all(), [
      'customer_id' => 'required',
      'descn' => 'required',
      'kit_type' => 'required|max:255|unique:kit_master',
      'uom_id' => 'required',
    ]);

    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }

    if(isset($request->po_date))
		{
			$request->po_date = str_replace('/', '-', $request->po_date);
			$request->po_date = date("Y-m-d", strtotime($request->po_date));
		}

    $kit_master = ModelFactory::getInstance('KitMaster');
    $kit_master->customer_id = $request->customer_id;
    $kit_master->descn = $request->descn;
    $kit_master->kit_type = $request->kit_type;
    $kit_master->po_no = $request->po_no;
    $kit_master->process = $request->process;
    $kit_master->remarks = $request->remarks;
    $kit_master->uom_id = $request->uom_id;

    if($kit_master->save())
    {
      Flash::success( 'Successfully created kit master.' );
      return redirect()->back();
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $kit_master = ModelFactory::getInstance('KitMaster')->find($id);
    $customers = \App\Models\Customer::getCustomers();
    $uom_masters = \App\Models\UomMaster::getUOMMasters();

    return view('kit_master.edit', compact('kit_master', 'customers', 'uom_masters'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $input = $request->except('_token');

    $validator = \Validator::make($request->all(), [
      'customer_id' => 'required',
      'descn' => 'required',
      'kit_type' => 'required|max:255|unique:kit_master,kit_type,'.$request->id. ',id',
      'uom_id' => 'required',
    ]);

    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }

    if(isset($request->po_date))
		{
			$request->po_date = str_replace('/', '-', $request->po_date);
			$request->po_date = date("Y-m-d", strtotime($request->po_date));
		}

    $kit_master = ModelFactory::getInstance('KitMaster')->find($request->id);
    $kit_master->customer_id = $request->customer_id;
    $kit_master->descn = $request->descn;
    $kit_master->kit_type = $request->kit_type;
    $kit_master->po_no = $request->po_no;
    $kit_master->process = $request->process;
    $kit_master->remarks = $request->remarks;
    $kit_master->uom_id = $request->uom_id;

    if($kit_master->save())
    {
      Flash::success( 'Successfully created kit master.' );
      return redirect()->back();
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $kit_master = ModelFactory::getInstance('KitMaster')->findOrFail($id);

    if( empty( $kit_master ) )
    {
      Flash::error( 'Selected kit master not found.' );
    }

    $kit_master->delete();

    Flash::success( 'Successfully deleted kit master.' );
    return redirect()->back();
  }
}

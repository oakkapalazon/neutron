<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Factories\ModelFactory;
use App\Filters\SearchFilter;
use Flash;
use Response;
use Carbon\Carbon;

class PostCleaningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $select = [
            'good_receipt_header.id',
            'good_receipt_header.job_status',
            'grn_no',
            'customer.customer_name',
            'job_id'
        ];

        $good_receipts = ModelFactory::getInstance('GoodReceiptHeader')->leftjoin('customer', 'customer.id', 'good_receipt_header.customer_id')
            ->select($select)
            ->orderBy('good_receipt_header.created_at', 'desc')
            ->get();

        return view('post_cleaning.index', compact('good_receipts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (isset($_GET['grn_id'])) {
            set_time_limit(0);

            // auto update empty data point values
//            $get_nullpoint = ModelFactory::getInstance('GoodPartDetail')::join('good_kit_detail', 'good_kit_detail.id', '=', 'good_part_detail.good_kit_detail_id')
//                ->join('param_datapoint_master', 'param_datapoint_master.part_type_id', '=', 'good_part_detail.part_type_id')
//                ->join('good_param_datapoint_detail', 'good_param_datapoint_detail.param_datapoint_master_id', '=', 'param_datapoint_master.id')
//                ->whereNull('param_datapoint_value');
//
//            $result = $get_nullpoint
//                ->join('parameter_master', 'parameter_master.id', '=', 'param_datapoint_master.parameter_master_id')
//                ->join('good_receipt_header', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id')
////                ->join('good_param_datapoint_detail', 'good_param_datapoint_detail.param_datapoint_master_id', '=', 'param_datapoint_master.id')
////                ->whereNull('param_datapoint_value')
//                ->where('good_receipt_header.id', $_GET['grn_id'])
//                ->groupBy('param_datapoint_master_id')
//                ->select('param_datapoint_master.kit_master_id', 'param_datapoint_master.part_type_id', 'good_part_detail.serial_no', 'param_datapoint_master.parameter_master_id', 'param_datapoint_master.datapoint', 'good_param_datapoint_detail.id', 'good_kit_detail.set_id')
//                ->get();
            $result = ModelFactory::getInstance('GoodPartDetail')::join('good_kit_detail', 'good_kit_detail.id', '=', 'good_part_detail.good_kit_detail_id')
                ->join('good_param_datapoint_detail', 'good_param_datapoint_detail.good_part_detail_id', '=', 'good_part_detail.id')
                ->where('good_kit_detail.good_receipt_header_id', $_GET['grn_id'])
                ->whereNull('param_datapoint_value')
                ->select('kit_master_id', 'set_id', 'part_type_id', 'good_part_detail.serial_no', 'param_datapoint_master_id', 'good_param_datapoint_detail.id')
                ->get();

            if (count($result) > 0) {
                foreach ($result as $row => $nullpoint) {
//                    $get_latestpoint_notnull = ModelFactory::getInstance('GoodPartDetail')::join('good_kit_detail', 'good_kit_detail.id', '=', 'good_part_detail.good_kit_detail_id')
//                        ->join('param_datapoint_master', 'param_datapoint_master.part_type_id', '=', 'good_part_detail.part_type_id')
//                        ->join('good_param_datapoint_detail', 'good_param_datapoint_detail.param_datapoint_master_id', '=', 'param_datapoint_master.id')
//                        ->whereNotNull('param_datapoint_value');
//
//                    $get_latestpoint = $get_latestpoint_notnull
//                        ->join('parameter_master', 'parameter_master.id', '=', 'param_datapoint_master.parameter_master_id')
//                        ->join('good_receipt_header', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id')
////                        ->join('good_param_datapoint_detail', 'good_param_datapoint_detail.param_datapoint_master_id', '=', 'param_datapoint_master.id')
////                        ->whereNotNull('param_datapoint_value')
//                        ->where('param_datapoint_master.kit_master_id', $nullpoint->kit_master_id)
//                        ->where('param_datapoint_master.part_type_id', $nullpoint->part_type_id)
//                        ->where('good_part_detail.serial_no', $nullpoint->serial_no)
//                        ->where('param_datapoint_master.parameter_master_id', $nullpoint->parameter_master_id)
//                        ->where('param_datapoint_master.datapoint', $nullpoint->datapoint)
//                        ->where('good_kit_detail.set_id', $nullpoint->set_id)
//                        ->orderBy('good_param_datapoint_detail.updated_at', 'desc')
//                        ->select('param_datapoint_value', 'avg')
//                        ->first();

                    $get_latestpoint = ModelFactory::getInstance('GoodPartDetail')::join('good_kit_detail', 'good_kit_detail.id', '=', 'good_part_detail.good_kit_detail_id')
                        ->join('good_param_datapoint_detail', 'good_param_datapoint_detail.good_part_detail_id', '=', 'good_part_detail.id')
                        ->whereNotNull('param_datapoint_value')
                        ->where('set_id', $nullpoint->set_id)
                        ->where('kit_master_id', $nullpoint->kit_master_id)
                        ->where('part_type_id', $nullpoint->part_type_id)
                        ->where('good_part_detail.serial_no', $nullpoint->serial_no)
                        ->where('param_datapoint_master_id', $nullpoint->param_datapoint_master_id)
                        ->orderBy('good_param_datapoint_detail.updated_at', 'desc')
                        ->select('param_datapoint_value', 'avg')
                        ->first();

                    if ($get_latestpoint) {

                        $part_type_master = ModelFactory::getInstance('PartTypeMaster')::find($nullpoint->part_type_id);
                        $param_datapoint = ModelFactory::getInstance('ParameterDatapointMaster')::find($nullpoint->param_datapoint_master_id);
                        $param_master = ModelFactory::getInstance('ParameterMaster')::find($param_datapoint->parameter_master_id);

                        $update_datapoint = ModelFactory::getInstance('GoodParamDataPointMaster')::find($nullpoint->id);

                        // reset all values
                        if($part_type_master->initial_required == 1)
                        {
                            // if it is boolean
                            if($param_master->is_boolean == 1)
                            {
                                $update_datapoint->param_datapoint_value = $get_latestpoint->param_datapoint_value;
                                $update_datapoint->avg = $get_latestpoint->avg;
                            }
                            else
                            {
                                $update_datapoint->param_datapoint_value = 0;
                                $update_datapoint->avg = 0;
                            }
                        }
                        else
                        {
                            $update_datapoint->param_datapoint_value = $get_latestpoint->param_datapoint_value;
                            $update_datapoint->avg = $get_latestpoint->avg;
                        }

                        $update_datapoint->save();
                    }
                }
            }
            // end auto update empty data point values

            $grn_id = $_GET['grn_id'];

            $select = [
                'good_receipt_header.id',
                'grn_no',
                'good_kit_detail.id as good_kit_detail_id',
                'good_kit_detail.kit_master_id',
                'good_part_detail.part_type_id',
                'kit_type',
                'part_type',
                'good_part_detail_id',
                'param_datapoint_value',
                'good_part_detail.serial_no',
                'good_kit_detail.set_id'
            ];

            $grn_row = ModelFactory::getInstance('GoodReceiptHeader')::find($grn_id);

            $customer = ModelFactory::getInstance('Customer')::where('id', $grn_row->customer_id)
                ->select('customer_code', 'customer_name', 'customer_address')
                ->first();

            $grn_detail_header = ModelFactory::getInstance('GoodPartDetail')::join('part_type_master', 'part_type_master.id', '=', 'good_part_detail.part_type_id')
                ->join('good_kit_detail', 'good_kit_detail.id', '=', 'good_part_detail.good_kit_detail_id')
                ->join('good_receipt_header', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id')
                ->join('kit_master', 'kit_master.id', '=', 'good_kit_detail.kit_master_id')
                ->join('good_param_datapoint_detail', 'good_param_datapoint_detail.good_part_detail_id', '=', 'good_part_detail.id')
                ->where('good_receipt_header.id', $grn_id)
                ->GroupBy('good_kit_detail_id')
                ->orderBy('good_kit_detail.kit_master_id', 'asc')
                ->select($select)
                ->get();

            $grn_detail = ModelFactory::getInstance('GoodPartDetail')::join('part_type_master', 'part_type_master.id', '=', 'good_part_detail.part_type_id')
                ->join('good_kit_detail', 'good_kit_detail.id', '=', 'good_part_detail.good_kit_detail_id')
                ->join('good_receipt_header', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id')
                ->join('kit_master', 'kit_master.id', '=', 'good_kit_detail.kit_master_id')
                ->join('good_param_datapoint_detail', 'good_param_datapoint_detail.good_part_detail_id', '=', 'good_part_detail.id')
                ->where('good_receipt_header.id', $grn_id)
                ->GroupBy('good_param_datapoint_detail.good_part_detail_id')
                ->orderBy('good_kit_detail.kit_master_id', 'asc')
                ->select($select)
                ->get();

            if (count($grn_detail) > 0) {
                return view('post_cleaning.create', compact('grn_detail_header', 'grn_detail', 'grn_row', 'customer'));
            } else {
                Flash::error('There is no kit assigned to this grn.');

                return redirect()->route('post_cleaning.index');
            }

        } else {
            return view('post_cleaning.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function postGRNDetail(Request $request)
    {
        $input = $request->except('_token');
        $boolean = false;

        // $get_parameter_master = ModelFactory::getInstance('ParameterMaster')::where('is_boolean', '0')
        //                         ->select('id')
        //                         ->get();

        //insert parameter values into good_param_datapoint_detail table
        for ($i = 0; $i < count($request->good_param_datapoint_detail_id); $i++) {
            $json[] = $request->good_param_datapoint_detail_id[$i];
            $good_param_datapoint = ModelFactory::getInstance('GoodParamDataPointMaster')->find($request->good_param_datapoint_detail_id[$i]);
            $good_param_datapoint->param_datapoint_value = $request->value[$i];
            $good_param_datapoint->save();
        }

        // below codes are to update average value in good param datapoint detail table
        $good_kit_detail = ModelFactory::getInstance('GoodKitDetail')::find($request->good_kit_detail_id);

        $kit_master_id = $good_kit_detail->kit_master_id;
        $part_type_id = $request->part_type_id;

        $part_param_master = ModelFactory::getInstance('PartParamMaster')::where('kit_master_id', $kit_master_id)
            ->where('part_type_id', $part_type_id)
            ->select('parameter_master_id')
            ->get();

        foreach ($part_param_master as $ppm) {
            $get_gpd_pdm_ids = ModelFactory::getInstance('GoodPartDetail')->join('good_kit_detail', 'good_kit_detail.id', '=', 'good_part_detail.good_kit_detail_id')
                ->join('good_param_datapoint_detail', 'good_part_detail_id', '=', 'good_part_detail.id')
                ->join('param_datapoint_master', 'param_datapoint_master.id', '=', 'good_param_datapoint_detail.param_datapoint_master_id')
                ->where('good_part_detail.good_kit_detail_id', $request->good_kit_detail_id)
                ->where('good_param_datapoint_detail.good_part_detail_id', $request->good_part_detail_id)
                ->where('parameter_master_id', $ppm->parameter_master_id)
                ->select('good_part_detail_id', 'param_datapoint_master_id')
                ->get();

            $get_avg = ModelFactory::getInstance('GoodPartDetail')->join('good_kit_detail', 'good_kit_detail.id', '=', 'good_part_detail.good_kit_detail_id')
                ->join('good_param_datapoint_detail', 'good_part_detail_id', '=', 'good_part_detail.id')
                ->join('param_datapoint_master', 'param_datapoint_master.id', '=', 'good_param_datapoint_detail.param_datapoint_master_id')
                ->where('good_part_detail.good_kit_detail_id', $request->good_kit_detail_id)
                ->where('good_param_datapoint_detail.good_part_detail_id', $request->good_part_detail_id)
                ->where('parameter_master_id', $ppm->parameter_master_id)
                ->avg('param_datapoint_value');

            $param_master = ModelFactory::getInstance('ParameterMaster')->find($ppm->parameter_master_id);

            if($param_master->is_boolean == 1)
            {
                $boolean = true;
                $get_avg = false;
            }

            // if it is not boolean
            if ($get_avg || $get_avg == 0) {
                if(!$boolean)
                {
                    foreach ($get_gpd_pdm_ids as $data) {
                        $part_param_master = ModelFactory::getInstance('GoodParamDataPointMaster')::where('good_part_detail_id', $data->good_part_detail_id)
                            ->where('param_datapoint_master_id', $data->param_datapoint_master_id)
                            ->update([
                                'avg' => $get_avg
                            ]);
                    }
                }
            }
            $test[] = $get_avg;
        }

        //get parameter_master_id without boolean
        // if($get_parameter_master)
        // {
        //   foreach($get_parameter_master as $row)
        //   {
        //     $parameter_master_id[] = $row->id;
        //   }
        // }

        //get hidden input of parameter_master_id
        // foreach($request->parameter_master_id as $param_master)
        // {
        //   $input_parameter_master_id[] = $param_master;
        // }

        //comparing two arrays whether have same value
        // $result_parameter_master_id = array_intersect($parameter_master_id, $input_parameter_master_id);

        // if there is same value between two arrays (if yes -> need to update avg_param_value)
        // if(count($result_parameter_master_id) > 0)
        // {
        //   foreach($result_parameter_master_id as $param_master_id)
        //   {
        //     $good_param_datapoint = ModelFactory::getInstance('KitMaster')->join('part_type_master', 'kit_master.id', '=', 'part_type_master.kit_master_id')
        //                             ->join('param_datapoint_master', 'part_type_master.id', '=', 'param_datapoint_master.part_type_id')
        //                             ->join('good_param_datapoint_detail', 'param_datapoint_master.id', '=', 'good_param_datapoint_detail.param_datapoint_master_id')
        //                             ->where('part_type_master.kit_master_id', $request->kit_master_id)
        //                             ->where('part_type_master.id', $request->part_type_id)
        //                             ->where('parameter_master_id', $param_master_id)
        //                             ->where('good_part_detail_id', $request->good_part_detail_id)
        //                             ->avg('param_datapoint_value');
        //
        //     $save_avg_measurement = ModelFactory::getInstance('GoodPartDetail')->find($request->good_part_detail_id);
        //     $save_avg_measurement->avg_measurement = $good_param_datapoint;
        //     $save_avg_measurement->save();
        //   }
        // }

        return Response::json(['data' => 'success', 'result' => $json, 'test' => $test], 200);
    }
}

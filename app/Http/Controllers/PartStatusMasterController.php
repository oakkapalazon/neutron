<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Factories\ModelFactory;
use App\Filters\SearchFilter;
use Flash;

class PartStatusMasterController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $select = [
      'id',
      'status_code',
      'description'
    ];

    $part_status = ModelFactory::getInstance('PartStatusMaster')->select($select);
    $searchFilter = SearchFilter::searchKeyword($part_status, $request->q, 'part_status_master');
    $part_status = $searchFilter->paginate(50);

    if(count($part_status) > 0)
    {
      foreach($part_status as $row)
      {
        $row->no_of_record = \App\Models\GoodPartDetail::where('actions', $row->id)->count();
      }
    }

    return view('part_status_master.index', compact('part_status'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('part_status_master.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $input = $request->except('_token');

    $validator = \Validator::make($request->all(), [
      'status_code' => 'required|unique:part_status_master|max:255',
      'description' => 'required',
    ]);

    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }

    $part_status = ModelFactory::getInstance('PartStatusMaster');
    $part_status->status_code = $request->status_code;
    $part_status->description = $request->description;

    if($part_status->save())
    {
      Flash::success( 'Successfully created part status master.' );
      return redirect()->back();
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $part_status = ModelFactory::getInstance('PartStatusMaster')->findOrFail($id);

    return view('part_status_master.edit', compact('part_status'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $input = $request->except('_token');

    $validator = \Validator::make($request->all(), [
      'status_code' => 'required|max:255|unique:part_status_master,status_code,'.$request->id,
      'description' => 'required',
    ]);

    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }

    $part_status = ModelFactory::getInstance('PartStatusMaster')->findOrFail($request->id);
    $part_status->status_code = $request->status_code;
    $part_status->description = $request->description;

    if($part_status->save())
    {
      Flash::success( 'Successfully updated part status master.' );
      return redirect()->back();
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $part_status_master = ModelFactory::getInstance('PartStatusMaster')->findOrFail($id);

    if( empty( $part_status_master ) )
    {
      Flash::error( 'Selected part status master not found.' );
    }

    $part_status_master->delete();

    Flash::success( 'Successfully deleted part status master.' );
    return redirect()->back();
  }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Factories\ModelFactory;
use App\Filters\SearchFilter;
use Flash;
use Illuminate\Validation\Rule;

class PartMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $select = [
            'part_master.id',
            'kit_type',
            'part_type',
            'serial_no',
            'set_id',
            'specs',
            'status',
            'additional_info',
            'part_master.grade',
            'part_master.remarks',
            'part_master.created_at',
            'kit_master.id as kit_master_id',
            'part_master.part_type_id'
        ];

        $part_masters = ModelFactory::getInstance('PartMaster')
            ->leftjoin('kit_master', 'kit_master.id', '=', 'part_master.kit_master_id')
            ->leftjoin('part_type_master', 'part_type_master.id', '=', 'part_master.part_type_id')
            ->select($select);

        $searchFilter = SearchFilter::searchKeyword($part_masters, $request->q, 'part_master');
        $part_masters = $searchFilter->paginate(10000);

        if (count($part_masters) > 0) {
            foreach ($part_masters as $row) {

                $row->no_of_goodpart = ModelFactory::getInstance('GoodKitDetail')
                    ->join('good_part_detail', 'good_kit_detail.id', 'good_part_detail.good_kit_detail_id')
                    ->where('good_kit_detail.kit_master_id', $row->kit_master_id)
                    ->where('part_type_id', $row->part_type_id)
                    ->where('good_part_detail.serial_no', $row->serial_no)
                    ->count();
            }
        }

        return view('part_master.index')->with('part_masters', $part_masters);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kit_types = \App\Models\KitMaster::getKitTypes();
        $part_type_master = \App\Models\PartTypeMaster::getPartTypeMaster();
        $part_status_master = \App\Models\PartStatusMaster::getStatusDesc();

        return view('part_master.create', compact('kit_types', 'part_type_master', 'part_status_master'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->except('_token');

        if (!empty($request->part_type_id)) {
            $get_part_type = ModelFactory::getInstance('PartTypeMaster')->where('id', $request->part_type_id)
                ->select('kit_identifier')
                ->get();

            $get_part_type_kit = $get_part_type[0]->kit_identifier;

            if ($get_part_type_kit == 1) {
                request()->validate([
                    'kit_master_id' => 'required',
                    'part_type_id' => 'required',
                    // 'serial_no' => 'required|unique_with:part_master,part_type_id,kit_master_id',
                    'set_id' => 'required',
                    'serial_no' => [
                        'required',
                        Rule::unique('part_master')->where(function ($query) use ($request) {
                            return $query
                                ->wherePartTypeId($request->part_type_id)
                                ->whereSerialNo($request->serial_no)
                                ->whereKitMasterId($request->kit_master_id);
                        }),
                    ],
                ],
                    [
                        'serial_no.unique' => __('Serial no. already exists for this kit and part type', [
                            'part_type_id' => $request->part_type_id,
                            'serial_no' => $request->serial_no,
                            'kit_master_id' => $request->kit_master_id
                        ]),
                    ]);
            }
        }

        // advanced validation includes validate unique with kit_master_id, part_type_id and serial_no
        // $validator = \Validator::make($request->all(), [
        request()->validate([
            'kit_master_id' => 'required',
            'part_type_id' => 'required',
            // 'serial_no' => 'required|unique_with:part_master,part_type_id,kit_master_id',
            // 'set_id' => 'min:0',
            'serial_no' => [
                'required',
                Rule::unique('part_master')->where(function ($query) use ($request) {
                    return $query
                        ->wherePartTypeId($request->part_type_id)
                        ->whereSerialNo($request->serial_no)
                        ->whereKitMasterId($request->kit_master_id);
                }),
            ],
        ],
            [
                'serial_no.unique' => __('Serial no. already exists for this kit and part type', [
                    'part_type_id' => $request->part_type_id,
                    'serial_no' => $request->serial_no,
                    'kit_master_id' => $request->kit_master_id
                ]),
            ]);

        // if ($validator->fails())
        // {
        //   return redirect()->back()->withErrors($validator)->withInput();
        // }

        $part_master = ModelFactory::getInstance('PartMaster');
        $part_master->kit_master_id = $request->kit_master_id;
        $part_master->part_type_id = $request->part_type_id;
        $part_master->serial_no = $request->serial_no;
        $part_master->set_id = $request->set_id;
        $part_master->specs = $request->specs;
        $part_master->status = $request->status;
        $part_master->remarks = $request->remarks;
        $part_master->additional_info = $request->additional_info;
        $part_master->grade = $request->grade;
        $part_master->refurb_count = $request->refurb_count;

        if ($part_master->save()) {
            $arf_table = ModelFactory::getInstance('AccumulatedRF')::where('kit_master_id', $request->kit_master_id)
                ->where('part_type_id', $request->part_type_id)
                ->where('serial_no', $request->serial_no)
                ->get();

            if (count($arf_table) == 0) {
                // // insert into accumulated rf table
                $insertarf_table = ModelFactory::getInstance('AccumulatedRF');
                $insertarf_table->kit_master_id = $request->kit_master_id;
                $insertarf_table->part_type_id = $request->part_type_id;
                $insertarf_table->serial_no = $request->serial_no;
                // first time default is 0
                $insertarf_table->arf = 0;
                $insertarf_table->save();
            }

            Flash::success('Successfully created part master.');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $part_master = ModelFactory::getInstance('PartMaster')->findorFail($id);
        $kit_masters = \App\Models\KitMaster::getKitTypes();
        $part_type_masters = \App\Models\PartTypeMaster::getPartTypeMaster($part_master->kit_master_id);
        $part_status_master = \App\Models\PartStatusMaster::getStatusDesc();

        $no_of_goodpart = ModelFactory::getInstance('GoodKitDetail')
            ->join('good_part_detail', 'good_kit_detail.id', 'good_part_detail.good_kit_detail_id')
            ->where('good_kit_detail.kit_master_id', $part_master->kit_master_id)
            ->where('part_type_id', $part_master->part_type_id)
            ->where('good_part_detail.serial_no', $part_master->serial_no)
            ->count();

        return view('part_master.edit', compact('part_master', 'kit_masters', 'part_type_masters', 'part_status_master', 'no_of_goodpart'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->except('_token');

        $get_part_type = ModelFactory::getInstance('PartTypeMaster')->where('id', $request->hidden_part_type_id)
            ->select('kit_identifier', 'id')
            ->get();

        $get_part_type_kit = $get_part_type[0]->kit_identifier;

        if ($get_part_type_kit == 1) {
            request()->validate([
                'kit_master_id' => 'required',
                'part_type_id' => 'required',
                'set_id' => 'required',
                'serial_no' => [
                    'required',
                    Rule::unique('part_master')->where(function ($query) use ($request) {
                        return $query
                            ->wherePartTypeId($request->part_type_id)
                            ->whereSerialNo($request->serial_no)
                            ->whereKitMasterId($request->kit_master_id);
                    })->ignore($request->id),
                ],
            ],
                [
                    'serial_no.unique' => __('Serial no. already exists for this kit and part type', [
                        'part_type_id' => $request->part_type_id,
                        'serial_no' => $request->serial_no,
                        'kit_master_id' => $request->kit_master_id
                    ]),
                ]);
        }

        // $validator = \Validator::make($request->all(), [
        //   'kit_master_id' => 'required',
        //   'part_type_id' => 'required',
        //   'set_id' => 'numeric|min:0',
        //   'serial_no' => 'required',
        //                  Rule::unique('part_master')->ignore($request->id)
        // ]);

        request()->validate([
            'kit_master_id' => 'required',
            'part_type_id' => 'required',
            // 'serial_no' => 'required|unique_with:part_master,part_type_id,kit_master_id',
            // 'set_id' => 'min:0',
            'serial_no' => [
                'required',
                Rule::unique('part_master')->where(function ($query) use ($request) {
                    return $query
                        ->wherePartTypeId($request->part_type_id)
                        ->whereSerialNo($request->serial_no)
                        ->whereKitMasterId($request->kit_master_id);
                })->ignore($request->id),
            ],
        ],
            [
                'serial_no.unique' => __('Serial no. already exists for this kit and part type', [
                    'part_type_id' => $request->part_type_id,
                    'serial_no' => $request->serial_no,
                    'kit_master_id' => $request->kit_master_id
                ]),
            ]);

        // if ($validator->fails())
        // {
        //   return redirect()->back()->withErrors($validator)->withInput();
        // }

        $part_master = ModelFactory::getInstance('PartMaster')->findOrFail($request->id);
        $part_master->kit_master_id = $request->kit_master_id;
        $part_master->part_type_id = $request->part_type_id;
        $part_master->serial_no = $request->serial_no;
        $part_master->set_id = $request->set_id;
        $part_master->specs = $request->specs;
        $part_master->status = $request->status;
        $part_master->remarks = $request->remarks;
        $part_master->additional_info = $request->additional_info;
        $part_master->grade = $request->grade;
        $part_master->refurb_count = $request->refurb_count;

        if ($part_master->save()) {
            $arf_table = ModelFactory::getInstance('AccumulatedRF')::where('kit_master_id', $request->kit_master_id)
                ->where('part_type_id', $request->part_type_id)
                ->where('serial_no', $request->serial_no)
                ->get();

            if (count($arf_table) == 0) {
                // // insert into accumulated rf table
                $insertarf_table = ModelFactory::getInstance('AccumulatedRF');
                $insertarf_table->kit_master_id = $request->kit_master_id;
                $insertarf_table->part_type_id = $request->part_type_id;
                $insertarf_table->serial_no = $request->serial_no;
                // first time default is 0
                $insertarf_table->arf = 0;
                $insertarf_table->save();
            }

            Flash::success('Successfully created part master.');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $part_master = \App\Models\PartMaster::findOrFail($id);

        if (empty($part_master)) {
            Flash::error('Selected part master not found.');
        }

        $part_master->delete();

        Flash::success('Successfully deleted part master.');
        return redirect()->back();
    }
}

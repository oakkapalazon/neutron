<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Factories\ModelFactory;
use App\Filters\SearchFilter;
use Flash;

class PartParamMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $select = [
            'part_param_master.id',
            'kit_type',
            'part_type',
            'param',
            'parameter_master_id',
            'point_msr',
            'min_limit',
            'max_limit',
            'specs',
            'exclude_post_cleaning',
            'print_pom_value',
            'print_average_value',
            'part_type_master.id as part_type_id',
            'kit_master.id as kit_master_id'
        ];

        $part_param_masters = ModelFactory::getInstance('PartParamMaster')
            ->leftjoin('kit_master', 'kit_master.id', '=', 'part_param_master.kit_master_id')
            ->leftjoin('part_type_master', 'part_type_master.id', '=', 'part_param_master.part_type_id')
            ->leftjoin('parameter_master', 'parameter_master.id', '=', 'part_param_master.parameter_master_id')
            ->select($select);

        $searchFilter = SearchFilter::searchKeyword($part_param_masters, $request->q, 'part_param_master');
        $part_param_masters = $searchFilter->paginate(10000);

        // check part master and part type master are assigned in kit master table
        if (count($part_param_masters) > 0) {
            foreach ($part_param_masters as $row) {
                $row->no_of_paramdatapoint = \App\Models\ParameterDatapointMaster::where('kit_master_id', $row->kit_master_id)
                    ->where('part_type_id', $row->part_type_id)
                    ->where('parameter_master_id', $row->parameter_master_id)
                    ->count();
            }
        }

        return view('part_param_master.index', compact('part_param_masters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kit_masters = \App\Models\KitMaster::getKitTypes();
        $part_type_masters = \App\Models\PartTypeMaster::getPartTypeMaster();
        $param_masters = \App\Models\ParameterMaster::getParameterMaster();

        $param_datapint = ModelFactory::getInstance('ParameterDatapointMaster')
            ->select('parameter_master_id')
            ->GroupBy('parameter_master_id')
            ->get();

        return view('part_param_master.create', compact('kit_masters', 'part_type_masters', 'param_masters'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->except('_token');

        $validator = \Validator::make($request->all(), [
            'kit_master_id' => 'required',
            'part_type_id' => 'required',
            'parameter_master_id' => 'required',
            'point_msr' => 'required',
            // 'min_limit' => 'required',
            // 'max_limit' => 'required',
            // 'specs' => 'required',
            // 'exclude_post_cleaning' => 'required',
            // 'print_pom_value' => 'required',
            // 'print_average_value' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $part_param = ModelFactory::getInstance('PartParamMaster');
        $part_param->kit_master_id = $request->kit_master_id;
        $part_param->part_type_id = $request->part_type_id;
        $part_param->parameter_master_id = $request->parameter_master_id;
        $part_param->point_msr = $request->point_msr;
        $part_param->min_limit = $request->min_limit;
        $part_param->max_limit = $request->max_limit;
        $part_param->specs = $request->specs;
        $part_param->exclude_post_cleaning = $request->exclude_post_cleaning ? 1 : 0 ?? 0;
        $part_param->print_pom_value = $request->print_pom_value ? 1 : 0 ?? 0;
        $part_param->print_average_value = $request->print_average_value ? 1 : 0 ?? 0;

        if ($part_param->save()) {
            // check parameter datapoint is already exist or not
            $old_param_datapoint = ModelFactory::getInstance('ParameterDatapointMaster')
                ->where('parameter_master_id', $request->parameter_master_id)
                ->get();

            // remove first if parameter datapoint
            if ($old_param_datapoint) {
                ModelFactory::getInstance('ParameterDatapointMaster')
                    ->where('parameter_master_id', $request->parameter_master_id)
                    ->delete();
            }

            $alphabet = range('A', 'Z');

            // generate new parameter datapoint
            for ($i = 0; $i < (int)$part_param->point_msr; $i++) {
                $param_datapoint = ModelFactory::getInstance('ParameterDatapointMaster');
                $param_datapoint->kit_master_id = $request->kit_master_id;
                $param_datapoint->part_type_id = $request->part_type_id;
                $param_datapoint->parameter_master_id = $request->parameter_master_id;
                $param_datapoint->datapoint = $alphabet[$i];
                $result = $param_datapoint->save();
            }

            // if($result)
            // {
            //   $parameter_master = ModelFactory::getInstance('ParameterMaster')->find($request->parameter_master_id);
            //   $parameter_master->is_assign = 1;
            //   $parameter_master->save();
            // }

            Flash::success('Successfully created part parameter master.');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $part_param_master = ModelFactory::getInstance('PartParamMaster')->findOrFail($id);
        $kit_masters = \App\Models\KitMaster::getKitTypes();
        $part_type_masters = \App\Models\PartTypeMaster::getPartTypeMaster($part_param_master->kit_master_id);
        $param_masters = \App\Models\ParameterMaster::getParameterMaster();

        return view('part_param_master.edit', compact('part_param_master', 'kit_masters', 'param_masters', 'part_type_masters'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->except('_token');

        $validator = \Validator::make($request->all(), [
            // 'kit_master_id' => 'required',
            // 'part_type_id' => 'required',
            'parameter_master_id' => 'required',
            'point_msr' => 'required',
            // 'min_limit' => 'required',
            // 'max_limit' => 'required',
            // 'specs' => 'required',
            // 'exclude_post_cleaning' => 'required',
            // 'print_pom_value' => 'required',
            // 'print_average_value' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $part_param = ModelFactory::getInstance('PartParamMaster')->findOrFail($request->id);
        // $part_param->kit_master_id = $request->kit_master_id;
        // $part_param->part_type_id = $request->part_type_id;
        $part_param->parameter_master_id = $request->parameter_master_id;
        $part_param->point_msr = $request->point_msr;
        $part_param->min_limit = $request->min_limit;
        $part_param->max_limit = $request->max_limit;
        $part_param->specs = $request->specs;
        $part_param->exclude_post_cleaning = $request->exclude_post_cleaning ? 1 : 0 ?? 0;
        $part_param->print_pom_value = $request->print_pom_value ? 1 : 0 ?? 0;
        $part_param->print_average_value = $request->print_average_value ? 1 : 0 ?? 0;

        if ($part_param->save()) {
            Flash::success('Successfully updated part parameter master.');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $part_param_master = ModelFactory::getInstance('PartParamMaster')->findOrFail($id);

        if (empty($part_param_master)) {
            Flash::error('Selected part parameter master not found.');
        }

        $part_param_master->delete();

        Flash::success('Successfully deleted part parameter master.');
        return redirect()->back();
    }
}

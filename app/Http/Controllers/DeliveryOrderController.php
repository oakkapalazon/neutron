<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Factories\ModelFactory;
use App\Filters\SearchFilter;
use Flash;
use Carbon\Carbon;

class DeliveryOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      // $host = env('DB_HOST');
      //   $user = env('DB_USERNAME');
      //   $password = env('DB_PASSWORD');
      //   $database = env('DB_DATABASE','pts');
      //   $filename = "backup-" . Carbon::now()->format('Y-m-d') . ".sql";
      //   $command = "\"C:\\xampp\\mysql\\bin\\mysqldump.exe\" --user={$user} --password={$password} --host={$host} --flush-logs --single-transaction {$database} > " .storage_path(). "/backup/" .$filename;
      //   // $command = "\"C:\\xampp\\mysql\\bin\\mysqldump.exe\" --user=" . $user ." --password=" . $password . " --host=" . $host . " " . $database . "  | gzip > C:/xampp/mysql/" . $filename;

      //   exec($command);

      $select = [
        'delivery_order.id',
        'do_no',
        'po_no',
        'date',
        'remarks',
        'status',
        'grn_no',
        'delivery_order.job_id',
        'customer.customer_code'
      ];

      $delivery_order = ModelFactory::getInstance('DeliveryOrder')
                        ->join('good_receipt_header', 'delivery_order.good_receipt_header_id', '=', 'good_receipt_header.id')
                        ->join('customer', 'good_receipt_header.customer_id', '=', 'customer.id')
                        ->select($select)
                        ->orderBy('delivery_order.id', 'desc');

      $searchFilter = SearchFilter::searchKeyword($delivery_order, $request->q, 'delivery_order');
      $delivery_order = $searchFilter->paginate(10000);

        if (count($delivery_order) > 0) {
            foreach ($delivery_order as $row) {
                $row->no_of_grn_child = \App\Models\DeliveryOrderChild::where('delivery_order_id', $row->id)->count();
            }
        }

      return view('delivery_order.index', compact('delivery_order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      $customer_code = \App\Models\Customer::getCustomerCodesOnly();

      //update and review action
      if(isset($request->delivery_order_id))
      {
        $delivery_order_child = ModelFactory::getInstance('DeliveryOrderChild')
            ->where('delivery_order_id', $request->delivery_order_id)
            ->get();

        $row_do_child = count($delivery_order_child);
        $grn = \App\Models\GoodReceiptHeader::getGoodReceiptHeader();
        $showed_child = 'yes';

        $status = array(
          '' => 'Please select',
          'O' => 'Open',
          'C' => 'Closed'
        );

        return view('delivery_order.create', compact('grn', 'status', 'showed_child', 'customer_code', 'row_do_child'));
      }

      //add new action
      else
      {
        // check latest row id in delivery order table
        $grn = \App\Models\GoodReceiptHeader::getGoodReceiptHeaderOpenStatus();
        $row = ModelFactory::getInstance('DeliveryOrder')->all()->last();
        $delivery_order_status = '';

        $status = array(
          '' => 'Please select',
          'O' => 'Open',
        );

        $row = ModelFactory::getInstance('RunningNo')->where('name', 'do')->first();

        $run_no = $row->number;
        $year = date('Y');
        $do_no =  'D-' . $year . '-' . $run_no;
        $row_do_child = 0;

        // if($row)
        // {
        //   $do_no = $row->do_no;
        //   $year = date('Y');
        //   $get_sequence = explode(' ', str_replace('-', ' ', $do_no));
        //   $no = $get_sequence[2] + 1;
        //   $do_no =  'D-' . $year . '-' . $no;
        // }
        //
        // else
        // {
        //   $year = date('Y');
        //   $do_no =  'D-' . $year . '-1';
        // }

        return view('delivery_order.create', compact('grn', 'status', 'do_no', 'run_no', 'delivery_order_status', 'customer_code', 'row_do_child'));
      }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $insert_do = false;
      $totChildQty = 0;
      $totActQty = 0;

      if(isset($request->delivery_order_id))
      {
        $request->validate([
          'grn_no' => 'required',
          'date' => 'date_format:"d/m/Y"|required',
          'status' => 'required',
          'po_no' => 'required',
          'job_id' => 'required',
          'cust_code' => 'required'
        ]);
      }

      else
      {
        $request->validate([
          'do_no' => 'required|unique:delivery_order',
          'grn_no' => 'required',
          'date' => 'date_format:"d/m/Y"|required',
          // 'status' => 'required',
          'po_no' => 'required',
          'job_id' => 'required',
          'cust_code' => 'required'
        ]);
      }

      $request->date = str_replace('/', '-', $request->date);
			$request->date = date('Y-m-d', strtotime($request->date));

      if(isset($request->delivery_order_id))
      {
        // check if close DO
        if($request->status == 'C')
        {
          $getResult = ModelFactory::getInstance( 'GoodReceiptHeader' )::join( 'good_kit_detail', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id' )
                        ->join( 'kit_master', 'good_kit_detail.kit_master_id', '=', 'kit_master.id' )
                        ->join( 'reason_master', 'good_kit_detail.reason_master_id', '=', 'reason_master.id' )
                        ->join( 'delivery_order', 'good_receipt_header.id', '=', 'delivery_order.good_receipt_header_id' )
                        ->where( 'good_receipt_header.id', $request->grn_no_hidden )
                        ->select( 'delivery_order.id', 'kit_master_id', 'tool_kit', 'good_kit_detail.serial_no', 'good_receipt_header.job_id', 'good_kit_detail.quantity' )
                        ->groupby( 'good_kit_detail.id' )
                        ->get();

          // to get total quantity in do child
          $getDoChild = ModelFactory::getInstance( 'DeliveryOrderChild' )
                        ->where( 'job_id', $getResult[0]->job_id )
                        ->get();

          // if there is records
          if(count($getResult) > 0)
          {
            foreach($getResult as $row)
            {
              $totActQty = $totActQty + $row->quantity;
            }

            if(count($getDoChild) > 0)
            {
              foreach($getDoChild as $do)
              {
                // sum up actual qty
                $totChildQty = $totChildQty + $do->qty;
              }
            }

            // check do child quantity compares with actual quantity, ables to close DO when it is matched
            if($totActQty != $totChildQty)
            {
              Flash::error( 'Disallow close DO for partial order delivered!' );

              return redirect()->route('delivery_order.create', ['delivery_order_id' => $request->delivery_order_id]);
            }
          }
        }

        $delivery_order = ModelFactory::getInstance('DeliveryOrder')->find($request->delivery_order_id);
        $delivery_order->good_receipt_header_id = $request->grn_id;
        $delivery_order->date = $request->date;
        $delivery_order->remarks = $request->remarks;
        $delivery_order->status = $request->status;
        $delivery_order->po_no = $request->po_no;
        $delivery_order->job_id = $request->job_id;
      }

      else
      {
        $insert_do = true;

        $delivery_order = ModelFactory::getInstance('DeliveryOrder');
        $delivery_order->do_no = $request->do_no;
        $delivery_order->good_receipt_header_id = $request->grn_id;
        $delivery_order->date = $request->date;
        $delivery_order->po_no = $request->po_no;
        $delivery_order->remarks = $request->remarks;
        $delivery_order->job_id = $request->job_id;
        $delivery_order->status = 'O';
      }

      if($delivery_order->save())
      {
        if($insert_do)
        {
          $new_run_no = $request->run_no + 1;
          $running_no = ModelFactory::getInstance('RunningNo')->where('name', 'do')
                        ->update([
                          'number' => $new_run_no
                        ]);
        }

        //if delivery order is closed
        if($request->status == 'C')
        {
          //find closed delivery order
          $delivery_order = ModelFactory::getInstance('DeliveryOrder')::join('delivery_order_child', 'delivery_order.id', '=', 'delivery_order_child.delivery_order_id')
                            ->where('delivery_order.id', $request->delivery_order_id)
                            ->where('delivery_order.status', 'C')
                            ->select('delivery_order_child.kit_master_id', 'tool_id', 'serial_no', 'delivery_order.good_receipt_header_id', 'delivery_order.job_id')
                            ->get();

          // to close good kit detail
          foreach($delivery_order as $row)
          {
              $doc = ModelFactory::getInstance('DeliveryOrder')::join('delivery_order_child', 'delivery_order.id', '=', 'delivery_order_child.delivery_order_id')
                  ->where('kit_master_id', $row->kit_master_id)
                  ->where('tool_id', $row->tool_id)
                  ->where('delivery_order.job_id', $row->job_id)
                  ->where('status', 'C');

              $gkd = ModelFactory::getInstance('GoodKitDetail')::where('good_receipt_header_id', $row->good_receipt_header_id)
                  ->where('kit_master_id', $row->kit_master_id)
                  ->where('tool_kit', $row->tool_id);

              // check whether output of serial no is null
              if(is_null($row->serial_no))
              {
                  $gkd->whereNull('serial_no');
                  $doc->whereNull('serial_no');
              }
              else
              {
                  $gkd->where('serial_no', $row->serial_no);
                  $doc->where('serial_no', $row->serial_no);
              }

              // comparison between qty in grn and qty in do
              if($gkd->first()->quantity == $doc->sum('qty'))
              {
                  $result = ModelFactory::getInstance('GoodKitDetail')::where('good_receipt_header_id', $row->good_receipt_header_id)
                      ->where('kit_master_id', $row->kit_master_id)
                      ->where('tool_kit', $row->tool_id)
                      ->where('serial_no', $row->serial_no)
                      ->update(['closed' => '1']);
              }

            $good_receipt_header_id = $row->good_receipt_header_id;
          }

          $good_receipt_header = ModelFactory::getInstance('GoodReceiptHeader')::join('good_kit_detail', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id')
                                 ->where('good_receipt_header.id', $request->grn_no_hidden)
                                 ->where('good_kit_detail.closed', '0')
                                 ->select('good_receipt_header.id as good_receipt_header_id', 'good_kit_detail.kit_master_id')
                                 ->get();

          // check all good kit details based on same grn has already been closed
          if(count($good_receipt_header) == 0)
          {
            $update_good_receipt_header = ModelFactory::getInstance('GoodReceiptHeader')->find($good_receipt_header_id);
            $update_good_receipt_header->job_status = 'closed';
            $update_good_receipt_header->save();
          }

          Flash::success( 'Successfully updated delivery order.' );

          return redirect()->route('delivery_order.create', ['delivery_order_id' => $request->delivery_order_id]);
        }

        Flash::success( 'Successfully created delivery order.' );

        return redirect()->route('delivery_order.create', ['delivery_order_id' => $delivery_order->id]);
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $delivery_order = ModelFactory::getInstance('DeliveryOrder')->find($id);

      if($delivery_order)
      {
        $get_do_child = ModelFactory::getInstance('DeliveryOrderChild')->where('delivery_order_id', $id)->get();

        if(count($get_do_child) > 0)
        {
          ModelFactory::getInstance('DeliveryOrderChild')->where('delivery_order_id', $id)->delete();
        }

        $delete_delivery_order = ModelFactory::getInstance('DeliveryOrder')->find($id)->delete();

        Flash::success( 'Successfully deleted delivery order.' );
        return redirect( route( 'delivery_order.index' ) );
      }

      Flash::success( 'Selected delivery order not found.' );
      return redirect( route( 'delivery_order.index' ) );
    }

    public function reopenDo(Request $request)
    {
      $totChildQty = 0;
      $totActQty = 0;
      
      $getResult = ModelFactory::getInstance( 'GoodReceiptHeader' )::join( 'good_kit_detail', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id' )
                    ->join( 'kit_master', 'good_kit_detail.kit_master_id', '=', 'kit_master.id' )
                    ->join( 'reason_master', 'good_kit_detail.reason_master_id', '=', 'reason_master.id' )
                    ->join( 'delivery_order', 'good_receipt_header.id', '=', 'delivery_order.good_receipt_header_id' )
                    ->where( 'good_receipt_header.id', $request->grn_id )
                    ->select( 'delivery_order.id', 'kit_master_id', 'tool_kit', 'good_kit_detail.serial_no', 'good_receipt_header.job_id', 'good_kit_detail.quantity' )
                    ->groupby( 'good_kit_detail.id' )
                    ->get();

      // to get total quantity in do child
      $getDoChild = ModelFactory::getInstance( 'DeliveryOrderChild' )
                    ->where( 'job_id', $getResult[0]->job_id )
                    ->get();

      // if there is records
      if(count($getResult) > 0)
      {
        foreach($getResult as $row)
        {
          $totActQty = $totActQty + $row->quantity;
        }

        if(count($getDoChild) > 0)
        {
          foreach($getDoChild as $do)
          {
            // sum up actual qty
            $totChildQty = $totChildQty + $do->qty;
          }
        }

        // check do child quantity compares with actual quantity, ables to close DO when it is matched
        if($totActQty != $totChildQty)
        {
          Flash::error( 'Disallow reopen DO for partial order delivered!' );

          return redirect()->route('delivery_order.create', ['delivery_order_id' => $request->do_id]);
        }
      }

      // update status of DO to open
      $delivery_order = ModelFactory::getInstance('DeliveryOrder')->findorFail($request->do_id);
      $delivery_order->status = 'O';

      // if DO has already open
      if($delivery_order->save())
      {
        // update status of grn to open
        $update_good_receipt_header = ModelFactory::getInstance('GoodReceiptHeader')->find($request->grn_id);
        $update_good_receipt_header->job_status = 'open';

        // if grn has already open
        if($update_good_receipt_header->save())
        {
          $Do = ModelFactory::getInstance('DeliveryOrder')::join('delivery_order_child', 'delivery_order.id', '=', 'delivery_order_child.delivery_order_id')
                ->where('delivery_order.id', $request->do_id)
                ->where('delivery_order.status', 'O')
                ->select('delivery_order_child.kit_master_id', 'tool_id', 'serial_no', 'delivery_order.good_receipt_header_id')
                ->get();

          // to open good kit detail based on grn no.
          foreach($Do as $row)
          {
            $result = ModelFactory::getInstance('GoodKitDetail')::where('good_receipt_header_id', $row->good_receipt_header_id)
                        ->where('kit_master_id', $row->kit_master_id)
                        ->where('tool_kit', $row->tool_id)
                        ->where('serial_no', $row->serial_no)
                        ->update(['closed' => '0']);

            Flash::success( 'Successfully reopen delivery order.' );
          }
        }
        else
        {
          Flash::error( 'Fail to reopen good receipt note.' );
        }
      }
      else
      {
        Flash::error( 'Fail to reopen delivery order.' );
      }

      return redirect()->route('delivery_order.create', ['delivery_order_id' => $request->do_id]);
    }
}

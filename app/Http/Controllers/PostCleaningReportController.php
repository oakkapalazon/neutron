<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Factories\ModelFactory;
use Flash;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\PostCleanExport;
use Session;

class PostCleaningReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $customer_code = \App\Models\Customer::getCustomerCodesOnly();
      $kit_types = \App\Models\KitMaster::getKitTypes();

      return view('post_cleaning_report.index', compact('customer_code', 'kit_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
        'do_no' => 'required'
      ]);

      $select = [
        'job_id',
        'delivery_order.date',
        'kit_master.process',
        'kit_master.kit_type',
        'tool_kit',
        'delivery_order.po_no',
        'set_id'
      ];

      $do_detail = ModelFactory::getInstance( 'GoodReceiptHeader' )::join('good_kit_detail', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id' )
                    ->join( 'kit_master', 'good_kit_detail.kit_master_id', '=', 'kit_master.id' )
                    ->join( 'delivery_order', 'good_receipt_header.id', '=', 'delivery_order.good_receipt_header_id' )
                    ->where( 'do_no', $request->do_no )
                    ->select( $select )
                    ->get();

      return view('post_cleaning_report.post-report-print', compact('do_detail'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function printPostCleaningReport(Request $request)
    {
        Session::forget(['printrf_arf']);

        session([
          'printrf_arf' => $request->print,
        ]);

        foreach($request->hidden_select_kit as $row => $kit)
        {
            if($kit == 1)
            {
               $good_kit_detail_id[] = $request->good_kit_detail_id[$row];
//              $kit_master_id[] = $request->kit_master_id[$row];
//              $tool_kit[] = $request->tool_kit[$row];
//              $set_id[] = $request->set_id[$row];
            }
        }

        // if(!empty($request->customer_code) || !empty($request->kit_type) || !empty($request->job_id)
        // || !empty($request->set_id) || !empty($request->grn_no))
        if( !empty($request->customer_code) || !empty($request->job_id) || !empty($kit_master_id) )
        {
            // $request->validate([
            //   'do_no' => 'required'
            // ]);
            //
            // $do_child = ModelFactory::getInstance( 'DeliveryOrder' )::join('delivery_order_child', 'delivery_order.id', '=', 'delivery_order_child.delivery_order_id' )
            //             ->where( 'do_no', $request->do_no )
            //             ->select( 'kit_master_id' )
            //             ->get();

            $select = [
              'good_receipt_header.job_id',
              'kit_master.process',
              'kit_master.kit_type',
              'tool_kit',
              'set_id',
              'customer_name',
              // 'kit_master.po_no',
              // 'delivery_order.date',
              'good_kit_detail.id as good_kit_detail_id',
              'good_kit_detail.kit_master_id',
              'good_receipt_header.id as good_receipt_header_id'
            ];

            $do_detail = ModelFactory::getInstance( 'GoodReceiptHeader' )::join('good_kit_detail', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id' )
                          ->join( 'kit_master', 'good_kit_detail.kit_master_id', '=', 'kit_master.id' )
                          // ->join( 'delivery_order', 'good_receipt_header.id', '=', 'delivery_order.good_receipt_header_id' )
                          ->join( 'customer', 'good_receipt_header.customer_id', '=', 'customer.id' )
                          // ->where( 'good_receipt_header.job_status', 'closed' )
                          // ->where( 'delivery_order.status', 'C' )
                          // ->groupBy( 'kit_master.id' )
                          ->orderBy( 'grn_no', 'asc' )
                          ->orderBy( 'customer_code', 'asc' )
                          ->orderBy( 'kit_master_id', 'asc' );

            // $do_detail = ModelFactory::getInstance( 'GoodReceiptHeader' )::join('good_kit_detail', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id' )
            //               ->join( 'kit_master', 'good_kit_detail.kit_master_id', '=', 'kit_master.id' )
            //               ->join( 'delivery_order', 'good_receipt_header.id', '=', 'delivery_order.good_receipt_header_id' )
            //               ->join( 'customer', 'good_receipt_header.customer_id', '=', 'customer.id' )
            //               ->where( 'do_no', $request->do_no )
            //               ->where( 'good_receipt_header.job_status', 'closed' )
            //               ->where( 'delivery_order.status', 'C' )
            //               ->whereIn( 'kit_master.id', $do_child )
            //               ->select( $select )
            //               ->get();

            if(!empty($request->customer_code))
            {
              $do_detail = $do_detail->where('customer_code', $request->customer_code);
            }

//            if(!empty($kit_master_id))
//            {
//              $do_detail = $do_detail->whereIn('kit_master_id', $kit_master_id);
//              $do_detail = $do_detail->whereIn('tool_kit', $tool_kit);
//              $do_detail = $do_detail->whereIn('set_id', $set_id);
//            }

            if(!empty($good_kit_detail_id))
            {
                $do_detail = $do_detail->whereIn('good_kit_detail.id', $good_kit_detail_id);
            }

            if(!empty($request->job_id))
            {
              $do_detail = $do_detail->where('good_receipt_header.job_id', $request->job_id);
            }

            // if(!empty($request->set_id))
            // {
            //   $do_detail = $do_detail->where('set_id', $request->set_id);
            // }

            // if(!empty($request->grn_no))
            // {
            //   $do_detail = $do_detail->where('grn_no', $request->grn_no);
            // }

            $do_detail = $do_detail->select($select)->get();
//            dd($do_detail, $request->all());

            return view('post_cleaning_report.post-report-print', compact('do_detail'));
        }
        // else
        // {
        //     Flash::error( 'Please input at least one field.' );
        //     return redirect( route( 'post_cleaning_report.index' ) );
        // }
    }

    public function exportPostCleaningReport(Request $request)
    {
        Session::forget(['printrf_arf']);

        session([
            'printrf_arf' => $request->print,
        ]);

        foreach($request->hidden_select_kit as $row => $kit)
        {
            if($kit == 1)
            {
                $good_kit_detail_id[] = $request->good_kit_detail_id[$row];
            }
        }

        if( !empty($request->customer_code) || !empty($request->job_id) || !empty($kit_master_id) )
        {
            $select = [
                'good_receipt_header.job_id',
                'kit_master.process',
                'kit_master.kit_type',
                'tool_kit',
                'set_id',
                'customer_name',
                'good_kit_detail.id as good_kit_detail_id',
                'good_kit_detail.kit_master_id',
                'good_receipt_header.id as good_receipt_header_id'
            ];

            $do_detail = ModelFactory::getInstance( 'GoodReceiptHeader' )::join('good_kit_detail', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id' )
                ->join( 'kit_master', 'good_kit_detail.kit_master_id', '=', 'kit_master.id' )
                ->join( 'customer', 'good_receipt_header.customer_id', '=', 'customer.id' )
                ->orderBy( 'grn_no', 'asc' )
                ->orderBy( 'customer_code', 'asc' )
                ->orderBy( 'kit_master_id', 'asc' );

            if(!empty($request->customer_code))
            {
                $do_detail = $do_detail->where('customer_code', $request->customer_code);
            }

            if(!empty($good_kit_detail_id))
            {
                $do_detail = $do_detail->whereIn('good_kit_detail.id', $good_kit_detail_id);
            }

            if(!empty($request->job_id))
            {
                $do_detail = $do_detail->where('good_receipt_header.job_id', $request->job_id);
            }

            $do_detail = $do_detail->select($select)->get();

            return Excel::download(new PostCleanExport($do_detail), 'post_clean.xlsx');
        }
    }
}

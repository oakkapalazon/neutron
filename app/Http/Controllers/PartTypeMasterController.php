<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Factories\ModelFactory;
use App\Filters\SearchFilter;
use Flash;

class PartTypeMasterController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $select = [
      'part_type_master.id',
      'part_type',
      'kit_type',
      'kit_identifier',
      'grade_required',
      'max_rf_hrs',
      'max_rf_print',
      'part_sequence'
    ];

    $part_type_masters = ModelFactory::getInstance('PartTypeMaster')
                          ->leftjoin('kit_master', 'kit_master.id', '=', 'part_type_master.kit_master_id')
                          ->select($select)
                          ->orderBy('part_type_master.id', 'desc');

    $searchFilter = SearchFilter::searchKeyword($part_type_masters, $request->q, 'part_type_master');
    $part_type_masters = $searchFilter->paginate(10000);

    if(count($part_type_masters) > 0)
    {
      foreach($part_type_masters as $row)
      {
        $row->count = ModelFactory::getInstance('PartMaster')
                      ->where('part_type_id', $row->id)
                      ->count();

        $row->count_pdm = ModelFactory::getInstance('ParameterDatapointMaster')
           ->where('part_type_id', $row->id)
           ->count();
      }
    }

    return view('part_type_master.index')->with('part_type_masters', $part_type_masters);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $kit_types = \App\Models\KitMaster::getKitTypes();

    return view('part_type_master.create', compact('kit_types'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $input = $request->except('_token');

    $validator = \Validator::make($request->all(), [
      'kit_master_id' => 'required',
      'part_type' => 'required|unique:part_type_master|max:255',
      'part_sequence' => 'required',
//      'max_rf_hrs' => 'required|integer',
//      'max_rf_print' => 'required|integer'
    ]);

    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }

    $part_type = ModelFactory::getInstance('PartTypeMaster');
    $part_type->kit_master_id = $request->kit_master_id;
    $part_type->part_type = $request->part_type;
    $part_type->kit_identifier = $request->kit_identifier ? 1 : 0 ?? 0;
    $part_type->grade_required = $request->grade_required ? 1 : 0 ?? 0;
    $part_type->max_rf_hrs = $request->max_rf_hrs;
    $part_type->max_rf_print = $request->max_rf_print;
    $part_type->part_sequence = $request->part_sequence;

    if($part_type->save())
    {
      Flash::success( 'Successfully created part type.' );
      return redirect()->back();
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $part_type_master = ModelFactory::getInstance('PartTypeMaster')->find($id);
    $kit_types = \App\Models\KitMaster::getKitTypes();

    return view('part_type_master.edit', compact('part_type_master', 'kit_types'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $input = $request->except('_token');

    $validator = \Validator::make($request->all(), [
      // 'kit_master_id' => 'required',
      // 'part_type' => 'required|max:255|unique:part_type_master,part_type,'.$request->id,
      'part_sequence' => 'required|integer',
//      'max_rf_hrs' => 'required|integer',
//      'max_rf_print' => 'required|integer'
    ]);

    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }

    $part_type_master = ModelFactory::getInstance('PartTypeMaster')->findOrFail($request->id);
    // $part_type_master->kit_master_id = $request->kit_master_id;
    // $part_type_master->part_type = $request->part_type;
    $part_type_master->kit_identifier = $request->kit_identifier ? 1 : 0 ?? 0;
    $part_type_master->grade_required = $request->grade_required ? 1 : 0 ?? 0;
    $part_type_master->max_rf_hrs = $request->max_rf_hrs;
    $part_type_master->max_rf_print = $request->max_rf_print;
    $part_type_master->part_sequence = $request->part_sequence;

    if($part_type_master->save())
    {
      Flash::success( 'Successfully updated part type master.' );
      return redirect()->back();
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $part_type_master = ModelFactory::getInstance('PartTypeMaster')->findOrFail($id);

    if( empty( $part_type_master ) )
    {
      Flash::error( 'Selected part type master not found.' );
    }

    $part_type_master->delete();

    Flash::success( 'Successfully deleted part type master.' );
    return redirect()->back();
  }
}

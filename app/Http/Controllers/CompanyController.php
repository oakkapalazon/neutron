<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Factories\ModelFactory;
use App\Filters\SearchFilter;
use Flash;

class CompanyController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $select = [
      'id',
      'company_code',
      'company_name',
      'address',
      'website',
      'email',
      'fax',
      'registration_no',
      'tel_no'
    ];

    $compaines = ModelFactory::getInstance('Company')->select($select);
    $searchFilter = SearchFilter::searchKeyword($compaines, $request->q, 'company');
    $compaines = $searchFilter->paginate(10000);

    // check companies are assigned in customer table
    if(count($compaines) > 0)
    {
      foreach($compaines as $row)
      {
        $row->is_assign = ModelFactory::getInstance('Customer')->where('company_id', $row->id)->count();
      }
    }

    return view('companies.index')->with('compaines', $compaines);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('companies.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $input = $request->except('_token');

    $validator = \Validator::make($request->all(), [
      'company_code' => 'required|unique:company|max:255',
      'website' => 'required'
    ]);

    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }

    $company = ModelFactory::getInstance('Company');
    $company->company_code = $request->company_code;
    $company->company_name = $request->company_name;
    $company->address = $request->address;
    $company->website = $request->website;
    $company->email = $request->email;
    $company->fax = $request->fax;
    $company->registration_no = $request->registration_no;
    $company->tel_no = $request->tel_no;

    if($company->save())
    {
      Flash::success( 'Successfully created company.' );
      return redirect()->back();
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $company = ModelFactory::getInstance('Company')->find($id);

    return view('companies.edit')->with('company', $company);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $input = $request->except('_token');

    $validator = \Validator::make($request->all(), [
      'company_code' => 'required|max:255|unique:company,company_code,'.$request->id. ',id',
      'website' => 'required'
    ]);

    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }

    $company = ModelFactory::getInstance('Company')->find($request->id);
    $company->company_code = $request->company_code;
    $company->company_name = $request->company_name;
    $company->address = $request->address;
    $company->website = $request->website;
    $company->email = $request->email;
    $company->fax = $request->fax;
    $company->registration_no = $request->registration_no;
    $company->tel_no = $request->tel_no;

    if($company->save())
    {
      Flash::success( 'Successfully updated company.' );
      return redirect()->back();
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $company = \App\Models\Company::findorfail($id);

    if( empty( $company ) )
    {
      Flash::error( 'Selected company not found.' );
    }

    $company->delete();

    Flash::success( 'Successfully deleted company.' );
    return redirect( route( 'companies.index' ) );
  }
}

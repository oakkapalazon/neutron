<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Factories\ModelFactory;
use App\Filters\SearchFilter;
use Flash;

class VisualConditionMasterController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $select = [
      'id',
      'code',
      'description'
    ];

    $visual_conditions = ModelFactory::getInstance('VisualConditionMaster')->select($select)->get();

    return view('visual_condition_master.index', compact('visual_conditions'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('visual_condition_master.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $input = $request->except('_token');

    $validator = \Validator::make($request->all(), [
      'code' => 'required|unique:visual_condition_master|max:255',
      'description' => 'required',
    ]);

    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }

    $visual_condition = ModelFactory::getInstance('VisualConditionMaster');
    $visual_condition->code = $request->code;
    $visual_condition->description = $request->description;

    if($visual_condition->save())
    {
      Flash::success( 'Successfully created visual condition master.' );
      return redirect()->back();
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $visual_condition = ModelFactory::getInstance('VisualConditionMaster')->findOrFail($id);

    return view('visual_condition_master.edit', compact('visual_condition'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $input = $request->except('_token');

    $validator = \Validator::make($request->all(), [
      'code' => 'required|max:255|unique:visual_condition_master,code,'.$request->id,
      'description' => 'required',
    ]);

    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }

    $visual_condition = ModelFactory::getInstance('VisualConditionMaster')->findOrFail($request->id);
    $visual_condition->code = $request->code;
    $visual_condition->description = $request->description;

    if($visual_condition->save())
    {
      Flash::success( 'Successfully updated visual condition master.' );
      return redirect()->back();
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $visual_condition = ModelFactory::getInstance('VisualConditionMaster')->findOrFail($id);

    if( empty( $visual_condition ) )
    {
      Flash::error( 'Selected visual condition master not found.' );
    }

    $visual_condition->delete();

    Flash::success( 'Successfully deleted visual condition master.' );
    return redirect()->back();
  }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Factories\ModelFactory;
use App\Filters\SearchFilter;
use Flash;
use Response;
use Session;
use Carbon\Carbon;
use App\Models\GoodReceiptHeader;
use Illuminate\Validation\Rule;

class PreCleaningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $select = [
            'good_receipt_header.id',
            'grn_no',
            'customer.customer_code',
            'collection_date',
            'job_id',
            'job_status',
            'narration'
        ];

        $value[] = array(
            'q' => $request->q,
            'date' => $request->search_date
        );

        $latest_grn = ModelFactory::getInstance('GoodReceiptHeader')->orderBy('id', 'desc')->first();

        $good_receipts = ModelFactory::getInstance('GoodReceiptHeader')->leftjoin('customer', 'customer.id', 'good_receipt_header.customer_id')
            ->select($select)
            ->orderBy('good_receipt_header.created_at', 'desc');

        $searchFilter = SearchFilter::searchKeyword($good_receipts, $value, 'pre_cleaning');
        $good_receipts = $searchFilter->paginate(10000);

        if (count($good_receipts) > 0) {
            foreach ($good_receipts as $row) {
                $row->collection_date = Carbon::parse($row->collection_date)->format("d/m/Y");
                $row->no_of_kit_detail = \App\Models\GoodKitDetail::where('good_receipt_header_id', $row->id)->count();
            }
        }

        return view('pre_cleaning.index', compact('good_receipts', 'latest_grn'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $good_receipts = ModelFactory::getInstance('GoodReceiptHeader')
            ->select('id', 'grn_no')
            ->orderBy('id', 'desc')
            ->first();

        $get_count_child = ModelFactory::getInstance('GoodReceiptHeader')->join('good_kit_detail', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id')
            ->where('good_receipt_header_id', $good_receipts->id)
            ->select('good_receipt_header_id')
            ->get();

        // add new and latest grn has no child yet, don't allow to create new
        if (!isset($request->grn_id) && count($get_count_child) == 0) {
            Flash::error('Please complete the transaction for ' .$good_receipts->grn_no. ' before add new grn!');
            return redirect(route('pre_cleaning.index'));
        }

        if (isset($request->grn_id)) {
            $grn = ModelFactory::getInstance('GoodReceiptHeader')->find($request->grn_id);

            Session::forget(['customer_id']);

            session([
                'customer_id' => $grn['customer_id']
            ]);
        }

        $kit_types = \App\Models\KitMaster::getKitTypesBasedCust(session('customer_id'));
        $customers = \App\Models\Customer::getCustomerCodes();
        $reasons = \App\Models\ReasonMaster::getReasonMasters();
        $visual_conditon = \App\Models\VisualConditionMaster::getVisualCondition();
        $part_status = \App\Models\PartStatusMaster::getPartStatus();

        // check latest row id in good receipt header table
        // $row = ModelFactory::getInstance('GoodReceiptHeader')->all()->last();
        $row = ModelFactory::getInstance('RunningNo')->where('name', 'grh')->first();
        // $row = GoodReceiptHeader::all()->last();

        $grn_job_status = '';
        $run_no = $row->number;
        $year = date('Y');
        $grn_no = 'G-' . $year . '-' . $run_no;

        // if($row)
        // {
        //   $year = date('Y');
        //   $no = $row->id + 1;
        //   $grn_no =  'G-' . $year . '-' . $no;
        // }
        //
        // else
        // {
        //   $year = date('Y');
        //   $grn_no =  'G-' . $year . '-1';
        // }

        return view('pre_cleaning.create', compact('kit_types', 'customers', 'reasons', 'grn_no', 'run_no', 'visual_conditon', 'part_status', 'grn_job_status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $insert_grh = false;
        $input = $request->except('_token');

        if (isset($request->id)) {
            $validator = \Validator::make($request->all(), [
                'grn_no' => 'required',
                'collection_date' => 'date_format:"d/m/Y"|required',
                'job_id' => [
                    'required',
                    Rule::unique('good_receipt_header')->ignore($request->id)
                ],

                'job_status' => 'required'
            ]);
        } else {
            $validator = \Validator::make($request->all(), [
                'grn_no' => 'required',
                'collection_date' => 'date_format:"d/m/Y"|required',
                'customer_id' => 'required',
                'job_id' => 'required|unique:good_receipt_header',
                'job_status' => 'required'
            ]);
        }

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        Session::forget(['customer_id']);

        session([
            'customer_id' => $request->customer_id
        ]);

        if (isset($request->collection_date)) {
            $request->collection_date = str_replace('/', '-', $request->collection_date);
            $request->collection_date = date('Y-m-d', strtotime($request->collection_date));
        }

        if (isset($request->id)) {
            $good_receipt = ModelFactory::getInstance('GoodReceiptHeader')->find($request->id);
            $good_receipt->grn_no = $request->grn_no;
            // $good_receipt->customer_id = $request->customer_id;
            $good_receipt->collection_date = $request->collection_date;
            $good_receipt->job_id = $request->job_id;
            $good_receipt->job_status = $request->job_status;
            $good_receipt->narration = $request->narration;
        } else {
            $insert_grh = true;

            $good_receipt = ModelFactory::getInstance('GoodReceiptHeader');
            $good_receipt->grn_no = $request->grn_no;
            $good_receipt->customer_id = $request->customer_id;
            $good_receipt->collection_date = $request->collection_date;
            $good_receipt->job_id = $request->job_id;
            $good_receipt->job_status = $request->job_status;
            $good_receipt->narration = $request->narration;
        }

        $grn_code = $request->grn_no;

        if ($good_receipt->save()) {
            if ($insert_grh) {
                $new_run_no = $request->run_no + 1;
                $running_no = ModelFactory::getInstance('RunningNo')->where('name', 'grh')
                    ->update([
                        'number' => $new_run_no
                    ]);
            }

            Flash::success('Successfully created pre cleaning.');

            return redirect()->route('pre_cleaning.create', [
                'grn_id' => $good_receipt->id
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $good_receipt_header = ModelFactory::getInstance('GoodReceiptHeader')->findorFail($id);

        $count_child = ModelFactory::getInstance('GoodReceiptHeader')->join('good_kit_detail', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id')
            ->where('good_receipt_header.id', $id)
            ->select('good_receipt_header.id')
            ->get();

        // double check if have child record cannot be deleted
        if(count($count_child) > 0)
        {
            Flash::error('You are not allowed to delete selected grn as child record already exists!');
            return redirect(route('pre_cleaning.index'));
        }

        $good_receipt_header = ModelFactory::getInstance('GoodReceiptHeader')->find($id);

        if($good_receipt_header->delete())
        {
            $row = ModelFactory::getInstance('RunningNo')->where('name', 'grh')->first();
            $run_no = $row->number;
            $new_run_no = $run_no - 1;
            $running_no = ModelFactory::getInstance('RunningNo')->where('name', 'grh')
                ->update([
                    'number' => $new_run_no
                ]);
        }

        Flash::success('Successfully deleted pre cleaning.');
        return redirect(route('pre_cleaning.index'));
  }

    // store good kit detail in pre cleaning module
    public function postKitTypePreCleaning(Request $request)
    {
        if (!empty($request->good_kit_detail_id)) {
            $good_kit_detail = ModelFactory::getInstance('GoodKitDetail')->find($request->good_kit_detail_id);
        } else {
            $good_kit_detail = ModelFactory::getInstance('GoodKitDetail');
        }

        $good_kit_detail->good_receipt_header_id = $request->good_receipt_header_id;
        $good_kit_detail->kit_master_id = $request->kit_master_id;
        $good_kit_detail->reason_master_id = $request->reason_master_id;
        $good_kit_detail->tool_kit = $request->tool_kit;
        $good_kit_detail->set_id = $request->set_id;
        $good_kit_detail->serial_no = $request->serial_no;
        $good_kit_detail->quantity = $request->quantity;
        $good_kit_detail->rf = $request->rf;

        if ($request->job_status != 'closed') {
            $good_kit_detail->save();
        }

        $get__id = $good_kit_detail->id;

        return Response::json(array('kit_type_id' => $good_kit_detail->id,), 200);
    }

    // store good part type detail in pre cleaning module
    public function postPartTypePreCleaning(Request $request)
    {
        $row_good_part_detail = ModelFactory::getInstance('GoodPartDetail')->whereIn('part_type_id', $request['part_type_id'])
                                ->whereIn('serial_no', $request['serial_no'])
                                ->where('arf', '')
                                ->where('good_kit_detail_id', '<', $request['good_kit_detail_id'])
                                ->get();

        // without outstanding transactions that have not been saved
        if($row_good_part_detail->count() == 0)
        {
            for ($i = 0; $i < count($request['part_type_id']); $i++) {
                $get_part_type = ModelFactory::getInstance('PartTypeMaster')->find($request['part_type_id'][$i]);

                if (isset($request['good_part_detail_ids'][$i])) {
                    $good_part_detail = ModelFactory::getInstance('GoodPartDetail')->find($request['good_part_detail_ids'][$i]);
                } else {
                    $good_part_detail = ModelFactory::getInstance('GoodPartDetail');
                }

                $good_part_detail->good_kit_detail_id = $request['good_kit_detail_id'];
                $good_part_detail->part_type_id = $request['part_type_id'][$i];
                $good_part_detail->serial_no = $request['serial_no'][$i];
                $good_part_detail->visual_condition = str_replace('/', ',', $request['various_conditions'][$i]);
                $good_part_detail->rf = $request['part_type_rf'][$i];
                $good_part_detail->arf = $request['arf'][$i];
                $good_part_detail->actions = $request['actions'][$i];
                $good_part_detail->part_type_name = $get_part_type->part_type;

                $vc_str = str_replace('/', ',', $request['various_conditions'][$i]);

                // get visual condition description and insert
                if ($vc_str != '') {
                    if (strpos($vc_str, ',') !== false) {
                        $explode_visual = explode(",", $vc_str);

                        foreach ($explode_visual as $array2) {
                            $visual_table = \App\Models\VisualConditionMaster::find($array2);

                            $result_visual[] = $visual_table->description;
                        }

                        $implode_visual = implode(",", $result_visual);
                        $good_part_detail->visual_condition_desc = $implode_visual;
                        $result_visual = array();
                    } else {
                        $visual_table = \App\Models\VisualConditionMaster::find($vc_str);
                        $good_part_detail->visual_condition_desc = $visual_table->description;
                    }
                }

                if ($good_part_detail->save()) {

                    // get group of parameter_master_id in order to get pom
                    $param_datapoint_group = ModelFactory::getInstance('ParameterDatapointMaster')->where('part_type_id', $request['part_type_id'][$i])
                        ->select('id', 'parameter_master_id', 'kit_master_id', 'part_type_id')
                        ->groupBy('parameter_master_id')
                        ->get();

                    $good_param_data_point_master_row = ModelFactory::getInstance('GoodParamDataPointMaster')
                        ->where('good_part_detail_id', $request['good_part_detail_ids'][$i])
                        ->get();

                    // if closed, no need to generate param data points
                    if($request['actions'][$i] <> '2')
                    {
                        if (count($param_datapoint_group) > 0) {
                            // there is no records yet
                            if (count($good_param_data_point_master_row) == 0) {
                                foreach ($param_datapoint_group as $row) {
                                    // loop through to get every pom
                                    $pom = ModelFactory::getInstance('PartParamMaster')->where('kit_master_id', $row->kit_master_id)
                                        ->where('part_type_id', $row->part_type_id)
                                        ->where('parameter_master_id', $row->parameter_master_id)
                                        ->select('point_msr')
                                        ->first();

                                    $param_datapoint = ModelFactory::getInstance('ParameterDatapointMaster')
                                        ->where('kit_master_id', $row->kit_master_id)
                                        ->where('part_type_id', $row->part_type_id)
                                        ->where('parameter_master_id', $row->parameter_master_id)
                                        ->select('id', 'parameter_master_id')
                                        ->orderBy('datapoint', 'asc')
                                        ->get();

                                    foreach ($param_datapoint as $count => $row) {

                                        $good_param_datapoint = ModelFactory::getInstance('GoodParamDataPointMaster');
                                        $good_param_datapoint->good_part_detail_id = $good_part_detail->id;
                                        $good_param_datapoint->param_datapoint_master_id = $row->id;
                                        $good_param_datapoint->save();

                                        // if meet pom value then go out
                                        if ($count == ($pom->point_msr - 1)) {
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $arf_table = ModelFactory::getInstance('AccumulatedRF')::where('kit_master_id', $request->kit_master_id)
                        ->where('part_type_id', $request['part_type_id'][$i])
                        ->where('serial_no', $request['serial_no'][$i])
                        ->get();

                    if (count($arf_table) > 0) {
                        // update accumulated rf table
                        $updatearf_table = ModelFactory::getInstance('AccumulatedRF')::where('kit_master_id', $request->kit_master_id)
                            ->where('part_type_id', $request['part_type_id'][$i])
                            ->where('serial_no', $request['serial_no'][$i])
                            ->update(['arf' => $request['arf'][$i], 'serial_no' => $request['serial_no'][$i]]);
                    } else {
                        // // insert into accumulated rf table
                        $insertarf_table = ModelFactory::getInstance('AccumulatedRF');
                        $insertarf_table->kit_master_id = $request->kit_master_id;
                        $insertarf_table->part_type_id = $request['part_type_id'][$i];
                        $insertarf_table->serial_no = $request['serial_no'][$i];
                        $insertarf_table->arf = $request['arf'][$i];
                        $insertarf_table->save();
                    }
                }
            }

            foreach ($request->part_type_id as $row => $part_type) {
                $part_status_master = ModelFactory::getInstance('PartStatusMaster')::find($request->actions[$row]);

                $part_master = ModelFactory::getInstance('PartMaster')
                    ->where('kit_master_id', $request->kit_master_id)
                    ->where('part_type_id', $part_type)
                    ->where('serial_no', $request['serial_no'][$row])
                    ->select('id')
                    ->get();

                // $part_master_table = ModelFactory::getInstance('PartMaster')::where('kit_master_id', $request->kit_master_id)
                //                     ->where('part_type_id', $part_type)
                //                     // ->where('serial_no', '<>', $serial_no)
                //                     ->where('set_id', $request->set_id)
                //                     ->where('status', 'active')
                //                     ->first();

                $get_kit_iden = ModelFactory::getInstance('KitMaster')
                    ->join('part_type_master', 'kit_master.id', '=', 'part_type_master.kit_master_id')
                    ->where('kit_master.id', $request->kit_master_id)
                    ->where('part_type_master.id', $part_type)
                    ->select('kit_identifier')
                    ->first();

                if ((count($part_master) > 0)) {
                    // update status only
                    $update_action = ModelFactory::getInstance('PartMaster')->where('kit_master_id', $request->kit_master_id)
                        ->where('part_type_id', $part_type)
                        ->where('serial_no', $request->serial_no[$row])
                        // ->where( 'status', 'Active' )
                        ->update([
                            'status' => $part_status_master->description
                        ]);

                    // skip because transfer has been performed
                    continue;
                }

                // insert for new
                $part_master = ModelFactory::getInstance('PartMaster');
                $part_master->kit_master_id = $request->kit_master_id;
                $part_master->part_type_id = $part_type;
                $part_master->serial_no = $request->serial_no[$row];
                $part_master->status = $part_status_master->description;

                // check if it is kit identifier
                if ($get_kit_iden && $get_kit_iden->kit_identifier == 1) {
                    $part_master->set_id = $request->set_id;
                }

                $part_master->save();
            }

            $final = true;
        }
        else
        {
            $final = false;
        }

        return Response::json(array(
            'good_kit_detail_id' => $request['good_kit_detail_id'],
            'result' => count($request['part_type_id']),
            'a' => $request['various_conditions'],
            'final' => $final
        ), 200);
    }

    public function postHeaderGoodReceiptHeader(Request $request)
    {
        $request->collection_date = str_replace('/', '-', $request->collection_date);
        $request->collection_date = date('Y-m-d', strtotime($request->collection_date));

        $good_receipt = ModelFactory::getInstance('GoodReceiptHeader')->find($request->grn_id);
        // $good_receipt->grn_no = $request->grn_no;
        // $good_receipt->customer_id = $request->customer_id;
        $good_receipt->collection_date = $request->collection_date;
        $good_receipt->job_id = $request->job_id;
        $good_receipt->job_status = $request->job_status;
        $good_receipt->narration = $request->narration;

        if ($good_receipt->save()) {
            return Response::json(array('data' => 'success', 'status' => $request->job_status), 200);
        } else {
            return Response::json(array('data' => 'fail'), 200);
        }

    }

    public function postGoodKitDetailMultiple(Request $request)
    {
        $result = $request->all();

        $kit_cust_fol = ModelFactory::getInstance('KitMaster')
            ->join('customer', 'kit_master.customer_id', '=', 'customer.id')
            ->where('kit_master.id', $request->kit_master_id)
            ->select('kit_master.folder_name AS kit_folder_name', 'customer.folder_name AS cust_folder_name')
            ->first();

        foreach ($request->good_kit_detail_id as $row => $id) {
            if (!empty($id)) {
                $good_kit_detail = ModelFactory::getInstance('GoodKitDetail')->find($id);
            } else {
                $good_kit_detail = ModelFactory::getInstance('GoodKitDetail');
            }

            $good_kit_detail->good_receipt_header_id = $request->grn_id;
            $good_kit_detail->kit_master_id = $request->kit_master_id[$row];
            $good_kit_detail->tool_kit = $request->tool_kit[$row];
            $good_kit_detail->reason_master_id = $request->reason_id[$row];
            $good_kit_detail->set_id = $request->set_id[$row];
            $good_kit_detail->serial_no = $request->serial_no[$row];
            $good_kit_detail->quantity = $request->quantity[$row];
            $good_kit_detail->rf = $request->rf[$row];
            $good_kit_detail->save();

            $good_kit_detail_id[] = $good_kit_detail->id;
        }

        return Response::json(array('result' => $result, 'good_kit_detail_id' => $good_kit_detail_id), 200);
    }
}

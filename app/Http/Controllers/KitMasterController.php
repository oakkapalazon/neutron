<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Factories\ModelFactory;
use App\Filters\SearchFilter;
use Flash;
use Response;
use Carbon\Carbon;
use File;

class KitMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $select = [
            'kit_master.id',
            'kit_type',
            'customer_code',
            'customer_name',
            'descn',
            'process',
            'remarks',
            // 'po_no',
            'uom_code'
        ];

        $kit_masters = ModelFactory::getInstance('KitMaster')
            ->leftjoin('customer', 'customer.id', '=', 'kit_master.customer_id')
            ->leftjoin('uom_master', 'uom_master.id', '=', 'kit_master.uom_id')
            ->select($select)
            ->orderBy('customer_code', 'asc')
            ->orderBy('kit_type', 'asc');

        $searchFilter = SearchFilter::searchKeyword($kit_masters, $request->q, 'kit_master');
        $kit_masters = $searchFilter->paginate(10000);

        // check part master and part type master are assigned in kit master table
        if (count($kit_masters) > 0) {
            foreach ($kit_masters as $row) {
                $row->no_of_partmaster = \App\Models\PartMaster::where('kit_master_id', $row->id)->count();
                $row->no_of_parttypemaster = \App\Models\PartTypeMaster::where('kit_master_id', $row->id)->count();
            }
        }

        return view('kit_master.index')->with('kit_masters', $kit_masters);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = \App\Models\Customer::getCustomers();
        $uom_masters = \App\Models\UomMaster::getUOMMasters();
        $param_masters = \App\Models\ParameterMaster::getParameterMaster();

        return view('kit_master.create', compact('uom_masters', 'customers', 'param_masters'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (empty($request->kit_master_id)) {
            $validator = \Validator::make($request->all(), [
                'customer_id' => 'required',
                'descn' => 'required',
                'kit_type' => 'required|max:255|unique:kit_master',
                'uom_id' => 'required',
//        'folder_name' => 'unique:kit_master|max:50|nullable'
            ]);
        } else {
            $validator = \Validator::make($request->all(), [
                'customer_id' => 'required',
                'descn' => 'required',
                'uom_id' => 'required',
                'folder_name' => 'unique:kit_master|max:50|nullable'
            ]);
        }

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        if (isset($request->kit_master_id)) {
            $kit_master = ModelFactory::getInstance('KitMaster')->find($request->kit_master_id);
            $kit_master->customer_id = $request->customer_id;
            // $kit_master->po_no = $request->po_no;
            $kit_master->kit_type = $request->kit_type;
            $kit_master->descn = $request->descn;
            $kit_master->process = $request->process;
            $kit_master->remarks = $request->remarks;
            $kit_master->uom_id = $request->uom_id;
//      $kit_master->folder_name = $request->kit_type;
        } else {
            $kit_master = ModelFactory::getInstance('KitMaster');
            $kit_master->customer_id = $request->customer_id;
            // $kit_master->po_no = $request->po_no;
            $kit_master->kit_type = $request->kit_type;
            $kit_master->descn = $request->descn;
            $kit_master->process = $request->process;
            $kit_master->remarks = $request->remarks;
            $kit_master->uom_id = $request->uom_id;
            // folder name is similar to kit type name
            $kit_master->folder_name = $request->kit_type;

            $customer = ModelFactory::getInstance('Customer')->find($request->customer_id);

            if (isset($request->kit_type)) {
                File::makeDirectory(public_path() . '/img/pts/' . $customer->folder_name . '/' . $request->kit_type . '/', 0777, true);
            }
        }

        if ($kit_master->save()) {
            Flash::success('Successfully created Kit Type Master.');

            return redirect()->route('kit_master.create', [
                'kit_master_id' => $kit_master->id
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kit_master_record = ModelFactory::getInstance('KitMaster')->findOrFail($id);

        if (empty($kit_master_record)) {
            Flash::error('Selected kit master not found.');
            return redirect()->back();
        }

        $customer = ModelFactory::getInstance('Customer')->find($kit_master_record->customer_id);

        if (isset($kit_master_record->folder_name)) {
            $delete_fol = File::deleteDirectory(public_path() . '/img/pts/' . $customer->folder_name . '/' . $kit_master_record->folder_name);
        }

        if ($kit_master_record->delete()) {
            Flash::success('Successfully deleted kit master.');
            return redirect()->back();
        }
    }

    // store part type master in kit master module
    public function postPartTypeMaster(Request $request)
    {
        if (!empty($request->part_type_id)) {
            $part_type = ModelFactory::getInstance('PartTypeMaster')->find($request->part_type_id);
        } else {
            $part_type = ModelFactory::getInstance('PartTypeMaster');
        }

        $kit_cust_fol = ModelFactory::getInstance('KitMaster')
            ->join('customer', 'kit_master.customer_id', '=', 'customer.id')
            ->where('kit_master.id', $request->kit_master_id)
            ->select('kit_master.folder_name AS kit_folder_name', 'customer.folder_name AS cust_folder_name')
            ->first();

        $part_type->part_type = $request->part_type;
        $part_type->kit_master_id = $request->kit_master_id;
        $part_type->max_rf_hrs = $request->max_rf_hrs;
        $part_type->max_rf_print = $request->max_rf_print;
        $part_type->part_sequence = $request->part_sequence;
        $part_type->kit_identifier = $request->kit_identifier;
        $part_type->grade_required = $request->grade_required;
        $part_type->initial_required = $request->initial_required;
        $part_type->image_path = 'img/pts/' . $kit_cust_fol->cust_folder_name . '/' . $kit_cust_fol->kit_folder_name . '/' . $request->part_type . '.jpg';
        $part_type->save();

        $no_of_goodpartdetail = \App\Models\GoodPartDetail::where('part_type_id', $request->part_type_id)->count();

        return Response::json(array('part_type_id' => $part_type->id, 'no_of_goodpartdetail' => $no_of_goodpartdetail), 200);
    }

    public function postPartTypeMasterMultiple(Request $request)
    {
        $result = $request->all();

        $kit_cust_fol = ModelFactory::getInstance('KitMaster')
            ->join('customer', 'kit_master.customer_id', '=', 'customer.id')
            ->where('kit_master.id', $request->kit_master_id)
            ->select('kit_master.folder_name AS kit_folder_name', 'customer.folder_name AS cust_folder_name')
            ->first();

        if (isset($request->part_type_id)) {
            foreach ($request->part_type_id as $row => $id) {
                if (!empty($id)) {
                    $part_type = ModelFactory::getInstance('PartTypeMaster')->find($id);
                } else {
                    $part_type = ModelFactory::getInstance('PartTypeMaster');
                }

                $part_type->part_type = $request->part_type[$row];
                $part_type->kit_master_id = $request->kit_master_id;
                $part_type->max_rf_hrs = $request->max_rf_hrs[$row];
                $part_type->max_rf_print = $request->max_rf_print[$row];
                $part_type->part_sequence = $request->part_sequence[$row];
                $part_type->kit_identifier = $request->kit_identifier[$row];
                $part_type->grade_required = $request->grade_required[$row];
                $part_type->initial_required = $request->initial_required[$row];
                $part_type->image_path = 'img/pts/' . $kit_cust_fol->cust_folder_name . '/' . $kit_cust_fol->kit_folder_name . '/' . $request->part_type[$row] . '.jpg';
                $part_type->save();

                $part_type_id[] = $part_type->id;
            }
        }

        if (isset($part_type_id)) {
            $part_type_id[] = $part_type_id;
        } else {
            $part_type_id = '';
        }

        return Response::json(array('result' => $result, 'part_type_id' => $part_type_id), 200);
    }

    // store part parameter master in kit master module
    public function postPartParameter(Request $request)
    {
        // $delete_row = ModelFactory::getInstance('PartParamMaster')
        //               ->where('part_type_id', $request['part_type_id'])
        //               ->get();
        //
        // if(count($delete_row) > 0)
        // {
        //   ModelFactory::getInstance('PartParamMaster')->where('part_type_id', $request['part_type_id'])->delete();
        // }

        foreach ($request['part_param_master_id'] as $row => $ppm_id) // for($i = 0; $i < count($request['parameter_id']); $i++)
        {
            if (!empty($ppm_id)) {
                $part_param = ModelFactory::getInstance('PartParamMaster')->find($ppm_id);
            } else {
                $part_param = ModelFactory::getInstance('PartParamMaster');
            }

            // $part_param = ModelFactory::getInstance('PartParamMaster');
            $part_param->kit_master_id = $request['kit_master_id'];
            $part_param->part_type_id = $request['part_type_id'];
            $part_param->parameter_master_id = $request['parameter_id'][$row];
            $part_param->point_msr = $request['point_msr'][$row];
            $part_param->min_limit = $request['min_limit'][$row];
            $part_param->max_limit = $request['max_limit'][$row];
            $part_param->specs = $request['specs'][$row];
            $part_param->exclude_post_cleaning = $request['exclude_post_cleaning'][$row];
            $part_param->print_pom_value = $request['print_pom_value'][$row];
            $part_param->print_average_value = $request['print_average_value'][$row];

            // save/update part param master table
            if ($part_param->save()) {
                // check parameter datapoint is already exist or not
                // $old_param_datapoint = ModelFactory::getInstance('ParameterDatapointMaster')
                //                     ->where('parameter_master_id', $request['parameter_id'][$i])
                //                     ->where('part_type_id', $request['part_type_id'])
                //                     ->where('kit_master_id', $request['kit_master_id'])
                //                     ->get();
                //
                // // remove first if parameter datapoint
                // if($old_param_datapoint)
                // {
                //   ModelFactory::getInstance('ParameterDatapointMaster')
                //   ->where('parameter_master_id', $request['parameter_id'][$i])
                //   ->where('part_type_id', $request['part_type_id'])
                //   ->where('kit_master_id', $request['kit_master_id'])
                //   ->delete();
                // }

                $param_datapoint_row = ModelFactory::getInstance('ParameterDatapointMaster')
                    ->where('parameter_master_id', $request['parameter_id'][$row])
                    ->where('part_type_id', $request['part_type_id'])
                    ->where('kit_master_id', $request['kit_master_id'])
                    ->get();

                $param_datapoint = ModelFactory::getInstance('ParameterDatapointMaster')
                    ->where('parameter_master_id', $request['parameter_id'][$row])
                    ->where('part_type_id', $request['part_type_id'])
                    ->where('kit_master_id', $request['kit_master_id'])
                    ->max('datapoint');

                // datapoint already exists
                if ($param_datapoint) {
                    $stop_at = $param_datapoint;
                    $stop_at++;
                    $alphabet = range($stop_at, 'Z');

                    $rest_no = $request['point_msr'][$row] - count($param_datapoint_row);

                    // there is an increment of point measurements
                    if ($rest_no > 0) {
                        // add in the rest of parameter datapoint
                        for ($j = 0; $j < $rest_no; $j++) {
                            $param_datapoint = ModelFactory::getInstance('ParameterDatapointMaster');
                            $param_datapoint->kit_master_id = $request['kit_master_id'];
                            $param_datapoint->part_type_id = $request['part_type_id'];
                            $param_datapoint->parameter_master_id = $request['parameter_id'][$row];
                            $param_datapoint->datapoint = $alphabet[$j];
                            $result = $param_datapoint->save();
                        }
                    }
//                    elseif ($rest_no < 0) {
//
//                    }
                } // new datapoint
                else {
                    $alphabet = range('A', 'Z');

                    // generate new parameter datapoint
                    for ($j = 0; $j < (int)$request['point_msr'][$row]; $j++) {
                        $param_datapoint = ModelFactory::getInstance('ParameterDatapointMaster');
                        $param_datapoint->kit_master_id = $request['kit_master_id'];
                        $param_datapoint->part_type_id = $request['part_type_id'];
                        $param_datapoint->parameter_master_id = $request['parameter_id'][$row];
                        $param_datapoint->datapoint = $alphabet[$j];
                        $result = $param_datapoint->save();
                    }
                }
            }
        }

        // return Response::json(array('all' => $request->all()), 200);
        return Response::json(array('kit_master_id' => $request['kit_master_id'], 'all' => $request->all()), 200);
    }

    public function postHeaderKitMaster(Request $request)
    {
        $kit_master = ModelFactory::getInstance('KitMaster')->findOrFail($request->kit_master_id);
        $kit_master->customer_id = $request->customer_id;
        // $kit_master->po_no = $request->po_no;
        $kit_master->kit_type = $request->kit_type;
        $kit_master->descn = $request->descn;
        $kit_master->process = $request->process;
        $kit_master->remarks = $request->remarks;
        $kit_master->uom_id = $request->uom_id;
        $kit_master->save();

        return Response::json(array('data' => 'success'), 200);
    }
}

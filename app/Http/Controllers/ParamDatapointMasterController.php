<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Factories\ModelFactory;
use App\Filters\SearchFilter;
use Flash;

class ParamDatapointMasterController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $select = [
      'param_datapoint_master.id',
      'kit_type',
      'part_type',
      'param',
      'datapoint'
    ];

    $param_datapoint = ModelFactory::getInstance('ParameterDatapointMaster')
                        ->leftjoin('kit_master', 'kit_master.id', '=', 'param_datapoint_master.kit_master_id')
                        ->leftjoin('part_type_master', 'part_type_master.id', '=', 'param_datapoint_master.part_type_id')
                        ->leftjoin('parameter_master', 'parameter_master.id', '=', 'param_datapoint_master.parameter_master_id')
                        ->select($select);

    $searchFilter = SearchFilter::searchKeyword($param_datapoint, $request->q, 'param_datapoint_master');
    $param_datapoint = $searchFilter->paginate(10000);

    return view('param_datapoint_master.index', compact('param_datapoint'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $kit_masters = \App\Models\KitMaster::getKitTypes();

    return view('param_datapoint_master.create', compact('kit_masters'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $input = $request->except('_token');

    $validator = \Validator::make($request->all(), [
      'kit_master_id' => 'required',
      'part_type_id' => 'required',
      'parameter_master_id' => 'required',
      'datapoint' => 'required',
    ]);

    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }

    $param_datapoint = ModelFactory::getInstance('ParameterDatapointMaster');
    $param_datapoint->kit_master_id = $request->kit_master_id;
    $param_datapoint->part_type_id = $request->part_type_id;
    $param_datapoint->parameter_master_id = $request->parameter_master_id;
    $param_datapoint->datapoint = $request->datapoint;

    if($param_datapoint->save())
    {
      Flash::success( 'Successfully created parameter datapoint master.' );
      return redirect()->back();
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $select = [
      'id',
      'part_type'
    ];

    $kit_masters = \App\Models\KitMaster::getKitTypes();
    $param_datapoint = ModelFactory::getInstance('ParameterDatapointMaster')->findOrFail($id);
    $part_type_masters = \App\Models\PartTypeMaster::getPartTypeMaster($param_datapoint->kit_master_id);
    $part_param_masters = \App\Models\PartParamMaster::getPartParamMaster($param_datapoint->part_type_id);

    return view('param_datapoint_master.edit', compact('kit_masters',
      'param_datapoint', 'part_type_masters', 'part_param_masters'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $input = $request->except('_token');

    $validator = \Validator::make($request->all(), [
      'kit_master_id' => 'required',
      'part_type_id' => 'required',
      'parameter_master_id' => 'required',
      'datapoint' => 'required',
    ]);

    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }

    $param_datapoint = ModelFactory::getInstance('ParameterDatapointMaster')->findOrFail($request->id);
    $param_datapoint->kit_master_id = $request->kit_master_id;
    $param_datapoint->part_type_id = $request->part_type_id;
    $param_datapoint->parameter_master_id = $request->parameter_master_id;
    $param_datapoint->datapoint = $request->datapoint;

    if($param_datapoint->save())
    {
      Flash::success( 'Successfully updated parameter datapoint master.' );
      return redirect()->back();
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $param_datapoint = ModelFactory::getInstance('ParameterDatapointMaster')->findOrFail($id);

    if( empty( $param_datapoint ) )
    {
      Flash::error( 'Selected parameter datapoint master not found.' );
    }

    $param_datapoint->delete();

    Flash::success( 'Successfully deleted parameter datapoint master.' );
    return redirect()->back();
  }
}

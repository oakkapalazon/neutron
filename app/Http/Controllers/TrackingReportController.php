<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Factories\ModelFactory;
use App\Filters\SearchFilter;
use App\Exports\TrackingQueryExport;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Session;
use Flash;

class TrackingReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $customer_code = \App\Models\Customer::getCustomerCodesOnly();

      return view('tracking_query_report.index', compact('customer_code'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @return BinaryFileResponse
     */
    public function exportTrackingQueryDetail(Request $request)
    {
        Session::forget(['grn_no', 'customer_code', 'job_id', 'date_from', 'date_to', 'kit_master_id', 'part_type', 'serial_no']);

        session([
//          'grn_no' => $request->hidden_grn_no,
          'set_id' => $request->hidden_set_id,
          'customer_code' => $request->hidden_customer_code,
          'job_id' => $request->hidden_job_id,
          'date_from' => $request->hidden_date_from,
          'date_to' => $request->hidden_date_to,
          'kit_master_id' => $request->hidden_kit_type,
          'part_type' => $request->hidden_part_type,
          'serial_no' => $request->hidden_serial_no
        ]);

        return Excel::download(new TrackingQueryExport(), 'tracking_query.xlsx');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Factories\ModelFactory;
use Flash;
use Response;
use Carbon\Carbon;
use DB;

class AjaxController extends Controller
{
    // get Part Parameter Master Lists
    public function getPartTypeLists(Request $request)
    {
        $kit_master_id = $_GET['kit_master_id'];

        $part_types = ModelFactory::getInstance('ParameterDatapointMaster')->select('part_type_id')
            ->GroupBy('part_type_id')
            ->get();

        $result = ModelFactory::getInstance('PartTypeMaster')::where('kit_master_id', $kit_master_id)
            ->select('id', 'part_type')
            ->orderBy('part_type', 'asc')
            ->get();

        return response()->json(array(
            'data' => $result
        ));
    }

    public function getPartParameterById(Request $request)
    {
        $part_type_id = $_GET['part_type_id'];

        $no_of_goodpartdetail = ModelFactory::getInstance('GoodPartDetail')::where('part_type_id', $part_type_id)->count();

        $result = ModelFactory::getInstance('PartParamMaster')
            ->where('part_type_id', $part_type_id)
            ->get();

        return response()->json(array(
            'data' => $result,
            'no_of_goodpartdetail' => $no_of_goodpartdetail
        ));
    }

    // get parameter lists
    public function getParameterLists(Request $request)
    {
        $kit_master_id = $_GET['kit_master_id'];
        $part_type_id = $_GET['part_type_id'];

        $result = ModelFactory::getInstance('PartParamMaster')
            ->leftjoin('parameter_master', 'parameter_master.id', '=', 'part_param_master.parameter_master_id')
            ->where('kit_master_id', $kit_master_id)
            ->where('part_type_id', $part_type_id)
            ->select('part_param_master.parameter_master_id as id', 'param')
            ->GroupBy('part_param_master.parameter_master_id')
            ->get();

        return response()->json(array(
            'data' => $result
        ));
    }

    // get customer lists
    public function getCustomerLists(Request $request)
    {
        $customer_id = $_GET['customer_id'];

        $select = ['id', 'customer_name'];

        $result = \App\Models\Customer::where('id', $customer_id)->select($select)->first();

        return response()->json(array(
            'data' => $result
        ));
    }

    // get good part details
    public function getGoodPartDetails(Request $request)
    {
        $good_kit_detail_id = $_GET['good_kit_detail_id'];
        $kit_master_id = $_GET['kit_master_id'];
        $set_id = $_GET['set_id'];
        $serial_no = $_GET['serial_no'];
        $part_type_id_array = $_GET['part_type_id_array'];
        $good_receipt_header_id = $_GET['good_receipt_header_id'];

        if ($good_kit_detail_id == '') {
            $good_kit_detail = ModelFactory::getInstance('GoodKitDetail')->latest('id')->first();
            $good_kit_detail_id = $good_kit_detail->id;
        }

        $part_types = ModelFactory::getInstance('PartMaster')->where('kit_master_id', $kit_master_id)
            ->where('set_id', $set_id)
            ->where('serial_no', $serial_no)
            ->orderBy('part_type_id', 'asc')
            ->select('part_type_id')
            ->get();

        $result_kit_part_type = ModelFactory::getInstance('KitMaster')
            ->join('part_type_master', 'kit_master.id', '=', 'part_type_master.kit_master_id')
            ->where('kit_master.id', $kit_master_id)
            ->select('part_type_master.id', 'part_type_master.kit_master_id', 'part_type_master.part_type', 'part_type_master.max_rf_hrs')
            ->get();

        $result_good_part_detail = ModelFactory::getInstance('GoodPartDetail')
            ->where('good_kit_detail_id', $good_kit_detail_id)
            ->select('id', 'good_kit_detail_id', 'part_type_id', 'serial_no')
            ->get();

        // $result_goodPartKit = ModelFactory::getInstance('GoodPartDetail')
        //                       ->join('good_kit_detail', 'good_part_detail.good_kit_detail_id', '=', 'good_kit_detail.id')
        //                       ->where('kit_master_id', $kit_master_id)
        //                       ->whereIn('part_type_id', $part_type_id_array)
        //                       ->select('good_part_detail.id', 'good_kit_detail_id', 'part_type_id', 'good_part_detail.created_at')
        //                       ->orderBy('good_part_detail.created_at', 'desc')
        //                       ->get();

        $result_goodPartKit = ModelFactory::getInstance('GoodReceiptHeader')
            ->join('good_kit_detail', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id')
            ->join('good_part_detail', 'good_kit_detail.id', '=', 'good_part_detail.good_kit_detail_id')
            ->whereIn('part_type_id', $part_type_id_array)
            ->where('good_kit_detail.set_id', $set_id)
            ->select('good_receipt_header.created_at')
            ->orderBy('good_receipt_header.created_at', 'desc')
            ->limit(10)
            ->get();

        // check if part type exists in kit master
        if ($result_kit_part_type) {
            // check if no data exists yet in good part detail table
            if (count($result_good_part_detail) == 0) {
                // check if no previous data exists with similar kit and part type
                if (count($result_goodPartKit) == 0) {
                    // perform insert new part type into good part detail table
                    foreach ($result_kit_part_type as $row => $value) {
                        $part_master_table = false;

                        $kit_part_type_table = ModelFactory::getInstance('KitMaster')
                            ->join('part_type_master', 'kit_master.id', '=', 'part_type_master.kit_master_id')
                            ->where('kit_master.id', $kit_master_id)
                            ->where('part_type_master.id', $value->id)
                            ->where('kit_identifier', 1)
                            ->select('part_type_master.id as part_type_master_id')
                            ->first();

                        if ($kit_part_type_table) {
                            $part_master_table = ModelFactory::getInstance('PartMaster')::where('set_id', $set_id)
                                ->where('status', 'Active')
                                ->where('part_type_id', $kit_part_type_table->part_type_master_id)
                                ->where('kit_master_id', $kit_master_id)
                                ->first();
                        }

                        if ($part_master_table) {
                            $seria_no = $part_master_table->serial_no;
                        } else {
                            $seria_no = '';
                        }

                        $get_tot_rf = ModelFactory::getInstance('GoodKitDetail')->join('good_part_detail', 'good_kit_detail.id', '=', 'good_part_detail.good_kit_detail_id')
                            ->where('kit_master_id', $kit_master_id)
                            ->where('part_type_id', $value->id)
                            ->sum('good_part_detail.rf');

                        // $part_master = ModelFactory::getInstance('PartMaster')
                        //                 ->where('kit_master_id', $kit_master_id)
                        //                 ->where('part_type_id', $value->id)
                        //                 ->where('status', 'active')
                        //                 ->select('serial_no')
                        //                 ->first();

                        $arf[] = $get_tot_rf;

                        $good_part_detail = ModelFactory::getInstance('GoodPartDetail');
                        $good_part_detail->good_kit_detail_id = $good_kit_detail_id;
                        $good_part_detail->part_type_id = $value->id;
                        $good_part_detail->serial_no = $seria_no;
                        $good_part_detail->visual_condition = '';
                        $good_part_detail->rf = '';
                        // $good_part_detail->arf = $arf[$row];
                        $good_part_detail->actions = '1';
                        $good_part_detail->save();

                        // get serial no from part master if have
                        // if($part_master)
                        // {
                        //   $good_part_detail->serial_no = $part_master['serial_no'];
                        // }

                        // if($good_part_detail->save())
                        // {
                        //   $arf_table = ModelFactory::getInstance('AccumulatedRF');
                        //   $arf_table->kit_master_id = $kit_master_id;
                        //   $arf_table->part_type_id = $value->id;
                        //   $arf_table->serial_no = $part_master['serial_no'];
                        //   $arf_table->arf = '0';
                        //   $arf_table->save();
                        // }

                    }
                } else {
                    // $result_last_records = ModelFactory::getInstance('GoodPartDetail')
                    //                       ->join('good_kit_detail', 'good_part_detail.good_kit_detail_id', '=', 'good_kit_detail.id')
                    //                       ->where('kit_master_id', $kit_master_id)
                    //                       ->whereIn('part_type_id', $part_type_id_array)
                    //                       ->where('good_part_detail.created_at', $result_goodPartKit[0]['created_at'])
                    //                       ->where('actions', '!=', 2)
                    //                       ->select('good_part_detail.*')
                    //                       ->orderBy('part_type_id', 'asc')
                    //                       ->get();

                    $result_last_records = ModelFactory::getInstance('GoodReceiptHeader')
                        ->join('good_kit_detail', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id')
                        ->join('good_part_detail', 'good_kit_detail.id', '=', 'good_part_detail.good_kit_detail_id')
                        ->where('kit_master_id', $kit_master_id)
                        ->where('good_kit_detail.set_id', $set_id)
                        ->where('good_receipt_header.created_at', $result_goodPartKit[0]['created_at'])
                        ->where('actions', '!=', 2)
                        ->select('good_part_detail.*')
                        ->orderBy('good_receipt_header.created_at', 'desc')
                        ->get();

                    // perform insert previous data into good part detail table
                    foreach ($result_last_records as $row => $last) {
                        // $get_tot_rf = ModelFactory::getInstance('GoodKitDetail')->join('good_part_detail', 'good_kit_detail.id', '=', 'good_part_detail.good_kit_detail_id')
                        //               ->where('kit_master_id', $kit_master_id)
                        //               ->where('part_type_id', $last->part_type_id)
                        //               ->sum('good_part_detail.rf');

                        // $arf[] = $get_tot_rf;

                        $good_part_detail = ModelFactory::getInstance('GoodPartDetail');
                        $good_part_detail->good_kit_detail_id = $good_kit_detail_id;
                        $good_part_detail->part_type_id = $last->part_type_id;
                        $good_part_detail->serial_no = $last->serial_no;
                        $good_part_detail->visual_condition = $last->visual_condition;
                        $good_part_detail->rf = '';
                        // $good_part_detail->arf = $arf[$row];
                        $good_part_detail->actions = $last->actions;
                        $good_part_detail->save();

                        // if($good_part_detail->save())
                        // {
                        //   $get_arf = ModelFactory::getInstance('AccumulatedRF')::where('kit_master_id', $kit_master_id)
                        //              ->where('part_type_id', $last->part_type_id)
                        //              ->where('serial_no', $last->serial_no)
                        //              ->select('arf')
                        //              ->first();
                        //
                        //   $arf_table = ModelFactory::getInstance('AccumulatedRF')::where('kit_master_id', $kit_master_id)
                        //                ->where('part_type_id', $last->part_type_id)
                        //                ->where('serial_no', $last->serial_no)
                        //                ->update(['arf' => $arf[$row]]);
                        // }
                    }
                }
            } else {
                // perform update good part detail table
//                foreach ($result_good_part_detail as $row => $value) {
//                    $get_arfs = ModelFactory::getInstance('AccumulatedRF')
//                        ->where('kit_master_id', $kit_master_id)
//                        ->where('part_type_id', $value->part_type_id)
//                        ->where('serial_no', $value->serial_no)
//                        ->first();

//                    $good_part_detail = ModelFactory::getInstance('GoodPartDetail')->find($value->id);
                    // $good_part_detail->arf = $arf[$row];

                    // if($good_part_detail->save())
                    // {
                    //   $get_arf = ModelFactory::getInstance('AccumulatedRF')::where('kit_master_id', $kit_master_id)
                    //              ->where('part_type_id', $value->part_type_id)
                    //              ->where('serial_no', $value->serial_no)
                    //              ->select('arf')
                    //              ->first();
                    //
                    //   $arf_table = ModelFactory::getInstance('AccumulatedRF')::where('kit_master_id', $kit_master_id)
                    //              ->where('part_type_id', $value->part_type_id)
                    //              ->where('serial_no', $value->serial_no)
                    //              ->update(['arf' => $arf[$row]]);
                    // }
//                }
            }
        }

        $result = ModelFactory::getInstance('GoodPartDetail')->leftjoin('part_type_master', 'part_type_master.id', '=', 'good_part_detail.part_type_id')
            ->where('good_kit_detail_id', $good_kit_detail_id)
            // ->where('actions', '<>', 2)
            ->select('good_part_detail.*', 'part_type_master.max_rf_hrs', 'kit_master_id')
            ->get();

        // get arf in array
        foreach ($result as $arf) {
            $get_arf = ModelFactory::getInstance('AccumulatedRF')::where('kit_master_id', $arf->kit_master_id)
                ->where('part_type_id', $arf->part_type_id)
                ->where('serial_no', $arf->serial_no)
                ->select('arf')
                ->first();

            if ($get_arf) {
                $arf_array[] = $get_arf->arf;
            } else {
                $arf_array[] = 0;
            }
        }

        //detect whether datapoint values exist in post-cleaning action
        $check_datapoint_value = ModelFactory::getInstance('GoodReceiptHeader')->join('good_kit_detail', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id')
            ->join('good_part_detail', 'good_kit_detail.id', '=', 'good_part_detail.good_kit_detail_id')
            ->join('good_param_datapoint_detail', 'good_part_detail.id', '=', 'good_param_datapoint_detail.good_part_detail_id')
            ->where('good_kit_detail.id', $good_kit_detail_id)
            ->whereNotNull('good_param_datapoint_detail.param_datapoint_value')
            ->select('good_param_datapoint_detail.param_datapoint_value')
            ->get();

        if (count($check_datapoint_value) > 0) {
            $datapoint_exist = true;
        } else {
            $datapoint_exist = false;
        }

        $get_curr_good_kit = ModelFactory::getInstance('GoodReceiptHeader')
            ->join('good_kit_detail', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id')
            ->where('kit_master_id', $kit_master_id)
            ->where('good_receipt_header.id', $good_receipt_header_id);

        if($set_id == '')
        {
            $get_curr_good_kit = $get_curr_good_kit->whereNull('set_id')->select('good_receipt_header.id')->first();
        }
        else
        {
            $get_curr_good_kit = $get_curr_good_kit->where('set_id', $set_id)->select('good_receipt_header.id')->first();
        }

        $get_latest_good_kit = ModelFactory::getInstance('GoodReceiptHeader')
            ->join('good_kit_detail', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id')
            ->where('kit_master_id', $kit_master_id);

        if($set_id == '')
        {
            $get_latest_good_kit = $get_latest_good_kit->whereNull('set_id')->max('good_receipt_header.id');
        }
        else
        {
            $get_latest_good_kit = $get_latest_good_kit->where('set_id', $set_id)->max('good_receipt_header.id');
        }

        // check good kit detail table has new good kit compares to current good kit in order to disable rf input fields
        if($get_latest_good_kit > $get_curr_good_kit->id)
        {
            $latest_good_kit = true;
        }
        else
        {
            $latest_good_kit = false;
        }

        return response()->json(array(
            'data' => $result,
            'part_types' => $part_types,
            'datapoint_exist' => $datapoint_exist,
            'arf_array' => $arf_array,
            'no_of_good_part_detail' => $result_good_part_detail,
            'latest_good_kit' => $latest_good_kit
        ));
    }

    // delete good kit detail in pre cleaning module
    public function getDeleteGoodKitDetailById(Request $request)
    {
        $id = $_GET['id'];

        $delete_child = ModelFactory::getInstance('GoodPartDetail')->where('good_kit_detail_id', $id)->get();
        $delete_row = ModelFactory::getInstance('GoodKitDetail')->find($id);

        if (count($delete_child) > 0) {
            ModelFactory::getInstance('GoodPartDetail')->where('good_kit_detail_id', $id)->delete();
        }

        if ($delete_row) {
            $row = ModelFactory::getInstance('GoodKitDetail')->find($id);
            $row->delete();
        }

        return Response::json(['data' => 'success'], 200);
    }

    // delete good part detail in pre cleaning module
    public function getDeleteGoodPartDetailById(Request $request)
    {
        $id = $_GET['id'];
        $rf = $_GET['rf'];
        $serial_no = $_GET['serial_no'];
        $kit_master_id = $_GET['kit_master_id'];

        $delete_row = ModelFactory::getInstance('GoodPartDetail')->find($id);

        if ($delete_row) {
            if ($delete_row['arf'] != '') {
                $arf_tb = ModelFactory::getInstance('AccumulatedRF')->where('part_type_id', $delete_row['part_type_id'])
                    ->where('serial_no', $serial_no)
                    ->where('kit_master_id', $kit_master_id)
                    ->select('arf', 'id')
                    ->first();

                $output = $arf_tb['arf'] - $rf;

                $save_arf = ModelFactory::getInstance('AccumulatedRF')->find($arf_tb['id']);
                $save_arf->arf = $output;

                if ($save_arf->save()) {
                    $delete_row->delete();
                }
            } else {
                $delete_row->delete();
            }
        }

        return Response::json(['data' => 'success'], 200);
    }

    // delete part type in kit master module
    public function getDeletePartType(Request $request)
    {
        $id = $_GET['id'];

        $delete_child = ModelFactory::getInstance('PartParamMaster')->where('part_type_id', $id)->get();
        $delete_row = ModelFactory::getInstance('PartTypeMaster')->find($id);

        if (count($delete_child) > 0) {
            ModelFactory::getInstance('PartParamMaster')->where('part_type_id', $id)->delete();
        }

        if ($delete_row) {
            $row = ModelFactory::getInstance('PartTypeMaster')->find($id);
            $row->delete();
        }

        $result = $delete_row;

        return Response::json(['data' => 'success', 'result' => $result], 200);
    }

    // delete part parameter in kit master module
    public function getDeletePartParameter(Request $request)
    {
        $id = $_GET['id'];

        $delete_row = ModelFactory::getInstance('PartParamMaster')->find($id);

        if (count($delete_row) > 0) {
            $delete_row->delete();
        }

        return Response::json(['data' => 'success'], 200);
    }

    public function getKitMasterData(Request $request)
    {
        // $exist = false;
        $kit_master_id = $_GET['kit_master_id'];

        $kit_master = ModelFactory::getInstance('KitMaster')->find($kit_master_id);

        // if(isset($kit_master->folder_name))
        // {
        //   $exist = true;
        // }

        return Response::json(['data' => $kit_master->folder_name], 200);
    }

    // get serial no by kit master id and kit identifier == 1
    public function getSetId(Request $request)
    {
        // $set_id = $_GET['term'];
        $kit_master_id = $_GET['kit_master_id'];
        //
        // $results = array();

        // $queries = ModelFactory::getInstance('PartMaster')
        //            ->leftjoin('part_type_master', 'part_type_master.id', '=', 'part_master.part_type_id')
        //            ->where('set_id', 'like', '%'.$set_id.'%')
        //            ->where('part_master.kit_master_id', $kit_master_id)
        //            ->where('kit_identifier', '1')
        // 					 ->orderBy('set_id', 'asc')
        //            ->GroupBy('serial_no')
        // 					 ->take(6)
        // 					 ->get();
        $part_master_set = ModelFactory::getInstance('PartMaster')
            ->where('kit_master_id', $kit_master_id)
            ->where('status', 'active')
            ->whereNotNull('set_id')
            ->where('set_id', 'LIKE', '%' . $request->term . '%')
            ->select('set_id')
            ->orderBy('set_id', 'asc')
            ->get();

        if (count($part_master_set) > 0) {
            foreach ($part_master_set as $query) {
                $results[] = [
                    'id' => $query->set_id,
                    'value' => $query->set_id
                ];
            }

            return response()->json($results);
        }
    }

    // get serial no by kit master id and kit identifier == 1
    public function getSerialNo(Request $request)
    {
        // $serial_no = $_GET['term'];
        $kit_master_id = $_GET['kit_master_id'];

        $part_master_set = ModelFactory::getInstance('PartMaster')
            ->where('kit_master_id', $kit_master_id)
            ->whereNotNull('set_id')
            ->where('status', 'active')
            ->where('serial_no', 'LIKE', '%' . $request->term . '%')
            ->select('serial_no')
            ->orderBy('serial_no', 'asc')
            ->get();

        if (count($part_master_set) > 0) {
            foreach ($part_master_set as $query) {
                $results[] = [
                    'id' => $query->serial_no,
                    'value' => $query->serial_no
                ];
            }

            return response()->json($results);
        }
    }

    public function getPartMasterSetId(Request $request)
    {
        $part_master = ModelFactory::getInstance('PartMaster')
            ->where('serial_no', $request->serial_no)
            ->where('kit_master_id', $request->kit_master_id)
            ->where('status', 'active')
            ->whereNotNull('set_id')
            ->select('set_id')
            ->get();

        if (count($part_master) > 0) {
            foreach ($part_master as $row) {
                $get_set_id = $row->set_id;
            }

            return response()->json(array(
                'data' => $get_set_id
            ));
        }
    }

    public function getPartMasterSerialNo(Request $request)
    {
        $part_master = ModelFactory::getInstance('PartMaster')
            ->where('set_id', $request->set_id)
            ->where('kit_master_id', $request->kit_master_id)
            ->where('status', 'active')
            ->select('serial_no')
            ->get();

        if (count($part_master) > 0) {
            foreach ($part_master as $row) {
                $get_serial_no = $row->serial_no;
            }

            return response()->json(array(
                'data' => $get_serial_no
            ));
        }
    }

    // get Max rf hrs
    public function getMaxRfHrs(Request $request)
    {
        $part_type_id = $_GET['part_type_id'];
        $kit_master_id = $_GET['kit_master_id'];
        $get_arf = '';

        $result = ModelFactory::getInstance('PartTypeMaster')->where('id', $part_type_id)
            ->select('max_rf_hrs')
            ->first();

        $part_master = ModelFactory::getInstance('PartMaster')->where('part_type_id', $part_type_id)
            ->where('kit_master_id', $kit_master_id)
            ->select('serial_no')
            ->get();

        if (count($part_master) == 1) {
            $get_arf = ModelFactory::getInstance('AccumulatedRF')->where('part_type_id', $part_type_id)
                ->where('kit_master_id', $kit_master_id)
                ->where('serial_no', $part_master[0]['serial_no'])
                ->select('arf')
                ->first();
        }

        return response()->json(array(
            'data' => $result,
            'part_master' => $part_master,
            'arf' => $get_arf
        ));
    }

    // get Max rf hrs
    public function getArfOfSerialNo(Request $request)
    {
        $part_type_id = $_GET['part_type_id'];
        $kit_master_id = $_GET['kit_master_id'];
        $serial_no = $_GET['serial_no'];
        $arf = false;

        $get_arf = ModelFactory::getInstance('AccumulatedRF')->where('part_type_id', $part_type_id)
            ->where('kit_master_id', $kit_master_id)
            ->where('serial_no', $serial_no)
            ->select('arf')
            ->first();

        if ($get_arf) {
            $arf = $get_arf;
        }

        return response()->json(array(
            'arf' => $arf
        ));
    }

    // post cleaning module
    public function getGRNDetailByPartType(Request $request)
    {
        $good_kit_detail_id = $_GET['good_kit_detail_id'];
        $part_type_id = $_GET['part_type_id'];
        $good_part_detail_id = $_GET['good_part_detail_id'];

        $select = [
            'good_part_detail.id as good_part_detail_id',
            'good_part_detail.serial_no',
            'good_part_detail.arf',
            'param_datapoint_master.parameter_master_id',
            'datapoint',
            'param',
            'grn_no',
            'good_kit_detail.set_id',
            'is_boolean',
            'good_param_datapoint_detail.id as good_param_datapoint_detail_id',
            'good_param_datapoint_detail.param_datapoint_value',
            'param_datapoint_master.kit_master_id',
            'param_datapoint_master.part_type_id',
            'avg'
        ];

        $result = ModelFactory::getInstance('GoodPartDetail')::leftjoin('good_kit_detail', 'good_kit_detail.id', '=', 'good_part_detail.good_kit_detail_id')
            ->leftjoin('param_datapoint_master', 'param_datapoint_master.part_type_id', '=', 'good_part_detail.part_type_id')
            ->leftjoin('parameter_master', 'parameter_master.id', '=', 'param_datapoint_master.parameter_master_id')
            ->leftjoin('good_receipt_header', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id')
            ->leftjoin('good_param_datapoint_detail', 'good_param_datapoint_detail.param_datapoint_master_id', '=', 'param_datapoint_master.id')
            ->where('good_part_detail.good_kit_detail_id', $good_kit_detail_id)
            ->where('good_part_detail.part_type_id', $part_type_id)
            ->where('good_param_datapoint_detail.good_part_detail_id', $good_part_detail_id)
            ->where('good_part_detail.actions', '<>', 2)
            ->select($select)
            ->orderBy('good_part_detail.id', 'asc')
            ->orderBy('param', 'asc')
            ->get();

        $result_group = ModelFactory::getInstance('GoodPartDetail')::leftjoin('good_kit_detail', 'good_kit_detail.id', '=', 'good_part_detail.good_kit_detail_id')
            ->leftjoin('param_datapoint_master', 'param_datapoint_master.part_type_id', '=', 'good_part_detail.part_type_id')
            ->leftjoin('parameter_master', 'parameter_master.id', '=', 'param_datapoint_master.parameter_master_id')
            ->leftjoin('good_receipt_header', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id')
            ->leftjoin('good_param_datapoint_detail', 'good_param_datapoint_detail.param_datapoint_master_id', '=', 'param_datapoint_master.id')
            ->where('good_part_detail.good_kit_detail_id', $good_kit_detail_id)
            ->where('good_part_detail.part_type_id', $part_type_id)
            ->where('good_param_datapoint_detail.good_part_detail_id', $good_part_detail_id)
            ->where('good_part_detail.actions', '<>', 2)
            ->select('param_datapoint_master.part_type_id', 'param_datapoint_master.kit_master_id', 'param_datapoint_master.parameter_master_id', 'param', 'avg')
            ->groupBy('parameter_master.id')
            ->orderBy('parameter_master.id', 'asc')
            ->get();

        foreach ($result_group as $row) {
            $partparammaster = ModelFactory::getInstance('PartParamMaster')::where('kit_master_id', $row->kit_master_id)
                ->where('part_type_id', $row->part_type_id)
                ->where('parameter_master_id', $row->parameter_master_id)
                ->where('exclude_post_cleaning', '0')
                ->select('parameter_master_id')
                ->first();

            if (!empty($partparammaster)) {
                $parameter_master_id[] = $partparammaster;
            }
        }

        foreach ($parameter_master_id as $row) {
            $parameter_master_id_array[] = $row->parameter_master_id;
        }

        return response()->json(array(
            'data' => $result,
            'group' => $result_group,
            'parameter_master_id_array' => $parameter_master_id_array
        ));
    }

    // get delivery order module
    public function getGoodKitDetail(Request $request)
    {
        $good_receipt_header_id = $_GET['good_receipt_header_id'];
        $delivery_order_id = $_GET['delivery_order_id'];

        $get_delivery_order_status = ModelFactory::getInstance('DeliveryOrder')::where('id', $delivery_order_id)
            ->select('status')
            ->first();

        $select = [
            'good_receipt_header.id',
            'good_receipt_header.grn_no',
            'good_receipt_header.job_id',
            'kit_master.kit_type',
            'reason_master.reason_desc',
            'good_kit_detail.kit_master_id',
            'good_kit_detail.tool_kit',
            'good_kit_detail.set_id',
            'good_kit_detail.serial_no',
            'good_kit_detail.quantity',
            'good_kit_detail.rf',
            'good_kit_detail.closed',
            'delivery_order.id as delivery_order_id',
            // 'delivery_order_child.qty as do_qty',
            // 'delivery_order_child.kit_master_id as do_kit_master_id',
            // 'delivery_order_child.tool_id as do_tool_id',
            // 'delivery_order_child.serial_no as do_serial_no'
        ];

        $result = ModelFactory::getInstance('GoodReceiptHeader')::join('good_kit_detail', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id')
            ->join('kit_master', 'good_kit_detail.kit_master_id', '=', 'kit_master.id')
            ->join('reason_master', 'good_kit_detail.reason_master_id', '=', 'reason_master.id')
            ->join('delivery_order', 'good_receipt_header.id', '=', 'delivery_order.good_receipt_header_id')
            // ->join( 'delivery_order_child', 'delivery_order.id', '=', 'delivery_order_child.delivery_order_id' )
            ->where('good_receipt_header.id', $good_receipt_header_id)
            // ->where( 'good_receipt_header.job_status', 'open' )
            // ->where( 'delivery_order.status', 'O' )
            // ->where( 'good_kit_detail.closed', '0' )
            ->select($select)
            ->groupby('good_kit_detail.id')
            ->orderBy('kit_master.kit_type', 'asc')
            ->get();

        //check if delivery order has already been closed
        if ($get_delivery_order_status->status == 'C') {
            $result = ModelFactory::getInstance('GoodReceiptHeader')::join('good_kit_detail', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id')
                ->join('kit_master', 'good_kit_detail.kit_master_id', '=', 'kit_master.id')
                ->join('reason_master', 'good_kit_detail.reason_master_id', '=', 'reason_master.id')
                ->join('delivery_order', 'good_receipt_header.id', '=', 'delivery_order.good_receipt_header_id')
                // ->join( 'delivery_order_child', 'delivery_order.id', '=', 'delivery_order_child.delivery_order_id' )
                ->where('good_receipt_header.id', $good_receipt_header_id)
                // ->select( $select )
                ->groupby('good_kit_detail.id')
                ->orderBy('kit_master.kit_type', 'asc')
                ->get();
        }

        // foreach get qty and total qty
        foreach ($result as $row) {
            //get qty
            $del_order_child = ModelFactory::getInstance('DeliveryOrderChild')::where('delivery_order_id', $delivery_order_id)
                ->where('kit_master_id', $row->kit_master_id)
                ->where('tool_id', $row->tool_kit);

            //get total qty
            $del_order_child_wt_doi = ModelFactory::getInstance('DeliveryOrderChild')::where('kit_master_id', $row->kit_master_id)
                ->where('tool_id', $row->tool_kit)
                ->where('job_id', $row->job_id);

            if (is_null($row->serial_no)) {
                $del_order_child->whereNull('serial_no');
                $del_order_child_wt_doi->whereNull('serial_no');
            } else {
                $del_order_child->where('serial_no', $row->serial_no);
                $del_order_child_wt_doi->where('serial_no', $row->serial_no);
            }

            $del_order_child = $del_order_child->first();

            $get_total = $del_order_child_wt_doi->sum('qty');

            if ($del_order_child) {
                $do_qty[] = $del_order_child->qty;
            } else {
                $do_qty[] = '';
            }

            if ($get_total) {
                $do_sum_qty[] = $get_total;
            } else {
                $do_sum_qty[] = '';
            }
        }

        $result_child = ModelFactory::getInstance('DeliveryOrder')::join('delivery_order_child', 'delivery_order.id', '=', 'delivery_order_child.delivery_order_id')
            ->where('delivery_order.id', $delivery_order_id)
            ->select('delivery_order_child.kit_master_id', 'delivery_order_child.tool_id', 'delivery_order_child.serial_no', 'delivery_order_child.qty')
            ->get();

        return response()->json(array(
            'data' => $result,
            'result_child' => $result_child,
            'do_qty' => $do_qty,
            'do_sum_qty' => $do_sum_qty,
            'status' => $get_delivery_order_status->status
        ));
    }

    //post delivery order child
    public function postDeliveryOrderChild(Request $request)
    {
        $kit_master_id = $request->kit_master_id;
        $result = false;

        $delete_do_child = ModelFactory::getInstance('DeliveryOrderChild')
            ->where('delivery_order_id', $request->delivery_order_id)
            ->get();

        if (count($delete_do_child) > 0) {
            ModelFactory::getInstance('DeliveryOrderChild')
                ->where('delivery_order_id', $request->delivery_order_id)
                ->delete();
        }

        if ($kit_master_id) {
            foreach ($kit_master_id as $row => $value) {
                $delivery_order_child = ModelFactory::getInstance('DeliveryOrderChild');
                $delivery_order_child->delivery_order_id = $request->delivery_order_id;
                $delivery_order_child->kit_master_id = $value;
                $delivery_order_child->tool_id = $request->tool_id[$row];
                $delivery_order_child->serial_no = $request->serial_no[$row];
                $delivery_order_child->job_id = $request->job_id;
                $delivery_order_child->qty = $request->qty[$row];
                $delivery_order_child->save();
            }

            $result = true;
        }

        return response()->json(array(
            'data' => $result
        ));
    }

    public function getGRNQueryDetail(Request $request)
    {
        if (!empty($request->grn_no) || !empty($request->customer_code) || !empty($request->job_id) || !empty($request->kit_master_id)
            || (!empty($request->date_from) && !empty($request->date_to)) || !empty($request->part_type) || !empty($request->serial_no)) {
            $select = [
                'grn_no',
                'collection_date',
                'job_id',
                'customer_code',
                'customer_name',
                'kit_type',
                'set_id',
                'good_kit_detail.tool_kit',
                'reason_desc',
                'part_type_name',
                'good_part_detail.serial_no',
                'good_part_detail.rf',
                'good_part_detail.arf',
                'good_part_detail.visual_condition',
                'part_status_master.description'
            ];

            $grn_report = ModelFactory::getInstance('GoodReceiptHeader')
                ->join('customer', 'good_receipt_header.customer_id', '=', 'customer.id')
                ->join('good_kit_detail', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id')
                ->join('kit_master', 'good_kit_detail.kit_master_id', '=', 'kit_master.id')
                ->leftjoin('good_part_detail', 'good_kit_detail.id', '=', 'good_part_detail.good_kit_detail_id')
                ->join('part_type_master', 'good_part_detail.part_type_id', '=', 'part_type_master.id')
                ->join('part_status_master', 'good_part_detail.actions', '=', 'part_status_master.id')
                ->join('reason_master', 'reason_master.id', '=', 'good_kit_detail.reason_master_id')
                // ->orderby('grn_no', 'asc')
                // ->orderby('kit_type', 'asc')
//                    ->where('good_kit_detail.closed', '0')
                ->orderby('collection_date', 'desc')
                ->orderby('set_id', 'desc')
                ->orderby('part_type_master.part_sequence', 'desc');
//                ->groupby('good_kit_detail.id')
//                ->groupby('part_type_id');

//      if(!empty($request->grn_no))
//      {
//        $grn_report = $grn_report->where('grn_no', $request->grn_no);
//      }

            if (!empty($request->set_id)) {
                $grn_report = $grn_report->where('set_id', $request->set_id);
            }

            if (!empty($request->customer_code)) {
                $grn_report = $grn_report->where('customer_code', $request->customer_code);
            }

            if (!empty($request->job_id)) {
                $grn_report = $grn_report->where('job_id', $request->job_id);
            }

            if (!empty($request->date_from) && !empty($request->date_to)) {
                // $date_from = Carbon::parse($request->date_from)->format("Y-m-d");
                // $date_to = Carbon::parse($request->date_to)->format("Y-m-d");

                $grn_report = $grn_report->whereBetween('collection_date', [$request->date_from, $request->date_to]);
            }

            if (!empty($request->kit_master_id)) {
                $grn_report = $grn_report->where('kit_master.id', $request->kit_master_id);
            }

            if (!empty($request->part_type)) {
                $grn_report = $grn_report->where('good_part_detail.part_type_name', $request->part_type);
            }

            if (!empty($request->serial_no)) {
                $grn_report = $grn_report->where('good_part_detail.serial_no', $request->serial_no);
            }

            $grn_report = $grn_report->select($select)->get();


            if (count($grn_report) > 0) {
                foreach ($grn_report as $row) {
                    $visual_array[] = $row->visual_condition;
                }

                foreach ($visual_array as $row => $array) {
                    if (strpos($array, ',') !== false) {
                        $explode_visual = explode(",", $array);

                        foreach ($explode_visual as $array2) {
                            $visual_table = \App\Models\VisualConditionMaster::find($array2);

                            $result_visual[] = $visual_table->description;
                        }

                        $implode_visual = implode(",", $result_visual);
                        $get_visual_desc[] = $implode_visual;
                        $result_visual = array();
                    } elseif (empty($array)) {
                        $get_visual_desc[] = '';
                    } else {
                        $visual_table = \App\Models\VisualConditionMaster::find($array);
                        $get_visual_desc[] = $visual_table->description;
                    }
                }
            } else {
                $get_visual_desc = '';
            }

            return response()->json(array(
                'data' => $grn_report,
                'result_vc' => $get_visual_desc
            ));
        }
    }

    public function getTrackingQueryDetail(Request $request)
    {
        if (!empty($request->grn_no) || !empty($request->customer_code) || !empty($request->job_id) || !empty($request->kit_master_id)
            || (!empty($request->date_from) && !empty($request->date_to) || !empty($request->part_type) || !empty($request->serial_no))) {
            $select = [
                'grn_no',
                'collection_date',
                'job_id',
                'customer_code',
                'customer_name',
                'kit_type',
                'set_id',
                'part_type_name',
                'good_part_detail.serial_no',
                'good_part_detail.rf',
                'good_part_detail.arf',
                'datapoint',
                'param_datapoint_value',
                'avg'
            ];

            $tracking_report = ModelFactory::getInstance('GoodReceiptHeader')
                ->join('customer', 'good_receipt_header.customer_id', '=', 'customer.id')
                ->join('good_kit_detail', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id')
                ->join('kit_master', 'good_kit_detail.kit_master_id', '=', 'kit_master.id')
                ->join('good_part_detail', 'good_kit_detail.id', '=', 'good_part_detail.good_kit_detail_id')
                ->join('part_type_master', 'good_part_detail.part_type_id', '=', 'part_type_master.id')
                ->join('good_param_datapoint_detail', 'good_part_detail.id', '=', 'good_param_datapoint_detail.good_part_detail_id')
                ->join('param_datapoint_master', 'good_param_datapoint_detail.param_datapoint_master_id', '=', 'param_datapoint_master.id')
//                ->orderby('job_id', 'desc')
//                ->orderby('set_id', 'desc')
//                ->orderby('kit_type', 'asc')
//                ->orderby('part_type_name', 'asc')
                ->orderby('collection_date', 'desc')
                ->orderby('set_id', 'desc')
                ->orderby('part_type_master.part_sequence', 'desc');
                // ->whereNotNull('param_datapoint_value');
//                ->groupby('good_kit_detail.id')
//                ->groupby('good_part_detail.part_type_id')
//                ->groupby('param_datapoint_master.id');

//      if(!empty($request->grn_no))
//      {
//        $tracking_report = $tracking_report->where('grn_no', $request->grn_no);
//      }

            if (!empty($request->set_id)) {
                $tracking_report = $tracking_report->where('set_id', $request->set_id);
            }

            if (!empty($request->customer_code)) {
                $tracking_report = $tracking_report->where('customer_code', $request->customer_code);
            }

            if (!empty($request->job_id)) {
                $tracking_report = $tracking_report->where('job_id', $request->job_id);
            }

            if (!empty($request->date_from) && !empty($request->date_to)) {
                // $date_from = Carbon::parse($request->date_from)->format("Y-m-d");
                // $date_to = Carbon::parse($request->date_to)->format("Y-m-d");

                $tracking_report = $tracking_report->whereBetween('collection_date', [$request->date_from, $request->date_to]);
            }

            if (!empty($request->kit_master_id)) {
                $tracking_report = $tracking_report->where('kit_master.id', $request->kit_master_id);
            }

            if (!empty($request->part_type)) {
                $tracking_report = $tracking_report->where('good_part_detail.part_type_name', $request->part_type);
            }

            if (!empty($request->serial_no)) {
                $tracking_report = $tracking_report->where('good_part_detail.serial_no', $request->serial_no);
            }

            $tracking_report = $tracking_report->select($select)->get();

            return response()->json(array(
                'data' => $tracking_report
            ));
        }
    }

    public function getPartMasterDetail(Request $request)
    {
        if (!empty($request->kit_type) || !empty($request->part_type) || !empty($request->serial_no) || !empty($request->set_id)
            || !empty($request->created_at)) {
            $select = [
                'part_master.id',
                'kit_type',
                'part_type',
                'serial_no',
                'set_id',
                'specs',
                'status',
                'additional_info',
                'part_master.grade',
                'part_master.remarks',
                'part_master.created_at'
            ];

            $part_masters = ModelFactory::getInstance('PartMaster')
                ->leftjoin('kit_master', 'kit_master.id', '=', 'part_master.kit_master_id')
                ->leftjoin('part_type_master', 'part_type_master.id', '=', 'part_master.part_type_id')
                ->select($select)
                ->orderby('kit_type', 'asc')
                ->orderby('part_type', 'asc')
                ->orderby('serial_no', 'asc')
                ->orderby('set_id', 'asc')
                ->orderby('created_at', 'asc');

            if (!empty($request->kit_type)) {
                $part_masters = $part_masters->where('kit_master.id', $request->kit_type);
            }

            if (!empty($request->part_type)) {
                $part_masters = $part_masters->where('part_type_master.id', $request->part_type);
            }

            if (!empty($request->serial_no)) {
                $part_masters = $part_masters->where('serial_no', $request->serial_no);
            }

            if (!empty($request->set_id)) {
                $part_masters = $part_masters->where('set_id', $request->set_id);
            }

            if (!empty($request->created_at)) {
                // $date_from = Carbon::parse($request->date_from)->format("Y-m-d");
                // $date_to = Carbon::parse($request->date_to)->format("Y-m-d");

                $part_masters = $part_masters->whereDate('part_master.created_at', $request->created_at);
            }

            $part_masters = $part_masters->select($select)->get();

            return response()->json(array(
                'data' => $part_masters,
            ));
        }
    }

    public function getPartMasterAllSerialNo(Request $request)
    {
        $part_master_serial = ModelFactory::getInstance('PartMaster')
            ->where('status', 'active')
            ->select('serial_no')
            ->get();

        return response()->json(array(
            'data' => $part_master_serial
        ));
    }

    public function getPartMasterAllSetId(Request $request)
    {
        // $kit_master_id = $request->kit_master_id;

        $part_master_set = ModelFactory::getInstance('PartMaster')
            ->where('status', 'active')
            ->whereNotNull('set_id')
            ->select('set_id')
            ->get();

        return response()->json(array(
            'data' => $part_master_set
        ));
    }

    public function getGoodPartRow(Request $request)
    {
        $good_kit_detail_id = $_GET['good_kit_detail_id'];

        $good_part_detail = ModelFactory::getInstance('GoodPartDetail')
            ->where('good_kit_detail_id', $good_kit_detail_id)
            ->select('id')
            ->get();

        return response()->json(array(
            'data' => count($good_part_detail)
        ));
    }

    public function getCustomerInfo(Request $request)
    {
        $cust_code = $_GET['cust_code'];

        $get_cust = ModelFactory::getInstance('GoodReceiptHeader')->join('customer', 'good_receipt_header.customer_id', '=', 'customer.id')
            ->where('good_receipt_header.id', $grn_no)
            ->select('customer_code', 'customer_name')
            ->first();

        return response()->json(array(
            'data' => $get_cust
        ));
    }

    public function getPurchaseOrder(Request $request)
    {
        $cust_code = $_GET['cust_code'];
        $exist_po = '';

        $get_po = ModelFactory::getInstance('Customer')->join('purchase_order', 'customer.id', '=', 'purchase_order.customer_id')
            ->where('customer_code', $cust_code)
            ->where('effective_at', '>=', Carbon::now()->toDateTimeString())
            ->select('purchase_order.po_no')
            ->orderBy('purchase_order.po_no', 'asc')
            ->get();

        if (isset($_GET['do_id'])) {
            $get_exist_po = ModelFactory::getInstance('DeliveryOrder')->find($_GET['do_id']);
            $exist_po = $get_exist_po->po_no;
        }

        return response()->json(array(
            'data' => $get_po,
            'exist_po' => $exist_po
        ));
    }

    public function postCustomerPurchaseOrder(Request $request)
    {
        foreach ($request->po_no as $row => $po_no) {
            $check_po = ModelFactory::getInstance('PurchaseOrder')
                ->where('po_no', $po_no)
                ->select('po_no', 'id')
                ->first();

            if ($check_po) {
                $id[] = $check_po->id;
                continue;
            }

            $effective = str_replace('/', '-', $request->effective[$row]);
            $effective = date('Y-m-d', strtotime($effective));

            $cust_po = ModelFactory::getInstance('PurchaseOrder');
            $cust_po->customer_id = $request->cust_id;
            $cust_po->po_no = $po_no;
            $cust_po->effective_at = $effective;
            $cust_po->save();

            $id[] = $cust_po->id;
        }

        return response()->json(array(
            'data' => 'success',
            'result' => $id
        ));
    }

    public function getDeletePurchaseOrder(Request $request)
    {
        $id = $_GET['id'];

        $delete_row = ModelFactory::getInstance('PurchaseOrder')->find($id);

        if ($delete_row) {
            ModelFactory::getInstance('PurchaseOrder')->find($id)->delete();
        }

        return Response::json(['data' => 'success'], 200);
    }

    public function getGRNDetail(Request $request)
    {
        $cust_code = $_GET['cust_code'];

        $customer = ModelFactory::getInstance('Customer')->where('customer_code', $cust_code)->first();

        $grn = ModelFactory::getInstance('GoodReceiptHeader')->where('customer_id', $customer->id)
//            ->join('good_kit_detail', 'good_receipt_header.id', 'good_kit_detail.good_receipt_header_id')
            ->select('job_id', 'grn_no')
            ->orderBy('job_id', 'desc')
            ->get();

        return Response::json(['data' => $grn], 200);
    }

    public function getSetIDList(Request $request)
    {
        $cust_code = $_GET['cust_code'];

        $customer = ModelFactory::getInstance('Customer')->where('customer_code', $cust_code)->first();

        $grn = ModelFactory::getInstance('GoodReceiptHeader')->where('customer_id', $customer->id)
            ->join('good_kit_detail', 'good_receipt_header.id', 'good_kit_detail.good_receipt_header_id')
            ->where('set_id', '<>', '')
            ->select('set_id')
            ->groupBy('set_id')
            ->orderBy('set_id', 'desc')
            ->get();

        return Response::json(['data' => $grn], 200);
    }

    public function getGRNDetailsWithoutClosed(Request $request)
    {
        $cust_code = $_GET['cust_code'];

        $customer = ModelFactory::getInstance('Customer')->where('customer_code', $cust_code)->first();

        $grn = ModelFactory::getInstance('GoodReceiptHeader')->where('customer_id', $customer->id)
            ->where('job_status', '<>', 'closed')
            ->select('job_id', 'grn_no', 'id')
            ->orderBy('job_id', 'asc')
            ->get();

        return Response::json(['data' => $grn], 200);
    }

    public function getKitDetail(Request $request)
    {
        // $grn_no = $_GET['grn_no'];
        $job_id = $_GET['job_id'];
        $customer_code = $_GET['customer_code'];

        // $do_detail = ModelFactory::getInstance( 'GoodReceiptHeader' )::join('good_kit_detail', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id' )
        //               ->join( 'kit_master', 'good_kit_detail.kit_master_id', '=', 'kit_master.id' )
        //               ->join( 'delivery_order', 'good_receipt_header.id', '=', 'delivery_order.good_receipt_header_id' )
        //               ->join( 'customer', 'good_receipt_header.customer_id', '=', 'customer.id' )
        //               ->where('customer_code', $customer_code)
        //               ->where('job_id', $job_id)
        //               ->select('kit_master.kit_type', 'grn_no')
        //               ->groupBy( 'kit_master.id' )
        //               ->orderBy( 'grn_no', 'asc' )
        //               ->orderBy( 'customer_code', 'asc' )
        //               ->orderBy( 'kit_master_id', 'asc' )
        //               ->get();

        $getKitList = ModelFactory::getInstance('GoodReceiptHeader')::join('good_kit_detail', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id')
            ->join('kit_master', 'good_kit_detail.kit_master_id', '=', 'kit_master.id')
            ->join('customer', 'good_receipt_header.customer_id', '=', 'customer.id')
            ->where('customer_code', $customer_code)
            ->where('job_id', $job_id)
            ->select('good_kit_detail.id as good_kit_detail_id', 'kit_master.id', 'kit_type', 'tool_kit', 'set_id', 'grn_no', 'good_receipt_header.id as grn_id')
            ->orderBy('kit_type', 'asc')
            ->get();

        return Response::json(['data' => $getKitList], 200);
    }

    public function getKitTypeList()
    {
        $customer_code = $_GET['cust_code'];

        $kit_type_list = ModelFactory::getInstance('Customer')::join('kit_master', 'customer.id', '=', 'kit_master.customer_id')
            ->where('customer_code', $customer_code)
            ->select('kit_type', 'kit_master.id')
            ->orderBy('kit_type', 'asc')
            ->get();

        return Response::json(['data' => $kit_type_list], 200);
    }

    public function getPartMasterKitIdentifier()
    {
        $kit_identifier = false;

        $part_type_master = ModelFactory::getInstance('PartTypeMaster')
            ->where('kit_identifier', 1)
            ->where('kit_master_id', $_GET['kit_master_id'])
            ->select('kit_identifier')
            ->get();

        if (count($part_type_master) == 1) {
            $kit_identifier = true;
        }

        return response()->json(array(
            'data' => $kit_identifier
        ));
    }

    public function getKitMasterRemarks()
    {
        $remarks = '';

        $kit_master = ModelFactory::getInstance('KitMaster')->find($_GET['kit_master_id']);

        if ($kit_master) {
            $remarks = $kit_master->remarks;
        }

        return response()->json(array(
            'data' => $remarks
        ));
    }

    // get serial no for kit header
    public function getSerialNoForKitHeader(Request $request)
    {
        $part_type_id = $_GET['part_type_id'];
        $kit_master_id = $_GET['kit_master_id'];

        $part_type_master = ModelFactory::getInstance('PartTypeMaster')->find($part_type_id);

        $part_type_master_ids = ModelFactory::getInstance('PartTypeMaster')->where('part_type', $part_type_master->part_type)
            ->select('id')
            ->get();

        foreach ($part_type_master_ids as $id) {
            $ids[] = $id->id;
        }

        $part_master = ModelFactory::getInstance('PartMaster')
            ->where('kit_master_id', $kit_master_id)
            ->whereIn('part_type_id', $ids)
            ->where('status', 'active')
            ->where('serial_no', 'LIKE', '%' . $request->term . '%')
            ->select('serial_no')
            ->orderBy('serial_no', 'asc')
            ->get();

        if (count($part_master) > 0) {
            foreach ($part_master as $query) {
                $results[] = [
                    'id' => $query->serial_no,
                    'value' => $query->serial_no
                ];
            }

            return response()->json($results);
        }
    }

    // check serial no. exists or not
    public function getPartMasterSerialNoExist(Request $request)
    {
        $arf = '';
        $trans_kit_master_id = '';
        $existing_kit_master = '';
        // $serial_no = '';
        $kit_type = '';
        $serialno_exist = false;
        $kit_identifier_notclosing = false;
        $kit_part_serial_exist = false;
        $serial_no = $_GET['serial_no'];
        $part_type_id = $_GET['part_type_id'];
        $kit_master_id = $_GET['kit_master_id'];

        $accumulated_rf = ModelFactory::getInstance('AccumulatedRF')::where('kit_master_id', $kit_master_id)
            ->where('part_type_id', $part_type_id)
            ->where('serial_no', $serial_no)
            ->first();

        if ($accumulated_rf) {
            $arf = $accumulated_rf->arf;
        }

        $part_master_normal = ModelFactory::getInstance('PartMaster')::where('serial_no', $serial_no)
            ->select('serial_no')
            ->get();

        $part_master = ModelFactory::getInstance('PartMaster')->join('part_type_master', 'part_master.part_type_id', '=', 'part_type_master.id')
            ->join('kit_master', 'part_type_master.kit_master_id', '=', 'kit_master.id')
            ->where('serial_no', $serial_no)
            // ->where('part_master.part_type_id', $part_type_id)
            ->where('part_master.kit_master_id', '<>', $kit_master_id)
            ->where('kit_identifier', '<>', 1)
            ->select('kit_master.id', 'part_master.kit_master_id', 'serial_no', 'part_type', 'kit_type')
            ->get();

        $part_master_check_exist = ModelFactory::getInstance('PartMaster')::where('serial_no', $serial_no)
            // ->where('part_master.part_type_id', $part_type_id)
            ->where('kit_master_id', $kit_master_id)
            ->where('part_type_id', $part_type_id)
            ->first();

        $kit_master = ModelFactory::getInstance('KitMaster')::where('id', $kit_master_id)
            ->select('kit_type')
            ->first();

        $kit_part_type = ModelFactory::getInstance('KitMaster')::join('part_type_master', 'kit_master.id', '=', 'part_type_master.kit_master_id')
            ->where('kit_master.id', $kit_master_id)
            ->where('part_type_master.id', $part_type_id)
            ->select('kit_identifier')
            ->first();

        // check whether is kit identifier for that kit and part type
        if ($kit_part_type && $kit_part_type->kit_identifier == 1) {
            $part_master_table = ModelFactory::getInstance('PartMaster')::where('kit_master_id', $kit_master_id)
                ->where('part_type_id', $part_type_id)
                ->where('serial_no', '<>', $serial_no)
                ->where('set_id', $request->set_id)
                ->where('status', 'active')
                ->first();

            // check part master table to disallow user inserts new serial no. with similar set id that has not been closed
            if ($part_master_table) {
                $kit_identifier_notclosing = true;
            }
        }

        // check serial no. already exists for that kit and part type, so no need to transfer
        if ($part_master_check_exist) {
            $kit_part_serial_exist = true;
        }

        if (count($part_master_normal) > 0) {
            $serialno_exist = true;
        }

        if (count($part_master) == 1) {
            $kit_master_new = ModelFactory::getInstance('KitMaster')::where('id', $part_master[0]->kit_master_id)
                ->select('kit_type')
                ->first();

            $existing_kit_master = $kit_master->kit_type;
            $serial_no = $part_master[0]->serial_no;
            $kit_type = $kit_master_new->kit_type;
            $trans_kit_master_id = $part_master[0]->kit_master_id;
        }

        return response()->json([
            'data' => $serialno_exist,
            'kit_identifier_notclosing' => $kit_identifier_notclosing,
            'kit_part_serial_exist' => $kit_part_serial_exist,

            // 'serial_no' => $serial_no,
            'kit_type' => $kit_type,
            'no_part_master' => count($part_master),
            'existing_kit_master' => $existing_kit_master,
            'trans_kit_master_id' => $trans_kit_master_id,
            'arf' => $arf
        ]);
    }

    //transfer serial no. to existing kit type
    public function postPartMasterSerialNoTransfer(Request $request)
    {
        $update = false;
        $updateArf = false;
        $trans_kit_master_id = $request->trans_kit_master_id;
        $kit_master_id = $request->kit_master_id;
        $part_type_id = $request->part_type_id;
        $serial_no = $request->serial_no;

        $part_type_master = ModelFactory::getInstance('PartTypeMaster')::where('id', $part_type_id)
            ->select('part_type')
            ->first();

        // get existing info
        $get_ptm_ids = ModelFactory::getInstance('PartTypeMaster')::where('part_type', $part_type_master->part_type)
            // ->where('kit_master_id', $trans_kit_master_id)
            ->select('id')
            ->get();

        // get to be transferred info
        $get_updated_ptm_id = ModelFactory::getInstance('PartTypeMaster')::where('part_type', $part_type_master->part_type)
            ->where('kit_master_id', $kit_master_id)
            ->select('id')
            ->first();

        foreach ($get_ptm_ids as $id) {
            $arf_table = ModelFactory::getInstance('AccumulatedRF')::where('kit_master_id', $trans_kit_master_id)
                ->where('part_type_id', $id->id)
                ->where('serial_no', $serial_no)
                ->first();

            $part_master = ModelFactory::getInstance('PartMaster')::where('kit_master_id', $trans_kit_master_id)
                ->where('part_type_id', $id->id)
                ->where('serial_no', $serial_no)
                ->first();

            // check both table has records
            if ($part_master && $arf_table) {

                // update part master table
                $update_kit = ModelFactory::getInstance('PartMaster')::where('kit_master_id', $trans_kit_master_id)
                    ->where('part_type_id', $id->id)
                    ->where('serial_no', $serial_no)
                    ->update([
                        'part_type_id' => $get_updated_ptm_id->id,
                        'kit_master_id' => $kit_master_id
                    ]);

                if ($update_kit) {
                    $update = true;

                    // check data exists in arf table
                    if ($arf_table) {
                        // update arf table
                        $update_arf = ModelFactory::getInstance('AccumulatedRF')::where('kit_master_id', $trans_kit_master_id)
                            ->where('part_type_id', $id->id)
                            ->where('serial_no', $serial_no)
                            ->update([
                                'part_type_id' => $get_updated_ptm_id->id,
                                'kit_master_id' => $kit_master_id
                            ]);

                        if ($update_arf) {
                            $updateArf = true;
                        }
                    }

                    break;
                }
            }

            if ($update) {
                // to avoid multiple rows to be updated
                break;
            }
        }

        return response()->json(array(
            'data' => $update,
            'update_arf' => $updateArf,
            'part_type_master' => $part_type_master,
            'get_ptm_ids' => $get_ptm_ids,
            'get_updated_ptm_id' => $get_updated_ptm_id,
            'trans_kit_master_id' => $trans_kit_master_id,
            'kit_master_id' => $kit_master_id
        ));
    }

    // post action to part master
    public function postActionPartMaster(Request $request)
    {
        $update = false;

        $part_status_master = ModelFactory::getInstance('PartStatusMaster')::find($request->action);

        if (!empty($request->kit_master_id) && !empty($request->part_type_id) &&
            !empty($request->serial_no) && !empty($request->action)) {
            $update_action = ModelFactory::getInstance('PartMaster')->where('kit_master_id', $request->kit_master_id)
                ->where('part_type_id', $request->part_type_id)
                ->where('serial_no', $request->serial_no)
                // ->where( 'status', 'Active' )
                ->update([
                    'status' => $part_status_master->description
                ]);

            if ($update_action) {
                $update = true;
            }
        }

        return Response::json(array('data' => $update), 200);
    }

    public function getDoDetails(Request $request)
    {
        $job_id = $_GET['job_id'];
        $customer_code = $_GET['customer_code'];

        $getDo = ModelFactory::getInstance('GoodReceiptHeader')::join('delivery_order', 'good_receipt_header.id', '=', 'delivery_order.good_receipt_header_id')
            ->where('good_receipt_header.job_id', $job_id)
            ->select('delivery_order.*')
            ->get();

        return Response::json(['data' => $getDo], 200);
    }

    public function getGoodPartDetailPartType(Request $request)
    {
        $cust_code = $request->cust_code;

        $result = ModelFactory::getInstance('GoodReceiptHeader')
            ->join('good_kit_detail', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id')
            ->join('good_part_detail', 'good_kit_detail.id', '=', 'good_part_detail.good_kit_detail_id')
            ->join('customer', 'good_receipt_header.customer_id', '=', 'customer.id')
            ->where('customer_code', $cust_code)
            ->select('good_part_detail.part_type_id', 'good_part_detail.part_type_name')
            ->orderBy('good_part_detail.part_type_name', 'desc')
            ->groupBy('good_part_detail.part_type_name')
            ->get();

        return response()->json(array(
            'data' => $result
        ));
    }

    public function getGoodPartDetailSerial(Request $request)
    {
        $cust_code = $request->cust_code;

        $result = ModelFactory::getInstance('GoodReceiptHeader')
            ->join('good_kit_detail', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id')
            ->join('good_part_detail', 'good_kit_detail.id', '=', 'good_part_detail.good_kit_detail_id')
            ->join('customer', 'good_receipt_header.customer_id', '=', 'customer.id')
            ->where('customer_code', $cust_code)
            ->where('good_part_detail.serial_no', 'LIKE', '%' . $request->term . '%')
            ->select('good_part_detail.serial_no')
            ->orderBy('good_part_detail.serial_no', 'desc')
            ->groupBy('good_part_detail.serial_no')
            ->get();

        if (count($result) > 0) {
            foreach ($result as $query) {
                $results[] = [
                    'id' => $query->serial_no,
                    'value' => $query->serial_no
                ];
            }
            return response()->json($results);
        }
//        return response()->json(array(
//            'data' => $result
//        ));

    }
}

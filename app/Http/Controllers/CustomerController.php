<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Factories\ModelFactory;
use App\Filters\SearchFilter;
use Flash;
use File;
use Response;

class CustomerController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $select = [
      'customer.id',
      'customer_code',
      'customer_name',
      'company_name',
      'customer_address',
      'purchaser',
      'delivery_address',
      'delivery_contact',
      'vendor_code'
    ];

    $customers = ModelFactory::getInstance('Customer')
                  ->leftjoin('company', 'company.id', '=', 'customer.company_id')
                  ->select($select);

    $searchFilter = SearchFilter::searchKeyword($customers, $request->q, 'customer');
    $customers = $searchFilter->paginate(10000);

    if(count($customers) > 0)
    {
      foreach($customers as $row)
      {
        $row->count = ModelFactory::getInstance('KitMaster')->where('customer_id', $row->id)->count();
      }
    }

    return view('customers.index')->with('customers', $customers);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $companies = $this->getCompanyLists();

    return view('customers.create')->with('companies', $companies);
  }

  // Get Company Lists
  public function getCompanyLists()
  {
    $select = ['id', 'company_name'];

    $companies = ModelFactory::getInstance('Company');

    $companydata = $companies->select($select)->get();

    $this->company[''] = 'Please select';

		foreach ($companydata as $data)
		{
      $this->company[$data->id] = $data->company_name;
    }

    return $this->company;
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $input = $request->except('_token');

    $validator = \Validator::make($request->all(), [
      'company_id' => 'required',
      'customer_code' => 'required|unique:customer|max:255',
      'customer_name' => 'required',
      'customer_address' => 'required',
//      'folder_name' => 'required|unique:customer|max:50'
    ]);

    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }

    $customer = ModelFactory::getInstance('Customer');
    $customer->company_id = $request->company_id;
    $customer->customer_code = $request->customer_code;
    $customer->customer_name = $request->customer_name;
    $customer->customer_address = $request->customer_address;
    $customer->purchaser = $request->purchaser;
    $customer->delivery_address = $request->delivery_address;
    $customer->delivery_contact = $request->delivery_contact;
    $customer->vendor_code = $request->vendor_code;
    $customer->folder_name = $request->customer_code;

    if($customer->save())
    {
      File::makeDirectory(public_path().'/img/pts/' .$customer->folder_name. '/', 0777, true);
      Flash::success( 'Successfully created customer.' );
      return redirect()->route('customers.edit', [$customer->id]);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {

  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $customer = ModelFactory::getInstance('Customer')->find($id);
    $companies = $this->getCompanyLists();
    $po_list = ModelFactory::getInstance('PurchaseOrder')->where('customer_id', $id)
               ->select('*')
               ->orderBy('created_at')
               ->get();

    return view('customers.edit', compact('customer', 'companies', 'po_list'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $input = $request->except('_token');

    $validator = \Validator::make($request->all(), [
      'company_id' => 'required',
      // 'customer_code' => 'required|max:255|unique:customer,customer_code,'.$request->id. ',id',
      // 'customer_name' => 'required',
      'customer_address' => 'required'
    ]);

    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }

    $customer = ModelFactory::getInstance('Customer')->find($request->id);
    $customer->company_id = $request->company_id;
    // $customer->customer_code = $request->customer_code;
    // $customer->customer_name = $request->customer_name;
    $customer->customer_address = $request->customer_address;
    $customer->purchaser = $request->purchaser;
    $customer->delivery_address = $request->delivery_address;
    $customer->delivery_contact = $request->delivery_contact;
    $customer->vendor_code = $request->vendor_code;

    if($customer->save())
    {
      Flash::success( 'Successfully updated customer.' );
      return redirect()->back();
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $customer = \App\Models\Customer::findorfail($id);

    if( empty( $customer ) )
    {
      Flash::error( 'Selected customer not found.' );
    }

    $delete_fol = File::deleteDirectory(public_path().'/img/pts/' .$customer->folder_name);

    if($delete_fol)
    {
      $customer->delete();

      Flash::success( 'Successfully deleted customer.' );
      return redirect( route( 'customers.index' ) );
    }
  }

  public function postHeaderCustomers(Request $request)
  {
    $customer = ModelFactory::getInstance('Customer')->find($request->customer_id);
    $customer->company_id = $request->company_id;
    $customer->customer_address = $request->customer_address;
    $customer->purchaser = $request->purchaser;
    $customer->delivery_address = $request->delivery_address;
    $customer->delivery_contact = $request->delivery_contact;
    $customer->vendor_code = $request->vendor_code;
    $customer->save();

    return Response::json(array('data' => 'success'), 200);
  }
}

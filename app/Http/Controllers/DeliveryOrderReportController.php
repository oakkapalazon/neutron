<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Factories\ModelFactory;
use Flash;
use Session;
use DB;

class DeliveryOrderReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $customer_code = \App\Models\Customer::getCustomerCodesOnly();
      $kit_types = \App\Models\KitMaster::getKitTypes();

      return view('delivery_order_report.index', compact('customer_code', 'kit_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
        'do_no' => 'required'
      ]);

      $select = [
        'job_id',
        'delivery_order.date',
        'kit_master.process',
        'kit_master.kit_type',
        'tool_kit',
        'delivery_order.po_no',
        'set_id'
      ];

      $do_detail = ModelFactory::getInstance( 'GoodReceiptHeader' )::join('good_kit_detail', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id' )
                    ->join( 'kit_master', 'good_kit_detail.kit_master_id', '=', 'kit_master.id' )
                    ->join( 'delivery_order', 'good_receipt_header.id', '=', 'delivery_order.good_receipt_header_id' )
                    ->where( 'do_no', $request->do_no )
                    ->select( $select )
                    ->get();

      return view('post_cleaning_report.post-report-print', compact('do_detail'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function printDeliveryOrderReport(Request $request)
    {
        // if(!empty($request->customer_code) || !empty($request->kit_type) || !empty($request->job_id)
        // || !empty($request->set_id) || !empty($request->grn_no))
        if( !empty($request->customer_code) && !empty($request->job_id) && !empty($request->do_no) )
        {
            $do_detail = ModelFactory::getInstance( 'DeliveryOrder' )::join('delivery_order_child', 'delivery_order.id', '=', 'delivery_order_child.delivery_order_id' )
                          ->where( 'delivery_order.id', $request->do_no )
                          ->groupBy( 'kit_master_id' )
                          ->orderBy( 'kit_master_id', 'asc' );

            $do_detail = $do_detail->get();

            if(count($do_detail) > 0)
            {
              return view('delivery_order_report.delivery-order-print', compact('do_detail'));
            }
            else
            {
              Flash::error( 'There is no kit assigned to this delivery order no.' );
              return redirect()->back();
            }

        }
    }
}

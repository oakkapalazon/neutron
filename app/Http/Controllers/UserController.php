<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Factories\ModelFactory;
use Flash;
use Hash;

class UserController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $select = [
      'id',
      'name',
      'user_id',
      'access_level'
    ];

    $users = ModelFactory::getInstance('User')->select($select)
              ->orderBy('name', 'asc')
              ->paginate(50);

    foreach($users as $user)
    {
      if($user->access_level == 1)
      {
        $user->access_level_desc = 'Administrator';
      }

      else
      {
        $user->access_level_desc = 'Standard';
      }
    }

    return view('users.index')->with('users', $users);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $access_level = \App\Models\SecurityLevel::accessRight();

    return view('users.create', compact('access_level'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $input = $request->except('_token');

    $validator = \Validator::make($request->all(), [
      'name' => 'required|unique:user|max:255',
      'user_id' => 'required',
      'new_password' => 'required|confirmed|max:255',
      'new_password_confirmation' => 'required|max:255',
      'access_level' => 'required'
    ]);

    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }

    $user = ModelFactory::getInstance('User');
    $user->name = $request->name;
    $user->access_level = $request->access_level;
    $user->user_id = $request->user_id;
    $user->email = $request->user_id;
    $user->password = Hash::make($request->new_password);

    if($user->save())
    {
      Flash::success( 'Successfully created user.' );
      return redirect()->back();
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $user = ModelFactory::getInstance('User')->findOrFail($id);
    $access_level = \App\Models\SecurityLevel::accessRight();

    return view('users.edit', compact('user', 'access_level'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $input = $request->except('_token');

    if(isset($request->new_password) && isset($request->new_password_confirmation))
    {
      $validator = \Validator::make($request->all(), [
        'name' => 'required|max:255|unique:user,name,'.$request->id,
        'user_id' => 'required',
        'new_password' => 'required|confirmed|max:255',
        'new_password_confirmation' => 'required|max:255',
        'access_level' => 'required'
      ]);

      if ($validator->fails())
      {
        return redirect()->back()->withErrors($validator)->withInput();
      }

      $user = ModelFactory::getInstance('User')->findOrFail($request->id);
      $user->name = $request->name;
      $user->access_level = $request->access_level;
      $user->user_id = $request->user_id;
      $user->email = $request->user_id;
      $user->password = Hash::make($request->new_password);
    }

    else
    {
      $validator = \Validator::make($request->all(), [
        'name' => 'required|max:255|unique:user,name,'.$request->id,
        'user_id' => 'required',
        'access_level' => 'required'
      ]);

      if ($validator->fails())
      {
        return redirect()->back()->withErrors($validator)->withInput();
      }

      $user = ModelFactory::getInstance('User')->findOrFail($request->id);
      $user->name = $request->name;
      $user->user_id = $request->user_id;
      $user->email = $request->user_id;
      $user->access_level = $request->access_level;
    }

    if($user->save())
    {
      Flash::success( 'Successfully updated user.' );
      return redirect()->back();
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $user = \App\Models\User::findOrFail($id);

    if( empty( $user ) )
    {
      Flash::error( 'Selected user not found.' );
    }

    $user->delete();

    Flash::success( 'Successfully deleted user.' );
    return redirect( route( 'users.index' ) );
  }
}

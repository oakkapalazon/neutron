<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Factories\ModelFactory;
use App\Filters\SearchFilter;
use Flash;

class ParameterMasterController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $select = [
      'id',
      'param',
      'description',
      'sequence'
    ];

    $param_masters = ModelFactory::getInstance('ParameterMaster')->select($select)->orderBy('param', 'asc');
    $searchFilter = SearchFilter::searchKeyword($param_masters, $request->q, 'parameter_master');
    $param_masters = $searchFilter->paginate(10000);

    if(count($param_masters) > 0)
    {
      foreach($param_masters as $row)
      {
        $row->no_of_pdm = ModelFactory::getInstance('ParameterDatapointMaster')->where('parameter_master_id', $row->id)->count();
        $row->no_of_ppm = ModelFactory::getInstance('PartParamMaster')->where('parameter_master_id', $row->id)->count();
      }
    }

    return view('parameter_master.index', compact('param_masters'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('parameter_master.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $input = $request->except('_token');

    $validator = \Validator::make($request->all(), [
      'param' => 'required|unique:parameter_master|max:255',
      'sequence' => 'required'
    ]);

    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }

    if(!isset($request->is_boolean))
    {
      $request->is_boolean = 0;
    }

    $param_master = ModelFactory::getInstance('ParameterMaster');
    $param_master->param = $request->param;
    $param_master->description = $request->description;
    $param_master->sequence = $request->sequence;
    $param_master->is_boolean = $request->is_boolean;

    if($param_master->save())
    {
      Flash::success( 'Successfully created parameter master.' );
      return redirect()->back();
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $param_master = ModelFactory::getInstance('ParameterMaster')->findOrFail($id);

    return view('parameter_master.edit', compact('param_master'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $input = $request->except('_token');

    $validator = \Validator::make($request->all(), [
      'param' => 'required|max:255|unique:parameter_master,param,'.$request->id,
      'sequence' => 'required'
    ]);

    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }

    if(!isset($request->is_boolean))
    {
      $request->is_boolean = 0;
    }

    $param_master = ModelFactory::getInstance('ParameterMaster')->findOrFail($request->id);
    $param_master->param = $request->param;
    $param_master->description = $request->description;
    $param_master->sequence = $request->sequence;
    $param_master->is_boolean = $request->is_boolean;

    if($param_master->save())
    {
      Flash::success( 'Successfully updated parameter master.' );
      return redirect()->back();
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $param_master = \App\Models\ParameterMaster::findOrFail($id);

    if( empty( $param_master ) )
    {
      Flash::error( 'Selected parameter master not found.' );
    }

    $param_master->delete();

    Flash::success( 'Successfully deleted parameter master.' );
    return redirect()->back();
  }
}

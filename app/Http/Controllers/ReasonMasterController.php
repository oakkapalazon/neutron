<?php

namespace App\Http\Controllers;

use App\Models\GoodKitDetail;
use Illuminate\Http\Request;
use App\Factories\ModelFactory;
use App\Filters\SearchFilter;
use Flash;

class ReasonMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $select = [
            'id',
            'reason_code',
            'reason_desc'
        ];

        $reason_masters = ModelFactory::getInstance('ReasonMaster')->select($select)
            ->orderBy('reason_code', 'asc');
        $searchFilter = SearchFilter::searchKeyword($reason_masters, $request->q, 'reason_master');
        $reason_masters = $searchFilter->paginate(50);

        if (count($reason_masters) > 0) {
            foreach ($reason_masters as $row) {
                $row->no_of_reason = GoodKitDetail::where('reason_master_id', $row->id)->count();
            }
        }

        return view('reason_master.index')->with('reason_masters', $reason_masters);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('reason_master.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->except('_token');

        $validator = \Validator::make($request->all(), [
            'reason_code' => 'required|unique:reason_master|max:255',
            'reason_desc' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $reason_master = ModelFactory::getInstance('ReasonMaster');
        $reason_master->reason_code = $request->reason_code;
        $reason_master->reason_desc = $request->reason_desc;

        if ($reason_master->save()) {
            Flash::success('Successfully created reason master.');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reason_master = ModelFactory::getInstance('ReasonMaster')->find($id);

        return view('reason_master.edit')->with('reason_master', $reason_master);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->except('_token');

        $validator = \Validator::make($request->all(), [
            'reason_code' => 'required|max:255|unique:reason_master,reason_code,' . $request->id . ',id',
            'reason_desc' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $reason_master = ModelFactory::getInstance('ReasonMaster')->find($request->id);
        $reason_master->reason_code = $request->reason_code;
        $reason_master->reason_desc = $request->reason_desc;

        if ($reason_master->save()) {
            Flash::success('Successfully updated reason master.');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reason_master = \App\Models\ReasonMaster::findOrFail($id);

        if (empty($reason_master)) {
            Flash::error('Selected reason master not found.');
        }

        $reason_master->delete();

        Flash::success('Successfully deleted reason master.');
        return redirect(route('reason_master.index'));
    }
}

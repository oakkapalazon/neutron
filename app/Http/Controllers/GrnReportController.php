<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Factories\ModelFactory;
use App\Filters\SearchFilter;
use App\Exports\GrnQueryExport;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Session;
use Flash;

class GrnReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
      $customer_code = \App\Models\Customer::getCustomerCodesOnly();

      // $select = [
      //   'grn_no',
      //   'collection_date',
      //   'job_id',
      //   'customer_name',
      //   'kit_type',
      //   'set_id',
      //   'part_type',
      //   'good_part_detail.serial_no',
      //   'good_part_detail.rf',
      //   'good_part_detail.arf'
      // ];
      //
      // $grn_report = ModelFactory::getInstance('GoodReceiptHeader')
      //               ->join('customer', 'good_receipt_header.customer_id', '=', 'customer.id')
      //               ->join('good_kit_detail', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id')
      //               ->join('kit_master', 'good_kit_detail.kit_master_id', '=', 'kit_master.id')
      //               ->join('part_type_master', 'kit_master.id', '=', 'part_type_master.kit_master_id')
      //               ->join('good_part_detail', 'part_type_master.id', '=', 'good_part_detail.part_type_id')
      //               ->select($select)
      //               ->orderby('grn_no', 'asc')
      //               ->orderby('kit_type', 'asc')
      //               ->orderby('part_type', 'asc');
      //
      // $searchFilter = SearchFilter::searchKeyword($grn_report, $request->q, 'grn_report');
      // $grn_report = $searchFilter->paginate(50);

      return view('grn_query_report.index', compact('customer_code'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @return BinaryFileResponse
     */
    public function exportGRNQueryDetail(Request $request)
    {
        Session::forget(['grn_no', 'customer_code', 'job_id', 'date_from', 'date_to', 'kit_master_id', 'part_type', 'serial_no']);

        session([
            'set_id' => $request->hidden_set_id,
//          'grn_no' => $request->hidden_grn_no,
          'customer_code' => $request->hidden_customer_code,
          'job_id' => $request->hidden_job_id,
          'date_from' => $request->hidden_date_from,
          'date_to' => $request->hidden_date_to,
          'kit_master_id' => $request->hidden_kit_type,
          'part_type' => $request->hidden_part_type,
          'serial_no' => $request->hidden_serial_no
        ]);

        return Excel::download(new GrnQueryExport(), 'grn_query.xlsx');
    }
}

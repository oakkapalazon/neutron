<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Factories\ModelFactory;
use App\Filters\SearchFilter;
use App\Exports\PartMasterExport;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Session;
use Flash;

class PartMasterReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customer_code = \App\Models\Customer::getCustomerCodesOnly();
        $kit_master = \App\Models\KitMaster::getKitTypes();

        return view('part_master_report.index', compact('customer_code', 'kit_master'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function exportPartMasterDetail(Request $request)
    {
        Session::forget(['kit_type', 'part_type', 'serial_no', 'set_id', 'created_at']);

        session([
            'kit_type' => $request->hidden_kit_type,
            'part_type' => $request->hidden_part_type,
            'serial_no' => $request->hidden_serial_no,
            'set_id' => $request->hidden_set_id,
            'created_at' => $request->hidden_created_at
        ]);

        return Excel::download(new PartMasterExport(), 'part_master_report.xlsx');
    }
}

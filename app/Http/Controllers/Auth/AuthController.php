<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use App\Factories\ModelFactory;
use Validator;
use Auth;

class AuthController extends Controller
{
  public function getLogin()
  {
    return view('auth.login');
  }

  public function logout()
  {
    Auth::logout();
    return redirect('/login');
  }
}

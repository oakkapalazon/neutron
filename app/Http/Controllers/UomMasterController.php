<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Factories\ModelFactory;
use App\Filters\SearchFilter;
use Flash;

class UomMasterController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $select = [
      'id',
      'uom_code',
      'uom_desc'
    ];

    $uom_masters = ModelFactory::getInstance('UomMaster')->select($select);
    $searchFilter = SearchFilter::searchKeyword($uom_masters, $request->q, 'uom_master');
    $uom_masters = $searchFilter->paginate(10000);

    if(count($uom_masters) > 0)
    {
      foreach($uom_masters as $row)
      {
        $row->count = ModelFactory::getInstance('KitMaster')->where('uom_id', $row->id)->count();
      }
    }

    return view('uom_master.index')->with('uom_masters', $uom_masters);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('uom_master.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $input = $request->except('_token');

    $validator = \Validator::make($request->all(), [
      'uom_code' => 'required|unique:uom_master|max:255',
      'uom_desc' => 'required'
    ]);

    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }

    $uom_master = ModelFactory::getInstance('UomMaster');
    $uom_master->uom_code = $request->uom_code;
    $uom_master->uom_desc = $request->uom_desc;

    if($uom_master->save())
    {
      Flash::success( 'Successfully created uom master.' );
      return redirect()->back();
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $uom_master = ModelFactory::getInstance('UomMaster')->find($id);

    return view('uom_master.edit')->with('uom_master', $uom_master);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $input = $request->except('_token');

    $validator = \Validator::make($request->all(), [
      'uom_code' => 'required|max:255|unique:uom_master,uom_code,'.$request->id. ',id',
      'uom_desc' => 'required'
    ]);

    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }

    $uom_master = ModelFactory::getInstance('UomMaster')->find($request->id);
    $uom_master->uom_code = $request->uom_code;
    $uom_master->uom_desc = $request->uom_desc;

    if($uom_master->save())
    {
      Flash::success( 'Successfully updated UOM Master.' );
      return redirect()->back();
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $uom_master = \App\Models\UomMaster::findOrFail($id);

    if( empty( $uom_master ) )
    {
      Flash::error( 'Selected uom master not found.' );
    }

    $uom_master->delete();

    Flash::success( 'Successfully deleted uom master.' );
    return redirect( route( 'uom_master.index' ) );
  }
}

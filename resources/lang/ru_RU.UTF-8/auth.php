<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Мы не смогли авторизовать Вас с введенными данными',
    'throttle' => 'Слишком много попыток входа. Попробуйте еще через :seconds сек.',

    'password' => 'Пароль',

    'form' => [
        'login' => [
            'title' => 'Авторизация',
            'remember' => 'запомнить меня',
            'login' => 'Войти',
            'forgot' => 'Забыли пароль?',
        ],
        'username_or_email' => 'Логин или e-mail',

    ]


];

<?php
return [
    'jobs' => 'Задачи',
    'settings' => 'Настройки',
    'users' => 'Пользователи',
    'permissions' => 'Разрешения',

    'profile' => 'Профиль',
    'logout' => 'Выйти',
];
<?php
return [
    'Permissions' => 'Разрешения',
    'access control' => 'управление доступом',

    'Permission' => 'Разрешение',
    'Yes' => 'Да',

    'role' => [
        'Administrator' => 'Администратор',
        'Content editor' => 'Редактор',
    ],

    'ability' => [
        'View Users' => 'Просмотр пользователей',
    ]

];

<?php

return [
    'users' => 'Пользователи',
    'manage' => 'Управление пользователями',

    'add' => 'Добавить пользователя',

    'table' => [
        'username' => 'логин',
        'name' => 'имя',
        'email' => 'email',
        'registered' => 'зарегистрирован',
        'actions' => 'действия',
    ],


];
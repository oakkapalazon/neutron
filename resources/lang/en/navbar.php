<?php
return [
    'jobs' => 'Jobs',
    'settings' => 'Settings',
    'users' => 'Users',
    'permissions' => 'Permissions',

    'profile' => 'Profile',
    'logout' => 'Logout',
];
<?php

return [
    'users' => 'Users',
    'manage' => 'Manage users',

    'add' => 'Add user',

    'table' => [
        'username' => 'username',
        'name' => 'name',
        'email' => 'email',
        'registered' => 'registered',
        'actions' => 'actions',
    ],
];
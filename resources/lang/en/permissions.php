<?php
return [
    'Permissions' => 'Permissions',
    'access control' => 'access control',

    'Permission' => 'Permission',
    'Yes' => 'Yes',

    'role' => [
        'Administrator' => 'Administrator',
        'Content editor' => 'Content Editor',
    ],

    'ability' => [
        'View Users' => 'View Users',
    ]

];

@extends('layouts.auth')

@section('htmlheader_title')
    Log in
@endsection

@section('content')
<body class="hold-transition login-page">

  <div class="login-box">
    <div class="login-logo">
      <a href="{{ url('/home') }}">NEUTRON Technology</a><br />
      <span class="sub-header">Parts Tracking System (PTS)</span>
    </div><!-- end login-logo -->

    @if (count($errors) > 0)
    <div class="alert alert-danger">
      <strong>Whoops!</strong> There were some problems with your input.<br><br>
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div><!-- end alert alert-danger -->
    @endif

    <div class="login-box-body">
      <p class="login-box-msg">Protected Area</p>

      <form action="{{ url('/login') }}" method="post">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="form-group has-feedback">
          <input type="text" class="form-control" placeholder="User ID" name="email"/>
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div><!-- end form-group -->

        <div class="form-group has-feedback">
          <input type="password" class="form-control" placeholder="Password" name="password"/>
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div><!-- end form-group -->

        <div class="row">
          <div class="col-xs-8">
          </div><!-- end col-xs-8 -->

          <div class="col-xs-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
          </div><!-- end col-xs-4 -->

        </div><!-- end row -->
      </form>

    </div><!--  end login-box-body -->

  </div><!-- /.login-box -->

  @include('layouts.partials.scripts_auth')

  <script>
    $(function () {

      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
      });

    });
  </script>

</body>

@endsection

@extends('layouts.app')

@section('content')

  <section class="content-header">
    <h1>Create User</h1>
  </section>

  <div class="content">

    @include('adminlte-templates::common.errors')
    {{--@include('adminlte-templates::common.success')--}}

    <div class="box box-primary">
      <div class="box-body">

        <div class="row">

          {!! Form::open(['route' => 'users.store']) !!}

          <div class="form-group col-sm-6 {{ ($errors->has('name') ? 'has-error' : '') }}">
            {!! Form::label('name', 'Name *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('name', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->


          <div class="form-group col-sm-6 {{ ($errors->has('user_id') ? 'has-error' : '') }}">
            {!! Form::label('user_id', 'User ID *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('user_id', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('new_password') ? 'has-error' : '') }}">
            {!! Form::label('new_password', 'Password *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::password('new_password', ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('new_password_confirmation') ? 'has-error' : '') }}">
            {!! Form::label('new_password_confirmation', 'Confirm Password *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::password('new_password_confirmation', ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ $errors->has('access_level') ? 'has-error' : '' }}">
            {!! Form::label('access_level', 'Access Level *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::select('access_level', $access_level, null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-12">
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
            <a href="{!! route('users.index') !!}" class="btn btn-default">Cancel</a>
          </div><!-- end form-group -->

          {!! Form::close() !!}
        </div><!-- end row -->
      </div><!-- end box-body -->
    </div><!-- end box -->

  </div><!-- end content -->

@endsection

@extends('layouts.app')

@section('content')

  <section class="content-header">
    <div class="col-md-8">
      <h1>Reason Master Lists</h1>
    </div><!-- end col-md-8 -->

    <div class="col-md-4">
      <a href="{!! route('reason_master.create') !!}" class="btn btn-primary pull-right">Add New</a>
    </div><!-- end col-md-4 -->
  </section>

  <div class="content">

    <div class="box box-primary">
      <div class="box-body">

        <!-- {!! Form::open(['method' => 'GET', 'class'=>'form-horizontal']) !!}

        <div class="form-group">
          <label class="col-md-2 control-label" for="search_field">Search Keyword: </label>

          <div class="col-md-3">
            {!! Form::text('q', null,['class'=>'form-control', 'id'=>'search_field']) !!}
          </div>

          <div class="col-md-5">
            {!!Form::submit('Search',['class'=>'btn btn-default btn-cs search_btn'])!!}
            <a href="{!! route('reason_master.index') !!}" class="btn btn-default btn-cs">Clear</a>
          </div>

        </div>

        {!!Form::close()!!} -->

        <div class="row">

          <div class="col-md-12">
            <table class="table table-bordered table-responsive table-striped" id="reason-table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Code</th>
                  <th>Description</th>
                  <th>Action</th>
                </tr>
              </thead>

              @php $count = 1; @endphp

              <tbody>
                @if(count($reason_masters) > 0)

                  @foreach($reason_masters as $row)
                  <tr>
                    <td>{{ $count }}</td>
                    <td>{{ $row->reason_code }}</td>
                    <td>{{ $row->reason_desc }}</td>
                    <td>
                      {!! Form::open(['route' => ['reason_master.destroy', $row->id], 'method' => 'delete']) !!}
                      <a href="{!! route('reason_master.edit', [$row->id]) !!}" class='btn btn-default btn-xs' title='Edit'><i class="glyphicon glyphicon-edit"></i></a>

                      @if($row->no_of_reason > 0)

                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'title' => 'Delete', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')", 'disabled' => true]) !!}

                      @else

                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'title' => 'Delete', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}

                      @endif

                      {!! Form::close() !!}
                    </td>
                  </tr>

                  @php $count++; @endphp

                  @endforeach

                @else

                  <tr>
                    <td colspan="4">No Result Found</td>
                  </tr>

                @endif
              </tbody>
              <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Code</th>
                  <th>Description</th>
                </tr>
              </tfoot>
            </table>

            {{ $reason_masters->links() }}

          </div><!-- end col-md-12 -->

        </div><!-- end row -->
      </div><!-- end box-body -->
    </div><!-- end box -->

  </div><!-- end content -->

@endsection


@section('extra-js')

  <script type="text/javascript">

    $(function() {

      $('#search_date').datepicker({
        format: 'dd/mm/yyyy',
      });

      $('#reason-table').DataTable({
        lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
        order: [],
        initComplete: function () {

        var r = $('#reason-table tfoot tr');
        $('#reason-table thead').append(r);

        this.api().columns().every(function () {
          var column = this;
          var input = document.createElement("input");
          input.className = "search";
          $(input).appendTo($(column.footer()).empty())
          .on('change', function () {
            column.search($(this).val(), false, false, true).draw();
          });
        });
      }
      });

    });

  </script>

@endsection

@extends('layouts.app')

@section('content')

  <section class="content-header">
    <h1>Edit Reason Master</h1>
  </section>

  <div class="content">

    @include('adminlte-templates::common.errors')
    {{--@include('adminlte-templates::common.success')--}}

    <div class="box box-primary">
      <div class="box-body">

        <div class="row">

          {!! Form::model($reason_master, ['route' => ['reason_master.update', $reason_master->id], 'method' => 'patch', 'enctype'=>'multipart/form-data']) !!}

          <input type="hidden" name="id" value="{{ $reason_master->id }}">

          <div class="form-group col-sm-6 {{ ($errors->has('reason_code') ? 'has-error' : '') }}">
            {!! Form::label('reason_code', 'Code *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('reason_code', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('uom_desc') ? 'has-error' : '') }}">
            {!! Form::label('reason_desc', 'Description *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('reason_desc', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-12">
            {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
            <a href="{!! route('reason_master.index') !!}" class="btn btn-default">Cancel</a>
          </div><!-- end form-group -->

          {!! Form::close() !!}
        </div><!-- end row -->
      </div><!-- end box-body -->
    </div><!-- end box -->

  </div><!-- end content -->

@endsection

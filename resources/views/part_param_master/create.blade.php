@extends('layouts.app')

@section('content')

  <section class="content-header">
    <h1>Create Part Parameter Master</h1>
  </section>

  <div class="content">

    @include('adminlte-templates::common.errors')
    {{--@include('adminlte-templates::common.success')--}}

    <div class="box box-primary">
      <div class="box-body">

        <div class="row">

          {!! Form::open(['route' => 'part_param_master.store']) !!}

          <div class="form-group col-sm-6 {{ ($errors->has('kit_master_id') ? 'has-error' : '') }}">
            {!! Form::label('kit_master_id', 'Kit Type *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::select('kit_master_id', $kit_masters, null, ['class' => 'form-control', 'id' => 'kit_master_id']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('part_type_id') ? 'has-error' : '') }}">
            {!! Form::label('part_type_id', 'Part Type *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              @if(empty(old('part_type_id')))

                {!! Form::select('part_type_id', array('' => 'Please select'), null, ['class' => 'form-control', 'id' => 'part_type_id']) !!}

              @else

              @php

                $part_types = \App\Models\PartTypeMaster::getPartTypeMaster(old('kit_master_id'));

              @endphp

                {!! Form::select('part_type_id', $part_types, null, ['class' => 'form-control', 'id' => 'part_type_id']) !!}

              @endif
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('parameter_master_id') ? 'has-error' : '') }}">
            {!! Form::label('parameter_master_id', 'Parameter *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::select('parameter_master_id', $param_masters, null, ['class' => 'form-control', 'id' => 'parameter_id']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('point_msr') ? 'has-error' : '') }}">
            {!! Form::label('point_msr', 'Point Of Msr *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::number('point_msr', null, ['min' => '0' ,'class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('min_limit') ? 'has-error' : '') }}">
            {!! Form::label('min_limit', 'Min Limit:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::number('min_limit', null, ['min' => '0' ,'class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('max_limit') ? 'has-error' : '') }}">
            {!! Form::label('max_limit', 'Max Limit:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::number('max_limit', null, ['min' => '0' ,'class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('specs') ? 'has-error' : '') }}">
            {!! Form::label('specs', 'Specs:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('specs', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="clearfix">
          </div><!-- end clearfix -->

          <div class="form-group col-sm-6 {{ ($errors->has('exclude_post_cleaning') ? 'has-error' : '') }}">
            {!! Form::label('exclude_post_cleaning', 'Exclude of Post Cleaning:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::checkbox('exclude_post_cleaning', 1, false) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('print_pom_value') ? 'has-error' : '') }}">
            {!! Form::label('print_pom_value', 'Print POM Value:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::checkbox('print_pom_value', 1, false) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('print_average_value') ? 'has-error' : '') }}">
            {!! Form::label('print_average_value', 'Print Average Value:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::checkbox('print_average_value', 1, false) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-12">
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
            <a href="{!! route('part_param_master.index') !!}" class="btn btn-default">Cancel</a>
          </div><!-- end form-group -->

          {!! Form::close() !!}
        </div><!-- end row -->
      </div><!-- end box-body -->
    </div><!-- end box -->

  </div><!-- end content -->

@endsection

@section('extra-js')

  <script src="{{ asset('/js/custom/main.js') }}"></script>

@endsection

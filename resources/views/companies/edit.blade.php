@extends('layouts.app')

@section('content')

  <section class="content-header">
    <h1>Edit Company</h1>
  </section>

  <div class="content">

    @include('adminlte-templates::common.errors')
    {{--@include('adminlte-templates::common.success')--}}

    <div class="box box-primary">
      <div class="box-body">

        <div class="row">

          {!! Form::model($company, ['route' => ['companies.update', $company->id], 'method' => 'patch', 'enctype'=>'multipart/form-data']) !!}

          <input type="hidden" name="id" value="{{ $company->id }}">

          <div class="form-group col-sm-6 {{ ($errors->has('company_code') ? 'has-error' : '') }}">
            {!! Form::label('company_code', 'Company Code *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('company_code', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('company_name') ? 'has-error' : '') }}">
            {!! Form::label('company_name', 'Company Name:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('company_name', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ $errors->has('website') ? 'has-error' : '' }}">
            {!! Form::label('website', 'Website *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('website', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('email') ? 'has-error' : '') }}">
            {!! Form::label('email', 'Email:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('email', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('fax') ? 'has-error' : '') }}">
            {!! Form::label('fax', 'Fax:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('fax', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('registration_no') ? 'has-error' : '') }}">
            {!! Form::label('registration_no', 'Registration No:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('registration_no', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('address') ? 'has-error' : '') }}">
            {!! Form::label('address', 'Address:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::textarea('address', null, ['class' => 'form-control', 'rows' => 4, 'cols' => 30]) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('tel_no') ? 'has-error' : '') }}">
            {!! Form::label('tel_no', 'Tel No:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('tel_no', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-12">
            {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
            <a href="{!! route('companies.index') !!}" class="btn btn-default">Cancel</a>
          </div><!-- end form-group -->

          {!! Form::close() !!}
        </div><!-- end row -->
      </div><!-- end box-body -->
    </div><!-- end box -->

  </div><!-- end content -->

@endsection

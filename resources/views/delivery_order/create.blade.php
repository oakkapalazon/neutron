@extends('layouts.app')

@section('content')

  <section class="content-header">
    <h1>Delivery Order</h1>
  </section>

  <div class="content">

    @include('adminlte-templates::common.errors')
    {{--@include('adminlte-templates::common.success')--}}

    <div class="box box-primary">
      <div class="box-body">

        <div class="row">

          {!! Form::open(['route' => 'delivery_order.store']) !!}

          @if(Request::get('delivery_order_id'))

          @php
            $delivery_order = \App\Models\DeliveryOrder::join('good_receipt_header', 'delivery_order.good_receipt_header_id', '=', 'good_receipt_header.id')
                              ->join('customer', 'good_receipt_header.customer_id', '=', 'customer.id')
                              ->where('delivery_order.id', Request::get('delivery_order_id'))
                              ->select('delivery_order.*', 'customer_code', 'customer_name', 'grn_no', 'po_no')
                              ->first();
            $delivery_order_status = $delivery_order->status;
          @endphp

          <input type="hidden" name="delivery_order_id" id="delivery_order_id" value="{{ $delivery_order->id }}">
          <input type="hidden" name="showed_child" value="{{ $showed_child }}">

          <div class="form-group col-sm-6 {{ ($errors->has('do_no') ? 'has-error' : '') }}">
            {!! Form::label('do_no', 'DO No:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('do_no', $delivery_order->do_no, ['class' => 'form-control', 'disabled' => true]) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <input type="hidden" name="do_no_hidden" value="{{ $delivery_order->do_no }}">

          <div class="form-group col-sm-6 {{ ($errors->has('cust_code') ? 'has-error' : '') }}">
            {!! Form::label('cust_code', 'Customer *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
{{--              {!! Form::text('cust_code', $delivery_order->customer_code, ['class' => 'form-control', 'disabled' => true]) !!}--}}
              {!! Form::select('cust_code', $customer_code, $delivery_order->customer_code, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('grn_no') ? 'has-error' : '') }}">
            {!! Form::label('grn_no', 'GRN No *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('grn_no', $delivery_order->grn_no, ['class' => 'form-control', 'readonly' => true]) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

            <input type="hidden" name="grn_id" id="grn_id">

          <input type="hidden" name="grn_no_hidden" id="grn_no_hidden" value="{{ $delivery_order->good_receipt_header_id }}">

          <div class="form-group col-sm-6 {{ ($errors->has('job_id') ? 'has-error' : '') }}">
            {!! Form::label('job_id', 'Job ID *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8 job_id">

              @if($delivery_order_status == 'C')
                {!! Form::text('job_id', $delivery_order->job_id, ['class' => 'form-control', 'disabled' => true]) !!}
              @else
                {!! Form::select('job_id', ['' => 'Please select'], null, ['class' => 'form-control']) !!}
              @endif

            </div>
          </div>

            <input type="hidden" name="hidden_job_id" id="hidden_job_id" value="{{ $delivery_order->job_id }}">

          <div class="form-group col-sm-6 {{ ($errors->has('remarks') ? 'has-error' : '') }}">
            {!! Form::label('remarks', 'Remark:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::textarea('remarks', $delivery_order->remarks, ['class' => 'form-control', 'rows' => 4, 'cols' => 40]) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <!-- <div class="form-group col-sm-6 {{ ($errors->has('cust_name') ? 'has-error' : '') }}">
            {!! Form::label('cust_name', 'Customer Name:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('cust_name', $delivery_order->customer_name, ['class' => 'form-control', 'disabled' => true]) !!}
            </div>
          </div> -->

          <div class="form-group col-sm-6 {{ ($errors->has('date') ? 'has-error' : '') }}">
            {!! Form::label('date', 'Date *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('date', \Carbon\Carbon::parse($delivery_order->date)->format('d/m/Y'), ['class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('po_no') ? 'has-error' : '') }}">
            {!! Form::label('po_no', 'PO No *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8 po_no">
              {!! Form::select('po_no', ['' => 'Please select'], null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <input type="hidden" name="po_no_hidden" value="{{ $delivery_order->po_no }}">

          <div class="form-group col-sm-6 {{ ($errors->has('status') ? 'has-error' : '') }}">
            {!! Form::label('status', 'Status *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">

              @if($delivery_order_status == 'C')
                {!! Form::select('status', $status, $delivery_order_status, ['class' => 'form-control', 'disabled' => true]) !!}
              @else
                {!! Form::select('status', $status, $delivery_order_status, ['class' => 'form-control']) !!}
              @endif

            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          @else

          <input type="hidden" name="run_no" value="{{ $run_no }}" />

          <div class="form-group col-sm-6 {{ ($errors->has('do_no') ? 'has-error' : '') }}">
            {!! Form::label('do_no', 'DO No:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('do_no', $do_no, ['class' => 'form-control', 'readonly' => true]) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('cust_code') ? 'has-error' : '') }}">
            {!! Form::label('cust_code', 'Customer *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::select('cust_code', $customer_code, '', ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('grn_no') ? 'has-error' : '') }}">
            {!! Form::label('grn_no', 'GRN No *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('grn_no', null, ['class' => 'form-control', 'readonly' => true]) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <input type="hidden" name="grn_id" id="grn_id">

          <div class="form-group col-sm-6 {{ ($errors->has('job_id') ? 'has-error' : '') }}">
            {!! Form::label('job_id', 'Job ID *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8 job_id">
              {!! Form::select('job_id', ['' => 'Please select'], null, ['class' => 'form-control']) !!}
            </div>
          </div>

          <input type="hidden" name="hidden_job_id" id="hidden_job_id" value="{{ old('job_id') }}">

          <div class="form-group col-sm-6 {{ ($errors->has('remarks') ? 'has-error' : '') }}">
            {!! Form::label('remarks', 'Remark:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::textarea('remarks', null, ['class' => 'form-control', 'rows' => 4, 'cols' => 40]) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('date') ? 'has-error' : '') }}">
            {!! Form::label('date', 'Date *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('date', \Carbon\Carbon::now()->format('d/m/Y'), ['class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('po_no') ? 'has-error' : '') }}">
            {!! Form::label('po_no', 'PO No *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8 po_no">
              {!! Form::select('po_no', ['' => 'Please select'], null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <!-- <div class="form-group col-sm-6 {{ ($errors->has('status') ? 'has-error' : '') }}">
            {!! Form::label('status', 'Status *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::select('status', $status, null, ['class' => 'form-control']) !!}
            </div>
          </div> -->

          @endif

          <div class="form-group col-sm-12">
            @if(Request::get('delivery_order_id'))
              @if($delivery_order_status == 'O' || $delivery_order_status == '')
                {!! Form::submit('Update', ['class' => 'btn btn-primary btn-update']) !!}
              @endif
            @else
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
            @endif

            @if($delivery_order_status == 'C')

              <a href="{{ url('/delivery_order/reopen-do?do_id=' .$delivery_order->id. '&grn_id=' .$delivery_order->good_receipt_header_id) }}" class="btn btn-danger btn-reopen">Reopen</a>

            @endif

            @if(Request::get('delivery_order_id') && $row_do_child > 0)

              <a href="{!! url('/report/delivery_order_report/print_delivery_order_report?customer_code=' .$delivery_order->customer_code. '&job_id=' .$delivery_order->job_id. '&do_no=' .$delivery_order->id) !!}" class="btn btn-primary" target="_blank">Print</a>

            @endif

            <a href="{!! route('delivery_order.index') !!}" class="btn btn-default">Cancel</a>
          </div>

          {!! Form::close() !!}

          <hr>


        </div><!-- end row -->
      </div><!-- end box-body -->

      <div class="box-body main-delivery-order-table" style="display: none;">
        <div class="row">

          <div class="col-md-12">
            <table class="table table-bordered table-responsive table-striped" id="delivery-order-table">
              <thead>
                <tr>
                  <th>Kit</th>
                  <th>Reason</th>
                  <th>Tool ID</th>
                  <th>Set ID</th>
                  <!-- <th>Serial No</th> -->
                  <!-- <th>RF</th> -->
                  <th>Qty</th>
                  <th>Qty (Bal.)</th>
                  <th style="width: 15%;">Selected + Qty</th>
                </tr>
              </thead>

              <tbody>
              </tbody>
            </table>

          </div><!-- end col-md-12 -->
          <div class="form-group col-sm-12">

            @if($delivery_order_status == 'O' || $delivery_order_status == '')
              <a href="#" class="btn btn-info btn-post" style="float: right;">Post</a>
            @endif
          </div>
        </div><!-- end row -->
      </div><!-- end box-body -->
    </div><!-- end box -->
  </div><!-- end content -->

@endsection

@section('extra-js')

  <script src="{{ asset('/js/custom/delivery-order.js') }}"></script>

  <script type="text/javascript">

    $(function() {

      var do_child = '{{ $row_do_child }}';

      if( do_child > 0 )
      {
        $( '#cust_code' ).attr( 'readonly', true );
        $( '#job_id' ).attr( 'readonly', true );
      }

      else
      {
        $( '#cust_code' ).attr( 'readonly', false );
        $( '#job_id' ).attr( 'readonly', false );
      }

      $("#date").datepicker({
        format: 'dd/mm/yyyy',
      });

    });

  </script>

@endsection

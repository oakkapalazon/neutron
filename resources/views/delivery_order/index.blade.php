@extends('layouts.app')

@section('content')

  <section class="content-header">
    <div class="col-md-8">
      <h1>Delivery Order Lists</h1>
    </div><!-- end col-md-8 -->

    <div class="col-md-4">
      <a href="{!! route('delivery_order.create') !!}" class="btn btn-primary pull-right">Add New</a>
    </div><!-- end col-md-4 -->
  </section>

  <div class="content">

    <div class="box box-primary">
      <div class="box-body">

        <!-- {!! Form::open(['method' => 'GET', 'class'=>'form-horizontal']) !!}

        <div class="form-group">
          <label class="col-md-2 control-label" for="search_field">Search Keyword: </label>

          <div class="col-md-3">
            {!! Form::text('q', null,['class'=>'form-control', 'id'=>'search_field']) !!}
          </div>

          <div class="col-md-5">
            {!!Form::submit('Search',['class'=>'btn btn-default btn-cs search_btn'])!!}
            <a href="{!! route('kit_master.index') !!}" class="btn btn-default btn-cs">Clear</a>
          </div>

        </div>

        {!!Form::close()!!} -->

        <div class="row">

          <div class="col-md-12">
            <table class="table table-bordered table-responsive table-striped" id="delivery-order-table">
              <thead>
                <tr>
{{--                  <th>ID</th>--}}
                  <th style="width: 15%;">DO No</th>
                  <th style="width: 15%;">Customer</th>
                  <th>Delivery Date</th>
                  <th>GRN No</th>
                  <th>Job ID</th>
                  <th>Remark</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>

              @php $count = 1; @endphp

              <tbody>
                @foreach($delivery_order as $row)
                <tr>
{{--                  <td>{{ $count }}</td>--}}
                  <td>{{ $row->do_no }}</td>
                  <td>{{ $row->customer_code }}</td>
                  <td>{{ $row->date }}</td>
                  <td>{{ $row->grn_no }}</td>
                  <td>{{ $row->job_id }}</td>
                  <td>{{ $row->remarks }}</td>
                  <td>

                  @if($row->status == 'O')
                    Open
                  @else
                    Closed
                  @endif

                  </td>
                  <td>

                    {!! Form::open(['route' => ['delivery_order.destroy', $row->id], 'method' => 'delete']) !!}
                    <a href="/delivery_order/create?delivery_order_id={{ $row->id }}" class='btn btn-default btn-xs' title='Edit'><i class="glyphicon glyphicon-edit"></i></a>

{{--                    @if($row->status == 'C' || $row->no_of_grn_child > 0)--}}
{{--                      {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'title' => 'Delete', 'class' => 'btn btn-danger btn-xs', 'disabled' => true]) !!}--}}
{{--                    @else--}}
{{--                      {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'title' => 'Delete', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}--}}
{{--                    @endif--}}
                    {!! Form::close() !!}

                  </td>
                </tr>

                @php $count++; @endphp

                @endforeach
              </tbody>
              <tfoot>
                <tr>
{{--                  <th>ID</th>--}}
                  <th>DO No</th>
                  <th>Code</th>
                  <th>GRN No</th>
                  <th>PO No</th>
                  <th>Date</th>
                  <th>Remark</th>
                  <th>Status</th>
                </tr>
              </tfoot>
            </table>

            {{ $delivery_order->links() }}

          </div><!-- end col-md-12 -->

        </div><!-- end row -->
      </div><!-- end box-body -->
    </div><!-- end box -->

    <!-- <div class="row">
      <div class="col-md-12">
        <p class="label label-info info-notice">
          * You cannot delete kit types which are assigned in part master, part type master and
          part parameter master modules.
        </p>
      </div>
    </div> -->

  </div><!-- end content -->

@endsection


@section('extra-js')

  <script type="text/javascript">

    $(function() {

//       setTimeout(function() {
//   location.reload();
// }, 30000);

      $('#search_date').datepicker({
        format: 'dd/mm/yyyy',
      });

      $('#delivery-order-table').DataTable({
        lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
        order: [],
        initComplete: function () {

        var r = $('#delivery-order-table tfoot tr');
        $('#delivery-order-table thead').append(r);

        this.api().columns().every(function () {
          var column = this;
          var input = document.createElement("input");
          input.className = "search";
          $(input).appendTo($(column.footer()).empty())
          .on('change', function () {
            column.search($(this).val(), false, false, true).draw();
          });
        });
      }
      });

    });

  </script>

@endsection

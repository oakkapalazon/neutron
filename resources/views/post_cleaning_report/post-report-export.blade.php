<!DOCTYPE html>
<html>
<head>
{{--    <meta charset="UTF-8">--}}
{{--    <title> @yield('htmlheader_title', config('app.name','Neutron') ) </title>--}}
{{--    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>--}}
{{--    <meta name="csrf-token" content="{{ csrf_token() }}">--}}

    <link href="{{ asset('css/postcleaning-report.css') }}" rel="stylesheet" type="text/css" />

    <style>
        table.report-container {
            page-break-after:always;
            width: 100%;
        }
        thead.report-header {
            display:table-header-group;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>

<body>

{{--  @php--}}
{{--    $count = 0;--}}
{{--  @endphp--}}
@foreach($do_detail as $norow => $do_main)
{{--    @if($norow == 1 || $norow == 2 || $norow == 3 || $norow == 4 || $norow == 5 || $norow == 6 || $norow == 7 || $norow == 8)--}}
{{--      @php--}}
{{--        $count = 0;--}}
{{--      @endphp--}}
{{--    @endif--}}
@php
$get_grn_detail = \App\Models\DeliveryOrder::where('good_receipt_header_id', $do_main->good_receipt_header_id)->first();
@endphp

<div class="box box-primary">

    <table class="report-container">
        <thead class="report-header">
        <tr>
            <td>
                <div class="rp-main-header-outer">
                    <div class="rp-main-header">
                        <div class="rp-head-title">Neutron Technology Enterprise Pte. Ltd</div>
                        <div class="rp-head-title">Post Cleaning Inspection Report</div>
                        {{--            <div class="rp-head-title-underline">{{ $do_main->customer_name }}</div>--}}
                    </div>
                </div>
                <div class="rp-mainSubheader">
                    <div><strong>Customer</strong><span style="margin-left: 48px;">{{ $do_main->customer_name }}</span></div>
                    <table border="0" cellpadding="0" cellspacing="0" class="rp-tblContent" style="font-size: 15px;">
                        <tbody>
                        <tr>
                            <td style="width: 37.3%;">
                                <span class="rp-title">Your Job Ref</span><span class="rp-desc">{{ $do_main->job_id }}</span>
                            </td>
                            <td style="width: 28%;">
                                <span class="rp-title">Process</span><span class="rp-desc">{{ $do_main->process }}</span>
                            </td>
                            <td>
                                <span class="rp-title">Kit</span><span class="rp-desc">{{ $do_main->kit_type }}</span>
                            </td>
                        </tr>
                        <tr>
                            <td>

                                @if($get_grn_detail)

                                <span class="rp-title">Report Date</span><span class="rp-desc">{{ $get_grn_detail->date }}</span>

                                @else

                                <span class="rp-title">Report Date</span><span class="rp-desc"></span>

                                @endif

                            </td>
                            <td>
                                <span class="rp-title">From Tool ID</span><span class="rp-desc">{{ $do_main->tool_kit }}</span>
                            </td>
                            <td>
                                <span class="rp-title">Set ID</span><span class="rp-desc">{{ $do_main->set_id }}</span>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </td>
        </tr>
        </thead>

        <tbody class="report-content">
        <tr>
            <td class="report-content-cell">
                <div class="box-body">

                    @php
                    $get_do_following_detail = \App\Models\GoodReceiptHeader::join('good_kit_detail', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id')
                    ->join('good_part_detail', 'good_kit_detail.id', '=', 'good_part_detail.good_kit_detail_id')
                    ->join('part_type_master', 'good_part_detail.part_type_id', '=', 'part_type_master.id')
                    ->where('good_receipt_header.id', $do_main->good_receipt_header_id)
                    ->where('good_kit_detail.id', $do_main->good_kit_detail_id)
                    ->where('good_part_detail.actions', '<>', 2)
                    ->select('part_type', 'max_rf_print', 'good_part_detail.serial_no', 'good_part_detail.rf', 'arf', 'part_type_master.id as part_type_master_id', 'good_kit_detail.kit_master_id', 'good_part_detail.id as good_part_detail_id', 'image_path', 'visual_condition_desc')
                    ->orderBy('part_type_master.part_sequence', 'asc')
                    ->get();
                    @endphp
                    @foreach($get_do_following_detail as $part)

                    {{--          <div class="rp-main-content-outer {{ $count == '7' ? ' page-break' : '' }}">--}}
                        <div class="rp-main-content-outer">
                            <div class="rp-contentSubHeader">
                                <table border="0" cellpadding="0" cellspacing="0" class="rp-tblContent">
                                    <tbody>
                                    <tr>
                                        <td class="tdImg" style="width: 15%;">

{{--                                            @if(file_exists($part->image_path) && !empty($part->image_path))--}}

{{--                                            <img src="{{ asset( $part->image_path ) }}" alt="part-type" />--}}

{{--                                            @else--}}

{{--                                            <img src="{{ asset('img/no-image.jpg') }}" alt="no-image" />--}}

{{--                                            @endif--}}

                                        </td>
                                        <td style="vertical-align: top;">
                                            <table border="0" cellpadding="0" cellspacing="0" class="rp-tblContent">
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <table border="0" cellpadding="0" cellspacing="0" class="rp-tblContent">
                                                            <tbody>
                                                            <tr>
                                                                <td style="width: 100%;">
                                                                    <table border="0" cellpadding="0" cellspacing="0" class="rp-tblContent">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td class="tdContTitle" colspan="3">
                                                                                <div class="rp-contentHeader">
                                                                                    <div class="rp-contentDesc" style="min-width: 20%;">
                                                                                        {{ $part->part_type }}
                                                                                    </div>
                                                                                    <div class="rp-contentDesc" style="min-width: 35.5%;">
                                                                                        {{ $part->serial_no }}
                                                                                    </div>
                                                                                    <div class="rp-contentDesc" style="min-width: 16.5%;">

                                                                                        @if(session('printrf_arf') == 'arf')

                                                                                        Total RF: {{ $part->arf }}

                                                                                        @else

                                                                                        RF: {{ $part->rf }}

                                                                                        @endif

                                                                                    </div>
                                                                                    <div class="rp-contentDesc" style="min-width: 28%;">

                                                                                        @if(!is_null($part->max_rf_print))

                                                                                        Lifetime RF Specs: {{ $part->max_rf_print }}

                                                                                        @endif

                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tdContTitle" style="width: 20%;">
                                                                                Parameters
                                                                            </td>
                                                                            <td class="tdContTitle" style="width: 21.5%;">
                                                                                Results
                                                                            </td>
                                                                            <td class="tdContTitle">
                                                                                Specs
                                                                            </td>
                                                                        </tr>

                                                                        @php
                                                                        $get_part_param_master = \App\Models\PartParamMaster::join('parameter_master', 'part_param_master.parameter_master_id', '=', 'parameter_master.id')
                                                                        ->where('kit_master_id', $part->kit_master_id)
                                                                        ->where('part_type_id', $part->part_type_master_id)
                                                                        ->orderBy('parameter_master.sequence', 'asc')
                                                                        ->select('param', 'print_average_value', 'specs', 'exclude_post_cleaning', 'print_pom_value', 'print_average_value', 'parameter_master.id as parameter_master_id', 'min_limit', 'max_limit', 'is_boolean')
                                                                        ->get();
                                                                        @endphp
                                                                        @foreach($get_part_param_master as $part_param)
                                                                        @php
                                                                        $get_avg_value = \App\Models\KitMaster::join('part_type_master', 'kit_master.id', '=', 'part_type_master.kit_master_id')
                                                                        ->join('param_datapoint_master', 'part_type_master.id', '=', 'param_datapoint_master.part_type_id')
                                                                        ->join('good_param_datapoint_detail', 'param_datapoint_master.id', '=', 'good_param_datapoint_detail.param_datapoint_master_id')
                                                                        ->join('parameter_master', 'param_datapoint_master.parameter_master_id', '=', 'parameter_master.id')
                                                                        ->where('part_type_master.kit_master_id', $part->kit_master_id)
                                                                        ->where('part_type_master.id', $part->part_type_master_id)
                                                                        ->where('parameter_master_id', $part_param->parameter_master_id)
                                                                        ->where('good_part_detail_id', $part->good_part_detail_id)
                                                                        ->where('parameter_master.is_boolean', '0')
                                                                        ->avg('param_datapoint_value');

                                                                        $get_boolean_value = \App\Models\KitMaster::join('part_type_master', 'kit_master.id', '=', 'part_type_master.kit_master_id')
                                                                        ->join('param_datapoint_master', 'part_type_master.id', '=', 'param_datapoint_master.part_type_id')
                                                                        ->join('good_param_datapoint_detail', 'param_datapoint_master.id', '=', 'good_param_datapoint_detail.param_datapoint_master_id')
                                                                        ->join('parameter_master', 'param_datapoint_master.parameter_master_id', '=', 'parameter_master.id')
                                                                        ->where('part_type_master.kit_master_id', $part->kit_master_id)
                                                                        ->where('part_type_master.id', $part->part_type_master_id)
                                                                        ->where('parameter_master_id', $part_param->parameter_master_id)
                                                                        ->where('good_part_detail_id', $part->good_part_detail_id)
                                                                        ->where('parameter_master.is_boolean', '1')
                                                                        ->select('param_datapoint_value')
                                                                        ->first();
                                                                        @endphp

                                                                        <tr>

                                                                            @if($part_param->exclude_post_cleaning == '0')
                                                                            @if($part_param->print_average_value == '1')
                                                                            <td class="tdContDesc">
                                                                                {{ $part_param->param }}
                                                                            </td>
                                                                            <td class="tdContDesc">

                                                                                {{--                                                    @if($part_param->print_average_value == '1' && $get_avg_value)--}}
                                                                                @if($part_param->print_average_value == '1')

                                                                                @if($part_param->is_boolean == 1)
                                                                                {{ $get_boolean_value['param_datapoint_value'] }}
                                                                                @else

                                                                                @if( (($part_param->min_limit <= $get_avg_value) && ($get_avg_value <= $part_param->max_limit))
                                                                                || (($part_param->min_limit <= $get_avg_value) && is_null($part_param->max_limit))
                                                                                || (is_null($part_param->min_limit) && ($get_avg_value <= $part_param->max_limit)) )
                                                                                {{ number_format($get_avg_value, 3, '.', ',') }} - Pass
                                                                                @else
                                                                                {{ number_format($get_avg_value, 3, '.', ',') }} - Fail
                                                                                @endif
                                                                                @endif

                                                                                @endif

                                                                            </td>
                                                                            <td class="tdContDesc">

                                                                                {{ $part_param->specs }}

                                                                            </td>
                                                                            @endif
                                                                            @endif

                                                                        </tr>

                                                                        @endforeach

                                                                        <tr>
                                                                            <td class="tdContDesc">
                                                                                Visual Condition(s)
                                                                            </td>
                                                                            <td colspan="2" class="tdContDesc">

                                                                                {{ $part->visual_condition_desc }}

                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td>

                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>

                                                        @php
                                                        $get_parameter_master = \App\Models\PartTypeMaster::join('part_param_master', 'part_type_master.id', '=', 'part_param_master.part_type_id')
                                                        ->join('parameter_master', 'part_param_master.parameter_master_id', '=', 'parameter_master.id')
                                                        ->where('part_type_master.id', $part->part_type_master_id)
                                                        ->select('parameter_master_id', 'part_param_master.kit_master_id', 'part_param_master.part_type_id', 'param', 'print_pom_value', 'exclude_post_cleaning')
                                                        ->get();
                                                        @endphp
                                                        @foreach($get_parameter_master as $parameter_master)
                                                        @if($parameter_master->print_pom_value == '1')
                                                        @if($parameter_master->exclude_post_cleaning == '0')

                                                        <div class="rf-paramValueBox">
                                                            <div class="paramHeader">{{ $parameter_master->param }}</div>
                                                            <div class="paramContentOuter">

                                                                @php
                                                                $get_parameter_master = \App\Models\GoodParamDataPointMaster::join('param_datapoint_master', 'good_param_datapoint_detail.param_datapoint_master_id', '=', 'param_datapoint_master.id')
                                                                ->join('good_part_detail', 'good_param_datapoint_detail.good_part_detail_id', '=', 'good_part_detail.id')
                                                                ->where('kit_master_id', $parameter_master->kit_master_id)
                                                                ->where('param_datapoint_master.part_type_id', $parameter_master->part_type_id)
                                                                ->where('parameter_master_id', $parameter_master->parameter_master_id)
                                                                ->where('good_part_detail.id', $part->good_part_detail_id)
                                                                ->select('param_datapoint_value', 'datapoint', 'param_datapoint_master.kit_master_id', 'param_datapoint_master.part_type_id', 'param_datapoint_master.parameter_master_id', 'good_param_datapoint_detail.good_part_detail_id')
                                                                ->get();
                                                                @endphp
                                                                @foreach($get_parameter_master as $parameter_master)

                                                                <div class="paramContentBox">{{ $parameter_master->datapoint }}&emsp;{{ $parameter_master->param_datapoint_value }}</div>

                                                                @endforeach

                                                            </div>
                                                        </div>

                                                        @endif
                                                        @endif
                                                        @endforeach

                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        {{--          @php--}}
                        {{--            $count++;--}}
                        {{--          @endphp--}}
                        @endforeach

                    </div>
            </td>
        </tr>
        </tbody>
    </table>

    </div><!-- end content -->


    @endforeach

</body>

</html>

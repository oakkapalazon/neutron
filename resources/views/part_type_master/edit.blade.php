@extends('layouts.app')

@section('content')

  <section class="content-header">
    <h1>Edit Part Type Master</h1>
  </section>

  <div class="content">

    @include('adminlte-templates::common.errors')
    {{--@include('adminlte-templates::common.success')--}}

    <div class="box box-primary">
      <div class="box-body">

        <div class="row">

          {!! Form::model($part_type_master, ['route' => ['part_type_master.update', $part_type_master->id], 'method' => 'patch', 'enctype'=>'multipart/form-data']) !!}

          <input type="hidden" name="id" value="{{ $part_type_master->id }}">

          <div class="form-group col-sm-6 {{ ($errors->has('kit_master_id') ? 'has-error' : '') }}">
            {!! Form::label('kit_master_id', 'Kit Type:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::select('kit_master_id', $kit_types, $part_type_master->kit_master_id, ['class' => 'form-control', 'disabled' => true]) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('part_type') ? 'has-error' : '') }}">
            {!! Form::label('part_type', 'Part Type:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('part_type', null, ['class' => 'form-control', 'readonly' => true]) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('kit_identifier') ? 'has-error' : '') }}">
            {!! Form::label('kit_identifier', 'Kit Identifier:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::checkbox('kit_identifier', 1, $part_type_master->kit_identifier) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('grade_required') ? 'has-error' : '') }}">
            {!! Form::label('grade_required', 'Grade Required:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::checkbox('grade_required', 1, $part_type_master->grade_required) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('max_rf_hrs') ? 'has-error' : '') }}">
            {!! Form::label('max_rf_hrs', 'Max RFHrs *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('max_rf_hrs', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('max_rf_print') ? 'has-error' : '') }}">
            {!! Form::label('max_rf_print', 'Max RF To Print *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('max_rf_print', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('part_sequence') ? 'has-error' : '') }}">
            {!! Form::label('part_sequence', 'Part Sequence *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('part_sequence', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-12">
            {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
            <a href="{!! route('part_type_master.index') !!}" class="btn btn-default">Cancel</a>
          </div><!-- end form-group -->

          {!! Form::close() !!}
        </div><!-- end row -->
      </div><!-- end box-body -->
    </div><!-- end box -->

  </div><!-- end content -->

@endsection

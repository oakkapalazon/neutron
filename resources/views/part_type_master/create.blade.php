@extends('layouts.app')

@section('content')

  <section class="content-header">
    <h1>Create Part Type Master</h1>
  </section>

  <div class="content">

    @include('adminlte-templates::common.errors')
    {{--@include('adminlte-templates::common.success')--}}

    <div class="box box-primary">
      <div class="box-body">

        <div class="row">

          {!! Form::open(['route' => 'part_type_master.store']) !!}

          <div class="form-group col-sm-6 {{ ($errors->has('kit_master_id') ? 'has-error' : '') }}">
            {!! Form::label('kit_master_id', 'Kit Type *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::select('kit_master_id', $kit_types, null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('part_type') ? 'has-error' : '') }}">
            {!! Form::label('part_type', 'Part Type *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('part_type', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('kit_identifier') ? 'has-error' : '') }}">
            {!! Form::label('kit_identifier', 'Kit Identifier:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::checkbox('kit_identifier', 1, false) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('grade_required') ? 'has-error' : '') }}">
            {!! Form::label('grade_required', 'Grade Required:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::checkbox('grade_required', 1, false) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('max_rf_hrs') ? 'has-error' : '') }}">
            {!! Form::label('max_rf_hrs', 'Max RFHrs:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::number('max_rf_hrs', null, ['min' => 0, 'class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('max_rf_print') ? 'has-error' : '') }}">
            {!! Form::label('max_rf_print', 'Max RF To Print:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::number('max_rf_print', null, ['min' => 0, 'class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('part_sequence') ? 'has-error' : '') }}">
            {!! Form::label('part_sequence', 'Part Sequence *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::number('part_sequence', null, ['min' => 0, 'class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-12">
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
            <a href="{!! route('part_type_master.index') !!}" class="btn btn-default">Cancel</a>
          </div><!-- end form-group -->

          {!! Form::close() !!}
        </div><!-- end row -->
      </div><!-- end box-body -->
    </div><!-- end box -->

  </div><!-- end content -->

@endsection

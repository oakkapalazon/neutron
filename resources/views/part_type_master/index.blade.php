@extends('layouts.app')

@section('content')

  <section class="content-header">
    <div class="col-md-8">
      <h1>Part Type Master Lists</h1>
    </div><!-- end col-md-8 -->

    <div class="col-md-4">
      <a href="{!! route('part_type_master.create') !!}" class="btn btn-primary pull-right">Add New</a>
    </div><!-- end col-md-4 -->
  </section>

  <div class="content">

    <div class="box box-primary">
      <div class="box-body">

        <!-- {!! Form::open(['method' => 'GET', 'class'=>'form-horizontal']) !!}

        <div class="form-group">
          <label class="col-md-2 control-label" for="search_field">Search Keyword: </label>

          <div class="col-md-3">
            {!! Form::text('q', null,['class'=>'form-control', 'id'=>'search_field']) !!}
          </div>

          <div class="col-md-5">
            {!!Form::submit('Search',['class'=>'btn btn-default btn-cs search_btn'])!!}
            <a href="{!! route('part_type_master.index') !!}" class="btn btn-default btn-cs">Clear</a>
          </div>

        </div>

        {!!Form::close()!!} -->

        <div class="row">

          <div class="col-md-12">
            <table class="table table-bordered table-responsive table-striped" id="part-type-table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Part Type</th>
                  <th>Kit Type</th>
                  <th style="width: 5%;">Kit Identifier</th>
{{--                  <th>Grade Required</th>--}}
                  <th style="width: 10%;">Max RF Hrs</th>
{{--                  <th>Max RF Print</th>--}}
                  <th style="width: 5%;">Part Sequence</th>
                  <th>Action</th>
                </tr>
              </thead>

              @php $count = 1; @endphp

              <tbody>
                @if(count($part_type_masters) > 0)

                  @foreach($part_type_masters as $row)
                  <tr>
                    <td>{{ $count }}</td>
                    <td>{{ $row->part_type }}</td>
                    <td>{{ $row->kit_type }}</td>
                    <td>{{ $row->kit_identifier }}</td>
{{--                    <td>{{ $row->grade_required }}</td>--}}
                    <td>{{ $row->max_rf_hrs }}</td>
{{--                    <td>{{ $row->max_rf_print }}</td>--}}
                    <td>{{ $row->part_sequence }}</td>
                    <td>
                      @if($row->count == 0 && $row->count_pdm == 0)

                      {!! Form::open(['route' => ['part_type_master.destroy', $row->id], 'method' => 'delete']) !!}
                      <a href="{!! route('part_type_master.edit', [$row->id]) !!}" class='btn btn-default btn-xs' title='Edit'><i class="glyphicon glyphicon-edit"></i></a>
                      {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'title' => 'Delete', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                      {!! Form::close() !!}

                      @else

                      <a href="{!! route('part_type_master.edit', [$row->id]) !!}" class='btn btn-default btn-xs' title='Edit'><i class="glyphicon glyphicon-edit"></i></a>
                      {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'title' => 'Delete', 'class' => 'btn btn-danger btn-xs', 'disabled' => 'disabled']) !!}

                      @endif
                    </td>
                  </tr>

                  @php $count++; @endphp

                  @endforeach

                @else

                <tr>
                  <td colspan="9">No Result Found</td>
                </tr>

                @endif
              </tbody>
              <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Part Type</th>
                  <th>Kit Type</th>
                  <th>Kit Identifier</th>
{{--                  <th>Grade Required</th>--}}
                  <th>Max RF Hrs</th>
{{--                  <th>Max RF Print</th>--}}
                  <th>Part Sequence</th>
                </tr>
              </tfoot>
            </table>

            {{ $part_type_masters->links() }}

          </div><!-- end col-md-12 -->

        </div><!-- end row -->
      </div><!-- end box-body -->
    </div><!-- end box -->

    <div class="row">
      <div class="col-md-12">
        <p class="label label-info info-notice">
          * You cannot delete part type masters which are assigned in parameter datapoint master module.
        </p>
      </div><!-- end col-md-12 -->
    </div><!-- end row -->

  </div><!-- end content -->

@endsection


@section('extra-js')

  <script type="text/javascript">

    $(function() {

      $('#part-type-table').DataTable({
        lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
        order: [],
        initComplete: function () {

				var r = $('#part-type-table tfoot tr');
    		$('#part-type-table thead').append(r);

				this.api().columns().every(function () {
          var column = this;
          var input = document.createElement("input");
          input.className = "search";
          $(input).appendTo($(column.footer()).empty())
          .on('change', function () {
            column.search($(this).val(), false, false, true).draw();
          });
        });
      }
      });
    });

  </script>

@endsection

@extends('layouts.app')

@section('content')

  <section class="content-header">
    <h1>Create UOM Master</h1>
  </section>

  <div class="content">

    @include('adminlte-templates::common.errors')
    {{--@include('adminlte-templates::common.success')--}}

    <div class="box box-primary">
      <div class="box-body">

        <div class="row">

          {!! Form::open(['route' => 'uom_master.store']) !!}

          <div class="form-group col-sm-6 {{ ($errors->has('uom_code') ? 'has-error' : '') }}">
            {!! Form::label('uom_code', 'Code *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('uom_code', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('uom_desc') ? 'has-error' : '') }}">
            {!! Form::label('uom_desc', 'Description *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('uom_desc', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-12">
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
            <a href="{!! route('uom_master.index') !!}" class="btn btn-default">Cancel</a>
          </div><!-- end form-group -->

          {!! Form::close() !!}
        </div><!-- end row -->
      </div><!-- end box-body -->
    </div><!-- end box -->

  </div><!-- end content -->

@endsection

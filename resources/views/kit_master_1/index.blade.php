@extends('layouts.app')

@section('content')

  <section class="content-header">
    <div class="col-md-8">
      <h1>Kit Type Master Lists</h1>
    </div><!-- end col-md-8 -->

    <div class="col-md-4">
      <a href="{!! route('kit_master.create') !!}" class="btn btn-primary pull-right">Add New</a>
    </div><!-- end col-md-4 -->
  </section>

  <div class="content">

    <div class="box box-primary">
      <div class="box-body">

        {!! Form::open(['method' => 'GET', 'class'=>'form-horizontal']) !!}

        <div class="form-group">
          <label class="col-md-2 control-label" for="search_field">Search Keyword: </label>

          <div class="col-md-3">
            {!! Form::text('q', null,['class'=>'form-control', 'id'=>'search_field']) !!}
          </div><!-- end col-md-3 -->

          <div class="col-md-5">
            {!!Form::submit('Search',['class'=>'btn btn-default btn-cs search_btn'])!!}
            <a href="{!! route('kit_master.index') !!}" class="btn btn-default btn-cs">Clear</a>
          </div><!-- end col-md-5 -->

        </div><!-- end form-group -->

        {!!Form::close()!!}

        <div class="row">

          <div class="col-md-12">
            <table class="table table-bordered table-responsive table-striped" id="users-table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Kit Type</th>
                  <th>Customer Name</th>
                  <th>Descn</th>
                  <th>Process</th>
                  <th>PO No</th>
                  <th>UOM Code</th>
                  <th>Remarks</th>
                  <th>Action</th>
                </tr>
              </thead>

              @php $count = 1; @endphp

              <tbody>
                @foreach($kit_masters as $row)
                <tr>
                  <td>{{ $count }}</td>
                  <td>{{ $row->kit_type }}</td>
                  <td>{{ $row->customer_name }}</td>
                  <td>{{ $row->descn }}</td>
                  <td>{{ $row->process }}</td>
                  <td>{{ $row->po_no }}</td>
                  <td>{{ $row->uom_code }}</td>
                  <td>{{ $row->remarks }}</td>
                  <td>
                    @if($row->no_of_partmaster == 0 && $row->no_of_parttypemaster == 0)

                    {!! Form::open(['route' => ['kit_master.destroy', $row->id], 'method' => 'delete']) !!}
                    <a href="{!! route('kit_master.edit', [$row->id]) !!}" class='btn btn-default btn-xs' title='Edit'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'title' => 'Delete', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    {!! Form::close() !!}

                    @else

                    <a href="{!! route('kit_master.edit', [$row->id]) !!}" class='btn btn-default btn-xs' title='Edit'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'title' => 'Delete', 'class' => 'btn btn-danger btn-xs', 'disabled' => 'disabled']) !!}

                    @endif
                  </td>
                </tr>

                @php $count++; @endphp

                @endforeach
              </tbody>
            </table>

            {{ $kit_masters->links() }}

          </div><!-- end col-md-12 -->

        </div><!-- end row -->
      </div><!-- end box-body -->
    </div><!-- end box -->

    <div class="row">
      <div class="col-md-12">
        <p class="label label-info info-notice">
          * You cannot delete kit types which are assigned in part master, part type master and
          part parameter master modules.
        </p>
      </div><!-- end col-md-12 -->
    </div><!-- end row -->

  </div><!-- end content -->

@endsection

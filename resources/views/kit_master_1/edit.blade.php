@extends('layouts.app')

@section('content')

  <section class="content-header">
    <h1>Edit Kit Master</h1>
  </section>

  <div class="content">

    @include('adminlte-templates::common.errors')
    @include('adminlte-templates::common.success')

    <div class="box box-primary">
      <div class="box-body">

        <div class="row">

          {!! Form::model($kit_master, ['route' => ['kit_master.update', $kit_master->id], 'method' => 'patch', 'enctype'=>'multipart/form-data']) !!}

          <input type="hidden" name="id" value="{{ $kit_master->id }}">

          <div class="form-group col-sm-6 {{ ($errors->has('customer_id') ? 'has-error' : '') }}">
            {!! Form::label('customer_id', 'Customer *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::select('customer_id', $customers, null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('descn') ? 'has-error' : '') }}">
            {!! Form::label('descn', 'Descn *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('descn', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('kit_type') ? 'has-error' : '') }}">
            {!! Form::label('kit_type', 'Kit Type *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('kit_type', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('po_no') ? 'has-error' : '') }}">
            {!! Form::label('po_no', 'PO No:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('po_no', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('process') ? 'has-error' : '') }}">
            {!! Form::label('process', 'Process:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('process', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('uom_id') ? 'has-error' : '') }}">
            {!! Form::label('uom_id', 'UOM Code *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::select('uom_id', $uom_masters, null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('remarks') ? 'has-error' : '') }}">
            {!! Form::label('remarks', 'Remarks:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('remarks', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-12">
            {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
            <a href="{!! route('kit_master.index') !!}" class="btn btn-default">Cancel</a>
          </div><!-- end form-group -->

          {!! Form::close() !!}
        </div><!-- end row -->
      </div><!-- end box-body -->
    </div><!-- end box -->

  </div><!-- end content -->

@endsection

@section('extra-js')

  <script type="text/javascript">

    $(function() {
      $("#po_date").datepicker({
        format: 'dd/mm/yyyy',
      });
    });

  </script>

@endsection

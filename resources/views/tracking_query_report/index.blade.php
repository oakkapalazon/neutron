@extends('layouts.app')

@section('content')

  <section class="content-header">
    <div class="col-md-8">
      <h1>Tracking Report</h1>
    </div><!-- end col-md-8 -->
  </section>

  <div class="content">

    <div class="box box-primary filter-tracking-query-box">
      <div class="box-body customBox">

        <div class="row">
          <div class="col-md-12">
            <h4>Filter</h4>
          </div><!-- end col-md-12 -->

          <div class="form-group col-md-4 {{ ($errors->has('customer_code') ? 'has-error' : '') }}">
            {!! Form::label('customer_code', 'Customer *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::select('customer_code', $customer_code, null, ['class' => 'form-control']) !!}
            </div>
          </div>

          <div class="form-group col-sm-4 {{ ($errors->has('job_id') ? 'has-error' : '') }}">
            {!! Form::label('job_id', 'Job ID:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8 job_id">
              {!! Form::select('job_id', ['' => 'Please select'], null, ['class' => 'form-control']) !!}
            </div>
          </div>

          <div class="form-group col-sm-4 {{ ($errors->has('grn_no') ? 'has-error' : '') }}">
            {!! Form::label('kit_type', 'Kit:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8 kit_type">
              {!! Form::select('kit_type', ['' => 'Please select'], null, ['class' => 'form-control']) !!}
            </div>
          </div>

{{--          <div class="form-group col-sm-4 {{ ($errors->has('grn_no') ? 'has-error' : '') }}">--}}
{{--            {!! Form::label('grn_no', 'GRN No.:', ['class' => 'col-sm-4 control-label']) !!}--}}

{{--            <div class="col-sm-8 grn_no">--}}
{{--              {!! Form::select('grn_no', ['' => 'Please select'], null, ['class' => 'form-control']) !!}--}}
{{--            </div>--}}
{{--          </div>--}}
        </div>

        <div class="row">
          <div class="form-group col-md-4 {{ ($errors->has('date_from') ? 'has-error' : '') }}">
            {!! Form::label('date_from', 'Collected Fr:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('date_from', '', ['class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-md-4 {{ ($errors->has('collection_date') ? 'has-error' : '') }}">
            {!! Form::label('date_to', 'Collected To:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('date_to', '', ['class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-4 {{ ($errors->has('set_id') ? 'has-error' : '') }}">
            {!! Form::label('set_id', 'Set ID:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8 set_id">
              {!! Form::select('set_id', ['' => 'Please select'], null, ['class' => 'form-control']) !!}
            </div>
          </div>
        </div>

        <div class="row">
          <div class="form-group col-sm-4" style="margin-top: 10px;">
            <a href="#" class="btn btn-success search-btn">Search</a>
          </div>
          <div class="form-group col-sm-4 divHide {{ ($errors->has('part_type') ? 'has-error' : '') }} ">
            {!! Form::label('part_type', 'Part Type:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8 part_type">
              {!! Form::select('part_type', ['' => 'Please select'], null, ['class' => 'form-control']) !!}
            </div>
          </div>

          <div class="form-group col-sm-4 divHide {{ ($errors->has('serial_no') ? 'has-error' : '') }}">
            {!! Form::label('serial_no', 'Serial No.:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8 serial_no">
              {!! Form::select('serial_no', ['' => 'Please select'], null, ['class' => 'form-control']) !!}
            </div>
          </div>
        </div>
      </div><!-- end box-body -->
    </div><!-- end box -->

    <div class="box box-primary">
      <div class="box-body">
        <div class="row">
          <div class="form-group col-sm-12">
            {!! Form::open(['action' => 'TrackingReportController@exportTrackingQueryDetail', 'method' => 'get']) !!}
              <input type="hidden" name="hidden_customer_code">
              <input type="hidden" name="hidden_job_id">
{{--              <input type="hidden" name="hidden_grn_no">--}}
              <input type="hidden" name="hidden_set_id">
              <input type="hidden" name="hidden_date_from">
              <input type="hidden" name="hidden_date_to">
              <input type="hidden" name="hidden_kit_type">
              <input type="hidden" name="hidden_part_type">
              <input type="hidden" name="hidden_serial_no">
              {!! Form::submit('Export', ['class' => 'btn btn-info export-btn', 'style' => 'float: right']) !!}
            {!! Form::close() !!}
          </div>
          <div class="col-md-12">

            <table class="table table-bordered table-responsive table-striped" id="tracking-report-table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>GRN</th>
                  <th>Date of Collection</th>
                  <th>Customer</th>
                  <th>Customer Job Ref</th>
                  <th>Kit</th>
                  <th>Set ID</th>
                  <th>Part Type</th>
                  <th>Serial Number</th>
                  <th>RF</th>
                  <th>Accumulated RF</th>
                  <th>Parameter</th>
                  <th>Measurement</th>
                  <th>Avg</th>
                </tr>
              </thead>
              <tbody>

              </tbody>
              <!-- <tfoot>
                <tr>
                  <th>ID</th>
                  <th>GRN</th>
                  <th>Date of Collection</th>
                  <th>Customer</th>
                  <th>Customer Job Ref</th>
                  <th>Kit</th>
                  <th>Set ID</th>
                  <th>Part Type</th>
                  <th>Serial Number</th>
                  <th>RF</th>
                  <th>Accumulated RF</th>
                  <th>Recorded Observations</th>
                </tr>
              </tfoot> -->
            </table>

          </div><!-- end col-md-12 -->
        </div><!-- end row -->
      </div>

  </div><!-- end content -->

@endsection


@section('extra-js')

  <script type="text/javascript" src="{{ asset('/js/custom/tracking-query.js') }}"></script>

  <script type="text/javascript">

    $(function() {

      $("#date_from, #date_to").datepicker({
        format: 'yyyy-mm-dd',
      });

      // $('#grn-report-table').DataTable({
      //   lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
      //   order: [],
      //   initComplete: function () {
      //
      //     var r = $('#grn-report-table tfoot tr');
      //     $('#grn-report-table thead').append(r);
      //
      //     this.api().columns().every(function () {
      //       var column = this;
      //       var input = document.createElement("input");
      //       input.className = "search";
      //       $(input).appendTo($(column.footer()).empty())
      //       .on('change', function () {
      //         column.search($(this).val(), false, false, true).draw();
      //       });
      //     });
      //   }
      // });

    });

  </script>

@endsection

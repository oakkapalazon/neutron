@extends('layouts.app')

@section('content')

  <section class="content-header">
    <div class="col-md-8">
      <h1>Parameter Master Lists</h1>
    </div><!-- end col-md-8 -->

    <div class="col-md-4">
      <a href="{!! route('parameter_master.create') !!}" class="btn btn-primary pull-right">Add New</a>
    </div><!-- end col-md-4 -->
  </section>

  <div class="content">

    <div class="box box-primary">
      <div class="box-body">

        <!-- {!! Form::open(['method' => 'GET', 'class'=>'form-horizontal']) !!}

        <div class="form-group">
          <label class="col-md-2 control-label" for="search_field">Search Keyword: </label>

          <div class="col-md-3">
            {!! Form::text('q', null,['class'=>'form-control', 'id'=>'search_field']) !!}
          </div>

          <div class="col-md-5">
            {!!Form::submit('Search',['class'=>'btn btn-default btn-cs search_btn'])!!}
            <a href="{!! route('parameter_master.index') !!}" class="btn btn-default btn-cs">Clear</a>
          </div>

        </div>

        {!!Form::close()!!} -->

        <div class="row">

          <div class="col-md-12">
            <table class="table table-bordered table-responsive table-striped" id="parameter-table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Param</th>
                  <th>Description</th>
                  <th>Sequence</th>
                  <th>Action</th>
                </tr>
              </thead>

              @php $count = 1; @endphp

              <tbody>
                @if(count($param_masters) > 0)

                  @foreach($param_masters as $row)
                  <tr>
                    <td>{{ $count }}</td>
                    <td>{{ $row->param }}</td>
                    <td>{{ $row->description }}</td>
                    <td>{{ $row->sequence }}</td>
                    <td>
                      @if($row->no_of_pdm == 0 && $row->no_of_ppm == 0)

                      {!! Form::open(['route' => ['parameter_master.destroy', $row->id], 'method' => 'delete']) !!}
                      <a href="{!! route('parameter_master.edit', [$row->id]) !!}" class='btn btn-default btn-xs' title='Edit'><i class="glyphicon glyphicon-edit"></i></a>
                      {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'title' => 'Delete', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                      {!! Form::close() !!}

                      @else

                      <a href="{!! route('parameter_master.edit', [$row->id]) !!}" class='btn btn-default btn-xs' title='Edit'><i class="glyphicon glyphicon-edit"></i></a>
                      {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'title' => 'Delete', 'class' => 'btn btn-danger btn-xs', 'disabled' => 'disabled']) !!}

                      @endif
                    </td>
                  </tr>

                  @php $count++; @endphp

                  @endforeach

                @else

                <tr>
                  <td colspan="8">No Result Found</td>
                </tr>

                @endif
              </tbody>
              <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Param</th>
                  <th>Description</th>
                  <th>Sequence</th>
                </tr>
              </tfoot>
            </table>

            {{ $param_masters->links() }}

          </div><!-- end col-md-12 -->

        </div><!-- end row -->
      </div><!-- end box-body -->
    </div><!-- end box -->

    <div class="row">
      <div class="col-md-12">
        <p class="label label-info info-notice">
          * You cannot delete parameter masters which are assigned in part parameter master
          and parameter datapoint master modules.
        </p>
      </div><!-- end col-md-12 -->
    </div><!-- end row -->

  </div><!-- end content -->

@endsection



@section('extra-js')

  <script type="text/javascript">

    $(function() {

      $('#search_date').datepicker({
        format: 'dd/mm/yyyy',
      });

      $('#parameter-table').DataTable({
        lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
        order: [],
        initComplete: function () {

        var r = $('#parameter-table tfoot tr');
        $('#parameter-table thead').append(r);

        this.api().columns().every(function () {
          var column = this;
          var input = document.createElement("input");
          input.className = "search";
          $(input).appendTo($(column.footer()).empty())
          .on('change', function () {
            column.search($(this).val(), false, false, true).draw();
          });
        });
      }
      });

    });

  </script>

@endsection

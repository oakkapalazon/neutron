@extends('layouts.app')

@section('content')

  <section class="content-header">
    <h1>Create Parameter Master</h1>
  </section>

  <div class="content">

    @include('adminlte-templates::common.errors')
    {{--@include('adminlte-templates::common.success')--}}

    <div class="box box-primary">
      <div class="box-body">

        <div class="row">

          {!! Form::open(['route' => 'parameter_master.store']) !!}

          <div class="form-group col-sm-6 {{ ($errors->has('param') ? 'has-error' : '') }}">
            {!! Form::label('param', 'Param *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('param', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('description') ? 'has-error' : '') }}">
            {!! Form::label('description', 'Description:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('description', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('sequence') ? 'has-error' : '') }}">
            {!! Form::label('sequence', 'Sequence *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::number('sequence', null, ['min' => 0, 'class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('is_boolean') ? 'has-error' : '') }}">
            {!! Form::label('is_boolean', 'Is Boolean:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::checkbox('is_boolean', 1, false) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-12">
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
            <a href="{!! route('parameter_master.index') !!}" class="btn btn-default">Cancel</a>
          </div><!-- end form-group -->

          {!! Form::close() !!}
        </div><!-- end row -->
      </div><!-- end box-body -->
    </div><!-- end box -->

  </div><!-- end content -->

@endsection

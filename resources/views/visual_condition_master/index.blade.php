@extends('layouts.app')

@section('content')

  <section class="content-header">
    <div class="col-md-8">
      <h1>Visual Condition Master Lists</h1>
    </div><!-- end col-md-8 -->

    <div class="col-md-4">
      <a href="{!! route('visual_condition_master.create') !!}" class="btn btn-primary pull-right">Add New</a>
    </div><!-- end col-md-4 -->
  </section>

  <div class="content">

    <div class="box box-primary">
      <div class="box-body">

        <div class="row">

          <div class="col-md-12">
            <table class="table table-bordered table-responsive table-striped" id="visual-condition-table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Status Code</th>
                  <th>Description</th>
                  <th>Action</th>
                </tr>
              </thead>

              @php $count = 1; @endphp

              <tbody>
                @if(count($visual_conditions) > 0)

                  @foreach($visual_conditions as $row)
                  <tr>
                    <td>{{ $count }}</td>
                    <td>{{ $row->code }}</td>
                    <td>{{ $row->description }}</td>
                    <td>
                      {!! Form::open(['route' => ['visual_condition_master.destroy', $row->id], 'method' => 'delete']) !!}
                      <a href="{!! route('visual_condition_master.edit', [$row->id]) !!}" class='btn btn-default btn-xs' title='Edit'><i class="glyphicon glyphicon-edit"></i></a>
                      {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'title' => 'Delete', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                      {!! Form::close() !!}
                    </td>
                  </tr>

                  @php $count++; @endphp

                  @endforeach

                @else

                <tr>
                  <td colspan="4">No Result Found</td>
                </tr>

                @endif
              </tbody>

              <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Status Code</th>
                  <th>Description</th>
                </tr>
              </tfoot>
            </table>

          </div><!-- end col-md-12 -->

        </div><!-- end row -->
      </div><!-- end box-body -->
    </div><!-- end box -->

  </div><!-- end content -->

@endsection

@section('extra-js')

  <script type="text/javascript">

    $(function() {

      $('#visual-condition-table').DataTable({
        lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
        order: [],
        initComplete: function () {

				var r = $('#visual-condition-table tfoot tr');
    		$('#visual-condition-table thead').append(r);

				this.api().columns().every(function () {
          var column = this;
          var input = document.createElement("input");
          input.className = "search";
          $(input).appendTo($(column.footer()).empty())
          .on('change', function () {
            column.search($(this).val(), false, false, true).draw();
          });
        });
      }
      });
    });

  </script>

@endsection

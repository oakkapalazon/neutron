<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

  <section class="sidebar">

    @if (! Auth::guest())
{{--    <div class="user-panel">--}}
{{--      <div class="pull-left image">--}}
{{--        <img src="{{asset('/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image" />--}}
{{--      </div>--}}
{{--      <div class="pull-left info">--}}
{{--        <p>{{ Auth::user()->name }}</p>--}}
{{--        <!-- Status -->--}}
{{--        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>--}}
{{--      </div>--}}
{{--    </div>--}}
    @endif

    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">

      <li class="treeview {{ active(['process/pre_cleaning*', 'process/post_cleaning*', 'delivery_order*']) }}">
        <a href="#">
          <i class="fab fa-whmcs"></i> <span>&nbsp; Process</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li class="treeview {{ active('process/pre_cleaning') }}">
            <a href="{{ url('/process/pre_cleaning') }}"><i class="fa fa-file"></i> Pre-Cleaning</a>
          </li>
          <li class="treeview {{ active('process/post_cleaning') }}">
            <a href="{{ url('/process/post_cleaning') }}"><i class="fa fa-file"></i> Post-Cleaning</a>
          </li>
          <li class="treeview {{ active('delivery_order') }}">
            <a href="{{ url('/delivery_order') }}"><i class="fa fa-file"></i> Delivery Order</a>
          </li>
        </ul>
      </li>

      <li class="treeview {{ active(['report/grn_query*', 'report/tracking_query*', 'report/post_cleaning_report*', 'report/part_master_report*', 'report/delivery_order_report*']) }}">
        <a href="#">
          <i class="fa fa-chart-bar"></i> <span>Reports</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li class="treeview {{ active('report/grn_query') }}">
            <a href="{{ url('report/grn_query') }}"><i class="fa fa-file"></i> GRN Query</a>
          </li>
          <li class="treeview {{ active('report/tracking_query') }}">
            <a href="{{ url('report/tracking_query') }}"><i class="fa fa-file"></i> Tracking Query</a>
          </li>
          <li class="treeview {{ active('report/part_master_report') }}">
            <a href="{{ url('report/part_master_report') }}"><i class="fa fa-file"></i> Part Master Report</a>
          </li>
          <li class="treeview {{ active('report/post_cleaning_report') }}">
            <a href="{{ url('report/post_cleaning_report') }}"><i class="fa fa-file"></i> Post Cleaning Report</a>
          </li>
{{--          <li class="treeview {{ active('report/delivery_order_report') }}">--}}
{{--            <a href="{{ url('report/delivery_order_report') }}"><i class="fa fa-file"></i> Deliver Order Report</a>--}}
{{--          </li>--}}
        </ul>
      </li>

      @if(auth()->user()->access_level == 1)

        <li class="treeview {{ active(['users*', 'companies*', 'customers*', 'kit_master*', 'parameter_master*', 'param_datapoint_master*', 'reason_master*', 'uom_master*',
          'parts/part_master*', 'parts/part_type_master*', 'parts/part_param_master*', 'parts/part_status_master*', 'parts/visual_condition_master*']) }}">
          <a href="#">
            <i class="fa fa-database"></i> <span>Database Setting</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="treeview {{ active('users') }}">
              <a href="{{ url('/users') }}"><i class="fa fa-users"></i> Users</a>
            </li>
            <li class="treeview {{ active('companies') }}">
              <a href="{{ url('/companies') }}"><i class="fa fa-file"></i> Companies</a>
            </li>
            <li class="treeview {{ active('customers') }}">
              <a href="{{ url('/customers') }}"><i class="fa fa-file"></i> Customers</a>
            </li>
            <li class="treeview {{ active('kit_master') }}">
              <a href="{{ url('/kit_master') }}"><i class="fa fa-file"></i> Kit Type Master</a>
            </li>
            <li class="treeview {{ active(['parts/part_master*', 'parts/part_type_master*', 'parts/part_param_master*', 'parts/part_status_master*']) }}">
              <a href="#"><i class="fa fa-file"></i> Parts
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="{{ active('parts/part_master') }}">
                  <a href="{{ url('/parts/part_master') }}"><i class="fa fa-file"></i> Part Master</a>
                </li>
                <li class="{{ active('parts/part_type_master') }}">
                  <a href="{{ url('/parts/part_type_master') }}"><i class="fa fa-file"></i> Part Type Master</a>
                </li>
                <li class="{{ active('parts/part_param_master') }}">
                  <a href="{{ url('/parts/part_param_master') }}"><i class="fa fa-file"></i> Part Parameter Master</a>
                </li>
                <li class="{{ active('parts/part_status_master') }}">
                  <a href="{{ url('/parts/part_status_master') }}"><i class="fa fa-file"></i> Part Status Master</a>
                </li>
                <!-- <li class="{{ active('parts/visual_condition_master') }}">
                  <a href="{{ url('/parts/visual_condition_master') }}"><i class="fa fa-file"></i> Visual Condition Master</a>
                </li> -->
              </ul>
            </li>
            <li class="treeview {{ active('parameter_master') }}">
              <a href="{{ url('/parameter_master') }}"><i class="fa fa-file"></i> Parameter Master</a>
            </li>
            <li class="treeview {{ active('param_datapoint_master') }}">
              <a href="{{ url('/param_datapoint_master') }}"><i class="fa fa-file"></i> Parameter Datapoint</a>
            </li>
            <li class="treeview {{ active('reason_master') }}">
              <a href="{{ url('/reason_master') }}"><i class="fa fa-file"></i> Reason Master</a>
            </li>
            <li class="treeview {{ active('uom_master') }}">
              <a href="{{ url('/uom_master') }}"><i class="fa fa-file"></i> UOM Master</a>
            </li>
            <li class="{{ active('parts/visual_condition_master') }}">
              <a href="{{ url('/parts/visual_condition_master') }}"><i class="fa fa-file"></i> Visual Condition Master</a>
            </li>
          </ul>
        </li>

      @endif

    </ul>

  </section>
</aside>

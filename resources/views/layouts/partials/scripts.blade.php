<!-- REQUIRED JS SCRIPTS -->

  <!-- jQuery 2.1.4 -->

  <script src="{{ asset('/js/jquery.min.js') }}" type="text/javascript"></script>

  <!-- Bootstrap 3.3.2 JS -->
  <script src="{{ asset('/js/bootstrap.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('/js/jquery.autocomplete.js') }}" type="text/javascript"></script>

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <!-- AdminLTE App -->
  <script src="{{ asset('/js/app.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('/js/sidebar.js') }}" type="text/javascript"></script>

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>

  <!-- Datatable -->
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>

  <!-- Optionally, you can add Slimscroll and FastClick plugins.
    Both of these plugins are recommended to enhance the
    user experience. Slimscroll is required when using the
    fixed layout. -->

@extends('layouts.app')

@section('content')

  <section class="content-header">
    <h1>Pre Cleaning</h1>
  </section>

  <div class="content">

    @include('adminlte-templates::common.errors')
    {{--@include('adminlte-templates::common.success')--}}

    <div class="box box-primary">
      <div class="box-body">

        <div class="row">

          <input type="hidden" id="grn_id" value="{{ Request::get('grn_id') }}" />

          {!! Form::open(['route' => 'pre_cleaning.store']) !!}

          @if(Request::get('grn_id'))

          @php
            $grn_row = \App\Models\GoodReceiptHeader::find(Request::get('grn_id'));
            $customer_name = \App\Models\Customer::where('id', $grn_row->customer_id)->pluck('customer_name');
            $grn_job_status = $grn_row->job_status;
          @endphp

          <input type="hidden" name="id" value="{{ $grn_row->id }}" class="grn-id" id="grn-id">

          <div class="form-group col-md-6 {{ ($errors->has('grn_no') ? 'has-error' : '') }}">
            {!! Form::label('grn_no', 'GRN No *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('grn_no', $grn_row->grn_no, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-md-6 {{ ($errors->has('customer_id') ? 'has-error' : '') }}">
            {!! Form::label('customer_id', 'Customer *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::select('customer_id', $customers, $grn_row->customer_id, ['class' => 'form-control', 'disabled' => true]) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-md-6 {{ ($errors->has('customer_name') ? 'has-error' : '') }}">
            {!! Form::label('customer_name', 'Customer Name:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('customer_name', $customer_name[0], ['class' => 'form-control', 'readonly' => 'readonly']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-md-6 {{ ($errors->has('job_id') ? 'has-error' : '') }}">
            {!! Form::label('job_id', 'Job ID *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('job_id', $grn_row->job_id, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <input type="hidden" name="hidden_job_status" value="{{ $grn_job_status }}">

          <div class="form-group col-md-6 {{ ($errors->has('job_status') ? 'has-error' : '') }}">
            {!! Form::label('job_status', 'Job Status *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              @if($grn_job_status == 'open')

              {{ Form::radio('job_status', 'open' , true) }} Open &nbsp;&nbsp;&nbsp;
              {{ Form::radio('job_status', 'closed' , false) }} Closed

              @else

              {{ Form::radio('job_status', 'open' , false) }} Open &nbsp;&nbsp;&nbsp;
              {{ Form::radio('job_status', 'closed' , true) }} Closed

              @endif
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-md-6 {{ ($errors->has('collection_date') ? 'has-error' : '') }}">
            {!! Form::label('collection_date', 'Collection Date *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('collection_date', \Carbon\Carbon::parse($grn_row->collection_date)->format("d/m/Y"), ['class' => 'form-control', 'autocomplete' => "off"]) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-md-12 {{ ($errors->has('narration') ? 'has-error' : '') }}">
            {!! Form::label('narration', 'Narration:', ['class' => 'col-sm-2 control-label']) !!}

            <div class="col-md-10" style="padding-left: 10px;">
              {!! Form::textarea('narration', $grn_row->narration, ['class' => 'form-control', 'rows' => 3]) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          @else

          <input type="hidden" name="run_no" value="{{ $run_no }}" />

          <div class="form-group col-md-6 {{ ($errors->has('grn_no') ? 'has-error' : '') }}">
            {!! Form::label('grn_no', 'GRN No *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('grn_no', $grn_no, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-md-6 {{ ($errors->has('customer_id') ? 'has-error' : '') }}">
            {!! Form::label('customer_id', 'Customer *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::select('customer_id', $customers, null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-md-6 {{ ($errors->has('customer_name') ? 'has-error' : '') }}">
            {!! Form::label('customer_name', 'Customer Name:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('customer_name', null, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-md-6 {{ ($errors->has('job_id') ? 'has-error' : '') }}">
            {!! Form::label('job_id', 'Job ID *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('job_id', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-md-6 {{ ($errors->has('job_status') ? 'has-error' : '') }}">
            {!! Form::label('job_status', 'Job Status *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {{ Form::radio('job_status', 'open' , true) }} Open &nbsp;&nbsp;&nbsp;
              {{ Form::radio('job_status', 'closed' , false) }} Closed
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-md-6 {{ ($errors->has('collection_date') ? 'has-error' : '') }}">
            {!! Form::label('collection_date', 'Collection Date *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('collection_date', \Carbon\Carbon::now()->format('d/m/Y'), ['class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-md-12 {{ ($errors->has('narration') ? 'has-error' : '') }}">
            {!! Form::label('narration', 'Narration:', ['class' => 'col-sm-2 control-label']) !!}

            <div class="col-sm-10" style="padding-left: 10px;">
              {!! Form::textarea('narration', null, ['class' => 'form-control', 'rows' => 2]) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-12">

            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}

            <a href="{!! route('pre_cleaning.index') !!}" class="btn btn-default">Cancel</a>
          </div>

          @endif

          <div class="clearfix"></div><!-- end clearfix -->

          <!-- <div class="form-group col-sm-12">
            @if(Request::get('grn_id'))
              @if($grn_job_status != 'closed')
                {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
              @endif
            @else
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
            @endif
            <a href="{!! route('pre_cleaning.index') !!}" class="btn btn-default">Cancel</a>
          </div> -->

          <div class="subSection">

            <div class="col-md-12">

              <h4>Kit Information</h4>

              <table class="table table-bordered table-responsive table-striped" id="kit-table">
                <thead>
                  <tr>
                    <th width="2%">#</th>
                    <th>S/No</th>
                    <th>Kit</th>
                    <th>Reason</th>
                    <th width="10%">Tool ID</th>
                    <th width="10%">Set ID</th>
                    <th>Serial No</th>
                    <th width="7%">Qty</th>
                    <th width="10%">RF</th>
                    <th width="5%"></th>
                  </tr>
                </thead>

                @if(Request::get('grn_id'))

                @php
                  $good_kit_row = \App\Models\GoodKitDetail::where('good_receipt_header_id', Request::get('grn_id'))->get();
                  $row_count = 1;
                @endphp

                <tbody>

                  @if(count($good_kit_row) > 0)

                    @foreach($good_kit_row as $row)
                    @php
                      $good_part_row = \App\Models\GoodPartDetail::where('good_kit_detail_id', $row->id)->get();
                    @endphp

                    <tr>

                      @if($grn_job_status != 'closed' && count($good_part_row) == '0')
                        <td class="valign-middle"><i data-id="{{ $row->id }}" class="fas fa-minus-circle remove-row"></i></td>
                      @else
                        <td class="valign-middle"><i data-id="{{ $row->id }}" class="fas fa-minus-circle remove-row" style="display: none;"></i></td>
                      @endif

                      <td class="valign-middle halign-center">{{ $row_count }}</td>
                      <td>
                        {!! Form::select('kit_master_id[]', $kit_types, $row->kit_master_id, ['class' => 'form-control kit-master-id', 'id' => 'kit-master-id', 'disabled' => true ]) !!}
                      </td>
                      <td>
                        {!! Form::select('reason_id[]', $reasons, $row->reason_master_id, ['class' => 'form-control reason-id', 'id' => 'reason-id' ]) !!}
                      </td>
                      <td>
                        {!! Form::text('tool_kit[]', $row->tool_kit, ['class' => 'form-control tool-kit', 'id' => 'tool-kit']) !!}
                      </td>
                      <td>
                        {!! Form::text('set_id[]', $row->set_id, ['min' => 0, 'class' => 'form-control set-id', 'id' => 'set-id']) !!}
                      </td>
                      <td>
                        {!! Form::text('serial_no[]', $row->serial_no, ['class' => 'form-control serial-no', 'id' => 'serial-no']) !!}
                      </td>
                      <td>
                        {!! Form::number('quantity[]', $row->quantity, ['min' => 0, 'class' => 'form-control quantity', 'id' => 'quantity']) !!}
                      </td>
                      <td>
                        {!! Form::number('rf[]', $row->rf, ['min' => 0, 'class' => 'form-control rf', 'id' => 'rf']) !!}
                      </td>
                      <td class="right-arrow">
                        <input type="hidden" name="hidden_dialog-arrow" value="{{ $row->id }}" class="hidden_dialog-arrow" data-closed="{{ $row->closed }}" />
                        <a href="#" class="" data-kit-type-id="{{ $row->id }}"><i class="fas fa-long-arrow-alt-right dialog-arrow"></i></a>
                      </td>
                    </tr>

                    @php $row_count++; @endphp

                    @endforeach

                  @else

                    <tr>
                      @if($grn_job_status != 'closed')
                        <td class="valign-middle"><i data-id="" class="fas fa-minus-circle remove-row"></i></td>
                      @endif
                      <td class="valign-middle halign-center">1</td>
                      <td>
                        {!! Form::select('kit_master_id[]', $kit_types, null, ['class' => 'form-control kit-master-id', 'id' => 'kit-master-id' ]) !!}
                      </td>
                      <td>
{{--                        default value to 1--}}
                        {!! Form::select('reason_id[]', $reasons, 1, ['class' => 'form-control reason-id', 'id' => 'reason-id' ]) !!}
                      </td>
                      <td>
                        {!! Form::text('tool_kit[]', null, ['class' => 'form-control tool-kit', 'id' => 'tool-kit']) !!}
                      </td>
                      <td>
                        {!! Form::text('set_id[]', null, ['min' => 0, 'class' => 'form-control set-id', 'id' => 'set-id']) !!}
                      </td>
                      <td>
                        {!! Form::text('serial_no[]', null, ['class' => 'form-control serial-no', 'id' => 'serial-no']) !!}
                      </td>
                      <td>
{{--                        default qty to 1--}}
                        {!! Form::number('quantity[]', 1, ['min' => 0, 'class' => 'form-control quantity', 'id' => 'quantity']) !!}
                      </td>
                      <td>
                        {!! Form::number('rf[]', null, ['min' => 0, 'class' => 'form-control rf', 'id' => 'rf']) !!}
                      </td>
                      <td class="right-arrow">
                        <input type="hidden" name="hidden_dialog-arrow" value="" class="hidden_dialog-arrow" data-closed="" />
                        <a href="#" class="" data-kit-type-id=""><i class="fas fa-long-arrow-alt-right dialog-arrow"></i></a>
                      </td>
                    </tr>

                  @endif
                </tbody>

                @else

                <tbody>
                  <tr>
                    @if($grn_job_status != 'closed')
                      <td class="valign-middle"><i data-id="" class="fas fa-minus-circle remove-row"></i></td>
                    @endif
                    <td class="valign-middle halign-center">1</td>
                    <td>
                      {!! Form::select('kit_master_id[]', $kit_types, null, ['class' => 'form-control kit-master-id', 'id' => 'kit-master-id' ]) !!}
                    </td>
                    <td>
                      {!! Form::select('reason_id[]', $reasons, null, ['class' => 'form-control reason-id', 'id' => 'reason-id' ]) !!}
                    </td>
                    <td>
                      {!! Form::text('tool_kit[]', null, ['class' => 'form-control tool-kit', 'id' => 'tool-kit']) !!}
                    </td>
                    <td>
                      {!! Form::number('set_id[]', null, ['min' => 0, 'class' => 'form-control set-id', 'id' => 'set-id']) !!}
                    </td>
                    <td>
                      {!! Form::text('serial_no[]', null, ['class' => 'form-control serial-no', 'id' => 'serial-no']) !!}
                    </td>
                    <td>
                      {!! Form::number('quantity[]', null, ['min' => 0, 'class' => 'form-control quantity', 'id' => 'quantity']) !!}
                    </td>
                    <td>
                      {!! Form::number('rf[]', null, ['min' => 0, 'class' => 'form-control rf', 'id' => 'rf']) !!}
                    </td>
                    <td class="right-arrow">
                      <input type="hidden" name="hidden_dialog-arrow" value="" class="hidden_dialog-arrow" data-closed="" />
                      <a href="#" class="" data-kit-type-id=""><i class="fas fa-long-arrow-alt-right dialog-arrow"></i></a>
                    </td>
                  </tr>
                </tbody>

                @endif
              </table>
            </div><!-- end col-md-12 -->

            <!-- @if($grn_job_status != 'closed')
              <div class="form-group col-md-12">
                <button type="button" name="button" class="btn btn-default add-kit">
                  <i class="fas fa-plus-circle"></i> &nbsp; Add
                </button>
              </div>
            @endif -->

            <p class="form-group col-md-12">

              @if($grn_job_status != 'closed')

                <button type="button" name="button" class="btn btn-default add-kit">
                  <i class="fas fa-plus-circle"></i> &nbsp; Add
                </button>

              @endif

              <a href="{!! route('pre_cleaning.index') !!}" class="btn btn-default btn-cancel" style="float: right;">Cancel</a>

              @if($grn_job_status != 'closed')

                <button type="button" class="btn btn-primary btn-save" style="float: right; margin-right: 2px;">Save</button>

              @endif

            </p><!-- end col-md-12 -->

          </div>

          {!! Form::close() !!}

        </div><!-- end row -->
      </div><!-- end box-body -->
    </div><!-- end box -->

    <table style="display: none">
      <tr id="append-row">
        @if($grn_job_status != 'closed')
          <td class="valign-middle"><i data-id="" class="fas fa-minus-circle remove-row"></i></td>
        @endif
        <td class="sr_no valign-middle halign-center"></td>
        <td>
          {!! Form::select('kit_master_id[]', $kit_types, null, ['class' => 'form-control kit-master-id', 'id' => 'kit-master-id' ]) !!}
        </td>
        <td>
{{--          default value to 1--}}
          {!! Form::select('reason_id[]', $reasons, 1, ['class' => 'form-control reason-id', 'id' => 'reason-id' ]) !!}
        </td>
        <td>
          {!! Form::text('tool_kit[]', null, ['class' => 'form-control tool-kit', 'id' => 'tool-kit']) !!}
        </td>
        <td>
          {!! Form::text('set_id[]', null, ['min' => 0, 'class' => 'form-control set-id', 'id' => 'set-id']) !!}
        </td>
        <td>
          {!! Form::text('serial_no[]', null, ['class' => 'form-control serial-no', 'id' => 'serial-no', 'onkeyup' => 'this.value = this.value.toUpperCase();']) !!}
        </td>
        <td>
{{--          default qty to 1--}}
          {!! Form::number('quantity[]', 1, ['min' => 0, 'class' => 'form-control quantity', 'id' => 'quantity']) !!}
        </td>
        <td>
          {!! Form::number('rf[]', null, ['min' => 0, 'class' => 'form-control rf', 'id' => 'rf']) !!}
        </td>
        <td class="right-arrow">
          <input type="hidden" name="hidden_dialog-arrow" value="" class="hidden_dialog-arrow" data-closed="" />
          <a href="#" class="" data-kit-type-id=""><i class="fas fa-long-arrow-alt-right dialog-arrow"></i></a>
        </td>
      </tr>
    </table>

    <!-- Modal -->
    <div class="modal fade" id="modal-dialog-kit-type" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
      <div class="modal-dialog" role="document" style="width: 90%;">
        <div class="modal-content">

          <div class="modal-header">
            <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
            <h4 class="modal-title" id="myModalLabel">Kit Header</h4>
          </div><!-- end modal-header -->

          <div class="modal-body">

            <div class="alert alert-danger check-part-type-fields" role="alert" style="display: none">
            </div><!-- end alert alert-danger -->

            <input type="hidden" name="kit_type_id" value="" class="modal-kit-type-id" id="modal-kit-type-id">
            <input type="hidden" name="good_kit_detail_id" value="" class="modal-good-kit-detail-id" id="modal-good-kit-detail-id">

            <div class="form-group col-sm-6">
              {!! Form::label('modal-kit-no', 'Kit:', ['class' => 'col-sm-3 control-label']) !!}

              <div class="col-sm-9">
                {!! Form::hidden('modal-kit-master-id', null, ['class' => 'form-control modal-kit-master-id']) !!}
                {!! Form::text('modal-kit-no', null, ['class' => 'form-control modal-kit-no', 'readonly' => 'readonly']) !!}
              </div><!-- end col-md-8 -->
            </div><!-- end form-group -->

            <div class="form-group col-sm-6">
              {!! Form::label('modal-set-id', 'Set ID:', ['class' => 'col-sm-2 control-label']) !!}

              <div class="col-sm-10">
                {!! Form::text('modal-set-id', null, ['class' => 'form-control modal-set-id', 'readonly' => 'readonly']) !!}
              </div><!-- end col-md-8 -->
            </div><!-- end form-group -->

            <div class="form-group col-sm-6">
              {!! Form::label('modal-tool-kit', 'Tool ID:', ['class' => 'col-sm-3 control-label']) !!}

              <div class="col-sm-9">
                {!! Form::text('modal-tool-kit', null, ['class' => 'form-control modal-tool-kit', 'readonly' => 'readonly']) !!}
              </div><!-- end col-md-8 -->
            </div><!-- end form-group -->

            <div class="form-group col-sm-6">
              {!! Form::label('modal-remarks', 'Remarks:', ['class' => 'col-sm-2 control-label']) !!}

              <div class="col-sm-10">
                {!! Form::text('modal-remarks', null, ['class' => 'form-control modal-remarks', 'readonly' => 'readonly']) !!}
              </div><!-- end col-md-8 -->
            </div><!-- end form-group -->

            <div class="clearfix"></div><!-- end clearfix -->

            {!! Form::open(['url' => '#', 'id' => 'part-type-form']) !!}

            <table class="table table-bordered table-responsive table-striped" id="part-type-table">
              <thead>
                <tr>
                  <th width="2%">#</th>
                  <th>Part Type</th>
                  <th width="21%">S/ No</th>
                  <th width="19%">Visual Condition</th>
                  <th width="10%">Max RFHrs</th>
                  <th width="7%">RF</th>
                  <th width="7%">ARF</th>
                  <th width="15%">Actions</th>
                </tr>
              </thead>

              <tbody>

              </tbody>
            </table>

            {!! Form::close() !!}

            <div style="margin-top: 10px;">
              <p class="label label-info info-notice">
                * Use Ctrl for multiple select in visual condition field.
              </p>
            </div>

          </div><!-- end modal-body -->

          @if($grn_job_status != 'closed')

            <div class="form-group col-md-12" style="margin-top: 15px;">
              <button type="button" name="button" class="btn btn-default add-part-type">
                <i class="fas fa-plus-circle"></i> &nbsp; Add
              </button>
            </div>

          @endif
          <!-- end col-md-12 -->

          <div class="modal-footer">
            <button type="button" class="btn btn-default modal-btn-close" data-dismiss="modal">Close</button>

            @if($grn_job_status != 'closed')

              <button type="button" class="btn btn-primary modal-btn-save">Save</button>

            @endif

          </div><!-- end modal-footer -->
        </div><!-- end modal-content -->
      </div><!-- end modal-dialog -->
    </div><!-- end modal -->

    <table id="hidden-modal-part-type-table" style="display: none">
      <tr id="modal-append-row">

        <td class="valign-middle">

          @if($grn_job_status != 'closed')

            <i data-id="" class="fas fa-minus-circle remove-row"></i>

          @endif

        </td>
        <td>
          {!! Form::select('part_type_id[]', array('' => 'Please select'), null, ['class' => 'form-control modal-part-type-id', 'id' => 'modal-part-type-id']) !!}
          {!! Form::hidden('hidden_part_type_id[]', null, ['class' => 'modal-hidden-part-type-id', 'id' => 'modal-hidden-part-type-id']) !!}
        </td>
        <td>
          {!! Form::text('serial_no[]', null, ['class' => 'form-control serial-no', 'id' => 'serial-no', 'onkeyup' => 'this.value = this.value.toUpperCase();']) !!}
        </td>
        <td>
          {!! Form::select('visual_condition[]', $visual_conditon, null, ['class' => 'form-control visual-condition mulitple-select', 'multiple' => 'multiple', 'id' => 'visual-condition']) !!}
        </td>
        <td>
          {!! Form::text('max_rf_hrs[]', null, ['class' => 'form-control max-rf-hrs', 'id' => 'max-rf-hrs', 'disabled' => true]) !!}
        </td>
        <td>
          {!! Form::number('part_type_rf[]', null, ['class' => 'form-control part-type-rf', 'id' => 'part-type-rf']) !!}
        </td>
        <td>
          {!! Form::number('arf[]', null, ['class' => 'form-control arf', 'id' => 'arf', 'disabled' => true]) !!}
        </td>
        <td>

          @if($grn_job_status != 'closed')

            {!! Form::select('actions[]', $part_status, null, ['class' => 'form-control actions', 'id' => 'actions']) !!}

          @else

            {!! Form::select('actions[]', $part_status, null, ['class' => 'form-control actions', 'id' => 'actions', 'disabled' => true]) !!}

          @endif

        </td>
      </tr>
    </table>

  </div><!-- end content -->

@endsection

@section('extra-js')

  <!-- <script src="{{ asset('/js/jquery.easy-autocomplete.min.js') }}"></script> -->
  <script src="{{ asset('/js/custom/pre-cleaning.js') }}"></script>

  <script type="text/javascript">

    $(function() {

      var status = '{{ $grn_job_status }}';

      // to disable all inputs and selects if that row has been closed (kit information)
      $( '#kit-table .hidden_dialog-arrow' ).each(function() {

        if( $( this ).attr( 'data-closed' ) == '1' )
        {
          $( this ).closest( 'tr' ).find( 'input' ).attr( 'readonly', 'readonly' );
          $( this ).closest( 'tr' ).find( 'select' ).attr( 'readonly', 'readonly' );
        }
      });

      // to disable all inputs and selects if grn is closed
      if( status == 'closed' )
      {
        $( 'input' ).attr( 'readonly', 'readonly' );
        $( 'select' ).attr( 'readonly', 'readonly' );
      }
      // $("#collection_date").attr("autocomplete", "off");

      $("#collection_date").datepicker({
        format: 'dd/mm/yyyy',
      });

      $('#customer_id').change(function() {

        var customer_id = $(this).val();

        if(customer_id != "")
        {
          var formData = {
            _token: $('meta[name="csrf-token"]').attr('content'),
            customer_id: customer_id
          };

          $.ajax({
            type: 'GET',
            url: '/get-customer-lists',
            data: formData,
            dataType: 'json',
            success: function(response)
            {
              $('#customer_name').val(response.data['customer_name']);
            },

            error: function (response) {
              console.log(response);
            }
          });
        }

        else
        {
          $('#customer_name').val('');
        }
      });

      // var max_rf_hrs = [];
      // console.log($( '#part-type-table' ));
      // $( '#part-type-table #max-rf-hrs' ).each(function() {
      //   console.log('hi');
      //   if ( $.trim($(this).val()).length )
      //   {
      //     max_rf_hrs.push( $(this).val() );
      //   }
      //
      // });
    });

  </script>

@endsection

@extends('layouts.app')

@section('content')

  <section class="content-header">
    <div class="col-md-8">
      <h1>Pre Cleaning Lists</h1>
    </div><!-- end col-md-8 -->

    <div class="col-md-4">
      <a href="{!! route('pre_cleaning.create') !!}" class="btn btn-primary pull-right">Add New</a>
    </div><!-- end col-md-4 -->
  </section>

  <div class="content">

    <div class="box box-primary">
      <div class="box-body">

        <!-- {!! Form::open(['method' => 'GET', 'class'=>'form-horizontal']) !!}

        <div class="form-group">
          <label class="col-md-2 control-label" for="search_field">Search Keyword: </label>

          <div class="col-md-3">
            {!! Form::text('q', null,['class'=>'form-control', 'id'=>'search_field']) !!}
          </div>

          <label class="col-md-1 control-label" for="search_field">Date: </label>

          <div class="col-md-3">
            {!! Form::text('search_date', null,['class'=>'form-control', 'id'=>'search_date']) !!}
          </div>

          <div class="col-md-2">
            {!!Form::submit('Search',['class'=>'btn btn-default btn-cs search_btn'])!!}
            <a href="{!! route('pre_cleaning.index') !!}" class="btn btn-default btn-cs">Clear</a>
          </div>

        </div>

        {!!Form::close()!!} -->

        <div class="row">

          <div class="col-md-12">
            <table class="table table-bordered table-responsive table-striped" id="pre-cleaning-table">
              <thead>
                <tr>
                  <th style="width: 20%;">GRN No</th>
                  <th style="width: 10%;">Date</th>
                  <th style="width: 10%;">Customer</th>
                  <th>Job ID</th>
                  <th style="width: 8%;">Job</th>
                  <th style="width: 18%;">Remarks</th>
                  <th style="width: 5%;">Action</th>
                </tr>
              </thead>

              @php $count = 1; @endphp

              <tbody>
                @if(count($good_receipts) > 0)

                  @foreach($good_receipts as $row)

                  <tr>
                    <td>{{ $row->grn_no }}</td>
                    <td>{{ $row->collection_date }}</td>
                    <td>{{ $row->customer_code }}</td>
                    <td>{{ $row->job_id }}</td>
                    <td>{{ $row->job_status }}</td>
                    <td>{{ $row->narration }}</td>
                    <td>
                      {!! Form::open(['route' => ['pre_cleaning.destroy', $row->id], 'method' => 'delete']) !!}
                      <a href="/process/pre_cleaning/create?grn_id={{ $row->id }}" class='btn btn-default btn-xs' title='Edit'><i class="glyphicon glyphicon-edit"></i></a>

                      @if($row->job_status == 'closed' || $row->no_of_kit_detail > 0 || ($latest_grn->grn_no != $row->grn_no))
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'title' => 'Delete', 'class' => 'btn btn-danger btn-xs', 'disabled' => true]) !!}
                      @else
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'title' => 'Delete', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                      @endif
                      {!! Form::close() !!}
                    </td>
                  </tr>

                  @endforeach

                @else

                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>

                @endif
              </tbody>
              <tfoot>
                <tr>
                  <th>GRN No</th>
                  <th>Date</th>
                  <th>Customer Code</th>
                  <th>Job ID</th>
                  <th>Job Status</th>
                  <th>Remarks</th>
                </tr>
              </tfoot>
            </table>

          </div><!-- end col-md-12 -->

        </div><!-- end row -->
      </div><!-- end box-body -->
    </div><!-- end box -->

  </div><!-- end content -->

@endsection

@section('extra-js')

  <script type="text/javascript">

    $(function() {

      $('#search_date').datepicker({
        format: 'dd/mm/yyyy',
      });

      $('#pre-cleaning-table').DataTable({
        lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
        order: [],
        initComplete: function () {

        var r = $('#pre-cleaning-table tfoot tr');
        $('#pre-cleaning-table thead').append(r);

        this.api().columns().every(function () {
          var column = this;
          var input = document.createElement("input");
          input.className = "search";
          $(input).appendTo($(column.footer()).empty())
          .on('change', function () {
            column.search($(this).val(), false, false, true).draw();
          });
        });
      }
      });

    });

  </script>

@endsection

@extends('layouts.app')

@section('content')

  <section class="content-header">
    <h1>Edit Paramter Datapoint Master</h1>
  </section>

  <div class="content">

    @include('adminlte-templates::common.errors')
    {{--@include('adminlte-templates::common.success')--}}

    <div class="box box-primary">
      <div class="box-body">

        <div class="row">

          {!! Form::model($param_datapoint, ['route' => ['param_datapoint_master.update', $param_datapoint->id], 'method' => 'patch', 'enctype'=>'multipart/form-data']) !!}

          <input type="hidden" name="id" value="{{ $param_datapoint->id }}">

          <div class="form-group col-sm-6 {{ ($errors->has('kit_master_id') ? 'has-error' : '') }}">
            {!! Form::label('kit_master_id', 'Kit Type *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::select('kit_master_id', $kit_masters, null, ['class' => 'form-control', 'id' => 'kit_master_id']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('part_type_id') ? 'has-error' : '') }}">
            {!! Form::label('part_type_id', 'Part Type *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              @if(empty(old('part_type_id')))

                {!! Form::select('part_type_id', $part_type_masters, null, ['class' => 'form-control', 'id' => 'part_type_id']) !!}

              @else

              @php

                $part_types = \App\Models\PartTypeMaster::getPartTypeMaster(old('kit_master_id'));

              @endphp

                {!! Form::select('part_type_id', $part_types, null, ['class' => 'form-control', 'id' => 'part_type_id']) !!}

              @endif
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('parameter_master_id') ? 'has-error' : '') }}">
            {!! Form::label('parameter_master_id', 'Parameter *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              @if(empty(old('parameter_master_id')))

                {!! Form::select('parameter_master_id', $part_param_masters, null, ['class' => 'form-control', 'id' => 'parameter_master_id']) !!}

              @else

              @php

                $part_param_masters = \App\Models\PartParamMaster::getPartParamMaster(old('part_type_id'));

              @endphp

                {!! Form::select('parameter_master_id', $part_param_masters, null, ['class' => 'form-control', 'id' => 'parameter_master_id']) !!}

              @endif
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('datapoint') ? 'has-error' : '') }}">
            {!! Form::label('datapoint', 'Datapoint *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('datapoint', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-12">
            {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
            <a href="{!! route('param_datapoint_master.index') !!}" class="btn btn-default">Cancel</a>
          </div><!-- end form-group -->

          {!! Form::close() !!}
        </div><!-- end row -->
      </div><!-- end box-body -->
    </div><!-- end box -->

  </div><!-- end content -->

@endsection

@section('extra-js')

  <script src="{{ asset('/js/custom/main.js') }}"></script>

@endsection

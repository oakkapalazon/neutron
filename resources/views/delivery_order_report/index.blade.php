@extends('layouts.app')

<!-- <style>
  .tblContent {
    width: 100%;
  }
</style> -->

@section('content')

  <section class="content-header">
    <div class="col-md-8">
      <h1>Delivery Order Report</h1>
    </div><!-- end col-md-8 -->
  </section>

  <div class="content">

    <div class="box box-primary filter-delivery-order-box">
      <div class="box-body customBox">

        {!! Form::open(['url' => 'report/delivery_order_report/print_delivery_order_report', 'method' => 'post', 'target' => '_blank']) !!}

        <div class="row">
          <div class="col-md-12">
            <h4>Filter</h4>
          </div><!-- end col-md-12 -->

          <div class="form-group col-sm-4 {{ ($errors->has('customer_code') ? 'has-error' : '') }}">
            {!! Form::label('customer_code', 'Customer:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::select('customer_code', $customer_code, '', ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <!-- <div class="form-group col-sm-4 {{ ($errors->has('kit_type') ? 'has-error' : '') }}">
            {!! Form::label('kit_type', 'Kit Type:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::select('kit_type', $kit_types, '', ['class' => 'form-control']) !!}
            </div>
          </div> -->

          <div class="form-group col-sm-4 {{ ($errors->has('job_id') ? 'has-error' : '') }}">
            {!! Form::label('job_id', 'Job ID:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8 job_id">
              {!! Form::select('job_id', ['' => 'Please select'], null, ['class' => 'form-control']) !!}
            </div>
          </div>

          <!-- <div class="form-group col-sm-4 {{ ($errors->has('set_id') ? 'has-error' : '') }}">
            {!! Form::label('set_id', 'Set ID:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('set_id', '', ['class' => 'form-control']) !!}
            </div>
          </div> -->

          <div class="form-group col-sm-4 {{ ($errors->has('do_no') ? 'has-error' : '') }}">
            {!! Form::label('do_no', 'Do No.:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8 do_no">
              {!! Form::select('do_no', ['' => 'Please select'], null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

        </div>

        <br>

        <!-- <div class="row customKitBox">

          <div class="col-md-12">

            <h4>Kit Info List</h4>

            <table class="table table-bordered table-responsive" id="kit-info-table">
              <thead>
                <tr>
                  <th>Kit</th>
                  <th>Tool ID</th>
                  <th>Set ID</th>
                  <th>Select</th>
                </tr>
              </thead>

              <tbody>
              </tbody>
            </table>
          </div>
        </div> -->

        <div class="row customKitBox">
          <div class="form-group col-sm-12" style="margin-top: 10px;">
            {!! Form::submit('Preview', ['class' => 'btn btn-success preview-btn']) !!}
          </div>
        </div>

        {!! Form::close() !!}

      </div><!-- end box-body -->
    </div><!-- end box -->

@endsection


@section('extra-js')

  <script src="{{ asset('/js/custom/delivery-order-report.js') }}"></script>

@endsection

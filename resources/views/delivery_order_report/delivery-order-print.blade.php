<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title> @yield('htmlheader_title', config('app.name','Neutron') ) </title>
  <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <link href="{{ asset('css/deliveryorder-report.css') }}" rel="stylesheet" type="text/css" />

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>

<body onload="window.print()">
{{--<body>--}}
  <a href="javascript:window.print()" id="printPageButton" style="float: right;"></i> Print</a>

    <div class="box box-primary">
      <div class="box-body" style="padding: 20px;">

        <div class="rp-main-header-outer">
          <div class="rp-main-header">
            <div class="rp-head-title">Neutron Technology Enterprise Pte. Ltd.</div>
            <div class="rp-head-reg-no">Co. Reg. No: 201103559K</div>
            <div class="rp-head-addr">Block 3029A Ubi Road 3 #01-97 Singapore 408661</div>
            <div class="rp-head-tel">Tel: 6744 8614 Fax: 67440384</div>
            <div class="rp-head-email">Email: finance@neutrontech.com.sg URL: www.neutrontech.com.sg</div>
          </div>
        </div>

        <div class="rp-sec-a">Delivery Order</div>

        <div class="rp-sec-b">{{ $do_detail[0]->do_no }}</div>

        <div class="rp-sec-c">DELIVER TO</div>

        @php
          $row_count = 1;
          $cust_detail = \App\Models\GoodReceiptHeader::join('customer', 'good_receipt_header.customer_id', '=', 'customer.id' )
                         ->where( 'good_receipt_header.id', $do_detail[0]->good_receipt_header_id )
                         ->first();

          $address = str_replace(",", "<br>", $cust_detail->customer_address);
        @endphp

        <div class="rp-sec-d">{{ $cust_detail->customer_name }}</div>

        <div class="rp-sec-e">{!! $address !!}</div>

        <div class="rp-sec-f">Attn: {{ $cust_detail->delivery_contact }}</div>

        <div class="rp-sec-g">
          <div class="inner-content">Job ID</div>
          <div class="inner-content">Vendor Code</div>
          <div class="inner-content">Your PO No.</div>
          <div class="inner-content">Date</div>
        </div>

        <div class="rp-sec-h">
          <div class="inner-content">{{ $cust_detail->job_id }}</div>
          <div class="inner-content">{{ $cust_detail->vendor_code }}</div>
          <div class="inner-content">{{ $do_detail[0]->po_no }}</div>
          <div class="inner-content">{{ \Carbon\Carbon::parse($do_detail[0]->date)->format('d-m-Y')}}</div>
        </div>

        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; font-size: 14px;">
          <tbody>
            <tr>
              <td>
                <table border="0" cellpadding="0" cellspacing="0" class="rp-tblContent">
                  <tbody>
                    <tr>
                      <td class="tbltdBold" style="width: 5%;">
                        S/N
                      </td>
                      <td class="tbltdBold" style="width: 30%;">
                        P/N
                      </td>
                      <td class="tbltdBold" style="width: 45%;">
                        Description
                      </td>
                      <td class="tbltdBold">
                        Quantity
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>

            @foreach($do_detail as $row)
              @php
                $kit_master = \App\Models\KitMaster::join('uom_master', 'kit_master.uom_id', '=', 'uom_master.id')
                              ->where( 'kit_master.id', $row->kit_master_id )
                              ->first();

                $count_kit = \App\Models\DeliveryOrderChild::where('delivery_order_id', $row->delivery_order_id)
                              ->where( 'kit_master_id', $row->kit_master_id )
                              ->sum( 'qty' );

              @endphp

              <tr>
                <td style="padding-bottom: 10px;">
                  <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tbody>
                      <tr>
                        <td style="width: 5%;">
                          {{ $row_count }}
                        </td>
                        <td style="width: 30%;">
                          {{ $kit_master->kit_type }}
                        </td>
                        <td style="width: 45%;">
                          {{ $kit_master->descn }}
                        </td>
                        <td style="width: 10%;">
                          {{ $count_kit }}
                        </td>
                        <td>
                          {{ $kit_master->uom_code }}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>

              @php $row_count++; @endphp
            @endforeach

          </tbody>
        </table>

        <div class="rp-sec-i">
          <div class="cntRemark">Remarks: </div><br>
          <span style="font-weight: normal; white-space: pre-line; float: left;">{{ $do_detail[0]->remarks }}</span>
          <div class="cntReceive">Received in good order & condition:</div>
        </div>

        <!-- <div style="clear: both;"></div> -->

        <div class="rp-sec-j">
          <div class="cntLine"></div>
          <div class="cntAuth">Authorized signature and stamp</div>
        </div>

      </div>

    </div><!-- end content -->

</body>

<script>

  setTimeout('window.close()', 1000);

</script>

</html>

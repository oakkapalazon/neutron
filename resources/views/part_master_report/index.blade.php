@extends('layouts.app')

@section('content')

  <section class="content-header">
    <div class="col-md-8">
      <h1>Part Master Report</h1>
    </div><!-- end col-md-8 -->
  </section>

  <div class="content">

    <div class="box box-primary filter-part-master-box">
      <div class="box-body customBox">

        <div class="row">
          <div class="col-md-12">
            <h4>Filter</h4>
          </div><!-- end col-md-12 -->

          <div class="form-group col-sm-4 {{ ($errors->has('kit_type') ? 'has-error' : '') }}">
            {!! Form::label('kit_type', 'Kit *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::select('kit_type', $kit_master, null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->
          <div class="form-group col-md-4 {{ ($errors->has('part_type') ? 'has-error' : '') }}">
            {!! Form::label('part_type', 'Part Type:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::select('part_type', ['' => 'Please select'], null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->
          <div class="form-group col-sm-4 {{ ($errors->has('serial_no') ? 'has-error' : '') }}">
            {!! Form::label('serial_no', 'Serial No.:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('serial_no', '', ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->
        </div>

        <div class="row">
          <div class="form-group col-md-4 {{ ($errors->has('set_id') ? 'has-error' : '') }}">
            {!! Form::label('set_id', 'Set ID:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('set_id', '', ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->
          <div class="form-group col-md-4 {{ ($errors->has('date_from') ? 'has-error' : '') }}">
            {!! Form::label('created_at', 'Created Fr:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('created_at', '', ['class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->
        </div>

        <div class="row">
          <div class="form-group col-sm-12" style="margin-top: 10px;">
            <a href="#" class="btn btn-success search-btn">Search</a>
          </div>
        </div>
      </div><!-- end box-body -->
    </div><!-- end box -->

    <div class="box box-primary">
      <div class="box-body">
        <div class="row">
          <div class="form-group col-sm-12">
            {!! Form::open(['action' => 'PartMasterReportController@exportPartMasterDetail', 'method' => 'get']) !!}
              <input type="hidden" name="hidden_kit_type">
              <input type="hidden" name="hidden_part_type">
              <input type="hidden" name="hidden_serial_no">
              <input type="hidden" name="hidden_set_id">
              <input type="hidden" name="hidden_created_at">
              {!! Form::submit('Export', ['class' => 'btn btn-info export-btn', 'style' => 'float: right']) !!}
            {!! Form::close() !!}
          </div>
          <div class="col-md-12">

            <table class="table table-bordered table-responsive table-striped" id="grn-report-table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Kit Type</th>
                  <th>Part Type</th>
                  <th>Serial No</th>
                  <th>Set ID</th>
                  <th>Specs</th>
                  <th>Status</th>
                  <th>Additonal Info</th>
                  <th>Grade</th>
                  <th>Remarks</th>
                  <th>Created At</th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>

          </div><!-- end col-md-12 -->
        </div><!-- end row -->
      </div>

  </div><!-- end content -->

@endsection


@section('extra-js')

  <script type="text/javascript" src="{{ asset('/js/custom/part_master_report.js') }}"></script>

  <script type="text/javascript">

    $(function() {

      $("#created_at").datepicker({
        format: 'yyyy-mm-dd',
      });

    });

  </script>

@endsection

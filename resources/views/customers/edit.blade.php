@extends('layouts.app')

@section('content')

  <section class="content-header">
    <h1>Edit Customer</h1>
  </section>

  <div class="content">

    @include('adminlte-templates::common.errors')
    {{--@include('adminlte-templates::common.success')--}}

    <div class="box box-primary">
      <div class="box-body">

        <div class="row">

          {!! Form::model($customer, ['route' => ['customers.update', $customer->id], 'method' => 'patch', 'enctype'=>'multipart/form-data']) !!}

          <input type="hidden" name="id" id="customer_id" value="{{ $customer->id }}" class="cust_id">

          <div class="form-group col-sm-6 {{ ($errors->has('company_id') ? 'has-error' : '') }}">
            {!! Form::label('company_id', 'Company *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::select('company_id', $companies, null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('vendor_code') ? 'has-error' : '') }}">
            {!! Form::label('vendor_code', 'Vendor Code:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('vendor_code', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('customer_code') ? 'has-error' : '') }}">
            {!! Form::label('customer_code', 'Customer *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('customer_code', null, ['class' => 'form-control', 'disabled' => true]) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('customer_name') ? 'has-error' : '') }}">
            {!! Form::label('customer_name', 'Customer Name *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('customer_name', null, ['class' => 'form-control', 'disabled' => true]) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('folder_name') ? 'has-error' : '') }}">
            {!! Form::label('folder_name', 'Folder Name *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('folder_name', null, ['class' => 'form-control', 'disabled' => 'true']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('purchaser') ? 'has-error' : '') }}">
            {!! Form::label('purchaser', 'Purchaser:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('purchaser', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('customer_address') ? 'has-error' : '') }}">
            {!! Form::label('customer_address', 'Customer Address *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::textarea('customer_address', null, ['class' => 'form-control', 'rows' => 4, 'cols' => 30]) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('delivery_contact') ? 'has-error' : '') }}">
            {!! Form::label('delivery_contact', 'Delivery Contact:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('delivery_contact', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('delivery_address') ? 'has-error' : '') }}">
            {!! Form::label('delivery_address', 'Delivery Address:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::textarea('delivery_address', null, ['class' => 'form-control', 'rows' => 4, 'cols' => 30]) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <!-- <div class="form-group col-sm-12">
            {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
            <a href="{!! route('customers.index') !!}" class="btn btn-default">Cancel</a>
          </div> -->

          <div class="subSection">

            <div class="col-md-12">

              <h4>Purchase Order List</h4>

              <table class="table table-bordered table-responsive table-striped" id="customers-table">
                <thead>
                  <tr>
                    <th width="2%">#</th>
                    <th>No.</th>
                    <th>Purchase Order No.</th>
                    <th>Expiry Date</th>
                  </tr>
                </thead>
                <tbody>

                  @php
                    $row_count = 1;
                  @endphp
                  @if(isset($po_list))
                    @foreach($po_list as $row)

                      <tr>

                        @php
                          $delivery = \App\Models\DeliveryOrder::where('po_no', $row->po_no)
                                      ->select('po_no')
                                      ->get();
                          $count_d = count($delivery);
                        @endphp

                        @if($count_d > 0)

                          <td class="valign-middle"><i data-id="{{ $row->id }}" class="fas fa-minus-circle remove-row" style="display: none;"></i></td>

                        @else

                          <td class="valign-middle"><i data-id="{{ $row->id }}" class="fas fa-minus-circle remove-row"></i></td>

                        @endif

                        <td class="valign-middle halign-center">{{ $row_count }}</td>
                        <td>
                          {!! Form::text('po_no[]', $row->po_no, ['class' => 'form-control po_no', 'id' => 'po_no', 'disabled' => true]) !!}
                        </td>
                        <td>
                          {!! Form::text('effective[]', \Carbon\Carbon::parse($row->effective_at)->format('d/m/Y'), ['class' => 'form-control effective', 'id' => 'effective', 'disabled' => true]) !!}
                        </td>
                      </tr>

                      @php
                        $row_count++;
                      @endphp
                    @endforeach
                  @endif

                </tbody>
              </table>
            </div><!-- end col-md-12 -->
            <p class="form-group col-md-12">
              <button type="button" name="button" class="btn btn-default add-btn">
                <i class="fas fa-plus-circle"></i> &nbsp;Add
              </button>
              <a href="{!! route('customers.index') !!}" class="btn btn-default btn-cancel" style="float: right;">Cancel</a>
              <button type="button" class="btn btn-primary btn-save" style="float: right; margin-right: 2px;">Save</button>
            </p><!-- end col-md-12 -->
          </div>

          {!! Form::close() !!}

        </div><!-- end row -->
      </div><!-- end box-body -->
    </div><!-- end box -->

    <table style="display: none;">
      <tr id="append-row">
        <td class="valign-middle"><i data-id="" class="fas fa-minus-circle remove-row"></i></td>
        <td class="sr_no valign-middle halign-center"></td>
        <td>
          {!! Form::text('po_no[]', null, ['class' => 'form-control po_no', 'id' => 'po_no']) !!}
        </td>
        <td>
          {!! Form::text('effective[]', null, ['class' => 'form-control effective', 'id' => 'effective']) !!}
        </td>
      </tr>
    </table>

  </div><!-- end content -->

@endsection

@section('extra-js')

  <!-- <script src="{{ asset('/js/jquery.easy-autocomplete.min.js') }}"></script> -->
  <script src="{{ asset('/js/custom/customers.js') }}"></script>

  <script type="text/javascript">

    $(function() {

      $( "#customers-table .effective" ).each(function() {

        $( this ).datepicker({
          format: 'dd/mm/yyyy',
        });
      });

    });

  </script>

@endsection

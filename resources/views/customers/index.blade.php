@extends('layouts.app')

@section('content')

  <section class="content-header">
    <div class="col-md-8">
      <h1>Customer Lists</h1>
    </div><!-- end col-md-8 -->

    <div class="col-md-4">
      <a href="{!! route('customers.create') !!}" class="btn btn-primary pull-right">Add New</a>
    </div><!-- end col-md-4 -->
  </section>

  <div class="content">

    <div class="box box-primary">
      <div class="box-body table-responsive">

        <!-- {!! Form::open(['method' => 'GET', 'class'=>'form-horizontal']) !!}

        <div class="form-group">
          <label class="col-md-2 control-label" for="search_field">Search Keyword: </label>

          <div class="col-md-3">
            {!! Form::text('q', null,['class'=>'form-control', 'id'=>'search_field']) !!}
          </div>

          <div class="col-md-5">
            {!!Form::submit('Search',['class'=>'btn btn-default btn-cs search_btn'])!!}
            <a href="{!! route('customers.index') !!}" class="btn btn-default btn-cs">Clear</a>
          </div>

        </div>

        {!!Form::close()!!} -->

        <div class="row">

          <div class="col-md-12">
            <table class="table table-bordered table-responsive table-striped" id="customers-table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Customer</th>
                  <th>Company Name</th>
                  <th>Customer Name</th>
                  <th>Customer Address</th>
                  <th>Purchaser</th>
                  <th>Delivery Address</th>
                  <th>Delivery Contact</th>
                  <th>Vendor Code</th>
                  <th>Action</th>
                </tr>
              </thead>

              @php $count = 1; @endphp

              <tbody>
                @if(count($customers) > 0)

                  @foreach($customers as $row)
                  <tr>
                    <td>{{ $count }}</td>
                    <td>{{ $row->customer_code }}</td>
                    <td>{{ $row->company_name }}</td>
                    <td>{{ $row->customer_name }}</td>
                    <td>{{ $row->customer_address }}</td>
                    <td>{{ $row->purchaser }}</td>
                    <td>{{ $row->delivery_address }}</td>
                    <td>{{ $row->delivery_contact }}</td>
                    <td>{{ $row->vendor_code }}</td>
                    <td>
                      @if($row->count == 0)

                      {!! Form::open(['route' => ['customers.destroy', $row->id], 'method' => 'delete']) !!}
                      <a href="{!! route('customers.edit', [$row->id]) !!}" class='btn btn-default btn-xs' title='Edit'><i class="glyphicon glyphicon-edit"></i></a>
                      {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'title' => 'Delete', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure? ( Note: Folder will be deleted as well!!! )')"]) !!}
                      {!! Form::close() !!}

                      @else

                      <a href="{!! route('customers.edit', [$row->id]) !!}" class='btn btn-default btn-xs' title='Edit'><i class="glyphicon glyphicon-edit"></i></a>
                      {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'title' => 'Delete', 'class' => 'btn btn-danger btn-xs', 'disabled' => 'disabled']) !!}

                      @endif
                    </td>
                  </tr>

                  @php $count++; @endphp

                  @endforeach

                @else

                <tr>
                  <td colspan="14">No Result Found</td>
                </tr>

                @endif
              </tbody>
              <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Customer Code</th>
                  <th>Company Name</th>
                  <th>Customer Name</th>
                  <th>Customer Address</th>
                  <th>Purchaser</th>
                  <th>Delivery Address</th>
                  <th>Delivery Contact</th>
                  <th>Vendor Code</th>
                </tr>
              </tfoot>
            </table>

            {{ $customers->links() }}

          </div><!-- end col-md-12 -->

        </div><!-- end row -->
      </div><!-- end box-body -->
    </div><!-- end box -->

    <div class="row">
      <div class="col-md-12">
        <p class="label label-info info-notice">
          * You cannot delete customers which are assigned in kit master module.
        </p>
      </div><!-- end col-md-12 -->
    </div><!-- end row -->

  </div><!-- end content -->

@endsection


@section('extra-js')

  <script type="text/javascript">

    $(function() {

      $('#search_date').datepicker({
        format: 'dd/mm/yyyy',
      });

      $('#customers-table').DataTable({
        lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
        order: [],
        initComplete: function () {

        var r = $('#customers-table tfoot tr');
        $('#customers-table thead').append(r);

        this.api().columns().every(function () {
          var column = this;
          var input = document.createElement("input");
          input.className = "search";
          $(input).appendTo($(column.footer()).empty())
          .on('change', function () {
            column.search($(this).val(), false, false, true).draw();
          });
        });
      }
      });

    });

  </script>

@endsection

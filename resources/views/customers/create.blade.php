@extends('layouts.app')

@section('content')

  <section class="content-header">
    <h1>Create Customer</h1>
  </section>

  <div class="content">

    @include('adminlte-templates::common.errors')
    {{--@include('adminlte-templates::common.success')--}}

    <div class="box box-primary">
      <div class="box-body">

        <div class="row">

          {!! Form::open(['route' => 'customers.store']) !!}

          <div class="form-group col-sm-6 {{ ($errors->has('company_id') ? 'has-error' : '') }}">
            {!! Form::label('company_id', 'Company *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::select('company_id', $companies, null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('vendor_code') ? 'has-error' : '') }}">
            {!! Form::label('vendor_code', 'Vendor Code:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('vendor_code', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('customer_code') ? 'has-error' : '') }}">
            {!! Form::label('customer_code', 'Customer *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('customer_code', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('customer_name') ? 'has-error' : '') }}">
            {!! Form::label('customer_name', 'Customer Name *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('customer_name', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('customer_address') ? 'has-error' : '') }}">
            {!! Form::label('customer_address', 'Customer Address *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::textarea('customer_address', null, ['class' => 'form-control', 'rows' => 4, 'cols' => 30]) !!}
            </div>
          </div>

{{--          <div class="form-group col-sm-6 {{ ($errors->has('folder_name') ? 'has-error' : '') }}">--}}
{{--            {!! Form::label('folder_name', 'Folder Name *:', ['class' => 'col-sm-4 control-label']) !!}--}}

{{--            <div class="col-sm-8">--}}
{{--              {!! Form::text('folder_name', null, ['class' => 'form-control']) !!}--}}
{{--            </div>--}}
{{--          </div>--}}

          <div class="form-group col-sm-6 {{ ($errors->has('purchaser') ? 'has-error' : '') }}">
            {!! Form::label('purchaser', 'Purchaser:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('purchaser', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

{{--          <div class="form-group col-sm-6 {{ ($errors->has('customer_address') ? 'has-error' : '') }}">--}}
{{--            {!! Form::label('customer_address', 'Customer Address *:', ['class' => 'col-sm-4 control-label']) !!}--}}

{{--            <div class="col-sm-8">--}}
{{--              {!! Form::textarea('customer_address', null, ['class' => 'form-control', 'rows' => 4, 'cols' => 30]) !!}--}}
{{--            </div>--}}
{{--          </div>--}}

          <div class="form-group col-sm-6 {{ ($errors->has('delivery_contact') ? 'has-error' : '') }}">
            {!! Form::label('delivery_contact', 'Delivery Contact:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('delivery_contact', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('delivery_address') ? 'has-error' : '') }}">
            {!! Form::label('delivery_address', 'Delivery Address:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::textarea('delivery_address', null, ['class' => 'form-control', 'rows' => 4, 'cols' => 30]) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-12">
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
            <a href="{!! route('customers.index') !!}" class="btn btn-default">Cancel</a>
          </div><!-- end form-group -->

          {!! Form::close() !!}

        </div><!-- end row -->
      </div><!-- end box-body -->
    </div><!-- end box -->

  </div><!-- end content -->

@endsection

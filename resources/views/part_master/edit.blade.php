@extends('layouts.app')

@section('content')

  <section class="content-header">
    <h1>Edit Part Master</h1>
  </section>

  <div class="content">

    @include('adminlte-templates::common.errors')
    {{--@include('adminlte-templates::common.success')--}}

    <div class="box box-primary">
      <div class="box-body">

        <div class="row">

          {!! Form::model($part_master, ['route' => ['part_master.update', $part_master->id], 'method' => 'patch', 'enctype'=>'multipart/form-data']) !!}

          <input type="hidden" name="id" value="{{ $part_master->id }}">

          <div class="form-group col-sm-6 {{ ($errors->has('kit_master_id') ? 'has-error' : '') }}">
            {!! Form::label('kit_master_id', 'Kit Type:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::select('kit_master_id', $kit_masters, $part_master->kit_master_id, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <input type="hidden" name="hidden_part_type_id" value="{{ $part_master->part_type_id }}" />
          <div class="form-group col-sm-6 {{ ($errors->has('part_type_id') ? 'has-error' : '') }}">
            {!! Form::label('part_type_id', 'Part Type:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              @if(empty(old('part_type_id')))

                {!! Form::select('part_type_id', $part_type_masters, $part_master->part_type_id, ['class' => 'form-control', 'id' => 'part_type_id']) !!}

              @else

              @php

                $part_types = \App\Models\PartTypeMaster::getPartTypeMaster(old('kit_master_id'));

              @endphp

                {!! Form::select('part_type_id', $part_types, null, ['class' => 'form-control', 'id' => 'part_type_id']) !!}

              @endif
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('serial_no') ? 'has-error' : '') }}">
            {!! Form::label('serial_no', 'Serial No *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('serial_no', null, ['class' => 'form-control', 'onkeyup' => 'this.value = this.value.toUpperCase();']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('set_id') ? 'has-error' : '') }}">
            {!! Form::label('set_id', 'SET ID:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('set_id', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('specs') ? 'has-error' : '') }}">
            {!! Form::label('specs', 'Other Ref No:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('specs', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('status') ? 'has-error' : '') }}">
            {!! Form::label('status', 'Status:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::select('status', $part_status_master, $part_master->status, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('remarks') ? 'has-error' : '') }}">
            {!! Form::label('remarks', 'Remarks:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('remarks', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('additional_info') ? 'has-error' : '') }}">
            {!! Form::label('additional_info', 'Additional Info:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('additional_info', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('grade') ? 'has-error' : '') }}">
            {!! Form::label('grade', 'Grade:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('grade', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('refurb_count') ? 'has-error' : '') }}">
            {!! Form::label('refurb_count', 'Refurb Count:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::number('refurb_count', null, ['min' => 0, 'class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-12">

{{--            @if($no_of_goodpart == 0)--}}

              {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}

{{--            @endif--}}

            <a href="{!! route('part_master.index') !!}" class="btn btn-default">Cancel</a>
          </div><!-- end form-group -->

          {!! Form::close() !!}
        </div><!-- end row -->
      </div><!-- end box-body -->
    </div><!-- end box -->

  </div><!-- end content -->

@endsection

@section('extra-js')

  <script src="{{ asset('/js/custom/main.js') }}"></script>

  <script>
    $(function() {

      var no_of_goodpart = '{{ $no_of_goodpart }}';

      if( no_of_goodpart > 0 )
      {
        $( '#kit_master_id' ).attr( 'readonly', true );
        $( '#serial_no' ).attr( 'readonly', true );
        $( '#part_type_id' ).attr( 'readonly', true );
        $( '#set_id' ).attr( 'readonly', true );
      }
    });
  </script>

@endsection

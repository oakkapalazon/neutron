@extends('layouts.app')

@section('content')

  <section class="content-header">
    <div class="col-md-8">
      <h1>Part Master Lists</h1>
    </div><!-- end col-md-8 -->

    <div class="col-md-4">
      <a href="{!! route('part_master.create') !!}" class="btn btn-primary pull-right">Add New</a>
    </div><!-- end col-md-4 -->
  </section>

  <div class="content">

    <div class="box box-primary">
      <div class="box-body">

        <!-- {!! Form::open(['method' => 'GET', 'class'=>'form-horizontal']) !!}

        <div class="form-group">
          <label class="col-md-2 control-label" for="search_field">Search Keyword: </label>

          <div class="col-md-3">
            {!! Form::text('q', null,['class'=>'form-control', 'id'=>'search_field']) !!}
          </div>

          <div class="col-md-5">
            {!!Form::submit('Search',['class'=>'btn btn-default btn-cs search_btn'])!!}
            <a href="{!! route('part_master.index') !!}" class="btn btn-default btn-cs">Clear</a>
          </div>

        </div>

        {!!Form::close()!!} -->

        <div class="row">

          <div class="col-md-12">
            <table class="table table-bordered table-responsive table-striped" id="part-table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Kit Type</th>
                  <th>Part Type</th>
                  <th>Serial No</th>
                  <th style="width: 3%;">SET ID</th>
{{--                  <th>Specs</th>--}}
                  <th style="width: 5%;">Status</th>
{{--                  <th>Additonal Info</th>--}}
{{--                  <th>Grade</th>--}}
{{--                  <th>Remarks</th>--}}
{{--                  <th>Created At</th>--}}
                  <th>Action</th>
                </tr>
              </thead>

              @php $count = 1; @endphp

              <tbody>
                

                  @foreach($part_masters as $row)
                  <tr>
                    <td>{{ $count }}</td>
                    <td>{{ $row->kit_type }}</td>
                    <td>{{ $row->part_type }}</td>
                    <td>{{ $row->serial_no }}</td>
                    <td>{{ $row->set_id }}</td>
{{--                    <td>{{ $row->specs }}</td>--}}
                    <td>{{ $row->status }}</td>
{{--                    <td>{{ $row->additional_info }}</td>--}}
{{--                    <td>{{ $row->grade }}</td>--}}
{{--                    <td>{{ $row->remarks }}</td>--}}
{{--                    <td>{{ $row->created_at }}</td>--}}
                    <td>
                      {!! Form::open(['route' => ['part_master.destroy', $row->id], 'method' => 'delete']) !!}
                      <a href="{!! route('part_master.edit', [$row->id]) !!}" class='btn btn-default btn-xs' title='Edit'><i class="glyphicon glyphicon-edit"></i></a>

                      @if($row->no_of_goodpart > 0)

                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'title' => 'Delete', 'class' => 'btn btn-danger btn-xs', 'disabled' => true]) !!}

                      @else

                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'title' => 'Delete', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}

                      @endif

                      {!! Form::close() !!}
                    </td>
                  </tr>

                  @php $count++; @endphp

                  @endforeach


              </tbody>
              <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Kit Type</th>
                  <th>Part Type</th>
                  <th>Serial No</th>
                  <th>SET ID</th>
{{--                  <th>Specs</th>--}}
                  <th>Status</th>
{{--                  <th>Additonal Info</th>--}}
{{--                  <th>Grade</th>--}}
{{--                  <th>Remarks</th>--}}
{{--                  <th>Created At</th>--}}
                </tr>
              </tfoot>
            </table>



          </div><!-- end col-md-12 -->

        </div><!-- end row -->
      </div><!-- end box-body -->
    </div><!-- end box -->

  </div><!-- end content -->

@endsection


@section('extra-js')

  <script type="text/javascript">

    $(function() {

      $('#part-table').DataTable({
        lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
        order: [],
        initComplete: function () {

				var r = $('#part-table tfoot tr');
    		$('#part-table thead').append(r);

				this.api().columns().every(function () {
          var column = this;
          var input = document.createElement("input");
          input.className = "search";
          $(input).appendTo($(column.footer()).empty())
          .on('change', function () {
            console.log(column.search($(this).val(), false, false, true).draw());
            column.search($(this).val(), false, false, true).draw();
          });
        });
      }
      });
    });

  </script>

@endsection

@extends('layouts.app')

@section('content')

  <section class="content-header">
    <h1>Kit Type Master</h1>
  </section>

  <div class="content">

    @include('adminlte-templates::common.errors')
    {{--@include('adminlte-templates::common.success')--}}

    <div class="box box-primary">
      <div class="box-body">

        <div class="row">

          {!! Form::open(['route' => 'kit_master.store', 'id' => 'form_header']) !!}

          <input type="hidden" id="kit_master_id" value="{{ Request::get('kit_master_id') }}" />

          @if(Request::get('kit_master_id'))

          @php
            $kit_master = \App\Models\KitMaster::find(Request::get('kit_master_id'));
          @endphp

          <input type="hidden" name="kit_master_id" value="{{ $kit_master->id }}" class="kit-master-id" id="kit-master-id">

          <div class="form-group col-sm-6 {{ ($errors->has('customer_id') ? 'has-error' : '') }}">
            {!! Form::label('customer_id', 'Customer *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::select('customer_id', $customers, $kit_master->customer_id, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <!-- <div class="form-group col-sm-6 {{ ($errors->has('po_no') ? 'has-error' : '') }}">
            {!! Form::label('po_no', 'PO No:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('po_no', $kit_master->po_no, ['class' => 'form-control']) !!}
            </div>
          </div> -->

          <div class="form-group col-sm-6 {{ ($errors->has('kit_type') ? 'has-error' : '') }}">
            {!! Form::label('kit_type', 'Kit *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('kit_type', $kit_master->kit_type, ['class' => 'form-control', 'readonly' => true]) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('descn') ? 'has-error' : '') }}">
            {!! Form::label('descn', 'Descn *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('descn', $kit_master->descn, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('process') ? 'has-error' : '') }}">
            {!! Form::label('process', 'Process:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('process', $kit_master->process, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('uom_id') ? 'has-error' : '') }}">
            {!! Form::label('uom_id', 'UOM Code *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::select('uom_id', $uom_masters, $kit_master->uom_id, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('remarks') ? 'has-error' : '') }}">
            {!! Form::label('remarks', 'Remarks:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('remarks', $kit_master->remarks, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('folder_name') ? 'has-error' : '') }}">
            {!! Form::label('folder_name', 'Folder Name:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              @if(isset($kit_master->folder_name))

                {!! Form::text('folder_name', $kit_master->folder_name, ['class' => 'form-control', 'readonly' => true]) !!}

              @else

                {!! Form::text('folder_name', $kit_master->folder_name, ['class' => 'form-control']) !!}

              @endif

            </div>
          </div>

            @if(!isset($kit_master->folder_name))

              <div class="form-group col-sm-12">

                {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}

                <a href="{!! route('kit_master.index') !!}" class="btn btn-default">Cancel</a>
              </div><!-- end form-group -->

            @endif
          @else

          <div class="form-group col-sm-6 {{ ($errors->has('customer_id') ? 'has-error' : '') }}">
            {!! Form::label('customer_id', 'Customer *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::select('customer_id', $customers, null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <!-- <div class="form-group col-sm-6 {{ ($errors->has('po_no') ? 'has-error' : '') }}">
            {!! Form::label('po_no', 'PO No:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('po_no', null, ['class' => 'form-control']) !!}
            </div>
          </div> -->

          <div class="form-group col-sm-6 {{ ($errors->has('kit_type') ? 'has-error' : '') }}">
            {!! Form::label('kit_type', 'Kit Type *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('kit_type', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('descn') ? 'has-error' : '') }}">
            {!! Form::label('descn', 'Descn *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('descn', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('process') ? 'has-error' : '') }}">
            {!! Form::label('process', 'Process:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('process', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('uom_id') ? 'has-error' : '') }}">
            {!! Form::label('uom_id', 'UOM Code *:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::select('uom_id', $uom_masters, null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

          <div class="form-group col-sm-6 {{ ($errors->has('remarks') ? 'has-error' : '') }}">
            {!! Form::label('remarks', 'Remarks:', ['class' => 'col-sm-4 control-label']) !!}

            <div class="col-sm-8">
              {!! Form::text('remarks', null, ['class' => 'form-control']) !!}
            </div><!-- end col-md-8 -->
          </div><!-- end form-group -->

{{--          <div class="form-group col-sm-6 {{ ($errors->has('folder_name') ? 'has-error' : '') }}">--}}
{{--            {!! Form::label('folder_name', 'Folder Name:', ['class' => 'col-sm-4 control-label']) !!}--}}

{{--            <div class="col-sm-8">--}}
{{--              {!! Form::text('folder_name', null, ['class' => 'form-control']) !!}--}}
{{--            </div><!-- end col-md-8 -->--}}
{{--          </div><!-- end form-group -->--}}

          <div class="form-group col-sm-12">

            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}

            <a href="{!! route('kit_master.index') !!}" class="btn btn-default">Cancel</a>
          </div><!-- end form-group -->

          @endif

          <hr>

          <div class="subSection">
            <div class="col-md-12">

              <h4>Part Type Information</h4>

              <table class="table table-bordered table-responsive table-striped" id="part-type-table">
                <thead>
                  <tr>
                    <th width="2%">#</th>
                    <th width="4%">S/No</th>
                    <th width="20%">Part Type</th>
                    <th width="15%">Max RFHrs</th>
                    <th width="15%">Max RF To Print</th>
                    <th width="10%">Part Sequence</th>
                    <th width="10%">Kit Identifier</th>
                    <th width="10%">Grade Required</th>
                    <th width="10%">Initial Required</th>
                    <th width="5%"></th>
                  </tr>
                </thead>

                @if(Request::get('kit_master_id'))

                @php
                  $part_types = \App\Models\PartTypeMaster::where('kit_master_id', Request::get('kit_master_id'))->get();
                  $has_kit_identifier = \App\Models\PartTypeMaster::where('kit_master_id', Request::get('kit_master_id'))->where('kit_identifier', 1)->count();
                  $good_kit_detail = \App\Models\GoodKitDetail::where('kit_master_id', Request::get('kit_master_id'))->get();
                  $row_count = 1;
                @endphp

                <tbody>

                  @foreach($part_types as $row)
                  <tr>

                    @if(count($good_kit_detail) > 0)

                      <td class="valign-middle"><i data-id="{{ $row->id }}" class="fas fa-minus-circle remove-row" style="display: none;"></i></td>

                    @else

                      <td class="valign-middle"><i data-id="{{ $row->id }}" class="fas fa-minus-circle remove-row"></i></td>

                    @endif

                    <td class="valign-middle halign-center">{{ $row_count }}</td>
                    <td>
                      {!! Form::text('part_type[]', $row->part_type, ['class' => 'form-control part-type', 'id' => 'part-type']) !!}
                    </td>
                    <td>
                      {!! Form::number('max_rf_hrs[]', $row->max_rf_hrs, ['min' => 0, 'class' => 'form-control max-rf-hrs', 'id' => 'max-rf-hrs']) !!}
                    </td>
                    <td>
                      {!! Form::number('max_rf_print[]', $row->max_rf_print, ['min' => 0, 'class' => 'form-control max-rf-print', 'id' => 'max-rf-print']) !!}
                    </td>
                    <td>
                      {!! Form::number('part_sequence[]', $row->part_sequence, ['min' => 0, 'class' => 'form-control part-sequence', 'id' => 'part-sequence']) !!}
                    </td>
                    <td class="valign-middle halign-center">
                      <input type="hidden" name="hidden_kit_identifier[]" value="{{ $row->kit_identifier }}" class="hidden_kit_identifier" />
                      @if($has_kit_identifier == 1)

                        @if($row->kit_identifier == 1)
                          {!! Form::checkbox('kit_identifier', 1, true, ['class' => 'kit-identifier']) !!}
                        @else
                          {!! Form::checkbox('kit_identifier', 1, false, ['class' => 'kit-identifier', 'disabled' => true]) !!}
                        @endif

                      @else

                        {!! Form::checkbox('kit_identifier', 1, false, ['class' => 'kit-identifier']) !!}

                      @endif
                    </td>
                    <td class="valign-middle halign-center">
                      <input type="hidden" name="hidden_grade_required[]" value="{{ $row->grade_required }}" class="hidden_grade_required" />
                      @if($row->grade_required == 1)
                        {!! Form::checkbox('grade_required', 1, true, ['class' => 'grade-required']) !!}
                      @else
                        {!! Form::checkbox('grade_required', 1, false, ['class' => 'grade-required']) !!}
                      @endif
                    </td>
                    <td class="valign-middle halign-center">
                      <input type="hidden" name="hidden_initial_required[]" value="{{ $row->initial_required }}" class="hidden_initial_required" />
                      @if($row->initial_required == 1)
                        {!! Form::checkbox('initial_required', 1, true, ['class' => 'initial-required']) !!}
                      @else
                        {!! Form::checkbox('initial_required', 1, false, ['class' => 'initial-required']) !!}
                      @endif
                    </td>
                    <td class="right-arrow">
                      <input type="hidden" name="hidden_dialog-arrow" value="{{ $row->id }}" class="hidden_dialog-arrow" />
                      <a href="#" class="" data-part-type-id="{{ $row->id }}"><i class="fas fa-long-arrow-alt-right dialog-arrow"></i></a>
                    </td>
                  </tr>

                  @php $row_count++; @endphp

                  @endforeach
                </tbody>

                @else

                <tbody>
                  <tr>
                    <td class="valign-middle"><i data-id="" class="fas fa-minus-circle remove-row"></i></td>
                    <td class="valign-middle halign-center">1</td>
                    <td>
                      {!! Form::text('part_type[]', null, ['class' => 'form-control part-type', 'id' => 'part-type']) !!}
                    </td>
                    <td>
                      {!! Form::number('max_rf_hrs[]', null, ['min' => 0, 'class' => 'form-control max-rf-hrs', 'id' => 'max-rf-hrs']) !!}
                    </td>
                    <td>
                      {!! Form::number('max_rf_print[]', null, ['min' => 0, 'class' => 'form-control max-rf-print', 'id' => 'max-rf-print']) !!}
                    </td>
                    <td>
                      {!! Form::number('part_sequence[]', null, ['min' => 0, 'class' => 'form-control part-sequence', 'id' => 'part-sequence']) !!}
                    </td>
                    <td class="valign-middle halign-center">
                      {!! Form::checkbox('kit_identifier', 1, false, ['class' => 'kit-identifier']) !!}
                    </td>
                    <td class="valign-middle halign-center">
                      {!! Form::checkbox('grade_required', 1, false, ['class' => 'grade-required']) !!}
                    </td>
                    <td class="right-arrow">
                      <input type="hidden" name="hidden_dialog-arrow" value="" class="hidden_dialog-arrow" />
                      <a href="#" class="" data-part-type-id=""><i class="fas fa-long-arrow-alt-right dialog-arrow"></i></a>
                    </td>
                  </tr>
                </tbody>

                @endif

              </table>
            </div><!-- end col-md-12 -->

            <!-- <div class="form-group col-sm-12">
              @if(Request::get('kit_master_id'))
              {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
              @else
              {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
              @endif
              <a href="{!! route('kit_master.index') !!}" class="btn btn-default">Cancel</a>
            </div> -->

            <p class="form-group col-md-12">
              <button type="button" name="button" class="btn btn-default add-btn">
                <i class="fas fa-plus-circle"></i> &nbsp;Add
              </button>
              <a href="{!! route('kit_master.index') !!}" class="btn btn-default btn-cancel" style="float: right;">Cancel</a>
              <button type="button" class="btn btn-primary btn-save" style="float: right; margin-right: 2px;">Save</button>
            </p><!-- end col-md-12 -->
          </div>

          {!! Form::close() !!}

        </div><!-- end row -->
      </div><!-- end box-body -->
    </div><!-- end box -->

    <table style="display: none">
      <tr id="append-row">
        <td class="valign-middle"><i data-id="" class="fas fa-minus-circle remove-row"></i></td>
        <td class="sr_no valign-middle halign-center"></td>
        <td>
          {!! Form::text('part_type[]', null, ['class' => 'form-control part-type', 'id' => 'part-type']) !!}
        </td>
        <td>
          {!! Form::number('max_rf_hrs[]', null, ['min' => 0, 'class' => 'form-control max-rf-hrs', 'id' => 'max-rf-hrs']) !!}
        </td>
        <td>
          {!! Form::number('max_rf_print[]', null, ['min' => 0, 'class' => 'form-control max-rf-print', 'id' => 'max-rf-print']) !!}
        </td>
        <td>
          {!! Form::number('part_sequence[]', null, ['min' => 0, 'class' => 'form-control part-sequence', 'id' => 'part-sequence']) !!}
        </td>
        <td class="valign-middle halign-center">
          <input type="hidden" name="hidden_kit_identifier[]" value="0" class="hidden_kit_identifier" />
          {!! Form::checkbox('kit_identifier[]', 1, false, ['class' => 'kit-identifier']) !!}
        </td>
        <td class="valign-middle halign-center">
          <input type="hidden" name="hidden_grade_required[]" value="0" class="hidden_grade_required" />
          {!! Form::checkbox('grade_required[]', 1, false, ['class' => 'grade-required']) !!}
        </td>
        <td class="valign-middle halign-center">
          <input type="hidden" name="hidden_initial_required[]" value="0" class="hidden_initial_required" />
          {!! Form::checkbox('initial_required[]', 1, false, ['class' => 'initial-required']) !!}
        </td>
        <td class="right-arrow">
          <input type="hidden" name="hidden_dialog-arrow" value="" class="hidden_dialog-arrow" />
          <a href="#" class="" data-part-type-id=""><i class="fas fa-long-arrow-alt-right dialog-arrow"></i></a>
        </td>
      </tr>
    </table>

    <!-- Modal -->
    <div class="modal fade" id="modal-dialog-part-parameter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
      <div class="modal-dialog" role="document" style="width: 90%;">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Part Type Header</h4>
          </div><!-- end modal-header -->

          <div class="modal-body">

            <div class="alert alert-danger check-part-parameter-fields" role="alert" style="display: none">
            </div><!-- end alert alert-danger -->

            <input type="hidden" name="kit_type_id" value="" class="modal-kit-type-id" id="modal-kit-type-id">
            <input type="hidden" name="part_type_id" value="" class="modal-part-type-id" id="modal-part-type-id">

            <div class="form-group col-sm-6">
              {!! Form::label('modal-part-type', 'Part Type:', ['class' => 'col-sm-4 control-label']) !!}

              <div class="col-sm-8">
                {!! Form::hidden('modal-part-type-id', null, ['class' => 'form-control modal-part-type-id']) !!}
                {!! Form::text('modal-part-type', null, ['class' => 'form-control modal-part-type', 'readonly' => 'readonly']) !!}
              </div><!-- end col-md-8 -->
            </div><!-- end form-group -->

            <div class="form-group col-sm-6">
              {!! Form::label('modal-max-rf-hrs', 'Max RFHrs:', ['class' => 'col-sm-4 control-label']) !!}

              <div class="col-sm-8">
                {!! Form::text('modal-max-rf-hrs', null, ['class' => 'form-control modal-max-rf-hrs', 'readonly' => 'readonly']) !!}
              </div><!-- end col-md-8 -->
            </div><!-- end form-group -->

            <div class="form-group col-sm-6">
              {!! Form::label('modal-max-rf-print', 'Max RF To Print:', ['class' => 'col-sm-4 control-label']) !!}

              <div class="col-sm-8">
                {!! Form::text('modal-max-rf-print', null, ['class' => 'form-control modal-max-rf-print', 'readonly' => 'readonly']) !!}
              </div><!-- end col-md-8 -->
            </div><!-- end form-group -->

            <div class="form-group col-sm-6">
              {!! Form::label('modal-part-sequence', 'Part Sequence:', ['class' => 'col-sm-4 control-label']) !!}

              <div class="col-sm-8">
                {!! Form::text('modal-part-sequence', null, ['class' => 'form-control modal-part-sequence', 'readonly' => 'readonly']) !!}
              </div><!-- end col-md-8 -->
            </div><!-- end form-group -->

            <div class="form-group col-sm-6">
              {!! Form::label('modal-kit-identifier', 'Kit Identifier:', ['class' => 'col-sm-4 control-label']) !!}

              <div class="col-sm-8">
                {!! Form::checkbox('kit_identifier', 1, false, ['class' => 'modal-kit-identifier', 'disabled' => 'disabled']) !!}
              </div><!-- end col-md-8 -->
            </div><!-- end form-group -->

            <div class="form-group col-sm-6">
              {!! Form::label('modal-grade-required', 'Grade Required:', ['class' => 'col-sm-4 control-label']) !!}

              <div class="col-sm-8">
                {!! Form::checkbox('grade_required', 1, false, ['class' => 'modal-grade-required', 'disabled' => 'disabled']) !!}
              </div><!-- end col-md-8 -->
            </div><!-- end form-group -->

            <div class="clearfix"></div><!-- end clearfix -->

            {!! Form::open(['url' => '#', 'id' => 'part-parameter-form']) !!}

            <table class="table table-bordered table-responsive table-striped" id="part-parameter-table">
              <thead>
                <tr>
                  <th width="2%">#</th>
                  <th width="20%">Parameter</th>
                  <th width="10%">Point Of Msrs</th>
                  <th width="10%">Min Limit</th>
                  <th width="10%">Max Limit</th>
                  <th>Specs</th>
                  <th width="8%">Exclude of Post Cleaning</th>
                  <th width="7%">Print POM Value</th>
                  <th width="7%">Print Average Value</th>
                </tr>
              </thead>

              <tbody>
              </tbody>
            </table>

            {!! Form::close() !!}

          </div><!-- end modal-body -->

          <div class="form-group col-md-12" style="margin-top: 15px;">
            <button type="button" name="button" class="btn btn-default add-part-parameter">
              <i class="fas fa-plus-circle"></i> &nbsp; Add
            </button>
          </div><!-- end col-md-12 -->

          <div class="modal-footer">
            <button type="button" class="btn btn-default modal-btn-close" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary modal-btn-save">Save</button>
          </div><!-- end modal-footer -->
        </div><!-- end modal-content -->
      </div><!-- end modal-dialog -->
    </div><!-- end modal -->

    <table id="hidden-modal-part-parameter-table" style="display: none">
      <tr id="modal-append-row">
        <td class="valign-middle"><i data-id="" class="fas fa-minus-circle remove-row"></i></td>
        <td>
          {!! Form::select('parameter_id[]', $param_masters, null, ['class' => 'form-control parameter-id', 'id' => 'parameter-id']) !!}
        </td>
        <td>
          {!! Form::number('point_msr[]', 0, ['class' => 'form-control point-msr', 'id' => 'point-msr', 'min' => 0]) !!}
        </td>
        <td>
          {!! Form::number('min_limit[]', '', ['class' => 'form-control min-limit', 'id' => 'min-limit', 'min' => 0]) !!}
        </td>
        <td>
          {!! Form::number('max_limit[]', '', ['class' => 'form-control max-limit', 'id' => 'max-limit', 'min' => 0]) !!}
        </td>
        <td>
          {!! Form::text('specs[]', null, ['class' => 'form-control specs', 'id' => 'specs']) !!}
        </td>
        <td class="valign-middle halign-center">
          {!! Form::checkbox('exclude_post_cleaning', 1, false, ['class' => 'exclude-post-cleaning']) !!}
        </td>
        <td class="valign-middle halign-center">
          {!! Form::checkbox('print_pom_value', 1, false, ['class' => 'point-pom-value']) !!}
        </td>
        <td class="valign-middle halign-center">
          {!! Form::checkbox('print_average_value', 1, false, ['class' => 'print-average-value']) !!}
        </td>
      </tr>
    </table>

  </div><!-- end content -->

@endsection

@section('extra-js')

  <script src="{{ asset('/js/jquery.autocomplete.js') }}"></script>
  <script src="{{ asset('/js/custom/kit-master.js') }}"></script>

@endsection

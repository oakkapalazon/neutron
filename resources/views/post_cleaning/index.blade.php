@extends('layouts.app')

@section('content')

  <section class="content-header">
    <div class="col-md-12">
      <h1>Post Cleaning Lists</h1>
    </div><!-- end col-md-12 -->
  </section>

  <div class="content">

    <div class="box box-primary">
      <div class="box-body">

        <!-- {!! Form::open(['method' => 'GET', 'class'=>'form-horizontal']) !!}

        <div class="form-group">
          <label class="col-md-2 control-label" for="search_field">Search Keyword: </label>

          <div class="col-md-3">
            {!! Form::text('q', null,['class'=>'form-control', 'id'=>'search_field']) !!}
          </div>

          <label class="col-md-1 control-label" for="search_field">Date: </label>

          <div class="col-md-3">
            {!! Form::text('search_date', null,['class'=>'form-control', 'id'=>'search_date']) !!}
          </div>

          <div class="col-md-2">
            {!!Form::submit('Search',['class'=>'btn btn-default btn-cs search_btn'])!!}
            <a href="{!! route('post_cleaning.index') !!}" class="btn btn-default btn-cs">Clear</a>
          </div>

        </div>

        {!!Form::close()!!} -->

        <div class="row">

          <div class="col-md-12">
            <table class="table table-bordered table-responsive table-striped" id="post-cleaning-table">
              <thead>
                <tr>
                  <th>GRN No</th>
                  <th>Customer Name</th>
                  <th>Job ID</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>

              @php $count = 1; @endphp

              <tbody>
                @if(count($good_receipts) > 0)

                  @foreach($good_receipts as $row)

                  <tr>
                    <td>{{ $row->grn_no }}</td>
                    <td>{{ $row->customer_name }}</td>
                    <td>{{ $row->job_id }}</td>
                    <td>{{ $row->job_status }}</td>
                    <td>
                      {!! Form::open(['route' => ['post_cleaning.destroy', $row->id], 'method' => 'delete']) !!}
                      <a href="/process/post_cleaning/create?grn_id={{ $row->id }}" class='btn btn-default btn-xs' title='Edit'><i class="glyphicon glyphicon-edit"></i></a>

                      <!-- @if($row->job_status == 'closed')
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'title' => 'Delete', 'class' => 'btn btn-danger btn-xs', 'disabled' => true]) !!}
                      @else
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'title' => 'Delete', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                      @endif -->
                      {!! Form::close() !!}
                    </td>
                  </tr>
                  
                  @endforeach

                @else

                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>

                @endif
              </tbody>
              <tfoot>
                <tr>
                  <th>GRN No</th>
                  <th>Customer Name</th>
                  <th>Job ID</th>
                  <th>Status</th>
                </tr>
              </tfoot>
            </table>

          </div><!-- end col-md-12 -->

        </div><!-- end row -->
      </div><!-- end box-body -->
    </div><!-- end box -->

  </div><!-- end content -->

@endsection


@section('extra-js')

  <script type="text/javascript">

    $(function() {

      $('#search_date').datepicker({
        format: 'dd/mm/yyyy',
      });

      $('#post-cleaning-table').DataTable({
        lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
        order: [],
        initComplete: function () {

        var r = $('#post-cleaning-table tfoot tr');
        $('#post-cleaning-table thead').append(r);

        this.api().columns().every(function () {
          var column = this;
          var input = document.createElement("input");
          input.className = "search";
          $(input).appendTo($(column.footer()).empty())
          .on('change', function () {
            column.search($(this).val(), false, false, true).draw();
          });
        });
      }
      });

    });

  </script>

@endsection

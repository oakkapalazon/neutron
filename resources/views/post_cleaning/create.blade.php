@extends('layouts.app')

@section('content')

  <section class="content-header">
    <h1>Post Cleaning</h1>
  </section>

  <div class="content">

    @include('adminlte-templates::common.errors')
    {{--@include('adminlte-templates::common.success')--}}

    <div class="box box-primary">
      <div class="box-body">

        <div class="row">

          <div class="col-md-12">
            <div class="alert alert-danger check-grn-fields" role="alert" style="display: none">
            </div><!-- end alert alert-danger -->
          </div><!-- end col-md-12 -->

          <div class="col-md-4">

            @if(Request::get('grn_id'))

            <!-- @php
              $count = 0;
              $kit_master_id = $grn_detail[0]->kit_master_id;
            @endphp -->

            <ul id="tree1" class="list-group">
              <!-- <li><a href="#">{{ $grn_detail[0]->grn_no }}</a> -->
                <ul>
                  @foreach($grn_detail_header as $row)

                  <li>{{ $row->kit_type }} ({{ $row->set_id }})
                    <ul>

                      @php
                        $good_part_detail = \App\Models\GoodPartDetail::join('part_type_master', 'part_type_master.id', '=', 'good_part_detail.part_type_id' )
                                            ->join( 'good_kit_detail', 'good_kit_detail.id', '=', 'good_part_detail.good_kit_detail_id' )
                                            ->join( 'good_receipt_header', 'good_receipt_header.id', '=', 'good_kit_detail.good_receipt_header_id' )
                                            ->join( 'kit_master', 'kit_master.id', '=', 'good_kit_detail.kit_master_id' )
                                            ->join( 'good_param_datapoint_detail', 'good_param_datapoint_detail.good_part_detail_id', '=', 'good_part_detail.id' )
                                            ->where( 'good_receipt_header.id', $row->id )
                                            ->where( 'good_kit_detail_id', $row->good_kit_detail_id )
                                            ->where( 'good_part_detail.actions', '<>', 2 )
                                            ->GroupBy( 'good_param_datapoint_detail.good_part_detail_id' )
                                            ->orderBy( 'part_sequence', 'asc' )
                                            ->select( 'part_type_master.part_type', 'good_part_detail.*', 'param_datapoint_value' )
                                            ->get();
                      @endphp
                      @foreach($good_part_detail as $row => $gpd)

                        <li>
                          <!-- for not empty data point value  -->
                          @if($gpd->param_datapoint_value != null)

                            <i class="glyphicon glyphicon-ok"></i>
                            <a href="#" class="part-type" data-id="{{ $gpd->part_type_id }}" data-good-kit-detail-id="{{ $gpd->good_kit_detail_id }}" data-good-part-detail-id="{{ $gpd->id }}">{{ $gpd->part_type }} ({{ $gpd->serial_no }})</a>

                          @else

                            <!-- for empty data point value  -->
                            <i></i>
                            <a href="#" class="part-type" data-id="{{ $gpd->part_type_id }}" data-good-kit-detail-id="{{ $gpd->good_kit_detail_id }}" data-good-part-detail-id="{{ $gpd->id }}">{{ $gpd->part_type }} ({{ $gpd->serial_no }})</a>

                          @endif

                        </li>

                      @endforeach

                    </ul>
                  </li>

                  @endforeach


                  <!-- @foreach($grn_detail as $row)

                    @if($count == 0)

                    <li>{{ $row->kit_type }} ({{ $row->set_id }})

                      <ul>
                        <li>
                        @if($row->param_datapoint_value != null)
                          <i class="glyphicon glyphicon-ok"></i>
                          <a href="#" class="part-type" style="padding-left: 17px;" data-id="{{ $row->part_type_id }}" data-good-kit-detail-id="{{ $row->good_kit_detail_id }}" data-good-part-detail-id="{{ $row->good_part_detail_id }}">{{ $row->part_type }} ({{ $row->serial_no }})</a>
                        @else
                          <i></i>
                          <a href="#" class="part-type" style="padding-left: 17px;" data-id="{{ $row->part_type_id }}" data-good-kit-detail-id="{{ $row->good_kit_detail_id }}" data-good-part-detail-id="{{ $row->good_part_detail_id }}">{{ $row->part_type }} ({{ $row->serial_no }})</a>
                        @endif
                        </li>
                    @elseif($kit_master_id == $row->kit_master_id && $count != 0)
                      glyphicon glyphicon-ok
                                              <li>
@if($row->param_datapoint_value != "")
                          <i class="glyphicon glyphicon-ok"></i>
                          <a href="#" class="part-type" style="padding-left: 17px;" data-id="{{ $row->part_type_id }}" data-good-kit-detail-id="{{ $row->good_kit_detail_id }}" data-good-part-detail-id="{{ $row->good_part_detail_id }}">{{ $row->part_type }} ({{ $row->serial_no }})</a>
                          @else
                          <i></i>
                          <a href="#" class="part-type" style="padding-left: 17px;" data-id="{{ $row->part_type_id }}" data-good-kit-detail-id="{{ $row->good_kit_detail_id }}" data-good-part-detail-id="{{ $row->good_part_detail_id }}">{{ $row->part_type }} ({{ $row->serial_no }})</a>
                          @endif
                        </li>

                    @elseif($kit_master_id != $row->kit_master_id && $count != 0)

                      </ul>

                    </li>

                    <li>{{ $row->kit_type }} ({{ $row->serial_no }})

                      <ul>
                        <li>
                          @if($row->param_datapoint_value != "")
                          <i class="glyphicon glyphicon-ok"></i>
                          <a href="#" class="part-type" style="padding-left: 17px;" data-id="{{ $row->part_type_id }}" data-good-kit-detail-id="{{ $row->good_kit_detail_id }}" data-good-part-detail-id="{{ $row->good_part_detail_id }}">{{ $row->part_type }} ({{ $row->serial_no }})</a>
                          @else
                          <i></i>
                          <a href="#" class="part-type" style="padding-left: 17px;" data-id="{{ $row->part_type_id }}" data-good-kit-detail-id="{{ $row->good_kit_detail_id }}" data-good-part-detail-id="{{ $row->good_part_detail_id }}">{{ $row->part_type }} ({{ $row->serial_no }})</a>
                          @endif
                        </li>

                    @else

                        <li>
                          @if($row->param_datapoint_value != "")
                          <i class="glyphicon glyphicon-ok"></i>
                          <a href="#" class="part-type" style="padding-left: 17px;" data-id="{{ $row->part_type_id }}" data-good-kit-detail-id="{{ $row->good_kit_detail_id }}" data-good-part-detail-id="{{ $row->good_part_detail_id }}">{{ $row->part_type }} ({{ $row->serial_no }})</a>
                          @else
                          <i></i>
                          <a href="#" class="part-type" style="padding-left: 17px;" data-id="{{ $row->part_type_id }}" data-good-kit-detail-id="{{ $row->good_kit_detail_id }}" data-good-part-detail-id="{{ $row->good_part_detail_id }}">{{ $row->part_type }} ({{ $row->serial_no }})</a>
                          @endif
                        </li>
                      </ul>

                    </li>

                    @endif

                    @php
                      $count++;
                      $kit_master_id = $row->kit_master_id;
                    @endphp

                  @endforeach -->
                </ul>
              <!-- </li> -->
            </ul>

            @endif

          </div><!-- end col-md-4 -->

          <div class="col-md-8">
            <div class="col-md-12">
              <h4>Transaction Header</h4>
            </div><!-- end col-md-12 -->

            @if(Request::get('grn_id'))

            <div class="form-group col-md-7">
              {!! Form::label('customer_name', 'Customer:', ['class' => 'col-sm-3 control-label']) !!}

              <div class="col-sm-9">
                {!! Form::text('customer_name', $customer->customer_name, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
              </div><!-- end col-md-8 -->
            </div><!-- end form-group -->

            <div class="form-group col-md-5">
              {!! Form::label('job_id', 'Job ID:', ['class' => 'col-sm-4 control-label']) !!}

              <div class="col-sm-8">
                {!! Form::text('job_id', $grn_row->job_id, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
              </div><!-- end col-md-8 -->
            </div><!-- end form-group -->

            <div class="form-group col-md-7">
              {!! Form::label('grn_no', 'GRN No:', ['class' => 'col-sm-3 control-label']) !!}

              <div class="col-sm-9">
                {!! Form::text('grn_no', $grn_row->grn_no, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
              </div><!-- end col-md-8 -->
            </div><!-- end form-group -->

            <!-- <div class="form-group col-md-6">
              {!! Form::label('customer_name', 'Customer Code:', ['class' => 'col-sm-5 control-label']) !!}

              <div class="col-sm-7">
                {!! Form::text('customer_name', $customer->customer_code, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
              </div>
            </div> -->

            <!-- <div class="form-group col-md-12">
              {!! Form::label('address', 'Address:', ['class' => 'col-sm-2 control-label', 'readonly' => 'readonly', 'style'=>'width:13.2%!important']) !!}

              <div class="col-md-10" style="width:86.8%!important">
                {!! Form::textarea('address', $customer->customer_address, ['class' => 'form-control', 'id' => 'address', 'rows' => 3, 'cols' => 20, 'style' => 'resize:none', 'readonly' => 'readonly']) !!}
              </div>
            </div> -->

            @else

            <div class="form-group col-md-6">
              {!! Form::label('grn_no', 'GRN No *:', ['class' => 'col-sm-5 control-label']) !!}

              <div class="col-sm-7">
                {!! Form::text('grn_no', null, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
              </div><!-- end col-md-8 -->
            </div><!-- end form-group -->

            <div class="form-group col-md-6">
              {!! Form::label('job_id', 'Job ID *:', ['class' => 'col-sm-5 control-label']) !!}

              <div class="col-sm-7">
                {!! Form::text('job_id', null, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
              </div><!-- end col-md-8 -->
            </div><!-- end form-group -->

            <div class="form-group col-md-6">
              {!! Form::label('customer_id', 'Customer Code *:', ['class' => 'col-sm-5 control-label']) !!}

              <div class="col-sm-7">
                {!! Form::text('customer_name', null, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
              </div><!-- end col-md-8 -->
            </div><!-- end form-group -->

            <div class="form-group col-md-6">
              {!! Form::label('customer_name', 'Customer Name:', ['class' => 'col-sm-5 control-label']) !!}

              <div class="col-sm-7">
                {!! Form::text('customer_name', null, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
              </div><!-- end col-md-8 -->
            </div><!-- end form-group -->

            <div class="form-group col-md-12">
              {!! Form::label('address', 'Address *:', ['class' => 'col-sm-5 control-label', 'readonly' => 'readonly']) !!}
            </div><!-- end form-group -->

            @endif

            <div class="clearfix"></div><!-- end clearfix -->

            <hr>

            <div>
              <input type="hidden" name="grn_id" value="{{ $grn_row->id }}" class="grn-id" id="grn-id">
            </div>

            <div class="col-md-12">

              <table class="table table-bordered table-responsive table-striped" id="grn-table">
                <thead>
                  <tr>
                    <!-- <th width="15%">GRN No</th> -->
                    <th width="10%">Set ID</th>
                    <th>Part S/N</th>
                    <th>POM Value</th>
                    <!-- <th width="15%">Parameter</th> -->
                    <th>ARF</th>
                    <th width="14%">Value</th>
                  </tr>
                </thead>

                <tbody>
                  <tr>
                    <td colspan="6">No Result</td>
                  </tr>
                </tbody>
              </table>
            </div><!-- end col-md-12 -->

            <div class="form-group col-md-12">
              @if($grn_row->job_status != 'closed')
                <button type="submit" class="btn btn-primary pull-right btn-save" style="margin-left: 25px;">
                  Save
                </button>
              @endif

              <a href="{!! route('post_cleaning.index') !!}" class="btn btn-default pull-right">
                Back
              </a>
            </div><!-- end col-md-12 -->
          </div><!-- end col-md-8 -->

        </div><!-- end row -->
      </div><!-- end box-body -->
    </div><!-- end box -->

  </div><!-- end content -->

@endsection

@section('extra-js')

  <script src="{{ asset('/js/custom/post-cleaning.js') }}"></script>
  <script src="{{ asset('/js/treeview.js') }}"></script>

@endsection
